$(document).ready(function() {
	$('#dialog-confirm').hide();

    $("#yesApproveId").click(function(){
        $("#approveOrRejectFlag").val(1);
        rejectGroup($('#approvedGroupId').val());
    });

    $("#yesRejectId").click(function(){
        $.mobile.showPageLoadingMsg();
        document.getElementById("BMFormId").action = localStorage.contextPath+"/client/ci/groups/cca1/rejectClients";
        document.getElementById("BMFormId").method = "POST";
        document.getElementById("BMFormId").submit().refresh();
    });
    getClientAddress();

    if($("#isLoanAmountUpdationApplicableInAuthorization").val() == "true") {
        $("#sameForAll").click(function () {
            if ($("#sameForAll").is(':checked')) {

                var loanAmount = 0;
                for (var count = 0; count < $('#clientIdLength').val(); count++) {
                    var loanAmount = $('#loanSanctioned_' + count).val();
                    if (loanAmount != 0) {
                        break;
                    }
                }
                if (loanAmount < 1000) {
                    $(window).scrollTop(0);
                    $("#errormessage").text("Please enter the amount above minimum allowed range for atleast one member.");
                    $("#sameForAll").prop('checked', false).checkboxradio("refresh");
                }
                else {
                    $("#errormessage").text("");
                    var groupLoanAmount = 0;
                    for (var count = 0; count < $('#clientIdLength').val(); count++) {
                        var isEnabled = $('#loanSanctioned_' + count).is(':enabled');
                        if (isEnabled) {
                            $('#loanSanctioned_' + count).val(loanAmount);
                            groupLoanAmount += parseInt(loanAmount);
                        }
                    }
                }
            } else {
                for (var count = 0; count < $('#clientIdLength').val(); count++) {
                    $('#loanSanctioned_' + count).val(0);
                }
            }
        });

        var groupLoanAmount = 0;
        for (var count = 0; count < $('#clientIdLength').val(); count++) {
            var loanAmount = $('#loanSanctioned_' + count).val();
            groupLoanAmount += parseInt(loanAmount);
        }
        $("#amount").val(groupLoanAmount);
    }
});

function totalGroupAmount(changedValue){
    var amount = changedValue.value;
    var length = $("#clientIdLength").val();
    var total = 0;
    if(isNaN(amount) == false){
        for(var i=0;i<length;i++){
            total = parseInt(total) + parseInt($("#loanSanctioned_"+i).val() || 0);
        }
        $("#amount").val(total);
    }
}

var hiddenclientid = new Array();
var hiddengroupid;
var rejectedClients = new Array();
function branchGroupsDisplay(selectThis) {
	var officeId = selectThis.value;
	if(officeId!=0) {
    $.mobile.showPageLoadingMsg();
	document.getElementById("BMFormId").method='POST';
	document.getElementById("BMFormId").action=localStorage.contextPath+"/client/ci/groups/authorization/"+authorizeGroupOperationId+"/"+officeId;
	document.getElementById("BMFormId").submit().refresh();
	}
}

function getGroupDetailsForAuthorization(groupId,branchIdAuthorization) {
    $.mobile.showPageLoadingMsg();
	document.getElementById("BMFormId").method='POST';
	document.getElementById("BMFormId").action=localStorage.contextPath+"/client/ci/groups/authorization/group/"+groupId+"/"+branchIdAuthorization;
	document.getElementById("BMFormId").submit().refresh();
}

function rejectClient(clientsId,rejectClientId,clientName) {
	hiddenclientid.push(clientsId);
    document.getElementById("rejectedClientID").value = hiddenclientid;
    $("#rejectedClient").val(clientName);
    if(hiddenclientid.length>0){
        if($('#membersCount').val() > minimumClients) {
            $.mobile.showPageLoadingMsg();
            document.getElementById("BMFormId").action = localStorage.contextPath+"/client/ci/groups/cca1/rejectClients";
            document.getElementById("BMFormId").method = "POST";
            document.getElementById("BMFormId").submit().refresh();
        }
        else{
            $(window).scrollTop(0);
            document.getElementById(rejectClientId).href= "#rejectConfirmationId";
            $("#"+rejectClientId).trigger('click');
        }
	}else{
		alert("No rejected Clients");
	}
}
var loanAmount = new Array();
function approveGroup(groupId) {
   var groupId= $('#currentgroupid').val();
    var clientcount;
    $.post(URIPrefix+ajaxcallip+localStorage.contextPath+"/client/ci/groups/uploadnocClients/"+authorizeGroupOperationId,
        {groupId:groupId},
        function(data){
            clientcount =data.clientCount;
            console.log(clientcount);
            if(clientcount >0){
                $('#errormessage').text("Group Cannot be Approved Since Clients are waiting for BM to Upload NOC");
                $(window).scrollTop(0);
                $('#successField').hide();
            }
            else{
                var flag = true;
                var clients = $('#reinitiatedClients').val().split(",");
                var reinitiatedClientsFlag = $('#reinitiatedClientsFlag').val().split(",");
                for(var i=0;i<clients.length;i++){
                    if(reinitiatedClientsFlag[i] == 'false') {
                        rejectedClients.push(clients[i]);
                        flag = false;
                    }
                }
                if(flag == true) {
                    hiddengroupid = groupId;
                    if(hiddengroupid>0){
                        if($('#membersCount').val() >= minimumClients){
                            if(($("#isLoanAmountUpdationApplicableInAuthorization").val() == "true" && validateLoanAmount() == true) || $("#isLoanAmountUpdationApplicableInAuthorization").val() == "false"){
                                for (var count = 0; count < $('#clientIdLength').val(); count++) {
                                    loanAmount.push($('#loanSanctioned_' + count).val());
                                }
                                $("#loanAmt").val(loanAmount);
                                $("#submitDivId").hide();
                                $.mobile.showPageLoadingMsg();
                                document.getElementById("approvedGroupId").value = $('#groupidfordownload').val();
                                document.getElementById("BMFormId").action = localStorage.contextPath + "/client/ci/groups/cca1/approvedGroup";
                                document.getElementById("BMFormId").method = "POST";
                                document.getElementById("BMFormId").submit().refresh();
                            } else {
                                $("#errormessage").text("Please enter minimum amount for all clients");
                            }
                        } else{
                            $(window).scrollTop(0);
                            $('#approvedGroupId').val(hiddengroupid);
                            document.getElementById('approve').href= "#approveConfirmationId";
                            $("#approve").trigger('click');
                        }
                    }else{
                        alert("No groups to Approve");
                    }
                }
                else{
                    if(rejectedClients.length>1) {
                        $('#errorLabelId').text(rejectedClients + " are re-initiated & waiting for approval");
                    }else {
                        $('#errorLabelId').text(rejectedClients + " is re-initiated & waiting for approval")
                    }
                    $('#errorLabelIdSecondRow').text("Please approve to authorize this group");
                }
            }
        });
    }
function rejectGroup(groupId){
	hiddengroupid = groupId;
		if(hiddengroupid>0){
            $.mobile.showPageLoadingMsg();
			document.getElementById("approvedGroupId").value=hiddengroupid;
			document.getElementById("BMFormId").action=localStorage.contextPath+"/client/ci/groups/cca1/rejectedGroup";
			document.getElementById("BMFormId").method="POST";
			document.getElementById("BMFormId").submit().refresh();
		}else{
			alert("No groups to Reject");
		}
}
function downloadClientDocumentsAuth(docId,clientId){
	//docId = $(docId).val();
	if(docId !=0){
        $.mobile.showPageLoadingMsg();
		document.getElementById("selectedDocTypeId").value = docId;
		document.getElementById("BMFormId").method='POST';
		document.getElementById("BMFormId").action=localStorage.contextPath+'/client/ci/generateClientDocuments/'+clientId+'/'+docId+'/downloadUploadedImages';
		document.getElementById("BMFormId").submit();
    }
}
function downloadDocs(index) {
    var storageLocationIndexArray = $('#storageIdentifier').val().split(',');
    var fileLocation = $('#fileLocation').val().split(',');
    document.getElementById("selectedDocId").value = fileLocation[index-1];
    if (storageLocationIndexArray[index-1] == 's3') {
        document.getElementById("s3Flag").value = true;
        document.getElementById("s3_Key").value = fileLocation[index-1];
        document.getElementById("BMFormId").method = 'POST';
        document.getElementById("BMFormId").action = URIPrefix + ajaxcallip + localStorage.contextPath + "/client/ci/kycdownload/s3Images";
        document.getElementById("BMFormId").submit();

    } else {
        document.getElementById("BMFormId").method = 'POST';
        document.getElementById("BMFormId").action = localStorage.contextPath + '/client/ci/downloadDocs';
        document.getElementById("BMFormId").submit();
    }
}

//Adarsh-Modified
//client Appraisal Answers
function clientAppraisalAnswersSmh(groupId,clientId,loanCount,clientRatingPerc,clientTotalWeightage,clientTotalWeightageRequired) {
	var redirectValue = 1;
    $.mobile.showPageLoadingMsg();
	document.getElementById("clientRatingPercHiddenId").value=clientRatingPerc;
	document.getElementById("clientTotalWeightageHiddenId").value=clientTotalWeightage;
	document.getElementById("clientTotalWeightageRequiredHiddenCCA1Id").value=clientTotalWeightageRequired;

	document.getElementById("BMFormId").method='POST';
	document.getElementById("BMFormId").action=localStorage.contextPath+"/client/ci/groups/appraisal/"+clientId+"/"+groupId+"/"+loanCount+"/"+redirectValue;
	document.getElementById("BMFormId").submit().refresh();
}
function cancelGroupAuthorization(branchId){
    document.getElementById("BMFormId").method='POST';
    document.getElementById("BMFormId").action=localStorage.contextPath+"/client/ci/groups/authorization/"+authorizeGroupOperationId+"/"+branchId;
    document.getElementById("BMFormId").submit();
}

function getHighMarkResultForGroup(groupId){
    $("#groupId").val(groupId);
    $.mobile.showPageLoadingMsg();
    document.getElementById("BMFormId").method='POST';
    document.getElementById("BMFormId").action=localStorage.contextPath+'/client/ci/highmark/getHighmarkDetailResponseView';
    document.getElementById("BMFormId").submit();
}
var _directionsRenderer = '';
function getClientAddress(){
    var groupId = $("#groupidfordownload").val();
    if(groupId) {
        $.post(URIPrefix + ajaxcallip + localStorage.contextPath + '/client/ci/getClientAddress',
        {groupId : groupId, flag : 'authorization'},
            function (data) {
                if (data.status == 'success') {
                    console.log(data);
                    $("#addressDetails").val(data.clientId);
                    for (var i = 0; i < data.address.length; i++) {
                        $("#" + data.clientId[i] + "location").html(data.address[i]);
                    }
                    getDestinationAddress(data.address);
                } else {
                    $('#errorLabelId').text("Error in retrieving Location");
                }
            }
        );
    }
}
var collectionInfoObject = new Array();
function getDestinationAddress(addressDetails){
    var latitudeArray = new Array();
    for(var i=0;i<addressDetails.length;i++){
        var geocoder = new google.maps.Geocoder();
        var addressString = addressDetails[i].replace(/^[,\s]+|[,\s]+$/g, '').replace(/,[,\s]*,/g, ',');
        console.log(addressString);
        addressDetails[i] = addressString;
        geocoder.geocode({ 'address': addressDetails[i]}, function(results, status){
            if(status == google.maps.GeocoderStatus.OK){
                latitudeArray.push(results[0].geometry.location);
                if(latitudeArray.length == addressDetails.length){
                    collectionInfoObject = new Array();
                    for(var j =0; j<latitudeArray.length;j++){
                        var collectionInfoObjectTemp = new Object();
                        collectionInfoObjectTemp.position =  latitudeArray[j];
                        collectionInfoObjectTemp.addressDetails =  addressDetails[j] ;
                        collectionInfoObject.push(collectionInfoObjectTemp);
                    }
                    storeLatitudeArray(collectionInfoObject,latitudeArray);
                }
            }
        });
    }
}
var _mapPoints = [];
function storeLatitudeArray(addressDetails,latitudeArray){
    _mapPoints = [];
    var startPositionLatLng = new google.maps.LatLng(addressDetails[0].position.lat(),addressDetails[0].position.lng()) ;
    _mapPoints[0] = latitudeArray[0];
    var distanceArray = new Array();
    distanceArray[0] = 0;
    var totalLength = latitudeArray.length;
    var i = 0;
    var tempDistance = 0;
    for(var j=1;j<latitudeArray.length;j++)
    {
        var distance =   findDistance(_mapPoints[i].lat(), _mapPoints[i].lng(),latitudeArray[j].lat(),latitudeArray[j].lng(),'M') ;
        distanceArray.push(distance);
        console.log(distanceArray);
    }
    if(distanceArray.length == latitudeArray.length){
        var data = $("#addressDetails").val().split(',');
        for(var i =0; i<distanceArray.length;i++){
            $("#"+data[i]+"distance").html(distanceArray[i].toFixed(2));
        }
    }
}
//To calculate the distance between the address using their latitude and longitude
function findDistance(lat1, lon1, lat2, lon2, unit) {
    var radlat1 = Math.PI * lat1/180;
    var radlat2 = Math.PI * lat2/180;
    var theta = lon1-lon2;
    var radtheta = Math.PI * theta/180;
    var dist = Math.sin(radlat1) * Math.sin(radlat2) + Math.cos(radlat1) * Math.cos(radlat2) * Math.cos(radtheta);
    dist = Math.acos(dist);
    dist = dist * 180/Math.PI;
    dist = dist * 60 * 1.1515;
    // Unit to be converted
    if (unit=="K") { dist = dist * 1.609344 } // Into Km
    if (unit=="N") { dist = dist * 0.8684 } // Into nautical miles
    if (unit=='M') { dist = dist * 1.609344 * 1000} // Into meter
    console.log(dist);
    return dist;
}
function sendBack(clientId,clientName){
    document.getElementById("rejectedClientID").value = clientId;
    $("#rejectedClient").val(clientName);
    $.mobile.showPageLoadingMsg();
    document.getElementById("BMFormId").action = localStorage.contextPath + "/client/ci/groups/sendBack/" + authorizeGroupOperationId;
    document.getElementById("BMFormId").method = "POST";
    document.getElementById("BMFormId").submit().refresh();
}

function validateLoanAmount(){
    for (var count = 0; count < $('#clientIdLength').val(); count++) {
        if($('#loanSanctioned_' + count).val().trim() == "" || $('#loanSanctioned_' + count).val().trim() == 0 || $('#loanSanctioned_' + count).val().trim() < 1000){
            return false;
        }
    }
    return true;
}
