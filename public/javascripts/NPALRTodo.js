$(document).ready(function () {
    var date = $('#date').val();
    if (date == 'current') {
        $('#currentTaskNavBarId').addClass("ui-btn-active");
    } else if (date == 'future') {
        $('#futureTaskNavBarId').addClass("ui-btn-active");
    } else if (date == 'overdue') {
        $('#overdueTaskNavBarId').addClass("ui-btn-active");
    }
    if ($('#taskLength').val() == 0) {
        $("#errorField").text("No Task Found");
    } else {
        $("#errorField").hide();
    }
});

function submitTask(i) {
    $("#successMessage").hide();
    if ($('#todoRemarksId' + i).val() != '') {
        $.mobile.showPageLoadingMsg();
        var data = {};
        data.remarks = $('#todoRemarksId' + i).val();
        data.taskId = $('#taskId' + i).val();
        ajaxVariable = $.ajax({
            type: 'POST',
            data: JSON.stringify(data),
            contentType: 'application/json',
            url: URIPrefix + ajaxcallip + localStorage.contextPath + '/NPALRGroups/submittask',
            success: function (data) {
                $("#errorField").hide();
                if (data.result == 'success') {
                    $('#statusMsg').val("Task updated successfully");
                    $('#successMsg').text("Task updated successfully");
                    document.getElementById("todoFormId").method='POST';
                    document.getElementById("todoFormId").action=localStorage.contextPath+"/NPALRGroups/todo/"+$("#date").val();
                    document.getElementById("todoFormId").submit();
                }
            }, error: function (jqXHR, textStatus, error) {
                alert("textStatus" + textStatus);
                
            }
        });
    } else {
        $("#errorField").show();
        $("#errorField").text("Please provide remarks");
    }
}

function loadFO(){
    document.getElementById('loanOfficerId').options.length = 0;
    var dropDown = document.getElementById("loanOfficerId");
    var option = document.createElement("option");
    option.text = "Select";
    option.value ="-1";
    try {
        dropDown.add(option, null);
    }catch(error) {
        dropDown.add(option);
    }
    $("#loanOfficerId").val('-1').selectmenu("refresh");
    $.post(URIPrefix+ajaxcallip+localStorage.contextPath+"/loanOfficers",
        { listoffice: $("#listoffice").val()},
        function (dataValue) {
            var data = dataValue.personnelIdArray;
            var dataName = dataValue.personnelNameArray;
            for(var i=0;i<data.length;i++){
                var dropDown = document.getElementById("loanOfficerId");
                if(!dataName[i] == '' || !dataName[i] == null){
                    option = document.createElement("option");
                    option.text = dataName[i];
                    option.value =data[i];
                    try {
                        dropDown.add(option, null);
                    }catch(error) {
                        dropDown.add(option);
                    }
                }
            }
        }
    );
}

function retrieveTask(){
    if($('#loanOfficerId').val() != -1) {
        $.mobile.showPageLoadingMsg();
        document.getElementById("todoFormId").method='POST';
        document.getElementById("todoFormId").action=localStorage.contextPath+"/NPALRGroups/todo/"+$("#date").val();
        document.getElementById("todoFormId").submit();
    }   
}