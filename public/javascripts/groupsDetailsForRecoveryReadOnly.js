var clientListcurrentrow = 0;
var clientListmaxrows = 0;
var clientListPageNo = 1;
var noOfPlan =0;
var planActivityArray = new Array();
var planExpectedDueDateArray = new Array();
var planExpectedDueTimeArray = new Array();

var inc = 0;
var sno = 1;
var noOfClients = 0;

$(document).ready(function () {

    $("#selectDocDivId").hide();
    $("#otherReasonDivId").hide();

    $("#reason").multiselect({
        noneSelectedText: "Select Reasons",
        selectedText: "# of # reasons selected",
        close: function (event, ui) {
            // event handler here
            if ($("#reason").val() != null) {
                var selectedValues = $("#reason").val();
                for (var i = 0; i < selectedValues.length; i++) {
                    if (selectedValues[i] == 1) {
                        $("#otherReasonDivId").show();
                        break;
                    } else
                        $("#otherReasonDivId").hide();
                }
            } else {
                $("#otherReasonDivId").hide();
            }
        }
    });

    $("#clientListPrevId").click(function () {
        if (clientListcurrentrow == clientListmaxrows) {
            $("#clientListNextId").show();
        }

        var hidenextrow = clientListcurrentrow;
        if (clientListmaxrows == clientListcurrentrow) {
            var x = clientListmaxrows % 5;
            if (x > 0) {
                hidenextrow = hidenextrow + (5 - x);
                clientListcurrentrow = clientListcurrentrow + (5 - x);
            }
        }
        for (var i = 0; i < 5; i++) {
            hidenextrow--;
            $('#clientListTableId tr.showhide:eq(' + hidenextrow + ')').hide();
            if (clientListcurrentrow > 0) {
                clientListcurrentrow--;
                $('#clientListTableId tr.showhide:eq(' + (clientListcurrentrow - 5) + ')').show();
            }
        }
        if (clientListcurrentrow == 5) {
            $("#clientListPrevId").hide();
        }
        clientListPageNo = clientListPageNo - 5;
        var $btn_text = $('#clientListPageNoId').find('.ui-btn-text')
        $btn_text.text("Records " + clientListPageNo + " - " + clientListcurrentrow + " of " + clientListmaxrows);
    });

    //Group Installment Due pagination//
    $("#clientListNextId").click(function () {
        if (clientListcurrentrow == 5) {
            $("#clientListPrevId").show();
        }
        var hidepreviousrow = clientListcurrentrow;
        for (var i = 0; i < 5; i++) {
            hidepreviousrow--;
            $('#clientListTableId tr.showhide:eq(' + hidepreviousrow + ')').hide();
            if (clientListcurrentrow < clientListmaxrows) {
                $('#clientListTableId tr.showhide:eq(' + clientListcurrentrow + ')').show();
                clientListcurrentrow++;
            }
        }
        if (clientListcurrentrow == clientListmaxrows) {
            $("#clientListNextId").hide();
        }
        clientListPageNo = clientListPageNo + 5;
        var $btn_text = $('#clientListPageNoId').find('.ui-btn-text')
        $btn_text.text("Records " + clientListPageNo + " - " + clientListcurrentrow + " of " + clientListmaxrows);
    });

    if ($("#loanstatus").val() == 1) {
        if ($("#answerNO1Id").val() == 1) {
            $('input:radio[name=gltraceable]:nth(0)').attr('checked', true).checkboxradio("refresh");
        } else if ($("#answerNO1Id").val() == 0) {
            $('input:radio[name=gltraceable]:nth(1)').attr('checked', true).checkboxradio("refresh");
        }
        if ($("#answerNO2Id").val() == 1) {
            $('input:radio[name=sltraceable]:nth(0)').attr('checked', true).checkboxradio("refresh");
        } else if ($("#answerNO2Id").val() == 0) {
            $('input:radio[name=sltraceable]:nth(1)').attr('checked', true).checkboxradio("refresh");
        }
        if ($("#answerNO3Id").val() == 1) {
            $('input:radio[name=gmtraceable]:nth(0)').attr('checked', true).checkboxradio("refresh");
        } else if ($("#answerNO3Id").val() == 0) {
            $('input:radio[name=gmtraceable]:nth(1)').attr('checked', true).checkboxradio("refresh");
        }
        if ($("#answerNO4Id").val() == 1) {
            $('input:radio[name=amtnotupdated]:nth(0)').attr('checked', true).checkboxradio("refresh");
        } else if ($("#answerNO4Id").val() == 0) {
            $('input:radio[name=amtnotupdated]:nth(1)').attr('checked', true).checkboxradio("refresh");
        }
        if ($("#answerNO5Id").val() == 1) {
            $('input:radio[name=capable]:nth(0)').attr('checked', true).checkboxradio("refresh");
        } else if ($("#answerNO5Id").val() == 0) {
            $('input:radio[name=capable]:nth(1)').attr('checked', true).checkboxradio("refresh");
        }

        var dataarray = $('#reasonsMultiselect').val().split(",");
        $("#reason").val(dataarray).multiselect("refresh");

        $("#percentageId").val($("#capabilityPercentage").val()).selectmenu("refresh");
        $("#approvalDateId").val($("#expectedNextPayment").val());
        if ($("#generalRemarks").val() != 'null') {
            $("#remarksId").val($("#generalRemarks").val());
        } else {
            $("#remarksId").val('');
        }
        $("#otherReasonId").val($("#reasonForNotCapable").val());
        $("#otherReasonDivId").show();
    }

    //upload Documents for group/client
    $("#clientNameDivId").hide();
    $("#groupDocRadioId").click(function () {
        $("#clientNameDivId").hide();
        $("#selectDocDivId").hide();
    });

    $("#clientDocRadioId").click(function () {
        $("#clientNameDivId").show();
        $("#selectDocDivId").hide();

        //retrieveClientDetails
        var data = {};
        data.accountId = $("#accountIdID").val();
        ajaxVariable = $.ajax({
            beforeSend: function () {
                $.mobile.showPageLoadingMsg();
            },
            complete: function () {
                $.mobile.hidePageLoadingMsg()
            },
            type: 'POST',
            data: JSON.stringify(data),
            contentType: 'application/json',
            url: URIPrefix + ajaxcallip + localStorage.contextPath + '/client/ci/retrieveClientDetails',
            success: function (data) {
                $("#clientNamesId").val('0').selectmenu("refresh");
                document.getElementById('clientNamesId').options.length = 0;
                var combo1 = document.getElementById("clientNamesId");

                option = document.createElement("option");
                option.text = "Select";
                option.value = "0";
                try {
                    combo1.add(option, null); //Standard 
                } catch (error) {
                    combo1.add(option); // IE only
                }
                for (var i = 0; i < data.customerIdArray.length; i++) {
                    var combo = document.getElementById("clientNamesId");

                    option = document.createElement("option");
                    option.text = data.customerNameArray[i];
                    option.value = data.customerIdArray[i];
                    try {
                        combo.add(option, null); //Standard 
                    } catch (error) {
                        combo.add(option); // IE only
                    }
                }
            }, error: function (jqXHR, textStatus, error) {
                alert("textStatus" + textStatus);
            }
        });
    });
    
    //Added By vishesh
    
    //Action plans
        
	$("#addPlanId").click(function () {
            $("#errorLabel").text('');
            $("#activityId").val('');
            $("#expectedDateId").val('');
            $("#expectedTimeId").val('')
        });

        //Dynamic add Plan 	
        $("#addPlan").click(function () {
            if ($("#activityId").val() == '' || $("#expectedDateId").val() == '' || $("#expectedTimeId").val() == '') {
                $("#errorLabel").text("Please fill all the fields");
                $("a#addPlan").attr('href', '');
            } else if ($("#expectedTimeId").val() > 24) {
                $("#errorLabel").text("Time should be less than 24hrs");
                $("a#addPlan").attr('href', '');
            } else {
                var newContent = '<div data-role="content" data-theme="b" class="content-primary">';
                newContent += '<ul data-role="listview" data-split-theme="b" data-inset="true" id="ulId" style="height:25px">';
                newContent += '<li style="padding-left:10px !important;height: 50% !important;min-height: 0">';
                newContent += "<label for='activityName'" + inc + " id='activityID'" + inc + ">Activity : " + $("#activityId").val() + "   Date : " + $("#expectedDateId").val() + "</label>";
                newContent += '<img title="Remove action plan" src="/images/delete.png" onclick="removePlan(this,' + inc + ')" height="25" width="25" style="padding-left:94%;padding-top:1%;cursor: pointer">';
                newContent += '</li>';
                newContent += '</ul>';
                newContent += '</div><br/>';
                $("#addPlanDivId").append(newContent).trigger('create');
                inc++;
                noOfPlan++;
                planActivityArray.push($('#activityId').val());
                planExpectedDueDateArray.push($('#expectedDateId').val());
                planExpectedDueTimeArray.push($('#expectedTimeId').val());


                $("a#addPlan").attr('href', '#addPlanPopupId');
            }
        });
        
        //NUMBER VALIDATION
        $("#expectedTimeId").keydown(function (event) {
            // Allow: backspace, delete, tab, escape, and enter
            if (event.keyCode == 46 || event.keyCode == 8 || event.keyCode == 9 || event.keyCode == 27 || event.keyCode == 13 || event.keyCode == 37 || event.keyCode == 39 ||
                    // Allow: Ctrl+A
                            (event.keyCode == 65 && event.ctrlKey === true) ||
                            // Allow: home, end, left, right
                                    (event.keyCode >= 35 && event.keyCode <= 39)) {
                        // let it happen, don't do anything
                        return;
                    } else {
                        // Ensure that it is a number and stop the keypress
                        if (event.shiftKey || (event.keyCode < 48 || event.keyCode > 57) && (event.keyCode < 96 || event.keyCode > 105)) {
                            event.preventDefault();
                        }
                        //for expected date zero validation
                        if($.trim($(this).val()) =='')
                        {
                            if(event.keyCode == 48){
                            event.preventDefault();	
                            }
                        }
                    }
                });
        $(function () {
            $("#expectedDateId").datepicker({
                dateFormat: 'dd/mm/yy',
                yearRange: "-90:+0",
                changeMonth: true,
                changeYear: true,
                minDate: new Date
            });
        });
});

function removePlan(remove, i) {
    r = remove.parentNode.parentNode;
    r.parentNode.removeChild(r);
    planActivityArray.splice(i,1);
    planExpectedDueDateArray.splice(i,1);
    planExpectedDueTimeArray.splice(i,1);
    noOfPlan--;
}
        
function showClientList(accountId) {
    var data = {};
    data.accountId = accountId;
    ajaxVariable = $.ajax({
        beforeSend: function () {
            $.mobile.showPageLoadingMsg();
        },
        complete: function () {
            $.mobile.hidePageLoadingMsg()
        },
        type: 'POST',
        data: JSON.stringify(data),
        contentType: 'application/json',
        url: URIPrefix + ajaxcallip + localStorage.contextPath + '/client/ci/retrieveClientDetails',
        success: function (data) {
            $('table').remove('#clientListTableId');
            var newContent = '<table id="clientListTableId" class="custom-table">';
            $("#clientListDivId").append(newContent).trigger('create');
            var newContent = '<tr>';
            newContent += '<th width="10%">';
            newContent += "S.NO";
            newContent += '</th>';
            newContent += '<th  width="20%">';
            newContent += "Client Name";
            newContent += '</th>';
            newContent += '<th  width="30%">';
            newContent += "Address";
            newContent += '</th>';
            newContent += '<th  width="15%">';
            newContent += 'Overdue Amount';
            newContent += '</th>';
            newContent += '<th  width="15%">';
            newContent += 'Arrear Days';
            newContent += '</th>';
            newContent += '</tr>';
            $("#clientListTableId").append(newContent).trigger('create');
            for (var i = 0; i < data.customerNameArray.length; i++) {
                var newContent = '<tr class = "showhide">';
                newContent += '<td align="right">';
                newContent += i + 1;
                newContent += '</td>';
                newContent += '<td>';
                newContent += data.customerNameArray[i];
                newContent += '</td>';
                newContent += '<td>';
                newContent += data.customerAddressArray[i];
                newContent += '</td>';
                newContent += '<td align="right">';
                newContent += data.overdueArray[i];
                newContent += '</td>';
                newContent += '<td align="right">';
                newContent += data.arrearDaysArray[i];
                newContent += '</td>';
                newContent += '</tr>';
                $("#clientListTableId").append(newContent).trigger('create');
            }

            clientListcurrentrow = 0;
            clientListmaxrows = $("#clientListTableId tr").length - 1;

            $("#clientListPrevId").hide();
            if (clientListmaxrows > 5) {
                $("#clientListNextId").show();
            } else {
                $("#clientListNextId").hide();
            }
            $('#clientListTableId tr.showhide').hide();
            for (var i = 0; i < 5; i++) {
                if (clientListcurrentrow < clientListmaxrows) {
                    $('#clientListTableId tr.showhide:eq(' + clientListcurrentrow + ')').show();
                    clientListcurrentrow++;
                }
            }
            clientListPageNo = 1;
            var $btn_text = $('#clientListPageNoId').find('.ui-btn-text')
            $btn_text.text("Records " + clientListPageNo + " - " + clientListcurrentrow + " of " + clientListmaxrows);

            document.getElementById("custommainTab").href = "#recentActivityPopup";
            $("#custommainTab").trigger('click');
        }, error: function (jqXHR, textStatus, error) {
            alert("textStatus" + textStatus);
        }
    });
}

//function to download docs
function downloadDocs(selectedDocLocation) {
    if(selectedDocLocation != 0){
        document.getElementById("selectedDocId").value = selectedDocLocation;
        document.getElementById("recoveryFormID").method = 'POST';
        document.getElementById("recoveryFormID").action = localStorage.contextPath + '/downloadDocs';
        document.getElementById("recoveryFormID").submit();

    }
}

function viewPlans() {
    document.getElementById("recoveryFormID").method = 'POST';
    document.getElementById("recoveryFormID").action = localStorage.contextPath + '/NPALRGroups/todo/current';
    document.getElementById("recoveryFormID").submit();
}

//Added By Vishesh

function submitRemarks(accountId){
    var remark = $.trim($('#submitRemarksId').val());
    if($('#status').val()<=0){
        $('#errorLabelStatus').text("Select a Status");
    }
    else if(remark.length<=0){
        $('#errorLabelStatus').text("Remarks cannot be empty");
    }
    else{
        $('#errorLabelStatus').text("");
        var data = { status: $('#status').val(), remarks:remark, accountId:accountId};
        console.log(data);
        console.log(URIPrefix + ajaxcallip + localStorage.contextPath + '/submitNpaReviewDetails');
        $.ajax({
            type: 'POST',
            data: data,
            url: URIPrefix + ajaxcallip + localStorage.contextPath + '/submitNpaReviewDetails',
            success: function (data) {
                $('#successMsg').text('Successfully updated');
                window.location.href=URIPrefix + ajaxcallip + localStorage.contextPath + '/searchnpa';
            }, error: function (jqXHR, textStatus, error) {
                alert("textStatus" + textStatus);
            }
        });
        
    }
}

function submitPlans(accountId){
    
    if(planActivityArray.length===0 && planExpectedDueDateArray.length===0 && planExpectedDueTimeArray.length===0){
        $('#errorLabelPlan').text("No Actions Added");
        $('#successMsgPlan').text('');
    }
    else{
        $('#errorLabelPlan').text("");
        var data = {};
        data.actionPlan = planActivityArray;
        data.dueDate = planExpectedDueDateArray;
        data.dueTime = planExpectedDueTimeArray;
        data.accountId = accountId;
        data.userId = $("#loanOfficer").val();
        data.noOfPlan = noOfPlan;
        $.ajax({
            beforeSend: function () {
                $.mobile.showPageLoadingMsg();
            },
            complete: function () {
                $.mobile.hidePageLoadingMsg()
            },
            type: 'POST',
            data: data,
            url: URIPrefix + ajaxcallip + localStorage.contextPath + '/addActionPlans',
            success: function (data1) {
                $('#successMsgPlan').text('Successfully updated');
                planActivityArray = [];
                planExpectedDueDateArray =[];
                planExpectedDueTimeArray = [];
                $("#addPlanDivId").text('');
            }, error: function (jqXHR, textStatus, error) {
                $('#errorLabelPlan').text("Unable to add action plans");
            }
        });
    }
}