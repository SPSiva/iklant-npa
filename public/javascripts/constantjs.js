isModeOfPaymentApplicable = false;
isFeesApplicable = true;
totalRecordsPerPage = 100;
headerImage = "/images/iklant.png";
defaultDisbDate = '2016-04-01';
ajaxcallip = window.location.hostname+":"+window.location.port;
URIPrefix = window.location.protocol+"//";
minimumClients = 2;  //Also change minimumNumberOfClients in Constants.js
minimumClientsForProductLoan = 2;
isGRTBeforeAppraisal = true; //Applicable if the GRT comes before appraisal for reinitiate purpose

hoId = 1;
smhRoleId = 1;
bmRoleId = 3;
foRoleId = 5;
bdeRoleId=4;
deoRoleId = 6;
naiveRoleId = 8;
aeRoleId = 9;
cceRoleId = 7;
amhRoleId = 12;
abmRoleId = 19;

var cgtStatus = 30;
branchOfficeLevelId = 5;

//Status Constants
preliminaryVerified = 2;
creditBureauAnalysedStatus = 5;
assignedFO = 6;
FieldVerified = 7;
appraisedStatus = 9;
RejectedPriliminaryVerification = 14;
RejectedCreditBureauAnalysisStatusId = 15;
RejectedFieldVerification = 16;
RejectedAppraisal = 17;
dataVerified = 19;
rejectedInNextLoanPreCheck = 21;
rejectedWhileIdleGroupsStatusId = 22;
groupRecognitionTested = 20;
bankDetailsUpdate = 31;

//CBA Manual/Automation Rules
totalOverDueAmount = 0;
totalWriteOffAmount = 0;
mFICount = 1;
hasOwnMFIAccount = 1;
ownMFIId = 'MFI0000126';
totalOutStandingGradeNew = 0;
totalOutStandingGradeVeryGoodMin = 1;
totalOutStandingGradeVeryGood = 5000;
totalOutStandingGradeGood = 15000;
totalOutStandingGradeAverage = 30000;
repaymentTrackNew = 50;
repaymentTrackVeryGood= 51;
repaymentTrackGood = 52;
repaymentTrackAverage = 53;
repaymentTrackReject = 80;



//Operation Constants
preliminaryVerificationOperationId = 2;
KYCUploadingOperationId = 3;
kycDownloadingOperationId = 4;
kycUpdatingOperationId = 5;
creditBureauAnalysedOperationId = 6;
fieldVerificationOperationId = 8;
neededMoreinformationOperationId = 9;
appraisalOperationId = 10;
authorizeGroupOperationId = 11;
loanSanctionOperationId = 12;
synchronizedOperationId = 13;
rejectedGroupsOperationId = 14;
rejectedClientOperationId = 15;
manageOfficeOperationId = 17;
dataVerificationOperationId = 19;
kycUpdatingStatusOperationId = 20;
groupRecognitionTestOperationId = 24;
holdGroupsOperationId = 25;
nextLoanPreCheck = 26;
idleGroupsOperationId = 28;
leaderSubLeaderUpdatingOperationId = 29;
leaderSubLeaderVerificationOperationId = 31;
kycReUpdateOperationId = 32;
uploadNOCOperationId=38;
memberTypeLookUp = 117;
subLeaderTypeLookUp = 116;

//Lookup Value
ownHouse = 32;
rentalHouse = 33;
leaseHouse = 34;

//DocumentId
MOMBookId = 1;
bankPassBookId = 2;

groupLevel = 1;
clientLevel = 2;

poorStatus = "Poor";
averageStatus = "Average";
goodStatus = "Good";
vGoodStatus = "Very Good";
excellentStatus = "Excellent";

callStatusSuccess = "Call Success";
callStatusNoResponse = "No response";
callStatusNotAware = "Family member not aware";
callStatusWrongNumber = "Wrong Number";
callStatusOff = "Switched off";
callStatusBusy = "Busy";
callStatusRing = "Ringing No Response";
callStatusNotReach = "Not Reachable";
callStatusIncorrect = "Incorrect Number";
callStatusBarred = "Barred";
callStatusNoNum = "No mobile no";
callStatusAvailability = "Member not available";

deoRejected = 2;
verificationFailed = 4;
verificationSuccess = 5;

//For loan amount update in loan disbursement
isLoanAmountEditable = false;
memberIdArray = [];

//For NPA selection
selectedAccountIds = [];
selectedcustomerIds = [];
selectedgroupNames = [];

// Max due for NPA recovery
maxDueCountForNpa = 2;