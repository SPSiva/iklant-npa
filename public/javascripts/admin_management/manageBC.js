$(document).ready(function() {
    $("#addBCDiv").hide();
    $('#addbcId').click(function(){
        $('#errorMessage').hide();
        $('#successMessage').hide();
        $("#bcId").val(0);
        $("#addButtonBCDiv").hide();
        $("#tableviewdiv").hide();
        $("#addBCDiv").show();
        $("#save").show();
        $("#reset").show();
        $("#bcOfficeName").prop("disabled",false);
        $("#bcShortNameId").prop("disabled",false);
    });
    
    $('#backOId').click(function(){
        $("#bcId").val(0);
        $("#save").hide();
        $("#update").hide();
        $("#reset").hide();
        $("#tableviewdiv").show();
        $("#addBCDiv").hide();
        $("#addButtonBCDiv").show();
        resetFields();
    });
    
    $('#yesDeleteId').click(function(){
        $.mobile.showPageLoadingMsg();
        document.getElementById("AdminFormId").method='POST';
        document.getElementById("AdminFormId").action=localStorage.contextPath+"/deleteBC/"+$('#bcId').val();
        document.getElementById("AdminFormId").submit();
    });
});

function getPopup(bcId){
    $("p").text('');
    $(window).scrollTop(0);
    $('#bcId').val(bcId);
    $("#deleteConfirmationId").popup('open');
}

function resetFields() {
    $('#AdminFormId')[0].reset();
    $('#workflowId').val(0).selectmenu("refresh");
    $('#bankGLId').val(0).selectmenu("refresh");
}

function populateBCDetails(bcId){
    $('#successMessage').hide();
    $('#errorMessage').hide();
    if(bcId != 0){
        var data = {};
        data.bcId = bcId;

        ajaxVariable = $.ajax({
            beforeSend : function() {
                $.mobile.showPageLoadingMsg();
            },
            complete: function() {
                $.mobile.hidePageLoadingMsg()
            },
            type: 'POST',
            data: JSON.stringify(data),
            contentType: 'application/json',
            url: URIPrefix+ajaxcallip+localStorage.contextPath+'/getBCDetails',
            success: function(data) {
                if(data.status){
                    $("#addButtonBCDiv").hide();
                    $("#tableviewdiv").hide();
                    $("#addBCDiv").show();
                    $("#update").show();
                    $('#reset').hide();
                    $("#bcId").val(data.bcDetails.bcId);
                    $("#bcOfficeName").val(data.bcDetails.bcName);
                    $("#bcShortNameId").val(data.bcDetails.bcShortName);
                    $("#bcOfficeName").prop("disabled",true);
                    $("#bcShortNameId").prop("disabled",true);
                    $("#bcAddress").val(data.bcDetails.bcAddress);
                    $("#workflowId").val(data.bcDetails.workFlow).selectmenu("refresh");
                    $("#glCode").val(data.bcDetails.glCode).selectmenu("refresh");
                }else{
                    $('#errorMessage').show();
                    $('#errorMessage').text("Error while retrieving BC details");
                }
            }
        });
    }
}

function saveOrUpdateBc(){
    if(validate()){
        $.mobile.showPageLoadingMsg();
        var bcDetails = {bcId: $("#bcId").val(),
            bcOfficeName: $("#bcOfficeName").val(),
            bcShortName: $("#bcShortNameId").val(),
            bcAddress: $("#bcAddress").val(),
            workflowId: $("#workflowId").val(),
            glCode: $("#glCode").val()
        };

        ajaxVariable = $.ajax({
            type: 'POST',
            data: JSON.stringify(bcDetails),
            contentType: 'application/json',
            url: URIPrefix+ajaxcallip+localStorage.contextPath+'/saveOrUpdateBc',
            success: function(response) {
                if(response.status){
                    if(response.isExists){
                        $.mobile.hidePageLoadingMsg();
                        $('#errorMessage').show();
                        $('#errorMessage').text("BC name or short name already exists. Please provide different one");
                    }else{
                        $("#message").val((bcDetails.bcId == 0) ? "BC details saved successfully" : "BC details updated successfully");
                        document.getElementById("AdminFormId").method='POST';
                        document.getElementById("AdminFormId").action=localStorage.contextPath+"/admin/operation/"+$('#currentOperationIndex').val();
                        document.getElementById("AdminFormId").submit();
                    }
                }else{
                    $.mobile.hidePageLoadingMsg();
                    $('#errorMessage').show();
                    $('#errorMessage').text("Error while save BC details");
                }
            }
        });
    }
}

function validate(){
    $('#errorMessage').show();
    if($('#bcOfficeName').val().trim().length == 0){
        $('#errorMessage').text("Please provide BC Name");
        $('#bcOfficeName').focus();
        return false;
    }else if($('#bcShortNameId').val().trim().length == 0){
        $('#errorMessage').text("Please provide BC short name");
        $('#bcShortNameId').focus();
        return false;
    }else if($('#bcShortNameId').val().trim().length == 0){
        $('#errorMessage').text("BC short name should have minimum 2 characters");
        $('#bcShortNameId').focus();
        return false;
    }else if($('#bcAddress').val().trim().length == 0){
        $('#errorMessage').text("Please provide BC address");
        $('#bcAddress').focus();
        return false;
    }else if($('#workflowId').val() == 0){
        $('#errorMessage').text("Please select workflow type");
        return false;
    }
    $('#errorMessage').hide();
    return true;
}