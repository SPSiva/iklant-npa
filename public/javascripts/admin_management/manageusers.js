$(document).ready(function () {
    $(function () {
        $("#dobId").datepicker({
            maxDate: new Date,
            dateFormat: 'yy-mm-dd',
            yearRange: "-90:+0",
            changeMonth: true,
            changeYear: true
        });
    });
   
    //Number length Validation
    $("#addUDiv").hide();
    $("#save").hide();
    $("#update").hide();
    $("#reset").hide();
    $("#passwordDiv").hide();
    $("#confirmPasswordDiv").hide();
    $("#imeiDivId").hide();
    
    $("#addUId").click(function () {
        $("#tableviewdiv").hide();
        $("#addUDiv").show();
        $("#reset").show();
        $("#save").show();
        $("#addUserDivId").hide();
        $("#userNameId").prop('disabled',false);
        $("p").text("");
    });
    $("#backUId").click(function () {
        $("#passwordDiv").hide();
        $("#confirmPasswordDiv").hide();
        $("#tableviewdiv").show();
        $("#save").hide();
        $("#update").hide();
        $("#reset").hide();
        $("#addUDiv").hide();
        $("#addUserDivId").show();
        $('#userIdHidden').val("")
        $("p").text("");
        resetFields();
    });

    $("#userNameId").focusout(function () {
        var userNameArray = $("#userNameArrayId").val().split(",");
        var enteredUserName = $("#userNameId").val().toLowerCase();
        var trimmedString = $.trim(enteredUserName);
        for (var i = 0; i < userNameArray.length; i++) {
            userNameArray[i] = userNameArray[i].toLowerCase();
        }
        if (($.inArray(trimmedString, userNameArray)) != -1 && $("#userNameIdHidden").val().toLowerCase() != enteredUserName) {
            $("#errorMessageId").show();
            $("#errorMessageId").text("User Name already Exists");
            $("#userNameId").focus();
            $(window).scrollTop(0);
        } else {
            $("#errorMessageId").hide();
            $("p").text("");
            $(window).scrollTop(0);
        }
        if (/\s/g.test(enteredUserName)) {
            $("#errorMessageId").show();
            $("#errorMessageId").text("Please do not include spaces in the user name.");
            $("#userNameId").focus();
            $(window).scrollTop(0);
        }
    });
    $("#imeiNumberIdID").focusout(function () {
        var idIMEI = "#" + this.id;
        var imeiNumberArray = $("#imeiNumberArrayId").val().split(",");
        var enteredIMEInumber = $(idIMEI).val();
        console.log(enteredIMEInumber);
        var trimmedString = $.trim(enteredIMEInumber);
        if (enteredIMEInumber == '' || enteredIMEInumber == null) {
            $("#errorMessageId").show();
            $("#errorMessageId").text("Please enter the IMEI number.");
            $(idIMEI).focus();
            $(window).scrollTop(0);
        } else if (($.inArray(trimmedString, imeiNumberArray)) != -1) {
            $("#errorMessageId").show();
            $("#errorMessageId").text("IMEI number already Exists");
            $(idIMEI).focus();
            $(window).scrollTop(0);
        } else {
            $("p").text("");
            $(window).scrollTop(0);
        }
    });
    $('#userNameId').keyup(function () {
        this.value = this.value.toLowerCase();
    });
    
//    $("#userNameId").keydown(function(event){
//        return ((!event.shiftKey && (event.keyCode == 190)) || (event.keyCode >= 65 && event.keyCode  <= 90) || (event.keyCode == 8 || event.keyCode  == 9 || event.keyCode  == 32 || event.keyCode  == 37 || event.keyCode  == 39 || event.keyCode  == 46 ) );
//    });

    $("#lastNameId").keydown(function (event) {
        return ((!event.shiftKey && (event.keyCode == 190)) || (event.keyCode >= 65 && event.keyCode <= 90) || (event.keyCode == 8 || event.keyCode == 9 || event.keyCode == 32 || event.keyCode == 37 || event.keyCode == 39 || event.keyCode == 46));
    });

    $("#emailIdID").keydown(function (event) {
        return ((!event.shiftKey && (event.keyCode == 190)) || (event.keyCode == 50 && event.shiftKey) || (event.keyCode >= 65 && event.keyCode <= 90) || (event.keyCode >= 48 && event.keyCode <= 57) || (event.keyCode >= 96 && event.keyCode <= 105) || (event.shiftKey && (event.keyCode == 189)) || (event.keyCode == 8 || event.keyCode == 46 || event.keyCode == 9 || event.keyCode == 37 || event.keyCode == 39 || event.keyCode == 32));
    });

    $("#imeiNumberIdID").keydown(function(event){
        return ( (!event.shiftKey && (event.keyCode == 190)) ||(event.keyCode == 50 && event.shiftKey) ||(event.keyCode >= 65 && event.keyCode  <= 90) || (event.keyCode >= 48 && event.keyCode  <= 57) || (event.keyCode >= 96 && event.keyCode  <= 105)|| (event.shiftKey && (event.keyCode == 189)) || (event.keyCode == 8 || event.keyCode  == 46 || event.keyCode  == 9 || event.keyCode  == 37 || event.keyCode  == 39 || event.keyCode  == 32) );
    });

    $('#contactNumberId').keyup(function () {
        var $this = $(this);
        if ($this.val().length > 10)
            $this.val($this.val().substr(0, 10));
    });

    //Alphabets restriction code
    $("#contactNumberId").keydown(function (event) {
        // Allow: backspace, delete, tab, escape, and enter
        if (event.keyCode == 46 || event.keyCode == 8 || event.keyCode == 9 || event.keyCode == 27 || event.keyCode == 13 || event.keyCode == 37 || event.keyCode == 39 ||
                // Allow: Ctrl+A
                        (event.keyCode == 65 && event.ctrlKey === true) ||
                        // Allow: home, end, left, right
                                (event.keyCode >= 35 && event.keyCode <= 39)) {
                    // let it happen, don't do anything
                    return;
                } else {
                    // Ensure that it is a number and stop the keypress
                    if (event.shiftKey || (event.keyCode < 48 || event.keyCode > 57) && (event.keyCode < 96 || event.keyCode > 105)) {
                        event.preventDefault();
                    }
                }
            });

    $("#yesUserDeleteId").click(function () {
        $(window).scrollTop(0);
        document.getElementById("AdminFormId").method = 'POST';
        document.getElementById("AdminFormId").action = localStorage.contextPath + "/" + $('#deleteUserId').val() + "/deleteUser";
        document.getElementById("AdminFormId").submit();
    });
    
    $('#userHierarchyId').on("change",function(e){
        if(e.target[1].selected){
            $("#imeiDivId").show();
        }else{
            $("#imeiDivId").hide();
        }
    });
    
    $("#roleId6").on("change", function (e) {
        if (!e.target.id) {
            return;
        }
        if (e.target.checked) {
            $(window).scrollTop(0);
            $('#selectedBcIdHidden').val($('#bcId').val());
            $("#bcConfirmation").show();
            $("#bcConfirmation").text("Please select BC office");
            $("#bcDivId").show();
        }
        else {
            $("#bcDivId").hide();
            $("#bcConfirmation").hide();
            $('#selectedBcIdHidden').val(0);
        }
    });
    
    $('#officeId').change(function() {
         $('#bcIdHidden').val($(this).find("option:selected").attr("alt"));
    });
});

function manageUsersSubmitForm(flag) {
    if(validate()){
        $.mobile.showPageLoadingMsg();
        document.getElementById("errorMessageId").innerText = "";
        document.getElementById("AdminFormId").method = 'POST';
        document.getElementById("AdminFormId").action = localStorage.contextPath + ((flag == 0) ? "/saveuser" : "/updateuser");
        document.getElementById("AdminFormId").submit();
    }
}

function populateUserDetails(uid) {
    $("p").text('');
    $("#tableviewdiv").hide();
    $("#errorMessageId").text("");
    $("#addUDiv").show();
    $("#update").show();
    $("#passwordDiv").show();
    $("#confirmPasswordDiv").show();
    $("#addUserDivId").hide();
    var userId = uid;
    if (userId != 0) {
        var data = {};
        data.userId = userId;

        ajaxVariable = $.ajax({
            beforeSend: function () {
                $.mobile.showPageLoadingMsg();
            },
            complete: function () {
                $.mobile.hidePageLoadingMsg()
            },
            type: 'POST',
            data: JSON.stringify(data),
            contentType: 'application/json',
            url: URIPrefix + ajaxcallip + localStorage.contextPath + '/populateuserdetails',
            success: function (data) {
                $("#userIdHidden").val(data.userId);
                $("#officeId").val(data.officeId).selectmenu("refresh");
                $("#bcIdHidden").val(data.bcId);
                $("#officeEditHidden").val(data.officeId);
                $("#userNameId").val(data.userNameId);
                $("#userNameId").prop('disabled',true);
                $("#userNameIdHidden").val(data.userNameId);
                $("#contactNumberId").val(data.contactNumberId);
                $("#emailIdID").val(data.emailIdID);
                $("#firstNameId").val(data.firstName);
                $("#lastNameId").val(data.lastName);
                $("#dobId").val(data.dob);
                $("#genderId").val(data.gender).selectmenu("refresh");
                $("#addressId").val(data.address);
                $("#oldPassword").val(data.password);
                $("#userHierarchyId").val(data.userHierarchy).selectmenu("refresh");
                if(data.userHierarchy == 1){
                    $("#imeiDivId").show();
                    $("#imeiNumberIdID").val(data.imeiNumberId);
                    $("#imeiNumberIdIDHidden").val(data.imeiNumberId);
                }  

                var role = data.roleId;
                for (var j = 0; j < role.length; j++) {
                    $("#roleId" + role[j]).val(role[j]);
                    $("#roleId" + role[j]).attr("checked", "true").checkboxradio('refresh');
                    if(role[j] == 6){
                        $("#bcDivId").show();
                        $("#bcId").val(data.bcId).selectmenu("refresh");
                        $('#selectedBcIdHidden').val($('#bcId').val());
                    }
                }
            }
        });
    }
}

function resetFields() {
    $(window).scrollTop(0);
    $('#AdminFormId')[0].reset();
    $("#officeId").val(0).selectmenu("refresh");
    $("#genderId").val(0).selectmenu("refresh");
    $("#userHierarchyId").val(0).selectmenu("refresh");
    $("#AdminFormId input[type=checkbox]").prop("checked", false).checkboxradio('refresh');
}

function deleteUsers(userid, deleteUserId) {
    $("p").text('');
    $(window).scrollTop(0);
    $('#deleteUserId').val(userid);
    document.getElementById(deleteUserId).href = "#deleteConfirmationId";
    $("#" + deleteUserId).trigger('click');
}

function validate(){
    $("#errorMessageId").show();
    if($("#roleId6").prop("checked") == true && $('#bcId').val() == 0){
        $("#errorMessageId").text("Please select BC Office");
        $(window).scrollTop(0);
        return false;
    }else if ($("#officeId").val() == 0) {
        $("#errorMessageId").text("Please select Office");
        $(window).scrollTop(0);
        return false;
    }else if ($("#userNameId").val().trim().length == 0) {
        $("#errorMessageId").text("Please provide user name");
        $("#userNameId").focus();
        $(window).scrollTop(0);
        return false;
    }else if ($('#userIdHidden').val() != "" && ($('#password').val().trim().length > 0) || $('#confirmPassword').val().trim().length > 0) {
        if ($('#password').val().trim().length != 0 && $('#password').val().trim().length < 6 ) {
            $("#errorMessageId").text("Please provide atleast 6 characters for password");
            $("#password").focus();
            $(window).scrollTop(0);
            return false;
        }else if ($('#password').val().trim().length != 0 && $('#confirmPassword').val().trim().length == 0) {
            $("#errorMessageId").text("Please provide confirm password");
            $("#confirmPassword").focus();
            $(window).scrollTop(0);
            return false;
        }else if ($('#password').val().trim() != $('#confirmPassword').val().trim()) {
            $("#errorMessageId").text("Password and confirm password should be same");
            $("#confirmPassword").focus();
            $(window).scrollTop(0);
            return false;
        }
    }else if ($("#firstNameId").val().trim().length == 0) {
        $("#errorMessageId").text("Please provide first name");
        $("#firstNameId").focus();
        $(window).scrollTop(0);
        return false;
    }else if ($("#lastNameId").val().trim().length == 0) {
        $("#errorMessageId").text("Please provide last name");
        $("#lastNameId").focus();
        $(window).scrollTop(0);
        return false;
    }else if ($("#dobId").val().trim().length == 0) {
        $("#errorMessageId").text("Please select DOB");
        $("#dobId").focus();
        $(window).scrollTop(0);
        return false;
    }else if ($("#genderId").val().trim().length == 0) {
        $("#errorMessageId").text("Please select gender");
        $(window).scrollTop(0);
        return false;
    }else if ($("#addressId").val().trim().length == 0) {
        $("#errorMessageId").text("Please provide address");
        $("#addressId").focus();
        $(window).scrollTop(0);
        return false;
    }else if ($("#userHierarchyId").val() == 0) {
        $("#errorMessageId").text("Please select user hierarchy");
        $(window).scrollTop(0);
        return false;
    }else if ($("#contactNumberId").val().trim().length == 0) {
        $("#errorMessageId").text("Please provide contact number");
        $("#contactNumberId").focus();
        $(window).scrollTop(0);
        return false;
    }else if ($("#contactNumberId").val().trim().length < 10) {
        $("#errorMessageId").text("Please provide atleast 10 digit Contact Number");
        $("#contactNumberId").focus();
        $(window).scrollTop(0);
        return false;
    }else if ($("#emailIdID").val().trim().length == 0) {
        $("#errorMessageId").text("Please provide email address");
        $("#emailIdID").focus();
        $(window).scrollTop(0);
        return false;
    }else if (!validateEmail($("#emailIdID").val().trim())) {
        $("#errorMessageId").text("Please provide valid email address");
        $("#emailIdID").focus();
        $(window).scrollTop(0);
        return false;
    }else if ($("#imeiNumberIdID").val().trim().length == 0 && $("#userHierarchyId").val() == 1) {
        $("#errorMessageId").text("Please provide IMEI number");
        $("#imeiNumberIdID").focus();
        $(window).scrollTop(0);
        return false;
    }else if ($('#AdminFormId input[type=checkbox]:checked').length == 0){
        $("#errorMessageId").text("Please select atleast one role for the user");
        $(window).scrollTop(0);
        return false;
    }
    
    $("#errorMessageId").hide();
    return true;
}

function bcChange(){
    $('#selectedBcIdHidden').val($('#bcId').val());
}