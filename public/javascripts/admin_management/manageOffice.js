$(document).ready(function () {
    $("#addODiv").hide();
    $("#save").hide();
    $("#update").hide();
    $("#addOId").click(function () {
        $("#save").show();
        $("#reset").show();
        $("#addDivId").hide();
        $("#addODiv").show();
        $("#tableviewdiv").hide();
        $("#errorMessageIdManageOffice").hide();
        $("#successMessage").hide();
        $("#stateId").prop('disabled',false);
        $("#officeNameId").prop('disabled',false);
        $("#officeShortNameId").prop('disabled',false);
        $("#officeLevel").prop('disabled',false);
    });
    $("#backOId").click(function () {
        $("#save").hide();
        $("#update").hide();
        $("#addDivId").show();
        $("#addODiv").hide();
        $("#tableviewdiv").show();
        $("#errorMessageIdManageOffice").hide();
        $("#errorMessageId").text("");
        resetFields();
    });
    $("#officeNameId").focusout(function () {
        var officeNameArray = $("#officeNameArray").val().split(",");
        var enteredOfficeName = $("#officeNameId").val().toLowerCase();
        var trimmedString = $.trim(enteredOfficeName);
        for (var i = 0; i < officeNameArray.length; i++) {
            officeNameArray[i] = officeNameArray[i].toLowerCase();
        }
        if (($.inArray(trimmedString, officeNameArray)) > -1 && $("#officeNameIdHidden").val().toLowerCase() != enteredOfficeName) {
            $("#errorMessageIdManageOffice").show();
            $("#errorMessageIdManageOffice").text("Office already Exists");
            $("#officeNameId").focus();
            $(window).scrollTop(0);
        } else {
            $("#errorMessageIdManageOffice").hide();
            $(window).scrollTop(0);
        }
    });
    $("#officeNameId").keydown(function (event) {
        return ((event.keyCode >= 65 && event.keyCode <= 90) || (event.keyCode == 8 || event.keyCode == 9 || event.keyCode == 32 || event.keyCode == 37 || event.keyCode == 39 || event.keyCode == 46));
    });

    $("#officeShortNameId").keydown(function (event) {
        return ((event.keyCode >= 65 && event.keyCode <= 90) || (event.keyCode == 8 || event.keyCode == 9 || event.keyCode == 32 || event.keyCode == 37 || event.keyCode == 39 || event.keyCode == 46));
    });

    $("#officeAddressId").keydown(function (event) {
        if ((!event.shiftKey && event.keyCode == 222) || event.keyCode == 220) {
            return false;
        }
    });
});
function manageOfficeSubmitForm(flag) {
    if (validate()) {
        $.mobile.showPageLoadingMsg();
        document.getElementById("AdminFormId").method = 'POST';
        document.getElementById("AdminFormId").action = localStorage.contextPath + ((flag == 0) ? "/saveoffice" : "/updateoffice");
        document.getElementById("AdminFormId").submit();
    }
}
function validate(){
    $("#errorMessageIdManageOffice").show();
    if($('#bcId').val() == 0){
        $("#errorMessageIdManageOffice").text("Please select BC");
        return false;
    }else if($('#officeLevel').val() == 0){
        $("#errorMessageIdManageOffice").text("Please select Office level");
        return false;
    }else if($('#parentOffice').val() == 0){
        $("#errorMessageIdManageOffice").text("Please select parent office");
        return false;
    }else if($('#officeNameId').val().trim().length == 0){
        $("#errorMessageIdManageOffice").text("Please provide office name");
        $('#officeNameId').focus();
        return false;
    }else if($('#officeShortNameId').val().trim().length == 0){
        $("#errorMessageIdManageOffice").text("Please provide office short name");
        $('#officeShortNameId').focus();
        return false;
    }else if($('#officeAddressId').val().trim().length == 0){
        $("#errorMessageIdManageOffice").text("Please provide office address");
        $('#officeAddressId').focus();
        return false;
    }else if($('#stateId').val() == 0){
        $("#errorMessageIdManageOffice").text("Please select State");
        return false;
    }
    $("#errorMessageIdManageOffice").hide();
    return true;
}
function populateOfficeDetails(officeEditId) {
    var officeId = officeEditId;
    $("#addODiv").show();
    $("#tableviewdiv").hide();
    $("#addDivId").hide();
    $("#reset").hide();
    $("#update").show();
    $("#successMessage").hide();
    $("#errorMessageIdManageOffice").hide();
    if (officeId != 0) {
        var data = {};
        data.officeId = officeId;

        ajaxVariable = $.ajax({
            beforeSend: function () {
                $.mobile.showPageLoadingMsg();
            },
            complete: function () {
                $.mobile.hidePageLoadingMsg()
            },
            type: 'POST',
            data: JSON.stringify(data),
            contentType: 'application/json',
            url: URIPrefix + ajaxcallip + localStorage.contextPath + '/populateofficedetails',
            success: function (data) {
                $("#officeId").val(data.officeId);
                $("#officeNameId").val(data.officeName);
                $("#officeNameIdHidden").val(data.officeName);
                $("#officeShortNameId").val(data.officeShortName);
                $("#officeAddressId").val(data.officeAddress);
                $("#stateId").val(data.stateName).selectmenu("refresh");
                $("#stateId").prop('disabled',true);
                $("#officeNameId").prop('disabled',true);
                $("#officeShortNameId").prop('disabled',true);
                $("#bcId").val(data.bcId).selectmenu('refresh');
                $("#officeLevel").prop('disabled',true);
                $("#officeLevel").val(data.officeLevel).selectmenu('refresh');
                $("#ldCallTrackingEnabled").val(data.ldCallTracking).selectmenu('refresh');
                levelChange(data.parentOfficeId);
            }
        });
    }
}

function resetFields() {
    $("#officeNameId").val("");
    $("#officeAddressId").val("");
    $("#officeShortNameId").val("");
    $("#errorMessageIdManageOffice").hide();
    $("#stateId").val('0').selectmenu("refresh");
    $("#officeLevel").val('0').selectmenu("refresh");
    $("#parentOffice").val('0').selectmenu("refresh");
    $("#bcId").val('0').selectmenu("refresh");
}

function deleteOffice(officeid) {
    $.mobile.showPageLoadingMsg();
    document.getElementById("AdminFormId").method = 'POST';
    document.getElementById("AdminFormId").action = localStorage.contextPath + "/" + officeid + "/deleteoffice";
    document.getElementById("AdminFormId").submit();
    alert("Office Deleted Successfully!");
}

function deleteOffice(officeid) {
    if (officeid != 0) {
        var data = {};
        data.officeid = officeid;
        ajaxVariable = $.ajax({
            type: 'POST',
            data: JSON.stringify(data),
            contentType: 'application/json',
            url: URIPrefix + ajaxcallip + localStorage.contextPath + '/checkForRoleIsAssigned',
            success: function (data) {
                if (data.noOfUsers == 0) {
                    document.getElementById("AdminFormId").method = 'POST';
                    document.getElementById("AdminFormId").action = localStorage.contextPath + "/" + officeid + "/deleteoffice";
                    document.getElementById("AdminFormId").submit();
                    alert("Role Deleted Successfully!");
                } else {
                    alert("Role cannot be deleted as it is assigned to " + data.noOfUsers + " users");
                }
            },
        });
    }
}
function stateChange() {
    var element = document.getElementById("stateId");
    $('#stateNameId').val(element.options[element.selectedIndex].text);
}

function levelChange(parentOffice) {
    var level = $('#officeLevel').val();
    var officeLevels = JSON.parse($('#officeHierarchyNameArray').val());
    var selectedOfficeLevel = $("#officeLevel option:selected").text();
    if (level != 0) {
        selectedIndex = checkExists(officeLevels,"officeLevelId",level);
        document.getElementById('parentOffice').options.length = 0;
        var combo1 = document.getElementById("parentOffice");
        option = document.createElement("option");
        option.text = "Select";
        option.value ="0";
        try {
            combo1.add(option, null);
        }catch(error) {
            combo1.add(option); 
        }
        $("#parentOffice").val(0).selectmenu('refresh');
        var data = {};
        data.officeLevelId = (selectedIndex == 0) ? 1 : officeLevels[selectedIndex-1].officeLevelId;

        ajaxVariable = $.ajax({
            beforeSend: function () {
                $.mobile.showPageLoadingMsg();
            },
            complete: function () {
                $.mobile.hidePageLoadingMsg()
            },
            type: 'POST',
            data: JSON.stringify(data),
            contentType: 'application/json',
            url: URIPrefix + ajaxcallip + localStorage.contextPath + '/getParentOfficeList',
            success: function (data) {
                if(data.parentOfficeList.length > 0){
                    for(var i=0;i<data.parentOfficeList.length;i++){
                        var combo = document.getElementById("parentOffice");
                        option = document.createElement("option");
                        option.value = data.parentOfficeList[i].office_id;
                        option.text =data.parentOfficeList[i].office_name;
                        try {
                            combo.add(option, null);
                        }catch(error) {
                            combo.add(option);
                        }
                    }
                    if(parentOffice != 0){
                        $("#parentOffice").val(parentOffice).selectmenu("refresh");
                    }
                }else{
                    $("#errorMessageIdManageOffice").text("Please create atleast one "+data.parentOfficeLevel+" to create "+selectedOfficeLevel+" office");
                }
            }
        });
    }
}