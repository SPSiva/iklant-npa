window.history.forward();
$(document).ready(function () {
    $('input, :input').attr('autocomplete', 'off');
});
function changePassword(){
    if($("#password").val().trim().length == 0){
        $("#errorMessageId").text("Please fill old password");
        $("#password").focus();
        $(window).scrollTop(0);
        return false;
    }
    else if($("#newPassword").val().trim().length == 0){
        $("#errorMessageId").text("Please fill new password");
        $("#newPassword").focus();
        $(window).scrollTop(0);
        return false;
    }
    else if($("#newPassword").val().trim().length < 6){
        $("#errorMessageId").text("Please provide atleast 6 characters For Password");
        $("#newPassword").focus();
        $(window).scrollTop(0);
        return false;
    }
    else if(!password_validation($("#newPassword").val().trim())){
        $("#errorMessageId").text("Password should contain atleast one uppercase and one digit or one special Character");
        $("#newPassword").focus();
        $(window).scrollTop(0);
        return false;
    }else if($("#confirmPassword").val().trim().length == 0){
        $("#errorMessageId").text("Please provide confirm password");
        $("#confirmPassword").focus();
        $(window).scrollTop(0);
        return false;
    }
    else if($("#confirmPassword").val().length < 6){
        $("#errorMessageId").text("Please provide atleast 6 characters For Password");
        $("#confirmPassword").focus();
        $(window).scrollTop(0);
        return false;
    }
    else if($("#newPassword").val().trim() != $("#confirmPassword").val().trim()){
        $("#errorMessageId").text("New Password and Confirm Password doesn't match");
        $("#confirmPassword").focus();
        $(window).scrollTop(0);
        return false;
    }
    else if($("#newPassword").val().trim() == $("#password").val().trim()){
        $("#errorMessageId").text("Old password and New password should not be same");
        $("#password").focus();
        $(window).scrollTop(0);
        return false;
    }
    else
    {
        $("#errorMessageId").val("");
        $.mobile.showPageLoadingMsg();
        document.getElementById("changePasswordFormID").method='POST';
        document.getElementById("changePasswordFormID").action=localStorage.contextPath+"/submitChangePassword";
        document.getElementById("changePasswordFormID").submit();
    }
}

function password_validation(password){
    var regExp = (/^(?=.*[A-Z])((?=.*\d)|(?=.*?[#?!@$%^&*-])).{6,}$/);
    return regExp.test(password);
}