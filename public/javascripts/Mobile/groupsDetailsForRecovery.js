var clientListcurrentrow = 0;
var clientListmaxrows = 0;
var clientListPageNo = 1;

var todoActivityArray = new Array();
var todoExpectedDueDateArray = new Array();
var todoExpectedDueTimeArray = new Array();
var inc = 0;
var noOfTodo = 0;
var biCount = 0;
var lnCount = 0;
var npaCount = 0;
var biNoCount = 0;
var lnNoCount = 0;
var installment1 = 0;
var installment2 = 0;


//for expected date zero validation
$(document).on('keyup', '.zero', function (event) {

    var input = event.currentTarget.value;
    if (input.search(/^0/) != -1) {
        alert("you have started with a 0");
    }

});

$(document).ready(function () {

    //NUMBER VALIDATION
    $(".zero").keydown(function (event) {
        // Allow: backspace, delete, tab, escape, and enter
        if (event.keyCode == 46 || event.keyCode == 8 || event.keyCode == 9 || event.keyCode == 27 || event.keyCode == 13 || event.keyCode == 37 || event.keyCode == 39 ||
                // Allow: Ctrl+A
                        (event.keyCode == 65 && event.ctrlKey === true) ||
                        // Allow: home, end, left, right
                                (event.keyCode >= 35 && event.keyCode <= 39)) {
                    // let it happen, don't do anything
                    return;
                } else {
                    // Ensure that it is a number and stop the keypress
                    if (event.shiftKey || (event.keyCode < 48 || event.keyCode > 57) && (event.keyCode < 96 || event.keyCode > 105)) {
                        event.preventDefault();
                    }
                    //for expected date zero validation
                    if ($.trim($(this).val()) == '')
                    {
                        if (event.keyCode == 48) {
                            event.preventDefault();
                        }
                    }
                }
            });
    //number validation for paid 
    $("#npaQuestion220").keydown(function (event) {
        if (event.keyCode == 190 || event.keyCode == 46 || event.keyCode == 8 || event.keyCode == 9 || event.keyCode == 27 || event.keyCode == 13 || event.keyCode == 37 || event.keyCode == 39 ||
                // Allow: Ctrl+A
                        (event.keyCode == 65 && event.ctrlKey === true) ||
                        // Allow: home, end, left, right
                                (event.keyCode >= 35 && event.keyCode <= 39)) {
                    // let it happen, don't do anything
                    return;
                } else {
                    if (event.shiftKey || (event.keyCode < 48 || event.keyCode > 57) && (event.keyCode < 96 || event.keyCode > 105)) {
                        event.preventDefault();
                    }
                }
            });

    $("#selectDocDivId").hide();
    $('#saveButtonDiv').hide();

    $('#multipleDocDivId').hide();
    $("#isMultipleDocCheckID").click(function () {
        if ($("#isMultipleDocCheckID").is(':checked') == true) {
            $("#isMultipleDocumentId").val($(this).is(':checked'));
            $('#multipleDocDivId').show();
            $('#singleDocDivId').hide();
            $('#singleUploadDocumentId').val("");
        } else if ($("#isMultipleDocCheckID").is(':checked') == false) {
            $("#isMultipleDocumentId").val($(this).is(':checked'));
            $('#multipleDocDivId').hide();
            $('#singleDocDivId').show();
            $('#multipleUploadDocumentId').val("");
        }
    });

    $("#addTodoId").click(function () {
        $("#errorLabel").text('');
        $("#activityId").val('');
        $("#expectedDateId").val('');
        $("#expectedTimeId").val('')
    });

    //Dynamic add todo 	
    $("#addTodo").click(function () {
        if ($("#activityId").val() == '' || $("#expectedDateId").val() == '' || $("#expectedTimeId").val() == '') {
            $("#errorLabel").text("Please fill all the fields");
            $("a#addTodo").attr('href', '');
        } else if ($("#expectedTimeId").val() > 24) {
            $("#errorLabel").text("Time should be less than 24hrs");
            $("a#addTodo").attr('href', '');
        } else {
            var newContent = '<div data-role="content" data-theme="b" class="content-primary">';
            newContent += '<ul data-role="listview" data-split-theme="b" data-inset="true" id="ulId">';
            newContent += '<li>';
            newContent += '<a href="">';
            newContent += "<label for='activityName'" + inc + " id='activityID'" + inc + ">Activity : " + $("#activityId").val() + "   Date : " + $("#expectedDateId").val() + "</label>";
            newContent += '<a href="", onclick="removeTodo(this,' + inc + ')" , data-icon="delete">';
            newContent += '</a>';
            newContent += '</li>';
            newContent += '</ul>';
            newContent += '</div>';
            $("#addTodoDivId").append(newContent).trigger('create');
            inc++;
            noOfTodo++;
            todoActivityArray.push($('#activityId').val());
            todoExpectedDueDateArray.push($('#expectedDateId').val());
            todoExpectedDueTimeArray.push($('#expectedTimeId').val());
            $('#todoActivity').val(todoActivityArray);
            $('#todoDueDate').val(todoExpectedDueDateArray);
            $('#todoDueTime').val(todoExpectedDueTimeArray);

            $("#addTodo").attr('href', '#addTodoPopupId');
        }
    });

    $("#other_reason").parent().parent().parent().parent().hide();
    $("#reason_not_paid").multiselect({
        noneSelectedText: "Select Reasons",
        selectedText: "# of # reasons selected",
        close: function (event, ui) {
            // event handler here
            if ($("#reason_not_paid").val() != null) {
                var selectedValues = $("#reason_not_paid").val();
                for (var i = 0; i < selectedValues.length; i++) {
                    if (selectedValues[i] == 1) {
                        $("#other_reason").parent().parent().parent().parent().show();
                        break;
                    } else
                        $("#other_reason").parent().parent().parent().parent().hide();
                }
            } else {
                $("#other_reason").parent().parent().parent().parent().hide();
            }
        }
    });

    $('#next_payment_date').change(function () {
        var selectedDate = $("#next_payment_date").val().split("-");
        var oneDay = 24 * 60 * 60 * 1000; // hours*minutes*seconds*milliseconds
        var firstDate = new Date();
        var secondDate = new Date(selectedDate[0], selectedDate[1] - 1, selectedDate[2]);
        var diffDays = Math.round(Math.abs((firstDate.getTime() - secondDate.getTime()) / (oneDay)));
        if (secondDate < firstDate) {
            alert("Next due date should be a future date");
            $("#next_payment_date").removeAttr('value');
        } else if (diffDays > 365) {
            alert("Next due date should not exceed more than a year");
            $("#next_payment_date").removeAttr('value');
        } else {
            calculateCapabilityPercentage(diffDays);
        }
    });

    $('#no_of_installments').change(function () {
        var numOfInstallments = ($('#no_of_installments').val() && $('#no_of_installments').val() != '') ? parseInt($('#no_of_installments').val()) : 0;
        if (numOfInstallments > 0 && numOfInstallments <= maxDueCountForNpa) {
            $('.numberofInstallmentsDiv').remove();
            for (var count = 0; count < numOfInstallments; count++) {
                var newElement = '<div data-role="horizontal" data-inline="true" class="numberofInstallmentsDiv">' +
                        '<div style="width:58%;padding-top:10px" class="ui-block-a">' +
                        '<div data-role="fieldcontain" class="ui-field-contain ui-body ui-br">' +
                        '<label for="next_payment_date' + (count + 1) + '" id="labelid">Installment - ' + (count + 1) + ' due date</label></div></div>' +
                        '<div style="width:100%" class="ui-block-a">' +
                        '<div data-role="fieldcontain" class="ui-field-contain ui-body ui-br">' +
                        '<input type="date" name="next_payment_date' + (count + 1) + '" id="next_payment_date' + (count + 1) + '" onchange="updateProbability(' + (count + 1) + ')"/></div></div></div>';

                $('#no_of_installments').parent().parent().parent().parent().parent().append(newElement).trigger("create");
            }
        } else {
            alert("Provide valid number of installment. Max installment count is: " + maxDueCountForNpa);
            $('#no_of_installments').focus();
        }
    });

    $("#clientListPrevId").click(function () {
        if (clientListcurrentrow == clientListmaxrows) {
            $("#clientListNextId").show();
        }

        var hidenextrow = clientListcurrentrow;
        if (clientListmaxrows == clientListcurrentrow) {
            var x = clientListmaxrows % 5;
            if (x > 0) {
                hidenextrow = hidenextrow + (5 - x);
                clientListcurrentrow = clientListcurrentrow + (5 - x);
            }
        }
        for (var i = 0; i < 5; i++) {
            hidenextrow--;
            $('#clientListTableId tr.showhide:eq(' + hidenextrow + ')').hide();
            if (clientListcurrentrow > 0) {
                clientListcurrentrow--;
                $('#clientListTableId tr.showhide:eq(' + (clientListcurrentrow - 5) + ')').show();

            }
        }
        if (clientListcurrentrow == 5) {
            $("#clientListPrevId").hide();
        }
        clientListPageNo = clientListPageNo - 5;
        var $btn_text = $('#clientListPageNoId').find('.ui-btn-text')
        $btn_text.text("Records " + clientListPageNo + " - " + clientListcurrentrow + " of " + clientListmaxrows);

    });

    //Group Installment Due pagination//
    $("#clientListNextId").click(function () {
        if (clientListcurrentrow == 5) {
            $("#clientListPrevId").show();
        }
        //alert("next"+clientListcurrentrow);
        var hidepreviousrow = clientListcurrentrow;
        for (var i = 0; i < 5; i++) {
            hidepreviousrow--;
            $('#clientListTableId tr.showhide:eq(' + hidepreviousrow + ')').hide();
            if (clientListcurrentrow < clientListmaxrows) {
                $('#clientListTableId tr.showhide:eq(' + clientListcurrentrow + ')').show();
                clientListcurrentrow++;
            }
        }
        if (clientListcurrentrow == clientListmaxrows) {
            $("#clientListNextId").hide();
        }
        clientListPageNo = clientListPageNo + 5;
        var $btn_text = $('#clientListPageNoId').find('.ui-btn-text')
        $btn_text.text("Records " + clientListPageNo + " - " + clientListcurrentrow + " of " + clientListmaxrows);
    });

    //upload Documents for group/client
    $("#clientNameDivId").hide();
    $("#groupDocRadioId").click(function () {
        $("#clientNameDivId").hide();
        $("#selectDocDivId").hide();
    });

    $("#clientDocRadioId").click(function () {
        $("#clientNameDivId").show();
        $("#selectDocDivId").hide();

        //retrieveClientDetails
        var data = {};
        data.accountId = $("#accountIdID").val();
        ajaxVariable = $.ajax({
            beforeSend: function () {
                $.mobile.showPageLoadingMsg();
            },
            complete: function () {
                $.mobile.hidePageLoadingMsg()
            },
            type: 'POST',
            data: JSON.stringify(data),
            contentType: 'application/json',
            url: URIPrefix + ajaxcallip + localStorage.contextPath + '/retrieveClientDetails',
            success: function (data) {
                $("#clientNamesId").val('0').selectmenu("refresh");
                document.getElementById('clientNamesId').options.length = 0;
                var combo1 = document.getElementById("clientNamesId");

                option = document.createElement("option");
                option.text = "Select";
                option.value = "0";
                try {
                    combo1.add(option, null); //Standard 
                } catch (error) {
                    combo1.add(option); // IE only
                }
                for (var i = 0; i < data.customerIdArray.length; i++) {
                    var combo = document.getElementById("clientNamesId");

                    option = document.createElement("option");
                    option.text = data.customerNameArray[i];
                    option.value = data.customerIdArray[i];
                    try {
                        combo.add(option, null); //Standard 
                    } catch (error) {
                        combo.add(option); // IE only
                    }
                }
            }, error: function (jqXHR, textStatus, error) {
                alert("textStatus" + textStatus);
            }
        });
    });
});

/* question populating based in its parent question */
function populateQuestions(questionSection, parentQuestionId, selected, utilQuestions, radioButtonCategory, count, yesNoFlag) {
    var chkBxVal = $('input[name="' + selected + '"]:checked').val();
    var idx = (radioButtonCategory == 'npa') ? questionSection + parentQuestionId + chkBxVal : questionSection + parentQuestionId;
    var toShow = $("#" + idx).attr("alt");
    var multiParent = ($("#parentIds3") && $("#parentIds3").val()) ? $("#parentIds3").val().split(",") : [];
    if (chkBxVal == toShow) {
        if (radioButtonCategory == 'npa') {

            if (selected == 'npaYesId4' && chkBxVal == 1) {
                $("#repayment_probability").parent().parent().parent().show();
            } else if (selected == 'npaYesId4' && chkBxVal == 0) {
                $("#repayment_probability").parent().parent().parent().hide();
            }

            if (selected == 'npaYesId4' && chkBxVal == 0) {
                $("#remarks").parent().parent().parent().show();
                $("#info").text("Enter remarks for not capable and submit verified details");
            } else {
                $("#remarks").parent().parent().parent().hide();
                if (chkBxVal === "0") {
                    if (document.getElementById('type') != null) {
                        $('#type').val(-1).selectmenu('refresh');
                    }
                    if (document.getElementById('paid_overdue') != null) {
                        $('#paid_overdue').val(-1).selectmenu('refresh');
                    }
                }
            }

            if (selected == 'npaYesId3' && chkBxVal == 1) {
                $("#info").text("Submit Verified Details,Head Office will take necessary action");
            }
        }
        $("#" + idx).show();
    } else if (multiParent.length > 1 && multiParent.indexOf(parentQuestionId) > -1) {
        var questionswithoutParse = utilQuestions;
        utilQuestions = JSON.parse(utilQuestions);
        if (selected == 'npaYesId2' && chkBxVal == 0) {
            $("#info").text("Couldn't trace any client,Submit Verified Details");
        } else {
            $("#parentIds3").parent().show();
        }
    } else {
        if (chkBxVal !== "1") {
            var set = radioButtonCategory;
            var questionswithoutParse = utilQuestions;
            utilQuestions = JSON.parse(utilQuestions);
            for (var j = 0; j < utilQuestions.length; j++) {
                if ((utilQuestions[j].parent_question_id) === parentQuestionId.toString()) {
                    $('#' + set + "YesId" + j).removeAttr('checked');
                    $('#' + set + "NoId" + j).removeAttr('checked');
                    $('#' + set + "YesId" + j).checkboxradio('refresh');
                    $('#' + set + "NoId" + j).checkboxradio('refresh');
                    $("#" + idx).hide();
                    populateQuestions(questionSection, utilQuestions[j].question_id, selected, questionswithoutParse, radioButtonCategory, yesNoFlag);
                }
            }
        }
    }
    if (radioButtonCategory === 'bi') {
        if (radioButtonCategory === 'bi' && yesNoFlag === 'no') {
            biCount = 0;
            biNoCount++;
        } else if (radioButtonCategory === 'bi' && yesNoFlag === 'yes') {
            $('#saveButtonDiv').hide();
            biCount++;
            biNoCount = 0;
        }
    }
    if (radioButtonCategory === 'ln') {
        if (radioButtonCategory === 'ln' && yesNoFlag === 'no' && idx === 'loanQuestion14') {
            lnCount = 0;
            lnNoCount++;
        } else if (radioButtonCategory === 'ln' && yesNoFlag === 'yes') {
            $('#saveButtonDiv').hide();
            lnCount++;
            lnNoCount = 0;
        }
    }
    if (radioButtonCategory === 'npa') {
        npaCount++;
    }
    if ((biCount > 0 && lnCount > 0 && npaCount > 0) || biNoCount > 0 || lnNoCount > 0) {
        $('#saveButtonDiv').show();
    }

    var childQuesions = ($('#hide_childs' + count).val()) ? $('#hide_childs' + count).val().split(",") : [];
    for (var i = 0; i < childQuesions.length; i++) {
        $("div[name='npaQuestion" + childQuesions[i] + "']").find(':radio').removeAttr('checked');
        $("div[name='npaQuestion" + childQuesions[i] + "']").find(':radio').checkboxradio('refresh');
        $("div[name='npaQuestion" + childQuesions[i] + "']").find(':text').val('');
        $("div[name='npaQuestion" + childQuesions[i] + "']").hide();
        $(".numberofInstallmentsDiv").remove();
        $("#info").text("");
    }

    var ifYes = ($('#show_if_yes' + count).val()) ? $('#show_if_yes' + count).val().split(",") : [];
    if ($('input[name="' + selected + '"]:checked').val() == 1) {
        for (var i = 0; i < ifYes.length; i++) {
            $("div[name='npaQuestion" + ifYes[i] + "']").show();
        }
    }
    var ifNo = ($('#show_if_no' + count).val()) ? $('#show_if_no' + count).val().split(",") : [];
    if ($('input[name="' + selected + '"]:checked').val() == 0) {
        for (var i = 0; i < ifNo.length; i++) {
            $("div[name='npaQuestion" + ifNo[i] + "']").show();
        }
    }

}
//function for NPA info payment type selection
function selectedPayment(property, parentId) {
    if (property.value == "1") {
        $('#npaQuestion' + parentId + '0').hide();
        $('#npaQuestion' + parentId + '1').show();
        if (document.getElementById('paid_overdue') != null) {
            $('#paid_overdue').val(-1).selectmenu('refresh');
            $('#npaQuestion232').hide();

        }
    } else if (property.value == "2") {
        $('#npaQuestion' + parentId + '0').show();
        $('#npaQuestion' + parentId + '1').show();
        if (document.getElementById('paid_overdue') != null) {
            $('#paid_overdue').val(-1).selectmenu('refresh');
            $('#npaQuestion232').hide();
        }

    } else if (property.value == "3") {
        $('#npaQuestion' + parentId + '2').hide();
    } else if (property.value == "4") {
        $('#npaQuestion' + parentId + '2').hide();
    } else if (property.value == "5") {
        $('#npaQuestion' + parentId + '2').show();
    }
}

/* */
function calculateCapabilityPercentage(diffDays) {
    if (diffDays <= 15) {
        $("#repayment_probability").val(95).selectmenu("refresh");
    } else if (diffDays > 15 && diffDays <= 30) {
        $("#repayment_probability").val(80).selectmenu("refresh");
    } else if (diffDays > 30 && diffDays <= 45) {
        $("#repayment_probability").val(60).selectmenu("refresh");
    } else if (diffDays > 45 && diffDays <= 60) {
        $("#repayment_probability").val(40).selectmenu("refresh");
    } else if (diffDays > 60 && diffDays <= 75) {
        $("#repayment_probability").val(20).selectmenu("refresh");
    } else if (diffDays > 75 && diffDays <= 90) {
        $("#repayment_probability").val(10).selectmenu("refresh");
    } else if (diffDays > 90) {
        $("#repayment_probability").val(5).selectmenu("refresh");
    }
}

function removeTodo(remove, i) {
    r = remove.parentNode.parentNode;
    r.parentNode.removeChild(r);
    todoActivityArray[i] = "";
    todoExpectedDueDateArray[i] = "";
    todoExpectedDueTimeArray[i] = "";
    noOfTodo--;

    $('#todoActivity').val(todoActivityArray);
    $('#todoDueDate').val(todoExpectedDueDateArray);
    $('#todoDueTime').val(todoExpectedDueTimeArray);
}

/* geo details gathering */
function getGeoPoints() {
    if (navigator.geolocation) {
        navigator.geolocation.getCurrentPosition(showPosition, showError);
    } else {
        alert("Geolocation is not supported by this browser");
    }
}
function showPosition(position) {
    var latitude = position.coords.latitude;
    var longitude = position.coords.longitude;
    $('#lattitude').val(latitude);
    $('#longitude').val(longitude);
}
function showError(error) {
    switch (error.code) {
        case error.PERMISSION_DENIED:
            alert("Location information is unavailable since the GPS is off or you have blocked location permission");
            break;
        case error.POSITION_UNAVAILABLE:
            alert("Location information is unavailable");
            break;
        case error.TIMEOUT:
            alert("The request to get user location timed out");
            break;
        case error.UNKNOWN_ERROR:
            alert("An unknown error occurred");
            break;
    }
}

/* */


function getRecieptName(element) {
    var file = element.files[0];
    var name = file.name;
    document.getElementById('recieptLabelId').innerHTML = name;
}
/*payment reciept upload */
function uploadPaymentReceiptFile(accountId) {
    var selectedClientId = 0;
    $.mobile.showPageLoadingMsg();
    var singleFileUpload = $("#receipt_upload").val();
    if (singleFileUpload.length != 0) {
        document.getElementById("recoveryFormID").method = 'POST';
        document.getElementById("recoveryFormID").action = localStorage.contextPath + "/npaloans/" + accountId + "/" + selectedClientId + "/uploadFile";
        document.getElementById("recoveryFormID").submit();
    } else {
        $("#errorField").text("Please browse a file to upload");
        $(window).scrollTop(0);
        $.mobile.hidePageLoadingMsg();
    }
}
/* */

function uploadFiles(accountId) {
    if ($('input[name="radioGrClSelect"]:checked').val() == 1) {
        $.mobile.showPageLoadingMsg();
        //Group Selected
        var selectedClientId = 0;
        var multipleFileUpload = $("#multipleUploadDocumentId").val();
        var singleFileUpload = $("#singleUploadDocumentId").val();

        if (multipleFileUpload.length != 0 | singleFileUpload.length != 0) {
            if ($('#multipleUploadDocumentId').get(0).files.length == 1) {
                $("#errorField").text("Uncheck Multiple Files to upload Single File");
                $(window).scrollTop(0);
                $.mobile.hidePageLoadingMsg();
            } else {
                document.getElementById("recoveryFormID").method = 'POST';
                document.getElementById("recoveryFormID").action = localStorage.contextPath + "/npaloans/" + accountId + "/" + selectedClientId + "/uploadFile";
                document.getElementById("recoveryFormID").submit();
            }
        } else {
            $("#errorField").text("Please browse a file to upload");
            $(window).scrollTop(0);
            $.mobile.hidePageLoadingMsg();
        }
    } else if ($('input[name="radioGrClSelect"]:checked').val() == 2) {
        //Client Selected
        if ($("#clientNamesId").val() != 0) {
            var selectedClientId = $("#clientNamesId").val();
            var multipleFileUpload = $("#multipleUploadDocumentId").val();
            var singleFileUpload = $("#singleUploadDocumentId").val();

            if (multipleFileUpload.length != 0 | singleFileUpload.length != 0) {
                if ($('#multipleUploadDocumentId').get(0).files.length == 1) {
                    $("#errorField").text("Uncheck Multiple Files to upload Single File");
                    $(window).scrollTop(0);
                } else {
                    document.getElementById("recoveryFormID").method = 'POST';
                    document.getElementById("recoveryFormID").action = localStorage.contextPath + "/npaloans/" + accountId + "/" + selectedClientId + "/uploadFile";
                    document.getElementById("recoveryFormID").submit();
                }
            } else {
                $("#errorField").text("Please browse a file to upload");
                $(window).scrollTop(0);
            }
        } else {
            $("#errorField").text("Please Select a Client Name");
            $(window).scrollTop(0);
        }
    }


}

function saveVerifedDetails(accountId, questions) {
    //Enabling capability percentage dropdown to get the value in router
    $('#percentageId').removeAttr('disabled');
    questions = JSON.parse(questions);
    var utilQuestionsCount = questions.length,
            npaQuestArray = [],
            questionId = [],
            otherReasonCount = 0,
            cabableFlag = 0,
            otherPaymentReciever = 0,
            paidAmountFlag = 0,
            checkPaidOfficerFlag = 0,
            checkPaidTypeFlag = 0,
            capableYesFlag = 0,
            capableNoFlag = 0,
            selectedReasonFlag = 0,
            paymentYesFlag = 0,
            paymentNoFlag = 0,
            selectedDateFlag = 0;

    for (var i = 0; i < utilQuestionsCount; i++) {
        var loanJson = {};
        var basicInfoJson = {};
        var npaInfoJson = {};
        var npaPaidJson = {};
        if (questions[i].applicable_for == 'LOAN_INFO') {
            var utilQuestionsYes = $('input[name="lnYesId' + i + '"]:checked').val();
            var utilQuestionsNo = $('input[name="lnNoId' + i + '"]:checked').val();
            if (typeof (utilQuestionsYes) == 'undefined') {
                if (typeof (utilQuestionsNo) == 'undefined') {
                    loanJson.questionId = questions[i].question_id;
                    loanJson.checked = -1;
                    //npaQuestArray.push(loanJson);
                } else {
                    loanJson.questionId = questions[i].question_id;
                    loanJson.checked = utilQuestionsNo;
                    npaQuestArray.push(loanJson);
                }
            } else {
                loanJson.questionId = questions[i].question_id;
                loanJson.checked = utilQuestionsYes;
                npaQuestArray.push(loanJson);
            }
        } else if (questions[i].applicable_for == 'NPA_INFO') {
            if (questions[i].input_type === 4) {
                var utilQuestionsYes = $('input[name="npaYesId' + i + '"]:checked').val();
                if (utilQuestionsYes == '1') {
                    npaInfoJson.questionId = questions[i].question_id;
                    npaInfoJson.checked = utilQuestionsYes;
                    npaQuestArray.push(npaInfoJson);
                    if (questions[i].question_id === 5) {
                        cabableFlag = 0;
                    }
                } else if(utilQuestionsYes == '0'){
                    npaInfoJson.questionId = questions[i].question_id;
                    npaInfoJson.checked = '0';
                    npaQuestArray.push(npaInfoJson);
                    if (questions[i].question_id === 5) {
                        cabableFlag++;
                    }
                }
                /* for checking NPA capable question */
                if (questions[i].element_id === "payment_cabability") {
                    if (utilQuestionsYes === "1") {
                        capableYesFlag++;
                        capableNoFlag = 0;
                    } else if (utilQuestionsYes === "0") {
                        capableNoFlag++;
                        capableYesFlag = 0;
                    }
                }
                if (questions[i].element_id === "payment_mode") {
                    if (utilQuestionsYes === "1") {
                        paymentYesFlag++;
                        paymentNoFlag = 0;
                    } else if (utilQuestionsYes === "0") {
                        paymentNoFlag++;
                        paymentYesFlag = 0;
                    }
                }
            } else if (questions[i].input_type === 6) {
                var dateValue = $('#' + questions[i].element_id).val();
                if (dateValue) {
                    $('#selectedDate').val(dateValue);
                    selectedDateFlag++;
                } else {
                    $('#selectedDate').val(dateValue);
                    selectedDateFlag = 0;
                }
            } else if (questions[i].input_type === 2) {
                var textAreaValue = $('#' + questions[i].element_id).val();
                if (typeof (textAreaValue) == 'undefined') {
                    $('#remarks').val(textAreaValue);
                } else {
                    $('#remarks').val(textAreaValue);
                }
            } else if (questions[i].input_type === 5) {
                if (questions[i].element_id == "repayment_probability") {
                    var selectedValue = $('select[name=' + questions[i].element_id + ']').val();
                    $('#capabilityPercentage').val(selectedValue);
                }
                var selectedValue = $('select[name=' + questions[i].element_id + ']').val();
                var otherReasonFlag = $("#reason_not_paid").val();
                //$('#otherReasonName').val(otherReasonFlag);
                if (otherReasonFlag) {
                    if (otherReasonFlag[0] === '1') {
                        otherReasonCount++;
                    }
                }
                if (typeof (selectedValue) == 'undefined' || selectedValue == "-1" || !selectedValue) {
                    $('#reason').val(selectedValue);
                    selectedReasonFlag = 0;
                } else {
                    $('#reason').val(selectedValue);
                    selectedReasonFlag++;
                }
            } else if (questions[i].input_type === 3) {
                var option = $("#" + questions[i].element_id).val();
                option = parseInt(option);
                var selected = $("#" + questions[i].element_id + " option:selected").text();
                if (option > -1 && option < 3) {
                    if (option === 2) {
                        paidAmountFlag++;
                    }
                    checkPaidTypeFlag++;
                    npaPaidJson.questionId = questions[i].question_id;
                    npaPaidJson.value = selected;
                    npaQuestArray.push(npaPaidJson);
                } else {
                    if (option > 2 && option < 6) {
                        checkPaidOfficerFlag++;
                    }
                    if (option === 5) {
                        otherPaymentReciever++;
                    }
                    if (option != -1) {
                        npaPaidJson.questionId = questions[i].question_id;
                        npaPaidJson.value = selected;
                        npaQuestArray.push(npaPaidJson);
                    }
                }
                
            } else {
                if (paidAmountFlag > 0) {
                    var paidAmount = $("#" + questions[i].element_id).val();
                    if (paidAmount) {
                        npaPaidJson.questionId = questions[i].question_id;
                        npaPaidJson.value = paidAmount;
                        paidAmountFlag = 0;
                        npaQuestArray.push(npaPaidJson);
                    }
                } else if (otherPaymentReciever > 0) {
                    var others = $("#" + questions[i].element_id).val();
                    if (others) {
                        npaPaidJson.questionId = questions[i].question_id;
                        npaPaidJson.value = others;
                        otherPaymentReciever = 0;
                        npaQuestArray.push(npaPaidJson);
                    }
                }
                //for other reason in text
                    var selectedReason = $('#other_reason').val();
                    $('#otherReasonName').val(selectedReason);
            }
        }else if (questions[i].applicable_for == 'BASIC_INFO') {
            var utilQuestionsYes = $('input[name="biYesId' + i + '"]:checked').val();
            var utilQuestionsNo = $('input[name="biNoId' + i + '"]:checked').val();
            if (typeof utilQuestionsYes == 'undefined') {
                if (typeof utilQuestionsNo == 'undefined') {
                    basicInfoJson.questionId = questions[i].question_id;
                    basicInfoJson.checked = -1;
                    //npaQuestArray.push(basicInfoJson);
                } else {
                    basicInfoJson.questionId = questions[i].question_id;
                    basicInfoJson.checked = utilQuestionsNo;
                    npaQuestArray.push(basicInfoJson);
                }
            } else {
                basicInfoJson.questionId = questions[i].question_id;
                basicInfoJson.checked = utilQuestionsYes;
                npaQuestArray.push(basicInfoJson);
            }
        }else if (questions[i].applicable_for == 'GEO_INFO') {
            questionId.push(questions[i].question_id);
        }
    }

    if ($('input[name="npaYesId2"]:checked').val() == 0) {
        $("#statusId").val(1);
    } else if ($('input[name="npaYesId3"]:checked').val() == 1) {
        $("#statusId").val(2);
    } else if ($('input[name="npaYesId4"]:checked').val() == 0) {
        $("#statusId").val(3);
    } else {
        $("#statusId").val(4);
    }
    $("#npaQuestions").val(JSON.stringify(npaQuestArray));

    /*Invoke function to get geo details*/
    getGeoPoints();
    $('#geoQuestionIds').val(JSON.stringify(questionId));
    
    if ($('input[name="npaYesId6"]:checked').val() == 0) {
        var noOfInstlment = ($('#no_of_installments').val()) ? parseInt($('#no_of_installments').val()) : 0;
        for (var i = 0; i < noOfInstlment; i++) {
            if (todoExpectedDueDateArray.indexOf($("#next_payment_date" + (i + 1)).val()) == -1) {
                todoActivityArray.push("Installment-" + (i + 1) + " Collection from " + $('#groupnameHidden').val());
                todoExpectedDueDateArray.push($("#next_payment_date" + (i + 1)).val());
                todoExpectedDueTimeArray.push("10");
            }
        }
    } else if ($('input[name="npaYesId6"]:checked').val() == 1) {
        if (todoExpectedDueDateArray.indexOf($("#next_payment_date").val()) == -1) {
            todoActivityArray.push("Collection from " + $('#groupnameHidden').val());
            todoExpectedDueDateArray.push($("#next_payment_date").val());
            todoExpectedDueTimeArray.push("10");
        }
    }

    // list of todos
    todoActivityArray = $.grep(todoActivityArray, function (n) {
        return(n);
    });
    todoExpectedDueDateArray = $.grep(todoExpectedDueDateArray, function (n) {
        return(n);
    });
    todoExpectedDueTimeArray = $.grep(todoExpectedDueTimeArray, function (n) {
        return(n);
    });
    $('#todoActivity').val(todoActivityArray);
    $('#todoDueDate').val(todoExpectedDueDateArray);
    $('#todoDueTime').val(todoExpectedDueTimeArray);
    $('#paid').text('');
    $('#otherReason').text('');
    $('#paidPerson').text('');
    $('#capableNo').text('');
    $('#otherReason').text('');
    $('#payementYes').text('');
    if ((($('#other_reason').val()).length === 0 && otherReasonCount > 0 && cabableFlag > 0) || paidAmountFlag > 0 || otherPaymentReciever > 0 || (!checkPaidOfficerFlag && checkPaidTypeFlag) || (!selectedReasonFlag && capableNoFlag) || (!selectedDateFlag && paymentYesFlag)) {
        if (paidAmountFlag > 0) {
            $('#paid').text('Please fill the paid amount field in NPA queries.');
            $(window).scrollTop(0);
        } else if (otherPaymentReciever > 0) {
            $('#otherReason').text('Please fill others field in NPA queries.');
            $(window).scrollTop(0);
        } else if (!checkPaidOfficerFlag && checkPaidTypeFlag) {
            $('#paidPerson').text('Please select the person that whom you paid in NPA queries.');
            $(window).scrollTop(0);
        } else if (!selectedReasonFlag && capableNoFlag) {
            $('#capableNo').text('Please select the reason for not paid in NPA queries.');
            $(window).scrollTop(0);
        } else if (!selectedDateFlag && paymentYesFlag) {
            $('#payementYes').text('Please select the next payment date in NPA queries.');
            $(window).scrollTop(0);
        } else {
            $('#otherReason').text('Please fill other reason field in NPA queries.');
            $(window).scrollTop(0);
        }

    } else {
        document.getElementById("recoveryFormID").method = 'POST';
        document.getElementById("recoveryFormID").action = localStorage.contextPath + "/npaloans/" + accountId + "/updateVerifiedInformation";
        document.getElementById("recoveryFormID").submit();
    }
}
function showClientList(accountId) {
    var data = {};
    data.accountId = accountId;
    ajaxVariable = $.ajax({
        beforeSend: function () {
            $.mobile.showPageLoadingMsg();
        },
        complete: function () {
            $.mobile.hidePageLoadingMsg()
        },
        type: 'POST',
        data: JSON.stringify(data),
        contentType: 'application/json',
        url: URIPrefix + ajaxcallip + localStorage.contextPath + '/retrieveClientDetails',
        success: function (data) {
            $('table').remove('#clientListTableId');
            var newContent = '<table id="clientListTableId">';
            $("#clientListDivId").append(newContent).trigger('create');
            var newContent = '<tr>';
            newContent += '<th width="5%">';
            newContent += "S.NO";
            newContent += '</th>';
            newContent += '<th width="15%">';
            newContent += "Client Code";
            newContent += '</th>';
            newContent += '<th width="20%">';
            newContent += "Client Name";
            newContent += '</th>';
            newContent += '<th width="30%">';
            newContent += "Address";
            newContent += '</th>';
            newContent += '<th width="25%">';
            newContent += "Overdue Amount";
            newContent += '</th>';
            newContent += '<th width="15%">';
            newContent += "Arrear Days";
            newContent += '</th>';
            newContent += '</tr>';
            $("#clientListTableId").append(newContent).trigger('create');
            for (var i = 0; i < data.customerNameArray.length; i++) {
                var newContent = '<tr class = "showhide">';
                newContent += '<td>';
                newContent += i + 1;
                newContent += '</td>';
                newContent += '<td>';
                newContent += data.customerCodeArray[i];
                newContent += '</td>';
                newContent += '<td>';
                newContent += data.customerNameArray[i];
                newContent += '</td>';
                newContent += '<td>';
                newContent += data.customerAddressArray[i];
                newContent += '</td>';
                newContent += '<td>';
                newContent += data.overdueArray[i];
                newContent += '</td>';
                newContent += '<td>';
                newContent += data.arrearDaysArray[i];
                newContent += '</td>';
                newContent += '</tr>';
                $("#clientListTableId").append(newContent).trigger('create');
            }

            clientListcurrentrow = 0;
            clientListmaxrows = $("#clientListTableId tr").length - 1;

            $("#clientListPrevId").hide();
            if (clientListmaxrows > 5) {
                $("#clientListNextId").show();
            } else {
                $("#clientListNextId").hide();
            }
            $('#clientListTableId tr.showhide').hide();
            for (var i = 0; i < 5; i++) {
                if (clientListcurrentrow < clientListmaxrows) {
                    $('#clientListTableId tr.showhide:eq(' + clientListcurrentrow + ')').show();
                    clientListcurrentrow++;
                }
            }
            clientListPageNo = 1;
            var $btn_text = $('#clientListPageNoId').find('.ui-btn-text')
            $btn_text.text("Records " + clientListPageNo + " - " + clientListcurrentrow + " of " + clientListmaxrows);

            document.getElementById("custommainTab").href = "#recentActivityPopup";
            $("#custommainTab").trigger('click');
        }, error: function (jqXHR, textStatus, error) {
            alert("textStatus" + textStatus);
        }
    });
}
//download Documents
function downloadFiles(accountId) {
    //alert(accountId);
    var selectedClientId = 0;
    var flag = 0;
    if ($('input[name="radioGrClSelect"]:checked').val() == 1) {
        //group selected
        selectedClientId = 0;
        flag = 1;
    } else if ($('input[name="radioGrClSelect"]:checked').val() == 2) {
        //client selected
        selectedClientId = $("#clientNamesId").val();
        if (selectedClientId != 0) {
            flag = 1;
        } else {
            $("#errorField").text("Please Select a Client Name");
            $(window).scrollTop(0);
        }

    }

    if (flag == 1) {
        var data = {};
        data.accountId = accountId;
        data.clientId = selectedClientId;
        ajaxVariable = $.ajax({
            beforeSend: function () {
                $.mobile.showPageLoadingMsg();
            },
            complete: function () {
                $.mobile.hidePageLoadingMsg()
            },
            type: 'POST',
            data: JSON.stringify(data),
            contentType: 'application/json',
            url: URIPrefix + ajaxcallip + localStorage.contextPath + '/retrieveUploadedDocs',
            success: function (data) {

                $("#downloadDocsId").val('0').selectmenu("refresh");
                document.getElementById('downloadDocsId').options.length = 0;
                var combo1 = document.getElementById("downloadDocsId");

                option = document.createElement("option");
                option.text = "Select";
                option.value = "0";
                try {
                    combo1.add(option, null); //Standard 
                } catch (error) {
                    combo1.add(option); // IE only
                }
                if (data.docsListArray.length != 0) {
                    $("#selectDocDivId").show();
                    for (var i = 0; i < data.docsListArray.length; i++) {
                        var combo = document.getElementById("downloadDocsId");

                        option = document.createElement("option");
                        option.text = "Doc" + (i + 1);
                        option.value = data.docsListArray[i];
                        try {
                            combo.add(option, null); //Standard 
                        } catch (error) {
                            combo.add(option); // IE only
                        }
                    }
                } else {
                    $("#errorField").text("No Documents Available.");
                    $(window).scrollTop(0);
                }

            }, error: function (jqXHR, textStatus, error) {
                alert("textStatus" + textStatus);
            }
        });
    }
}

//function to download docs
function downloadDocs(selectedDocLocation) {
    document.getElementById("selectedDocId").value = selectedDocLocation;
    document.getElementById("recoveryFormID").method = 'POST';
    document.getElementById("recoveryFormID").action = localStorage.contextPath + '/downloadDocs';
    document.getElementById("recoveryFormID").submit();
}

function updateProbability(count) {
    var selectedDate = $("#next_payment_date" + count).val().split("-");
    var oneDay = 24 * 60 * 60 * 1000; // hours*minutes*seconds*milliseconds
    if (count == 1) {
        var firstDate = new Date();
        var secondDate = new Date(selectedDate[0], selectedDate[1] - 1, selectedDate[2]);
        var diffDays = Math.round(Math.abs((firstDate.getTime() - secondDate.getTime()) / (oneDay)));
        if (secondDate < firstDate) {
            alert("Next due date should be a future date");
            $("#next_payment_date" + count).removeAttr('value');
        } else if (diffDays > 365) {
            alert("Next due date should not exceed more than a year");
            $("#next_payment_date" + count).removeAttr('value');
        } else {
            installment1++;
            calculateCapabilityPercentage(diffDays);
        }
    } else {
        var selectedPreDate = $("#next_payment_date" + (count - 1)).val().split("-");
        var date2 = new Date(selectedDate[0], selectedDate[1] - 1, selectedDate[2]);
        var date1 = new Date(selectedPreDate[0], selectedPreDate[1] - 1, selectedPreDate[2]);
        var diff = Math.round(Math.abs((date1.getTime() - date2.getTime()) / (oneDay)));
        if (+date2 == +date1) {
            alert("Installment date should not be the same");
            $("#next_payment_date" + count).removeAttr('value');
            installment2;
        } else if (date2 < date1) {
            alert("Next due date should be greater than previous due");
            $("#next_payment_date" + count).removeAttr('value');
            installment2;
        } else if (diff > 365) {
            alert("Next due date should not exceed more than a year");
            $("#next_payment_date" + count).removeAttr('value');
            installment2;
        }
    }
}