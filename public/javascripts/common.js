$(document).ready(function () {
    var mobCheckBox = $("input[id=mobileNumber]");
    var landCheckBox = $("input[id=landlineNumber]");

    mobCheckBox.on("change", function (e) {
        if (e.target.checked) {
            $('#mobileNumberDiv').show();
            $('#message').text('');
            $('#newMobileNumber').focus();
        }
        else {
            $('#mobileNumberDiv').hide();
            $('#errorKYCMessage').text('');
        }
    });

    landCheckBox.on("change", function (e) {
        if (e.target.checked) {
            $('#landLineNumberDiv').show();
            $('#message').text('');
            $('#newLandlineNumber').focus();
        }
        else {
            $('#landLineNumberDiv').hide();
            $('#errorKYCMessage').text('');
        }
    });
});

function forNumbers(currentVal) {
    var regex = /[^0-9]+/g;
    if (regex.test(currentVal.value)) {
        currentVal.value = (currentVal.value.replace(regex, '')).trim();
    }
}

function forPhoneNumbers(currentVal,length,next) {
    var regex = /[^0-9-]+/g;
    if (regex.test(currentVal.value)) {
        currentVal.value = (currentVal.value.replace(regex, '')).trim();
    }
    if(length == currentVal.value.length && next){
        $('#'+next).focus();
    }
}
function forNumbersWithDot(currentVal) {
    var regex = /[^0-9.]+/g;
    if (regex.test(currentVal.value)) {
        currentVal.value = (currentVal.value.replace(regex, '')).trim();
    }
}

function forNamesValidation(currentVal) {
    var regex = /[^a-zA-Z\s]/g;
    if (regex.test(currentVal.value)) {
        currentVal.value = (currentVal.value.replace(regex, '')).trim();
    }
}
function forNamesValidationWithNumbers(currentVal) {
    var regex = /[^a-zA-Z0-9\s]/g;
    if (regex.test(currentVal.value)) {
        currentVal.value = (currentVal.value.replace(regex, '')).trim();
    }
}
function remarksWithoutSpecialCharacters(currentVal) {
    //var regex = /^(^ )|( $)[a-zA-Z0-9_\s-]/g;
    var regex = /[^a-zA-Z0-9\s]/g;
    if(regex.test(currentVal.value)){
        currentVal.value = (currentVal.value.replace(regex, '')).trim();
    }
}
function forAlphaNumeric(currentVal,length,next) {
    var regex = /[^a-zA-Z0-9/]/g;
    if (regex.test(currentVal.value)) {
        currentVal.value = (currentVal.value.replace(regex, '')).trim();
    }
    if(length == currentVal.value.length && next){
        $('#'+next).focus();
    }
}

function forAlphaNumericWoutSlash(currentVal) {
    var regex = /[^a-zA-Z0-9]/g;
    if (regex.test(currentVal.value)) {
        currentVal.value = (currentVal.value.replace(regex, '')).trim();
    }
}

function forAlphabetsWithSlash(currentVal) {
    var regex = /[^a-zA-Z/]/g;
    if (regex.test(currentVal.value)) {
        currentVal.value = (currentVal.value.replace(regex, '')).trim();
    }
}

function forRemoveSpecialCharcters(currentVal) {
    var regex = /[^a-zA-Z0-9.[],()#[#:&#@\s]/g;
    if (regex.test(currentVal.value)) {
        currentVal.value = (currentVal.value.replace(regex, '')).trim();
    }
}

function forAddress(currentVal) {
    var regex = /[^a-zA-Z0-9/.,\-()#[#:&#@]/g;
    if (regex.test(currentVal.value)) {
        currentVal.value = currentVal.value//.replace(regex, '')).trim();
    }
}

function forEmailId(currentVal) {
    var regex = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    var regex1 = /[,'"/\s]/g;
    if (regex.test(currentVal)) {
        currentVal.value = (currentVal.value.replace(regex, '')).trim();
    } else if (regex1.test(currentVal.value)) {
        currentVal.value = (currentVal.value.replace(regex1, '')).trim();
    } else {
        currentVal.value = (currentVal.value).trim();
    }
}

function forUserName(currentVal) {
    var regex = /[^a-zA-Z0-9@]/g;
    if (regex.test(currentVal.value)) {
        currentVal.value = (currentVal.value.replace(regex, '')).trim();
    }
}
function forNumbersWithSlash(currentVal) {
    var regex = /[^0-9]/g;
    if (regex.test(currentVal.value)) {
        currentVal.value = (currentVal.value.replace(regex, '')).trim();
    }
}

function updateContactDetails(clientsId,buttonId,clientName,mobileNumber,landLineNumber){
    $(window).scrollTop(0);
    $('#errorKYCMessage').text('');
    $('#message').text('');
    $('#kycUpdateClientMobileNumber').val(mobileNumber);
    $('#kycUpdateClientLandLineNumber').val(landLineNumber);
    if(mobileNumber != "")
        $('#mobNumber').text(mobileNumber);
    else
        $('#mobNumber').text("Not Available");

    if(landLineNumber != "")
        $('#landNumber').text(landLineNumber);
    else
        $('#landNumber').text("Not Available");

    $('#kycUpdateClientId').val(clientsId);
    $('#buttonId').val(buttonId);
    $("#updateContactDetails").popup('open');
}

function closePopup(){
    $('#message').text('');
    $('#message').css('color','black');
    $('#mobileNumberDiv').hide();
    $('#landLineNumberDiv').hide();
    $('#mobileNumber').removeAttr("checked").checkboxradio('refresh');
    $('#landlineNumber').removeAttr("checked").checkboxradio('refresh');
    $('#kycUpdateClientId').val('');
    $('#kycUpdateClientMobileNumber').val('');
    $('#newMobileNumber').val('');
    $('#kycUpdateClientLandLineNumber').val('');
    $('#newLandlineNumber').val('');
    $('#errorKYCMessage').text('');
    $('#remarks_popup').val('');
    $('#mobNumber').text('');
    $('#landNumber').text('');
    $("#updateContactDetails").popup('close');
}

function submitUpdateKYC(){
    var count = $('#buttonId').val();
    if($("#mobileNumber").is(":checked") && $('#newMobileNumber').val().trim() == ""){
        $('#message').css('color','black');
        $('#errorKYCMessage').text('Please enter the mobile number');
        $('#newMobileNumber').focus();
    }
    else if($("#landlineNumber").is(":checked") && $('#newLandlineNumber').val().trim() == ""){
        $('#message').css('color','black');
        $('#errorKYCMessage').text('Please enter the landline number');
        $('#newLandlineNumber').focus();
    }else if(!$("#mobileNumber").is(":checked") && !$("#landlineNumber").is(":checked")){
        $('#message').css('color','red');
        $('#message').text('Please select the field/fields which you want change');
    }
    else if($('#mobileNumber').is(':checked') && $("#newMobileNumber").val().length < 10){
        $('#errorKYCMessage').text('Please provide a 10 digit mobile number');
    } else if($('#remarks_popup').val().trim() == ""){
        $('#errorKYCMessage').text('Please enter the remarks');
        $("#remarks_popup").focus();
    }
    else{
        $('#message').css('color','black');
        $('#errorMessage').text('');

        var data = {
            clientId : $('#kycUpdateClientId').val(),
            oldMobileNumber : $('#kycUpdateClientMobileNumber').val(),
            oldLandLineNumber : $('#kycUpdateClientLandLineNumber').val(),
            newMobileNumber : $('#newMobileNumber').val(),
            newLandLineNumber : $('#newLandlineNumber').val(),
            remarks : $('#remarks_popup').val(),
            operationId : $('#operationId').val()
        };

        $.post(URIPrefix + ajaxcallip + localStorage.contextPath+'/updateKYCContactDetails',
            data, function (response) {
                if(response.status == 'success') {
                    $('#message').css('color','black');
                    $('#mobileNumberDiv').hide();
                    $('#landLineNumberDiv').hide();
                    $('#mobileNumber').removeAttr("checked").checkboxradio('refresh');
                    $('#landlineNumber').removeAttr("checked").checkboxradio('refresh');
                    $('#kycUpdateClientId').val('');
                    $('#kycUpdateClientMobileNumber').val('');
                    $('#newMobileNumber').val('');
                    $('#kycUpdateClientLandLineNumber').val('');
                    $('#newLandlineNumber').val('');
                    $('#errorKYCMessage').text('');
                    $('#remarks_popup').val('');
                    $('#mobNumber').text('');
                    $('#landNumber').text('');
                    $('#updateContact_' + count).remove();
                    $('#labelId_' + count).text('Update request sent');
                    $("#updateContactDetails").popup('close');
                }
                else{
                    $("#updateContactDetails").popup('close');
                    $('#errorLabelId').text('Your request could not processed. Please try later...')
                }
            }
        );
    }
}

function findWithAttr(array, attr, value) {
    for(var i = 0; i < array.length; i += 1) {
        if(array[i][attr] === value) {
            return i;
        }
    }
}

function showPagination() {
    if($('#isSummary').val()){
        var pageSize = totalRecordsPerPage;
        var totalRecords = $('#totalRecords').val();
        var offset = parseInt($('#offset').val());
        var num_pages = 0;
        if (totalRecords % pageSize == 0) {
            num_pages = totalRecords / pageSize;
        }
        if (totalRecords % pageSize >= 1) {
            num_pages = totalRecords / pageSize;
            num_pages++;
            num_pages = Math.floor(num_pages++);
        }
        if (totalRecords > pageSize) {
            if(offset == 0) {
                $('#list-pagination').append("<label id='previous-icon' style='padding: 3px;cursor: not-allowed;padding-left:300px;'><img src='/images/previous.png' height='13' width='13'/></label>");
            }else {
                $('#list-pagination').append("<label style='padding-left:300px;'><a href='JavaScript:paginate(\"previous\")' id='previous-icon'><img src='/images/previous.png' height='13' width='13'/></a></label>");
            }
            for (var i = 1; i <= num_pages; i++) {
                $('#list-pagination').append("<a id=page" + i + " href='JavaScript:paginate(" + i + ")'>" + i + "</a>");
            }
            if(offset+totalRecordsPerPage > totalRecords) {
                $('#list-pagination').append("<label id='next-icon' style='padding: 3px;cursor: not-allowed;'><img src='/images/next.png' height='13' width='13'/></label>");
            }else {
                $('#list-pagination').append("<a href='JavaScript:paginate(\"next\")' id='next-icon'><img src='/images/next.png' height='13' width='13'/></a>");
            }
        }
    }else{
        var req_num_row = ($('#cgtCount').val() > 0) ? 5 : 9;
        var $tr = $('#pagination-table tbody tr');
        var total_num_row = $tr.length;
        var num_pages = 0;
        var page = 1;
        if (total_num_row % req_num_row == 0) {
            num_pages = total_num_row / req_num_row;
        }
        if (total_num_row % req_num_row >= 1) {
            num_pages = total_num_row / req_num_row;
            num_pages++;
            num_pages = Math.floor(num_pages++);
        }
        if (total_num_row > req_num_row) {
            $('#pagination').append("<a href='#' style='padding-left:210px;' title='previous'><img src='/images/previous.png' height='13' width='13'/></a>");
            for (var i = 1; i <= num_pages; i++) {
                $('#pagination').append("<a id=page"+i+" href='#'>" + i + "</a>");
            }
            $('#pagination').append("<a href='#' title='next'><img src='/images/next.png' height='13' width='13'/></a>");
            $tr.each(function (i) {
                $(this).hide();
                if (i + 1 <= req_num_row) {
                    $tr.eq(i).show();
                }
            });
        }
        $('#pagination a').click(function (e) {
            e.preventDefault();
            page = ($(this).text() != "") ? $(this).text() : $(this).attr('title');
            var selectedPage = parseInt($('#selectedPage').val());
            page = (page == 'next') ? (selectedPage+1) : (page == 'previous') ? (selectedPage-1) : page;
            if(page <= num_pages ){
                $tr.hide();
                var temp = (page != 0) ? page - 1 : page;
                var start = temp * req_num_row;

                for (var i = 0; i < req_num_row; i++) {
                    $tr.eq(start + i).show();
                }
                if(page == 0){
                    page = page+1;
                }
                $('#selectedPage').val(page);
                $('#page'+selectedPage).removeClass('active-page');
                $('#page'+$('#selectedPage').val()).addClass('active-page');
            }
        });
    }
}

function validateEmail(email){
    var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(email);
}

function checkExists(valueArray, key, value) {
    for (var i = 0; valueArray.length > i; i ++) {
        if (valueArray[i][key] == value) {
            return i;
        }
    }
    return -1;
}