var clientListcurrentrow = 0;
var clientListmaxrows = 0;
var clientListPageNo = 1;
$(document).ready(function () {
    showPagination();
    $('#page' + $('#selectedPage').val()).addClass('active-page');

    $("#assignRODivId").show();
    $("#closedGroupsDivId").hide();
    $("#malpracticeGroupsDivId").hide();
    $("#assignRONavBarId").click(function () {
        $("#assignRODivId").show();
        $("#closedGroupsDivId").hide();
        $("#malpracticeGroupsDivId").hide();
    });
    $("#closedGroupsNavBarId").click(function () {
        $("#assignRODivId").hide();
        $("#closedGroupsDivId").show();
        $("#malpracticeGroupsDivId").hide();
    });
    $("#malGroupsNavBarId").click(function () {
        $("#assignRODivId").hide();
        $("#closedGroupsDivId").hide();
        $("#malpracticeGroupsDivId").show();
    });

    /* client pagination for previous button */
    $("#clientListPrevId").click(function () {
        if (clientListcurrentrow == clientListmaxrows) {
            $("#clientListNextId").show();
        }

        var hidenextrow = clientListcurrentrow;
        if (clientListmaxrows == clientListcurrentrow) {
            var x = clientListmaxrows % 5;
            if (x > 0) {
                hidenextrow = hidenextrow + (5 - x);
                clientListcurrentrow = clientListcurrentrow + (5 - x);
            }
        }
        for (var i = 0; i < 5; i++) {
            hidenextrow--;
            $('#clientDetailsTableId tr.showhide:eq(' + hidenextrow + ')').hide();
            if (clientListcurrentrow > 0) {
                clientListcurrentrow--;
                $('#clientDetailsTableId tr.showhide:eq(' + (clientListcurrentrow - 5) + ')').show();
            }
        }
        if (clientListcurrentrow == 5) {
            $("#clientListPrevId").hide();
        }
        clientListPageNo = clientListPageNo - 5;
        var $btn_text = $('#clientListPageNoId').find('.ui-btn-text')
        $btn_text.text("Records " + clientListPageNo + " - " + clientListcurrentrow + " of " + clientListmaxrows);
    });

    /* client pagination for next button */
    $("#clientListNextId").click(function () {
        if (clientListcurrentrow == 5) {
            $("#clientListPrevId").show();
        }
        var hidepreviousrow = clientListcurrentrow;
        for (var i = 0; i < 5; i++) {
            hidepreviousrow--;
            $('#clientDetailsTableId tr.showhide:eq(' + hidepreviousrow + ')').hide();
            if (clientListcurrentrow < clientListmaxrows) {
                $('#clientDetailsTableId tr.showhide:eq(' + clientListcurrentrow + ')').show();
                clientListcurrentrow++;
            }
        }
        if (clientListcurrentrow == clientListmaxrows) {
            $("#clientListNextId").hide();
        }
        clientListPageNo = clientListPageNo + 5;
        var $btn_text = $('#clientListPageNoId').find('.ui-btn-text')
        $btn_text.text("Records " + clientListPageNo + " - " + clientListcurrentrow + " of " + clientListmaxrows);
    });

    for (var i = 0; i < $("#noOfGroupId").val(); i++) {
        $("#groupListId" + i).click(function () {
            var currentIndex = this.id.substr(this.id.length - 1);
            var statusId = $("#statusIdId").val().split(",");
            var reason = $("#reasonForNotPaidId").val().split(",");
            var remark = $("#remarksId").val().split(",");
            var capabilityPercentage = $("#capablilityPercentageId").val().split(",");
            var expectedPaymentDate = $("#expectedPaymentDateId").val().split(",");
            $('div').remove('#reasonPopupDivId');
            if (statusId[currentIndex] == 3) {
                var newContent = '<div data-role="content" id="reasonPopupDivId">';
                $("#parentPopupDivId").append(newContent).trigger('create');
                var newContent = '<div data-role="horizontal" class="ui-bar ui-grid-a">';
                newContent += '<div class="ui-block-a">';
                newContent += "<label for='groupname'><b> Reason for not Paid </b></label>";
                newContent += '</div>';
                newContent += '<div class="ui-block-b">';
                newContent += "<label id='reasonForNotPaidlabelId'> : " + reason[currentIndex] + "</label>";
                newContent += '</div>';
                newContent += '</div>';
                newContent += '<div data-role="horizontal" class="ui-bar ui-grid-a">';
                newContent += '<div class="ui-block-a">';
                newContent += "<label for='groupname'><b> Remark </b></label>";
                newContent += '</div>';
                newContent += '<div class="ui-block-b">';
                newContent += '<label id="remarkLabelId"> : ' + remark[currentIndex] + '</label>';
                newContent += '</div>';
                newContent += '</div>';
                $("#reasonPopupDivId").append(newContent).trigger('create');
            } else if (statusId[currentIndex] == 4) {
                var newContent = '<div data-role="content" id="reasonPopupDivId">';
                $("#parentPopupDivId").append(newContent).trigger('create');
                var newContent = '<div data-role="horizontal" class="ui-bar ui-grid-a">';
                newContent += '<div class="ui-block-a">';
                newContent += "<label for='reason'> <b> Reason for not Paid </b></label>";
                newContent += '</div>';
                newContent += '<div class="ui-block-b">';
                newContent += "<label id='reasonForNotPaidlabelId'> : " + reason[currentIndex] + "</label>";
                newContent += '</div>';
                newContent += '</div>';
                newContent += '<div data-role="horizontal" class="ui-bar ui-grid-a">';
                newContent += '<div class="ui-block-a">';
                newContent += "<label for='capabilitypercentage '> <b> Capability Percentage </b></label>";
                newContent += '</div>';
                newContent += '<div class="ui-block-b">';
                newContent += '<label id="capabilitypercentageLabelId"> : ' + capabilityPercentage[currentIndex] + '</label>';
                newContent += '</div>';
                newContent += '</div>';
                newContent += '<div data-role="horizontal" class="ui-bar ui-grid-a">';
                newContent += '<div class="ui-block-a">';
                newContent += "<label for='expectedDate'><b> Expected Payment Date </b></label>";
                newContent += '</div>';
                newContent += '<div class="ui-block-b">';
                newContent += '<label id="expectedDateLabelId"> : ' + expectedPaymentDate[currentIndex] + ' </label>';
                newContent += '</div>';
                newContent += '</div>';
                $("#reasonPopupDivId").append(newContent).trigger('create');
            }
        });
    }
    //set branch and ro value if selected during filter
    if (localStorage.branch || localStorage.RO) {
        $('#branches').val(localStorage.branch);
        $("#branches").selectmenu('refresh');
        $('#selectROId').val(localStorage.RO);
        $("#selectROId").selectmenu('refresh');
        localStorage.removeItem('branch');
        localStorage.removeItem('RO');
    }

});

//Global variable
var Ro_office_id;

function assignFOSubmission() {
    $("#accountIdÏd").val(selectedAccountIds);
    $("#customerIdÏd").val(selectedcustomerIds);
    $("#groupNameÏd").val(selectedgroupNames);
    $("#officerNameÏd").val($(selectROId).find('option:selected').text());
    if ($("#selectROId").val() <= 0) {
        $("#errorField").text("Please select RO");
        $("#successField").text("");
    } else if (selectedAccountIds.length === 0) {
        $("#errorField").text(" Please select atleast one group to assign");
        $("#successField").text("");
    } else {
        $.mobile.showPageLoadingMsg();
        document.getElementById("assignROFormId").method = 'POST';
        document.getElementById("assignROFormId").action = localStorage.contextPath + "/NPALRGroups/assignRO";
        document.getElementById("assignROFormId").submit().refresh();
    }
}

function paginate(count) {
    $.mobile.showPageLoadingMsg();
    var totalRecords = parseInt($('#totalRecords').val());
    var selectedPage = parseInt($('#selectedPage').val());
    count = (count == 'next') ? (selectedPage * totalRecordsPerPage < totalRecords) ? selectedPage + 1 : selectedPage : (count == 'previous') ? (selectedPage > 1) ? selectedPage - 1 : selectedPage : count;
    $('#selectedPage').val(count);
    document.getElementById("assignROFormId").method = 'POST';
    document.getElementById("assignROFormId").action = localStorage.contextPath + "/NPALRGroups/assignROLoad";
    document.getElementById("assignROFormId").submit();
}

function selectedAccounts(e, accountId, customerId, groupName, office_id) {

    if ($('#' + $("#selectROId").val()).attr('title') === office_id) {
        $('#errorField').text("");
        var idx = selectedAccountIds.indexOf(accountId);
        if (idx > -1) {
            selectedAccountIds.splice(idx, 1);
        } else {
            selectedAccountIds.push(accountId);
        }
        var idx = selectedcustomerIds.indexOf(customerId);
        if (idx > -1) {
            selectedcustomerIds.splice(idx, 1);
        } else {
            selectedcustomerIds.push(customerId);
        }
        var idx = selectedgroupNames.indexOf(groupName);
        if (idx > -1) {
            selectedgroupNames.splice(idx, 1);
        } else {
            selectedgroupNames.push(groupName);
        }
    } else if ($("#selectROId").val() <= 0) {
        e.preventDefault();
        $('#errorField').text("Select RO");
    } else {
        $('#errorField').text("Select RO from their appropriate group branch");
        e.preventDefault();
    }
}

function loadGroups() {
    $.mobile.showPageLoadingMsg();
    document.getElementById("assignROFormId").method = 'POST';
    document.getElementById("assignROFormId").action = localStorage.contextPath + "/NPALRGroups/assignROLoad";
    document.getElementById("assignROFormId").submit();
}

function onchangeSearchCriteriaBranchAssign() {
    $.mobile.showPageLoadingMsg();
    var branch_id = $("#branches").val();
    localStorage.RO = $("#selectROId").val();
    localStorage.branch = branch_id;
    document.getElementById("assignROFormId").method = 'GET';
    document.getElementById("assignROFormId").action = localStorage.contextPath + "/NPALRGroups/assignROLoad?branchId=" + branch_id;
    document.getElementById("assignROFormId").submit();
}

function roFilter() {
    var ro = $("#selectROId").val();
    var branch_id = $("#branches").val();
    if (parseInt(ro, 10)) {
        $.mobile.showPageLoadingMsg();
        $('#errorField').text('');
        localStorage.RO = ro;
        localStorage.branch = branch_id;
        document.getElementById("assignROFormId").method = 'GET';
        if (branch_id) {
            document.getElementById("assignROFormId").action = localStorage.contextPath + '/NPALRGroups/assignROLoad?RO=' + ro + '&branchId=' + branch_id;
        } else {
            document.getElementById("assignROFormId").action = localStorage.contextPath + '/NPALRGroups/assignROLoad?RO=' + ro;
        }
        document.getElementById("assignROFormId").submit();
    } else {
        $.mobile.hidePageLoadingMsg();
        $('#errorField').text('Select RO to filter');
        $("#successField").text("");
    }

}

/* client details */
function showClientDetails(accountId) {
    var data = {};
    data.accountId = accountId;
    ajaxVariable = $.ajax({
        beforeSend: function () {
            $.mobile.showPageLoadingMsg();
        },
        complete: function () {
            $.mobile.hidePageLoadingMsg()
        },
        type: 'POST',
        data: JSON.stringify(data),
        contentType: 'application/json',
        url: URIPrefix + ajaxcallip + localStorage.contextPath + '/retrieveClientDetails',
        success: function (data) {
            console.log(data);
            $('table').remove('#clientDetailsTableId');
            var newContent = '<table id="clientDetailsTableId" class="custom-table">';
            $("#clientListDivId").append(newContent).trigger('create');
            var newContent = '<tr>';
            newContent += '<th width="10%">';
            newContent += "S.NO";
            newContent += '</th>';
            newContent += '<th width="25%">';
            newContent += 'Client Code';
            newContent += '</th>';
            newContent += '<th width="25%">';
            newContent += "First Name";
            newContent += '</th>';
            newContent += '<th width="10%">';
            newContent += "Last Name";
            newContent += '</th>';
            newContent += '<th width="20%">';
            newContent += 'Overdue Amount';
            newContent += '</th>';
            newContent += '<th width="10%">';
            newContent += 'Arrear Days';
            newContent += '</th>';
            newContent += '</tr>';
            $("#clientDetailsTableId").append(newContent).trigger('create');
            for (var i = 0; i < data.customerNameArray.length; i++) {
                var newContent = '<tr class = "showhide">';
                newContent += '<td align="right">';
                newContent += i + 1;
                newContent += '</td>';
                newContent += '<td>';
                newContent += data.customerCodeArray[i];
                newContent += '</td>';
                newContent += '<td>';
                newContent += data.customerFirstNameArray[i];
                newContent += '</td>';
                newContent += '<td>';
                newContent += data.customerLastNameArray[i];
                newContent += '</td>';
                newContent += '<td align="right">';
                newContent += data.overdueArray[i];
                newContent += '</td>';
                newContent += '<td align="right">';
                newContent += data.arrearDaysArray[i];
                newContent += '</td>';
                newContent += '</tr>';
                $("#clientDetailsTableId").append(newContent).trigger('create');
            }

            clientListcurrentrow = 0;
            clientListmaxrows = $("#clientDetailsTableId tr").length - 1;

            $("#clientListPrevId").hide();
            if (clientListmaxrows > 5) {
                $("#clientListNextId").show();
            } else {
                $("#clientListNextId").hide();
            }
            $('#clientDetailsTableId tr.showhide').hide();
            for (var i = 0; i < 5; i++) {
                if (clientListcurrentrow < clientListmaxrows) {
                    $('#clientDetailsTableId tr.showhide:eq(' + clientListcurrentrow + ')').show();
                    clientListcurrentrow++;
                }
            }
            clientListPageNo = 1;
            var $btn_text = $('#clientListPageNoId').find('.ui-btn-text')
            $btn_text.text("Records " + clientListPageNo + " - " + clientListcurrentrow + " of " + clientListmaxrows);

            document.getElementById("clientDetailsId").href = "#clientDetailsPopup";
            $("#clientDetailsId").trigger('click');
        }, error: function (jqXHR, textStatus, error) {
            alert("textStatus" + textStatus);
        }
    });
}