window.history.forward();
//Jagan
function operationSubmit(operationId) {
    $.mobile.showPageLoadingMsg();
	document.getElementById("BMFormId").method='POST';
	document.getElementById("BMFormId").action=localStorage.contextPath+"/client/ci/groups/operation/"+operationId;
	document.getElementById("BMFormId").submit();
}
function operationSubmitFromBM(operationId) {
    $.mobile.showPageLoadingMsg();
	document.getElementById("BMFormId").method='POST';
	document.getElementById("BMFormId").action=localStorage.contextPath+"/client/ci/groups/operation/"+operationId;
	document.getElementById("BMFormId").submit();
}
function operationSubmitFromAdmin(operationId) {
    $.mobile.showPageLoadingMsg();
    document.getElementById("AdminFormId").method='POST';
    document.getElementById("AdminFormId").action=localStorage.contextPath+"/client/ci/admin/operation/"+operationId;
    document.getElementById("AdminFormId").submit();
}

function showPreliminaryVerificationForm(grp){
    $.mobile.showPageLoadingMsg();
	var url=localStorage.contextPath+"/client/ci/groups/"+grp+"/preVerification";
	document.getElementById("BMFormId").method='POST';
	document.getElementById("BMFormId").action=url;
	document.getElementById("BMFormId").submit();
}

function editGroupFormSubmission(groupIdLabelValue) {
    $.mobile.showPageLoadingMsg();
	document.getElementById("groupId").value=groupIdLabelValue;
	var url=localStorage.contextPath+"/client/ci/groups/"+groupIdLabelValue;
	var form="BMFormId";
	document.getElementById(form).method='POST';
	document.getElementById(form).action=url;
	document.getElementById(form).submit();
}

function getClientListForLoanSanctionForms(groupid,isSynchronized,mifosCustomerId) {
    $.mobile.showPageLoadingMsg();
	document.getElementById("BMFormId").method='POST';
	document.getElementById("BMFormId").action=localStorage.contextPath+'/client/ci/groups/member/synchronizedpage/'+groupid+'/'+isSynchronized+'/'+mifosCustomerId+'/upload';
	document.getElementById("BMFormId").submit();
}

function branchGroupsDisplaySynchronized(selectThis) {
	var officeId = selectThis.value;
    $('#docLanguage').val(($('#branchName option:selected').attr('rel')));
	if(officeId!=0) {
        $.mobile.showPageLoadingMsg();
		document.getElementById("BMFormId").method='POST';
		document.getElementById("BMFormId").action=localStorage.contextPath+"/client/ci/groups/synchronizedpageonchange/"+synchronizedOperationId+"/"+officeId;
		document.getElementById("BMFormId").submit().refresh();
	}
}

function getIdleGroupInformation (groupId,statusId) {
	$.mobile.showPageLoadingMsg();
	$('#statusId').val(statusId);
	document.getElementById("BMFormId").method='POST';
	document.getElementById("BMFormId").action=localStorage.contextPath+"/client/ci/idleGroups/"+groupId+"/listClients";
	document.getElementById("BMFormId").submit();
}

function showKYCFormsForRMReview(clientId){
	document.getElementById("BMFormId").method = 'POST';
	document.getElementById("BMFormId").action = localStorage.contextPath + "/client/ci/kycDocuments/" + clientId;
	document.getElementById("BMFormId").submit();
}

function activeGroups(groupId){
	$("#groupId").val(groupId);
	$.mobile.showPageLoadingMsg();
	document.getElementById("BMFormId").method = 'POST';
	document.getElementById("BMFormId").action = localStorage.contextPath + "/client/ci/ActiveGroups";
	document.getElementById("BMFormId").submit();
}

function rejectGroup(groupId,statusId){
	$("#groupId").val(groupId);
	$("#statusId").val(statusId);
	var remarks=document.getElementById("remarks").value.trim();
	if($('#remarks').val() != "" && remarks != ''){
                $("#errorField").hide();
		$.mobile.showPageLoadingMsg();
		document.getElementById("BMFormId").method = 'POST';
		document.getElementById("BMFormId").action = localStorage.contextPath + "/client/ci/rejectActiveGroup";
		document.getElementById("BMFormId").submit();
	} else {
                $("#errorField").show();
		$("#errorField").text("Remarks To Reject Is Mandatory");
		$(window).scrollTop(0);
	}
}

function clientuploadNOC(clientId){
	$("#clientId").val(clientId);
	$.mobile.showPageLoadingMsg();
	document.getElementById("BMFormId").method = 'POST';
	document.getElementById("BMFormId").action = localStorage.contextPath + "/client/ci/toUploadNOC";
	document.getElementById("BMFormId").submit();
}
