module.exports = reportManagement;
var path = require('path');
var rootPath = path.dirname(process.mainModule.filename);
var props = require(path.join(rootPath, "properties.json"));
var ReportManagementModel = require(path.join(rootPath, "app_modules/model/ReportManagementModel"));
var commonModel = require(path.join(rootPath,"app_modules/model/CommonModel"));
var customlog = require(path.join(rootPath, "logger/loggerConfig.js"))('ReportManagementRouter.js');

var AWS = require('aws-sdk');
iklantPort = props.iklantPort;

function reportManagement(constants) {
    customlog.debug("Inside Router");
    this.model = new ReportManagementModel(constants);
    this.constants = constants;
}

reportManagement.prototype = {

    showMaintenancePage: function (reportName, reportCategory, res) {
        res.render("maintenancePage", {reportName: reportName, reportCategory: reportCategory, contextPath: props.contextPath});
    },

    retrieveClientDetailsForVerificationPage: function (parent_customer_id, callback) {
        this.model.retrieveClientDetailsForVerificationPageModel(parent_customer_id, callback);
    },

    retrieveClientDetailsForVerification: function (req, res) {
        try {
            var self = this;
            var parent_customer_id = req.body.parent_customer_id;
            self.retrieveClientDetailsForVerificationPage(parent_customer_id, function (clientResult) {
                req.body.clientResult = clientResult;
                res.send(req.body);
            });
        } catch (e) {
            customlog.error("Exception while retrieveClientDetailsForVerification " + e);
            self.commonRouter.showErrorPage(req, res);
        }
    },
    loanOfficers: function (req, res) {
        try {
            customlog.info('Inside get loan officers ');
            var self = this;
            var constantsObj = this.constants;
            var office_id = req.body.listoffice;
            var officeId = req.session.officeId;
            var roleId = req.session.roleId;
            var personnelArray = new Array();
            this.commonRouter.getPersonnelDetailsCall(office_id, req.session.userId, function (personnelIdArray, personnelNameArray) {
                req.body.personnelIdArray = personnelIdArray;
                req.body.personnelNameArray = personnelNameArray
                res.send(req.body);
            });
        } catch (e) {
            customlog.error("Exception while loanOfficers " + e);
            self.commonRouter.showErrorPage(req, res);
        }
    },

    listNPASearchGroupCall: function (tenantId, recoveryOfficer, capabilityPercentage, isLeaderTraceableID, reasonForNPA, overdueDurationFrom,
            overdueDurationTo, amountFrom, amountTo, branch, callback) {
        this.model.listNPASearchGroupModel(tenantId, recoveryOfficer, capabilityPercentage, isLeaderTraceableID, reasonForNPA, overdueDurationFrom,
                overdueDurationTo, amountFrom, amountTo, branch, callback);
    },

    getNPAReasonsCall: function (tenantId, userId, callback) {
        this.model.getNPAReasonsModel(tenantId, userId, callback);
    },

    npaDefaultSearchCall: function (userId, callback) {
        this.model.npaDefaultSearchModel(userId, callback);
    },

    getOfficeListAjaxcall: function (req, res) {
        var self = this;
        var tenantId = req.session.tenantId;
        var branchId = (typeof req.body.branch == 'undefined') ? req.session.officeId : req.body.branch;
        try {
            self.commonRouter.getPersonnelDetailsCall(branchId, req.session.userId, function (FOIdsArray, FONamesArray) {
                req.body.FOIds = FOIdsArray;
                req.body.FONames = FONamesArray;
                res.send(req.body);
            });
        } catch (e) {
            customlog.error("Exception while Retriving office Details " + e);
            self.commonRouter.showErrorPage(req, res);
        }
    },

    searchNPA: function (req, res) {
        try {
            var self = this;
            var tenantId = req.session.tenantId;
            var userId = req.session.userId;
            if (typeof tenantId == 'undefined' || typeof userId == 'undefined') {
                res.redirect(props.contextPath + '/client/ci/login');
            } else {
                self.commonRouter.retrieveOfficeDetails(function (officeIdArray, officeNameArray, officeAddressArray, officeShortNameArray) {
                    self.getNPAReasonsCall(tenantId, userId, function (npaReasonIdArray, npaReasonArray, recoveryOfficerId, recoveryOfficerName) {
                        self.npaDefaultSearchCall(req.session.userId, function (accountIdArray, customerNameArray, overDueAmountArray, recoveryOfficerNameArray, daysInArrearsArray, expectedCompletionDateArray, groupName) {
                            res.render('SearchNPALoans', {npaReasonIdArray: npaReasonIdArray, npaReasonArray: npaReasonArray,
                                accountIdArray: accountIdArray, customerNameArray: customerNameArray,
                                overDueAmountArray: overDueAmountArray, recoveryOfficerNameArray: recoveryOfficerNameArray, groupName: groupName,
                                daysInArrearsArray: daysInArrearsArray, recoveryOfficerId: recoveryOfficerId,
                                recoveryOfficerName: recoveryOfficerName, officeIdArray: officeIdArray,
                                officeNameArray: officeNameArray, expectedCompletionDateArray: expectedCompletionDateArray, contextPath: props.contextPath});
                        });
                    });
                });
            }
        } catch (e) {
            customlog.error("Exception while search NPA " + e);
            self.commonRouter.showErrorPage(req, res);
        }
    },

    listSearchedNPA: function (req, res) {
        try {
            var self = this;
            var tenantId = req.session.tenantId;
            var recoveryOfficer = req.body.recoveryOfficer;
            var capabilityPercentage = req.body.capabilityPercentage;
            var isLeaderTraceableID = req.body.isLeaderTraceableID;
            var reasonForNPA = req.body.reasonForNPA;
            var overdueDurationFrom = req.body.overdueDurationFrom;
            var overdueDurationTo = req.body.overdueDurationTo;
            var amountFrom = req.body.amountFrom;
            var amountTo = req.body.amountTo;
            var branch = req.body.branch;
            self.listNPASearchGroupCall(tenantId, recoveryOfficer, capabilityPercentage, isLeaderTraceableID, reasonForNPA, overdueDurationFrom,
                    overdueDurationTo, amountFrom, amountTo, branch, function (accountIdArray, customerNameArray, overDueAmountArray,
                            daysInArrearsArray, recoveryOfficerNameArray, expectedCompletionDateArray, groupName) {
                        req.body.accountId = accountIdArray;
                        req.body.customerNameArray = customerNameArray;
                        req.body.overDueAmountArray = overDueAmountArray;
                        req.body.daysInArrearsArray = daysInArrearsArray;
                        req.body.recoveryOfficerNameArray = recoveryOfficerNameArray;
                        req.body.expectedCompletionDateArray = expectedCompletionDateArray;
                        req.body.groupName = groupName;
                        res.send(req.body);
                    });
        } catch (e) {
            customlog.error("Exception while List Searched NPA " + e);
            self.commonRouter.showErrorPage(req, res);
        }
    },

    getNpaCaseStatusCall: function (tenantId, callback) {
        this.model.getNpaCaseStatusModel(tenantId, callback);
    },

    getNpaCaseCall: function (userId, date, callback) {
        this.model.getNpaCaseModel(userId, date, callback);
    },

    NPALRTodo: function (req, res) {
        try {
            var self = this;
            var userId = req.session.userId;
            var tenantId = req.session.tenantId;
            var date = req.params.taskType;
            var constantsObj = this.constants;
            var selectedOffice = (typeof req.body.listoffice != 'undefined') ? req.body.listoffice : req.session.officeId;
            var selectedRO = (typeof req.body.loanOfficer != 'undefined') ? req.body.loanOfficer : req.session.userId;
            var statusMsg = (typeof req.body.statusMsg != 'undefined') ? req.body.statusMsg : "";
            if (typeof tenantId == 'undefined' || typeof userId == 'undefined') {
                res.redirect(props.contextPath + '/login');
            } else {
                if (date == 'closed') {
                    self.NPALRTodoClosed(req, res);
                } else {
                    self.getNpaCaseStatusCall(tenantId, function (npaCaseStatusIdArray, npaCaseStatusNameArray) {
                        self.getNpaCaseCall(selectedRO, date, function (taskIdArray, accountIdArray, customerArray, taskNameArray, dueDateArray, dueTimeArray, statusIdArray, statusNameArray) {
                            if (req.session.browser == "mobile") {
                                res.render('Mobile/NPALRTodoMobile', {date: date, npaCaseStatusIdArray: npaCaseStatusIdArray,
                                    npaCaseStatusNameArray: npaCaseStatusNameArray, taskIdArray: taskIdArray, accountIdArray: accountIdArray,
                                    customerArray: customerArray, taskNameArray: taskNameArray, dueDateArray: dueDateArray,
                                    dueTimeArray: dueTimeArray, statusIdArray: statusIdArray, statusNameArray: statusNameArray, contextPath: props.contextPath,
                                    title: date.charAt(0).toUpperCase() + date.slice(1) + " Task", statusMsg: statusMsg});
                            } else {
                                self.commonRouter.retrieveOfficeDetails(function (officeIds, officeNames) {
                                    self.commonRouter.getPersonnelDetailsCall(selectedOffice, userId, function (FOIdsArray, FONamesArray) {
                                        res.render('NPALRTodo', {date: date, npaCaseStatusIdArray: npaCaseStatusIdArray,
                                            npaCaseStatusNameArray: npaCaseStatusNameArray, taskIdArray: taskIdArray, accountIdArray: accountIdArray,
                                            customerArray: customerArray, taskNameArray: taskNameArray, dueDateArray: dueDateArray,
                                            dueTimeArray: dueTimeArray, statusIdArray: statusIdArray, statusNameArray: statusNameArray, contextPath: props.contextPath,
                                            title: date.charAt(0).toUpperCase() + date.slice(1) + " Task", officeIds: officeIds, officeNames: officeNames, roleId: req.session.roleId, FOIdsArray: FOIdsArray,
                                            FONamesArray: FONamesArray, constantsObj: constantsObj, selectedOffice: selectedOffice, selectedRO: selectedRO, statusMsg: statusMsg});
                                    });
                                });
                            }
                        });
                    });
                }
            }
        } catch (e) {
            customlog.error("Exception while NPALR To do " + date + " Task ", e);
            self.commonRouter.showErrorPage(req, res);
        }
    },

    getNpaClosedCaseCall: function (userId, date, callback) {
        this.model.getNpaClosedCaseModel(userId, date, callback);
    },

    NPALRTodoClosed: function (req, res) {
        try {
            var self = this;
            var userId = req.session.userId;
            var tenantId = req.session.tenantId;
            var constantsObj = this.constants;
            var selectedOffice = (typeof req.body.listoffice != 'undefined') ? req.body.listoffice : req.session.officeId;
            var selectedRO = (req.body.loanOfficer) ? req.body.loanOfficer : req.session.userId;
            if (typeof tenantId == 'undefined' || typeof userId == 'undefined') {
                res.redirect(props.contextPath + '/login');
            } else {
                var date = "closed";
                self.getNpaClosedCaseCall(selectedRO, date, function (taskIdArray, accountIdArray, customerArray, taskNameArray, dueDateArray, statusIdArray, statusNameArray, closedDateArray, remarksArray) {
                    if (req.session.browser == "mobile") {
                        res.render('Mobile/NPALRTodoClosedMobile', {date: date, taskIdArray: taskIdArray, accountIdArray: accountIdArray,
                            customerArray: customerArray, taskNameArray: taskNameArray, dueDateArray: dueDateArray, statusIdArray: statusIdArray,
                            statusNameArray: statusNameArray, closedDateArray: closedDateArray, remarksArray: remarksArray, contextPath: props.contextPath, title: "Closed Task"});
                    } else {
                        self.commonRouter.retrieveOfficeDetails(function (officeIds, officeNames) {
                            self.commonRouter.getPersonnelDetailsCall(selectedOffice, userId, function (FOIdsArray, FONamesArray) {
                                res.render('NPALRTodoClosed', {date: date, taskIdArray: taskIdArray, accountIdArray: accountIdArray,
                                    customerArray: customerArray, taskNameArray: taskNameArray, dueDateArray: dueDateArray, statusIdArray: statusIdArray,
                                    statusNameArray: statusNameArray, closedDateArray: closedDateArray, remarksArray: remarksArray, contextPath: props.contextPath,
                                    title: "Closed Task", officeIds: officeIds, officeNames: officeNames, roleId: req.session.roleId, FOIdsArray: FOIdsArray,
                                    FONamesArray: FONamesArray, constantsObj: constantsObj, selectedOffice: selectedOffice, selectedRO: selectedRO});
                            });
                        });
                    }
                });
            }
        } catch (e) {
            customlog.error("Exception while NPALR to do Closed " + e);
            self.commonRouter.showErrorPage(req, res);
        }
    },

    submitNpaCaseCall: function (taskId, taskRemarks, callback) {
        this.model.submitNpaCaseModel(taskId, taskRemarks, callback);
    },

    submitNpaCase: function (req, res) {
        try {
            var self = this;
            var userId = req.session.userId;
            var tenantId = req.session.tenantId;
            var taskRemarks = req.body.remarks;
            var taskId = req.body.taskId;
            self.submitNpaCaseCall(taskId, taskRemarks, function (result) {
                var activityDetails = new Array(iklantPort, req.session.tenantId, req.session.userId, req.session.userName, req.originalUrl, req.connection.remoteAddress, "router.js", "submitNpaCase", "success", "NPA groups", "NPA TaskId " + taskId + " submited successfully", "insert");
                self.commonRouter.insertActivityLogModel(activityDetails);
                req.body.result = result;
                res.send(req.body);
            });
        } catch (e) {
            customlog.error("Exception while Submit NPA Case " + e);
            self.commonRouter.showErrorPage(req, res);
        }
    },

    getGroupsForRecoveryPage: function (userId, callback) {
        this.model.getGroupsForRecoveryModel(userId, callback);
    },

    getGroupsForRecovery: function (req, res) {
        try {
            var self = this;
            var userId = req.session.userId;
            var flag = (req.query.flag) ? 1 : 0;
            if (typeof (req.session.tenantId) == 'undefined' || typeof (req.session.userId) == 'undefined') {
                res.redirect(props.contextPath + '/client_ci/login');
            } else {
                self.getGroupsForRecoveryPage(userId, function (groupDetailsArray) {
                    if (req.session.browser == "mobile") {
                        res.render('Mobile/groupsForLoanRecoveryMobile', {groupDetailsArray: groupDetailsArray, contextPath: props.contextPath, flag: flag});
                    } else {
                        res.redirect(props.contextPath + "/client/ci/login");
                    }
                });
            }
        } catch (e) {
            customlog.error("Exception while get groups for Recovery " + e);
            self.commonRouter.showErrorPage(req, res);
        }
    },

    //Upload File
    updateFileLocation: function (accountId, fileName, selectedClientId, callback) {
        this.model.updateFileLocationModel(accountId, fileName, selectedClientId, callback);
    },

    uploadFile: function (req, res) {
        try {
            var self = this;
            var userId = req.session.userId;
            var constantsObj = this.constants;
            var accountId = req.params.accountId;
            var selectedClientId = req.params.clientId;
            var backFlag = req.body.backFlagId;
            customlog.info("accountId" + accountId + "clientId" + selectedClientId);
            var alertMsg = "";
            var fs = require('fs'),
                    util = require('util');
            var fileName = new Array();
            var isMulitpleDoc = req.body.isMultipleDocument;
            customlog.info("Multiple Doc=" + isMulitpleDoc);
            if (isMulitpleDoc == "true") {
                customlog.info("inside true");
                for (var i = 0; i < req.files.multipleUploadDocument.length; i++) {
                    fileName[i] = req.body.groupnameHidden + "_" + accountId + "_" + getCurrentTimeStamp() + "." + req.files.multipleUploadDocument[i].name.split('.').pop();
                    ;
                    var is = fs.createReadStream(req.files.multipleUploadDocument[i].path)
                    var os = fs.createWriteStream(rootPath + "/documents/group_documents/" + fileName[i]);
                    is.pipe(os);
                    is.on('end', function () {
                        alertMsg = "Files has been Uploaded Successfully!"
                        customlog.info('Successfully uploaded');
                    });
                    fs.unlink(req.files.multipleUploadDocument[i].path, function (err) {
                        if (err) {
                            customlog.info('Error while unlinking ' + err);
                        } else {
                            customlog.info('Successfully unlinked');
                        }
                        ;
                    });
                    is.on('error', function (err) {
                        customlog.info("Error while uploading " + err);
                    });
                }
            } else if (isMulitpleDoc == "false") {
                if (req.files.singleUploadDocument.name != "") {
                    fileName[0] = selectedClientId + "_" + accountId + "_" + getCurrentTimeStamp() + "." + req.files.singleUploadDocument.name.split('.').pop();
                    customlog.info("fileName=" + fileName);
                    if (req.files.singleUploadDocument.name != "") {
                        var is = fs.createReadStream(req.files.singleUploadDocument.path)
                        var os = fs.createWriteStream(rootPath + "/documents/group_documents/" + fileName[0]);
                        is.pipe(os);
                        is.on('end', function () {
                            customlog.info('Successfully uploaded');
                            alertMsg = "File has been Uploaded Successfully!"
                        });
                        fs.unlink(req.files.singleUploadDocument.path, function (err) {
                            if (err) {
                                customlog.info('Error while unlinking ' + err);
                            } else {
                                customlog.info('Successfully unlinked');
                            }
                            ;
                        });
                        is.on('error', function (err) {
                            customlog.info("Error while uploading " + err);
                        });
                    }
                }
            }
            self.updateFileLocation(accountId, fileName, selectedClientId, function () {
                self.getGroupsDetailsForRecoveryPage(userId, accountId, function (accountDetailsArray) {
                    self.recoveryReasons(function (recoveryReasonId, recoveryReasonDescription) {
                        self.getUtilQuestions(function (questions) {
                            if (req.session.browser == "mobile") {
                                res.render('Mobile/groupsDetailsForRecoveryMobile', {alertMsg: alertMsg, accountDetailsArray: accountDetailsArray, recoveryReasonId: recoveryReasonId, recoveryReasonDescription: recoveryReasonDescription, backFlag: backFlag, contextPath: props.contextPath, utilQuestions: questions});
                            } else {
                                res.render('groupsDetailsForRecovery', {alertMsg: alertMsg, accountDetailsArray: accountDetailsArray, recoveryReasonId: recoveryReasonId, recoveryReasonDescription: recoveryReasonDescription, backFlag: backFlag, contextPath: props.contextPath});
                            }
                        });
                    });
                });
            });
        } catch (e) {
            customlog.error("Exception while Upload File " + e);
            self.commonRouter.showErrorPage(req, res);
        }
    },

    getUtilQuestions: function (callback) {
        this.model.getUtilQuestionsModel(callback);
    },
    getGroupsDetailsForRecoveryPage: function (userId, accountId, callback) {
        this.model.getGroupDetailsForRecoveryModel(userId, accountId, callback);
    },
    getGroupDetailsForWebModelCall: function (userId, accountId, callback) {
        this.model.getGroupDetailsForWebModel(userId, accountId, callback);
    },
    recoveryReasons: function (callback) {
        this.model.getRecoveryReasons(callback);
    },

    updateVerifiedInformationPage: function (params, callback) {
        this.model.updateVerifiedInformationModel(params, callback);
    },

    saveActionPlans: function (todoActivity, todoDueDate, todoDueTime, RoId, userId, accountId, callback) {
        this.model.saveActionPlansModel(todoActivity, todoDueDate, todoDueTime, RoId, userId, accountId, callback);
    },

    addActionPlans: function (req, res) {
        var self = this;
        try {
            var actionPlan = (req.body["actionPlan[]"]) ? req.body["actionPlan[]"] : [];
            var dueDate = (req.body["dueDate[]"]) ? req.body["dueDate[]"] : [];
            var dueTime = (req.body["dueTime[]"]) ? req.body["dueTime[]"] : [];
            var RoId = req.body.userId;
            var userId = req.session.userId;
            var accountId = req.body.accountId;
            var noOfPlan = req.body.noOfPlan;
            customlog.info("actionPlan: ", actionPlan + ",dueDate: ", dueDate + ",dueTime: ", dueTime + ",userId: " + userId + ",accountId: " + accountId + ",noOfPlan: " + noOfPlan);
            if (actionPlan.length > 0) {
                if (noOfPlan > 1) {
                    self.saveActionPlans(actionPlan, dueDate, dueTime, RoId, userId, accountId, function (err) {
                        if (!err) {
                            res.send({addPlanStatusMessage: "success"});
                            customlog.info("Outside");
                        }

                    });
                } else {
                    self.saveActionPlans([actionPlan], [dueDate], [dueTime], RoId, userId, accountId, function (err) {
                        if (!err) {
                            res.send({addPlanStatusMessage: "success"});
                            customlog.info("Outside");
                        }
                    });
                }

            } else {
                res.send({addPlanStatusMessage: "No action plans given"});
            }
        } catch (ex) {
            customlog.error("Exception while add action plans File ", ex);
            self.commonRouter.showErrorPage(req, res);
        }
    },

    updateVerifiedInformation: function (req, res) {
        try {
            var self = this;
            var userId = req.session.userId;
            var accountId = req.params.accountId;
            customlog.info("accountId" + accountId);
            var statusId = req.body.statusId;
            var backFlag = req.body.backFlagId;
            var flag;

            var npaQuestionsArray = JSON.parse(req.body.npaQuestions);

            customlog.info("npaQuestionsArray ",npaQuestionsArray)
            /* geo details */
            
            var geoQuestionIds = JSON.parse(req.body.geoQuestionIds);
            var lattitude = (req.body.lattitude) ? req.body.lattitude : 0;
            var longitude = (req.body.longitude) ? req.body.longitude : 0;
            
            var todoActivity = (req.body.todoActivity) ? (req.body.todoActivity).split(",") : [];
            var todoDueDate = (req.body.todoDueDate) ? (req.body.todoDueDate).split(",") : [];
            var todoDueTime = (req.body.todoDueTime) ? (req.body.todoDueTime).split(",") : [];

            if (req.body.loanstatus === 1) {
                flag = false;
            } else {
                flag = true;
            }

            if (statusId == 3) {
                var reason = (req.body.reason) ? req.body.reason : '';
                var otherReason = (req.body.otherReasonName) ? req.body.otherReasonName : '';
                var remarks = (req.body.remarks) ? req.body.remarks : '';
            } else if (statusId == 4) {
                var reason = (req.body.reason) ? req.body.reason : '';
                var otherReason = (req.body.otherReasonName) ? req.body.otherReasonName : '';
                var capabilitypercentage = (req.body.capabilityPercentage) ? req.body.capabilityPercentage : '';
                var expecteddate = (req.body.selectedDate) ? req.body.selectedDate : '';
            }
            var params = {accountId: accountId, statusId: statusId, reason: reason, otherReason: otherReason, remarks: remarks,
                capabilitypercentage: capabilitypercentage, expecteddate: expecteddate, npaQuestionsArray: npaQuestionsArray,
                flag: flag, userId: userId, todoActivity: todoActivity, todoDueDate: todoDueDate, todoDueTime: todoDueTime,
                geoQuestionIds: geoQuestionIds, lattitude: lattitude, longitude: longitude};
            self.updateVerifiedInformationPage(params, function () {
                var activityDetails = new Array(iklantPort, req.session.tenantId, req.session.userId, req.session.userName, req.originalUrl, req.connection.remoteAddress, "router.js", "updateVerifiedInformation", "success", "NPA Loan Recovery", "AccountId " + accountId + " NPA VerifiedInformation successfully", "update");
                self.commonRouter.insertActivityLogModel(activityDetails);
                if (backFlag == 0) {
                    res.redirect(props.contextPath + "/client/ci/getGroupsForRecovery?flag=" + 1);
                } else if (backFlag == 1) {
                    res.redirect(props.contextPath + "/client/ci/pastDueLoans");
                }
            });
        } catch (e) {
            customlog.error("Exception while Update verified information " + e);
            self.commonRouter.showErrorPage(req, res);
        }
    },

    retrieveClientDetailsPage: function (accountId, callback) {
        this.model.retrieveClientDetailsPageModel(accountId, callback);
    },

    retrieveClientDetails: function (req, res) {
        try {
            var self = this;
            var accountId = req.body.accountId;
            self.retrieveClientDetailsPage(accountId, function (customerIdArray, customerNameArray, customerAddressArray, customerFirstNameArray, customerLastNameArray, customerCodeArray, overdueArray, arrearDaysArray) {
                req.body.customerIdArray = customerIdArray;
                req.body.customerNameArray = customerNameArray;
                req.body.customerAddressArray = customerAddressArray;
                req.body.customerFirstNameArray = customerFirstNameArray;
                req.body.customerLastNameArray = customerLastNameArray;
                req.body.customerCodeArray = customerCodeArray;
                req.body.overdueArray = overdueArray;
                req.body.arrearDaysArray = arrearDaysArray;
                res.send(req.body);
            });
        } catch (e) {
            customlog.error("Exception while retrieve Client Details " + e);
            self.commonRouter.showErrorPage(req, res);
        }
    },

    retrieveUploadedDocsPage: function (accountId, clientId, callback) {
        this.model.retrieveUploadedDocsPageModel(accountId, clientId, callback);
    },

    retrieveUploadedDocs: function (req, res) {
        try {
            var self = this;
            var accountId = req.body.accountId;
            var clientId = req.body.clientId;
            customlog.info("accountId " + accountId);
            customlog.info("clientId " + clientId);
            self.retrieveUploadedDocsPage(accountId, clientId, function (docsListArray, docsNameListArray) {
                req.body.docsListArray = docsListArray;
                req.body.docsNameListArray = docsNameListArray;
                res.send(req.body);
            });
        } catch (e) {
            customlog.error("Exception while retrieve upload Document " + e);
            self.commonRouter.showErrorPage(req, res);
        }
    },
    //Loan Sanction Document Verification
    showDocVerification: function (req, res, clientId, mifosCustomerId, isSynchronized, iklantGroupId, centerName, clientIdArray, clientNameArray, docTypeIdArray, docTypeNameArray, docId, fileLocation, docVerificationFlag, clientLoanCountArray, storageLocationIndicatorArray, centerCode) {
        try {
            var self = this;
            res.render('docVerification', {clientId: clientId, mifosCustomerId: mifosCustomerId, isSynchronized: isSynchronized, iklantGroupId: iklantGroupId,
                centerName: centerName, clientIdArray: clientIdArray, clientNameArray: clientNameArray, docTypeIdArray: docTypeIdArray, clientLoanCountArray: clientLoanCountArray, centerCode: centerCode,
                docTypeNameArray: docTypeNameArray, docId: docId, fileLocation: fileLocation, docVerificationFlag: docVerificationFlag, contextPath: props.contextPath, storageLocationIndicatorArray: storageLocationIndicatorArray});
        } catch (e) {
            customlog.error("Exception while showDocVerification " + e);
            self.commonRouter.showErrorPage(req, res);
        }
    },

    docVerificationCall: function (iklantGroupId, docVerificationFlag, callback) {
        this.model.docVerificationCallModel(iklantGroupId, docVerificationFlag, callback);
    },

    docVerification: function (req, res) {
        try {
            var self = this;
            var userId = req.session.userId;
            var tenantId = req.session.tenantId;
            var iklantGroupId = req.body.iklantGroupIdHidden;
            var isSynchronized = req.body.isSynchronizedHidden;
            var mifosCustomerId = req.body.mifosCustomerIdHidden;
            var docVerificationFlag = req.body.docVerificationFlagHidden;
            var fileLocation = "";
            var clientId = "";
            var docId = "";
            customlog.info("iklantGroupIddoc " + iklantGroupId);
            if (typeof tenantId == 'undefined' || typeof userId == 'undefined') {
                res.redirect(props.contextPath + '/client/ci/login');
            } else {
                self.commonRouter.retrieveDocTypeList(tenantId, function (docTypeIdArray, docTypeNameArray) {
                    self.docVerificationCall(iklantGroupId, docVerificationFlag, function (centerName, clientIdArray, clientNameArray, clientLoanCountArray, centerCode) {
                        self.showDocVerification(req, res, clientId, mifosCustomerId, isSynchronized, iklantGroupId, centerName, clientIdArray, clientNameArray, docTypeIdArray, docTypeNameArray, docId, fileLocation, docVerificationFlag, clientLoanCountArray, [], centerCode);
                    });
                });
            }
        } catch (e) {
            customlog.error("Exception while docVerification " + e);
            self.commonRouter.showErrorPage(req, res);
        }
    },

    //Document Verification Groups List
    showDocVerificationGroups: function (req, res, groupIdArray, groupNameArray, centerNameArray, roleId, officeIdArray, officeNameArray, selectedOfficeId, errorField) {
        try {
            var self = this;
            var constantsObj = this.constants;
            //var activityDetails = new Array(iklantPort, req.session.tenantId, req.session.userId, req.session.userName, req.originalUrl, req.connection.remoteAddress, "router.js", "showDocVerificationGroups", "success", "DocVerificationGroupList", "showDocVerificationGroups");
            //self.commonRouter.insertActivityLogModel(activityDetails);
            res.render('docVerificationGroupList', {groupIdArray: groupIdArray, groupNameArray: groupNameArray,
                centerNameArray: centerNameArray, roleId: roleId, officeIdArray: officeIdArray,
                officeNameArray: officeNameArray, selectedOfficeId: selectedOfficeId, errorField: errorField, constantsObj: constantsObj, contextPath: props.contextPath});
        } catch (e) {
            customlog.error("Exception while showDocVerificationGroups " + e);
            self.commonRouter.showErrorPage(req, res);
        }
    },

    generateDocVerificationGroupsCall: function (tenantId, officeId, userId, callback) {
        this.model.generateDocVerificationGroupsCallModel(tenantId, officeId, userId, callback);
    },

    generateDocVerificationGroups: function (req, res) {
        try {
            var self = this;
            var constantsObj = this.constants;
            var userId = req.session.userId;
            var tenantId = req.session.tenantId;
            var roleId = req.session.roleId;
            var roleIds = req.session.roleIds;
            var officeId = req.body.selectedOfficeHidden;
            if ((typeof officeId == 'undefined')) {
                officeId = (roleId == constantsObj.getSMHroleId() || roleIds.indexOf(constantsObj.getCCEroleId()) > -1) ? -1 : req.session.officeId;
            }

            if (typeof tenantId == 'undefined' || typeof userId == 'undefined') {
                res.redirect(props.contextPath + '/login');
            } else {
                self.generateDocVerificationGroupsCall(tenantId, officeId, userId, function (groupIdArray, groupNameArray, centerNameArray, errorField) {
                    self.commonRouter.retriveOfficeCall(tenantId, userId, function (officeIdArray, officeNameArray, officeAddressArray, officeShortNameArray) {
                        self.showDocVerificationGroups(req, res, groupIdArray, groupNameArray, centerNameArray, roleId, officeIdArray,
                                officeNameArray, officeId, errorField);
                    });
                });
            }
        } catch (e) {
            customlog.error("Exception while generateDocVerificationGroups " + e);
            self.commonRouter.showErrorPage(req, res);
        }
    },

    //generic function to download docs from path
    downloadDocs: function (req, res) {
        try {
            var self = this;
            var fs = require('fs');
            var selectedDocLocation = req.body.selectedDocName;
            customlog.info("selectedDocLocation= " + selectedDocLocation);
            if (typeof selectedDocLocation != "undefined") {
                var docName = selectedDocLocation.split("/");
                if (docName.length > 1) {
                    res.download(selectedDocLocation, docName[docName.length - 1], function (err) {
                        if (err) {
                            customlog.error(err);
                        } else {
                            customlog.info(selectedDocLocation);
                            var docNameArray = selectedDocLocation.split('/');
                            customlog.info(docNameArray);
                            for (var i = 0; i < docNameArray.length - 1; i++) {
                                if (docNameArray[i] == "report_documents" || docNameArray[i] == "voucher_documents") {
                                    fs.unlink(selectedDocLocation);
                                }
                            }
                        }
                    });
                } else {
                    customlog.error(selectedDocLocation + " not found");
                    self.commonRouter.showErrorPage(req, res);
                }
            }
        } catch (e) {
            customlog.error("Exception while downloadDocs " + e);
            self.commonRouter.showErrorPage(req, res);
        }
    },
    getGroupDetailCall: function (RO, branchId, tenantId, userId, offset, limit, officeId, callback) {
        this.model.getGroupDetailModel(RO, branchId, tenantId, userId, offset, limit, officeId, callback);
    },

    assignROLoad: function (req, res) {
        try {
            var self = this;
            var constantsObj = this.constants;
            if (typeof (req.session.tenantId) == 'undefined' || typeof (req.session.userId) == 'undefined') {
                res.redirect(props.contextPath + '/client/ci/login');
            } else {
                var selectedPage = (req.body.selectedPage) ? req.body.selectedPage : 1;
                var statusMsg = (req.body.statusMsg) ? req.body.statusMsg : '';
                var offset = (selectedPage > 0) ? (selectedPage - 1) * constantsObj.getLimit() : constantsObj.getOffset();
                var limit = constantsObj.getLimit();
                self.getGroupDetailCall(req.query.selectRO, req.query.branches, req.session.tenantId, req.session.userId, offset, limit, function (NPALoanRecoveryGroupsObject, NPALoanRecoveryGroupsStatusObject, userName, userId, totalRecords, userOfficeId) {
                    self.commonRouter.retrieveOfficeDetails(function (officeIds, officeNames) {
                        res.render("assignRO", {NPALoanRecoveryGroupsObject: NPALoanRecoveryGroupsObject, userName: userName,
                            userId: userId, NPALoanRecoveryGroupsStatusObject: NPALoanRecoveryGroupsStatusObject, contextPath: props.contextPath,
                            selectedPage: selectedPage, totalRecords: totalRecords, statusMsg: statusMsg, officeIds: officeIds, officeNames: officeNames, userOfficeId: userOfficeId});
                    });
                });
            }
        } catch (e) {
            customlog.error("Exception while assignROLoad " + e);
            self.commonRouter.showErrorPage(req, res);
        }
    },
    assignGroupToROCall: function (customerId, accountId, roId, callback) {
        this.model.assignGroupToROModel(customerId, accountId, roId, callback);
    },
    assignGroupToROLog: function (userId, groupName, officer, userName, customerId, accountId, roId, callback) {
        this.model.assignGroupToROLogModel(userId, groupName, officer, userName, customerId, accountId, roId, callback);
    },

    assignRO: function (req, res) {
        try {
            var self = this;
            var loanRecoveryOfficer = req.body.selectRO;
            var accountId = req.body.accountId.split(",");
            var customerId = req.body.customerId.split(",");
            var groupName = req.body.groupName.split(",");
            var officer = req.body.officerName;
            self.assignGroupToROCall(customerId, accountId, loanRecoveryOfficer, function () {
                self.assignGroupToROLog(req.session.userId, groupName, officer, req.session.userName, customerId, accountId, loanRecoveryOfficer, function () {
                    req.body.statusMsg = "Successfully assigned";
                    self.assignROLoad(req, res);
                });
            });
        } catch (e) {
            customlog.error("Exception while assignRo " + e);
            self.commonRouter.showErrorPage(req, res);
        }
    },
    //Added by vishesh for NPA submitNPAReviewDetails
    assignNPAReviewDetails: function (status, remarks, accountId, callback) {
        this.model.assignNPAReviewDetailsModel(status, remarks, accountId, callback);
    },
    submitNPAReviewDetails: function (req, res) {
        try {
            var self = this;
            var status = req.body.status;
            var remarks = req.body.remarks;
            var accountId = req.body.accountId;
            customlog.info("status: " + status + ", remarks: " + remarks + ", accountId:" + accountId);
            self.assignNPAReviewDetails(status, remarks, accountId, function () {
                res.send('success');
            });
        } catch (e) {
            customlog.error("Exception in submitNPAReviewDetails " + e);
        }
    },
    showPDF: function (res, path) {
        res.render('generatePDF', {path: path, contextPath: props.contextPath});
    },

    retrieveGroupDetails: function (req, res) {
        try {
            var self = this;
            var constantsObj = this.constants;
            var accountId = req.params.accountId;
            var backFlag = req.params.backFlag;
            var userId = req.session.userId;
            self.getGroupsDetailsForRecoveryPage(userId, accountId, function (accountDetailsArray, lengthVal) {
                self.getUtilQuestions(function (questions) {
                    if (req.session.roleId == constantsObj.getSMHroleId()) {
                        self.recoveryReasons(function (recoveryReasonId, recoveryReasonDescription) {
                            self.model.retrieveAllUploadedDocs(accountId, function (documentsArray) {
                                var comModel = new commonModel(constantsObj);
                                comModel.getLoanStatusList(function (statusList) {
                                    res.render('groupsDetailsForRecoveryReadOnly', {alertMsg: "", accountDetailsArray: accountDetailsArray, recoveryReasonId: recoveryReasonId, recoveryReasonDescription: recoveryReasonDescription,
                                        constantsObj: constantsObj, contextPath: props.contextPath, statusList: statusList, documentsArray: documentsArray});
                                });
                            });
                        });
                    } else if (lengthVal > 0) {
                        if (req.session.browser == "mobile") {
                            res.render('Mobile/groupsDetailsForRecoveryMobile', {alertMsg: "", accountDetailsArray: accountDetailsArray, backFlag: backFlag, contextPath: props.contextPath, utilQuestions: questions});
                        } else {
                            self.recoveryReasons(function (recoveryReasonId, recoveryReasonDescription) {
                                res.render('groupsDetailsForRecovery', {alertMsg: "", accountDetailsArray: accountDetailsArray, recoveryReasonId: recoveryReasonId, recoveryReasonDescription: recoveryReasonDescription, backFlag: backFlag, contextPath: props.contextPath});
                            });
                        }
                    } else {
                        res.redirect(props.contextPath + '/pastDueLoans/0');
                    }
                });
            });
        } catch (e) {
            customlog.error("Exception while retrieve Group Details " + e);
            self.commonRouter.showErrorPage(req, res);
        }
    },
    getGroupDetailsForWeb: function (req, res) {
        try {
            var self = this;
            var constantsObj = this.constants;
            var accountId = req.params.accountId;
            var backFlag = req.params.backFlag;
            var userId = req.session.userId;
            self.getGroupDetailsForWebModelCall(userId, accountId, function (accountDetailsArray, QADeatils, lengthVal) {
                if (req.session.roleId == constantsObj.getSMHroleId()) {
                    self.recoveryReasons(function (recoveryReasonId, recoveryReasonDescription) {
                        self.model.retrieveAllUploadedDocs(accountId, function (documentsArray) {
                            var comModel = new commonModel(constantsObj);
                            comModel.getLoanStatusList(function (statusList) {
                                res.render('groupsDetailsForRecoveryReadOnly', {alertMsg: "", accountDetailsArray: accountDetailsArray, recoveryReasonId: recoveryReasonId, recoveryReasonDescription: recoveryReasonDescription,
                                    constantsObj: constantsObj, contextPath: props.contextPath, statusList: statusList, documentsArray: documentsArray, QADeatils: QADeatils});
                            });
                        });
                    });
                } else if (lengthVal > 0) {
                    if (req.session.browser == "mobile") {
                        res.render('Mobile/groupsDetailsForRecoveryMobile', {alertMsg: "", accountDetailsArray: accountDetailsArray, backFlag: backFlag, contextPath: props.contextPath, QADeatils: QADeatils});
                    } else {
                        self.recoveryReasons(function (recoveryReasonId, recoveryReasonDescription) {
                            res.render('groupsDetailsForRecovery', {alertMsg: "", accountDetailsArray: accountDetailsArray, recoveryReasonId: recoveryReasonId, recoveryReasonDescription: recoveryReasonDescription, backFlag: backFlag, contextPath: props.contextPath});
                        });
                    }
                } else {
                    res.redirect(props.contextPath + '/pastDueLoans/0');
                }
            });
        } catch (e) {
            customlog.error("Exception while retrieve Group Details " + e);
            self.commonRouter.showErrorPage(req, res);
        }
    },

    uploadDocs: function (req, res, index, fileName, callback) {
        var self = this,
                fs = require('fs'),
                s3 = new AWS.S3(),
                constantsObj = this.constants,
                status = "success",
                currentDate = new Date(),
                isMulitpleDoc = req.body.isMultipleDocument,
                clientId = req.body.selectedClientForNOCUpload,
                clientName = req.body.selectedClientNameForNOCUpload;

        if (isMulitpleDoc == "true") {
            var fileType = req.files.multipleUploadDocument[index].name.split('.');
            var fileSize = req.files.multipleUploadDocument.length;
        } else {
            var fileType = req.files.singleUploadDocument.name.split('.');
            var fileSize = 1;
        }

        var docNamePrefix;
        if ((parseInt(req.body.docId)) == constantsObj.getOwnHouseReceiptDocId()) {
            docNamePrefix = "_" + parseInt(req.body.docId) + "_";
        } else if ((parseInt(req.body.docId)) == constantsObj.getbankpassbookDocId()) {
            docNamePrefix = "_" + parseInt(req.body.docId) + "_";
        } else if ((parseInt(req.body.docId)) == constantsObj.getApplicationFormDocId()) {
            docNamePrefix = "_" + parseInt(req.body.docId) + "_";
        } else {
            docNamePrefix = "_NOC_";
        }
        var file_name = clientId + "_" + clientName + docNamePrefix + (currentDate.getFullYear() + "_" + (currentDate.getMonth() + 1) + "_" + currentDate.getTime()) + index + "." + fileType[1];
        if (isMulitpleDoc == "true") {
            var is = fs.createReadStream(req.files.multipleUploadDocument[index].path);
            var path = req.files.multipleUploadDocument[index].path;
        } else {
            var is = fs.createReadStream(req.files.singleUploadDocument.path);
            var path = req.files.singleUploadDocument.path;
        }

        fs.stat(path, function (err, file_info) {
            if (!err) {
                var params = {
                    Bucket: constantsObj.getS3BucketName(),
                    Key: file_name,
                    ContentType: 'image/jpeg',
                    ContentLength: file_info.size,
                    Body: is
                };
                s3.putObject(params, function (err, data) {
                    if (!err) {
                        fileName.push({file_name: file_name, eTag: data.ETag.replace(/["']/g, ""), s3key: 1});
                        index++;
                        if (index == fileSize) {
                            callback(status, clientId, clientName, constantsObj, fileName)
                        } else {
                            self.uploadDocs(req, res, index, fileName, callback);
                        }
                    } else {
                        status = "failure";
                        callback(status);
                    }
                });
            } else {
                status = "failure";
                callback(status);
            }
        });
    },

    migrateCustomerData: function (res, res) {
        this.model.migrateCustomerModel();
        res.send("Migrated successfully");
    }

};

function getCurrentTimeStamp() {
    var currentdate = new Date();
    var datetime = checkDate_Time(currentdate.getFullYear()) + checkDate_Time((currentdate.getMonth() + 1))
            + checkDate_Time(currentdate.getDate()) + checkDate_Time(currentdate.getHours())
            + checkDate_Time(currentdate.getMinutes()) + checkDate_Time(currentdate.getSeconds());
    return datetime;
}
function checkDate_Time(time) {
    return (time < 10) ? ("0" + time) : time;
}