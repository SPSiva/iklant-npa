module.exports = reportManagementModel;

var path = require('path');
var applicationHome = path.dirname(process.mainModule.filename);
var ReportManagementDataModel = require(path.join(applicationHome, "app_modules/data_model/ReportManagementDataModel"));
var customLog = require(path.join(applicationHome, "logger/loggerConfig.js"))('ReportManagementModel.js');

//Business Layer
function reportManagementModel(constants) {
    customLog.debug("Inside business layer");
    this.dataModel = new ReportManagementDataModel(constants);
}

reportManagementModel.prototype = {
    assignGroupToROModel: function (customerId, accountId, roId, callback) {
        this.dataModel.assignGroupToRODataModel(customerId, accountId, roId, callback);
    },
    assignGroupToROLogModel: function (userId, groupName, officer, userName, customerId, accountId, roId, callback) {
        this.dataModel.assignGroupToROLogDataModel(userId, groupName, officer, userName, customerId, accountId, roId, callback);
    },
    assignNPAReviewDetailsModel: function (status, remarks, accountId, callback) {
        this.dataModel.NPAReviewDetailsDataModel(status, remarks, accountId, callback);
    },
    reportManagementModel: function (tenantId, officeId, userId, callback) {
        this.dataModel.reportManagementDataModel(tenantId, officeId, userId, callback);
    },
    generateReportModel: function (startdate, enddate, statusId, officeId, userId, callback) {
        this.dataModel.generateReportDataModel(startdate, enddate, statusId, officeId, userId, callback);
    },
    listNPASearchGroupModel: function (tenantId, recoveryOfficer, capabilityPercentage, isLeaderTraceableID, reasonForNPA,
            overdueDurationFrom, overdueDurationTo, amountFrom, amountTo, branch, callback) {
        this.dataModel.listNPASearchGroupDatamodel(tenantId, recoveryOfficer, capabilityPercentage, isLeaderTraceableID,
                reasonForNPA, overdueDurationFrom, overdueDurationTo, amountFrom, amountTo, branch, callback);
    },

    getNPAReasonsModel: function (tenantId, userId, callback) {
        this.dataModel.getNPAReasonsDatamodel(tenantId, userId, callback);
    },

    npaDefaultSearchModel: function (userId, callback) {
        this.dataModel.getNPADefaultSearchDatamodel(userId, callback);
    },

    getNpaCaseStatusModel: function (tenantId, callback) {
        this.dataModel.getNpaCaseStatusDatamodel(tenantId, callback);
    },

    getNpaCaseModel: function (userId, date, callback) {
        this.dataModel.getNpaCaseDatamodel(userId, date, callback);
    },

    getNpaClosedCaseModel: function (userId, date, callback) {
        this.dataModel.getNpaClosedCaseDatamodel(userId, date, callback);
    },

    submitNpaCaseModel: function (taskId, taskRemarks, callback) {
        this.dataModel.submitNpaCaseDatamodel(taskId, taskRemarks, callback);
    },
    //NPA LR
    getGroupsForRecoveryModel: function (userId, callback) {
        this.dataModel.getGroupsForRecoveryDataModel(userId, callback);
    },

    updateVerifiedInformationModel: function (params, callback) {
        this.dataModel.updateVerifiedInformationDataModel(params, callback);
    },

    getUtilQuestionsModel: function(callback){
        this.dataModel.getUtilQuestionsDataModel(callback);
    },
    
    getGroupDetailsForRecoveryModel : function(userId,accountId,callback) {
        this.dataModel.getGroupDetailsForDataModel(userId,accountId,callback);
    },
    getGroupDetailsForWebModel : function(userId,accountId,callback) {
        this.dataModel.getGroupDetailsForWebDataModel(userId,accountId,callback);
    },
    getRecoveryReasons : function(callback){
        this.dataModel.getRecoveryReasonsDataModel(callback);
    },
    //added on 25-7-17
    retrieveAllUploadedDocs : function(accountId, callback){
        this.dataModel.retrieveAllUploadedDocsDataModel(accountId, callback);
    },
    saveActionPlansModel: function (todoActivity, todoDueDate, todoDueTime, RoId, userId, accountId, callback) {
        this.dataModel.saveActionPlansDataModel(todoActivity, todoDueDate, todoDueTime, RoId, userId, accountId, callback);
    },
    //upload File
    updateFileLocationModel: function (accountId, fileName, selectedClientId, callback) {
        this.dataModel.updateFileLocationDataModel(accountId, fileName, selectedClientId, callback);
    },
    retrieveClientDetailsPageModel: function (accountId, callback) {
        this.dataModel.retrieveClientDetailsDataModel(accountId, callback);
    },
    retrieveUploadedDocsPageModel: function (accountId, clientId, callback) {
        this.dataModel.retrieveUploadedDocsPageDataModel(accountId, clientId, callback);
    },
    getGroupDetailModel: function (RO, branchId, tenantId, userId, offset, limit, callback) {
        this.dataModel.getGroupDetailDataModel(RO, branchId, tenantId, userId, offset, limit, callback);
    },
    callOfficeAndPersonnelForBC: function (bcOfficeId, callback) {
        this.dataModel.getcallOfficeAndPersonnelForBC(bcOfficeId, callback);
    },
    migrateCustomerModel: function () {
        this.dataModel.migrateCustomerDataModel();
    }
};