module.exports = commonModel;

var path = require('path');
var applicationHome = path.dirname(process.mainModule.filename);
var CommonDataModel = require(path.join(applicationHome,"app_modules/data_model/CommonDataModel"));
var customlog = require(path.join(applicationHome,"logger/loggerConfig.js"))('CommonModel.js');

//Business Layer
function commonModel(constants) {
    customlog.debug("Inside business layer");
    this.dataModel = new CommonDataModel(constants);
}

commonModel.prototype = {
    getBranchesModel: function(tenantId,userId,roleId,officeId,callback) {
        this.dataModel.getBranchesDataModel(tenantId,userId,roleId,officeId,callback);
    },
    listGroupsModel: function(tenantId,userId,officeId,roleId,requestedOperationId,callback) {
        this.dataModel.listGroups(tenantId,userId,officeId,roleId,requestedOperationId,callback);
    },
    createGroupModel: function(tenantId,officeId,userId,callback) {
        this.dataModel.createGroup(tenantId,officeId,userId,callback);
    },
    retrieveLoanTypelistModel : function(tenantId,callback){
        this.dataModel.retrieveLoanTypelistDataModel(tenantId,callback);
    },
    retriveOfficeModel : function(tenantId,userId,callback) {
        this.dataModel.retriveOfficeDatamodel(tenantId,userId,callback);
    },
    retriveOfficeByOfficeLevelModel : function(userOfficeLevelId,callback) {
        this.dataModel.retriveOfficeByOfficeLevelDatamodel(userOfficeLevelId,callback);
    },

    retrieveOfficeDetailsModel : function(callback){
        this.dataModel.retrieveOfficeDetailsDataModel(callback);
    },
    
    retrieveOfficeListModel : function(officeId, officeLevelId,callback){
        this.dataModel.retrieveOfficeListDataModel(officeId, officeLevelId,callback);
    },

    retrieveGroupsModel: function(tenantId,officeId,callback){
        this.dataModel.retrieveGroupsDataModel(tenantId,officeId,callback);
    },

    listClientsModel : function(tenantId,userId,officeId,roleId,callback){
        this.dataModel.listClientsDataModel(tenantId,userId,officeId,roleId,callback);
    },
    listUploadNOCClientsModel:function(tenantId,userId,officeId,roleId,callback){
        this.dataModel.listUploadNOCClientsDataModel(tenantId,userId,officeId,roleId,callback);
    },
    listClientsForRMAuthorizationModel : function(tenantId,userId,officeId,roleId,callback){
        this.dataModel.listClientsForRMAuthorizationDataModel(tenantId,userId,officeId,roleId,callback);
    },
    retrieveLookUpIdModel  : function(callback){
        this.dataModel.retrieveLookUpIdDataModel(callback);
    },
    insertActivityLogModel: function(activityDetails){
        this.dataModel.insertActivityLogDataModel(activityDetails);
    },
    getFONamesForAssigningFOModel:function(tenantId,officeId,callback) {
        this.dataModel.getFONamesForAssigningFODatamodel(tenantId,officeId,callback);
    },
    getDEONamesForAssigningDEOCallModel:function(tenantId,officeId,getDEOFlag,callback) {
        this.dataModel.getDEONamesForAssigningDEOCallDataModel(tenantId,officeId,getDEOFlag,callback);
    },
    listGroupsForAssignToDEOModel:function(tenantId,officeValue,regionalOffice,callback){
        this.dataModel.listGroupsForAssignToDEODataModel(tenantId,officeValue,regionalOffice,callback);
    },
    listGroupsAllForAssignToDEOModel:function(tenantId,callback){
        this.dataModel.listGroupsAllForAssignToDEODataModel(tenantId,callback);
    },
    removeAssignDEOOcallModel:function(assigndeoIds,callback){
        this.dataModel.removeAssignDEOOcallDataModel(assigndeoIds,callback);
    },
    getlistClientModel:function(deoName,callback){
        this.dataModel.getlistClientDataModel(deoName,callback);
    },
    retrieveStateModel : function(callback) {
        this.dataModel.retriveStateDatamodel(callback);
    },
    manageRolesModel :function(callback) {
        this.dataModel.manageRoles(callback);
    },
    retriveFieldOfficersModel : function(isFromCL,officeId,callback) {
        this.dataModel.retriveFieldOfficersDataModel(isFromCL,officeId,callback);
    },
    // Added by Paramasivan for the Reports
    getPersonnelDetailsCallModel : function(office_id,userId,callBack){
        this.dataModel.getPersonnelDetailsDataModel(office_id,userId,callBack);
    },
    retrieveClientDetailsForGeneratePDFModel : function(mifosCustomerId,selectedMemberId,callback){
        this.dataModel.retrieveClientDetailsForGeneratePDFDataModel(mifosCustomerId,selectedMemberId,callback);
    },
    //added by Chitra
    updateLeaderAndSubLeaderDetailsModel : function(groupId,mifosCustomerId,callback){
        this.dataModel.updateLeaderAndSubLeaderDetailsDataModel(groupId,mifosCustomerId,callback);
    },
    updateLeaderAndSubLeaderDetailsInMifosModel : function(groupId,mifosCustomerId,callback){
        this.dataModel.updateLeaderAndSubLeaderDetailsInMifosDataModel(groupId,mifosCustomerId,callback);
    },
    updateLeaderAndSubLeaderDetailsForRejectedModel : function(iklantGroupId,subLeaderRejectedGlobalNumber,subLeaderRejectedClientId,callback){
        this.dataModel.updateLeaderAndSubLeaderDetailsForRejectedDataModel(iklantGroupId,subLeaderRejectedGlobalNumber,subLeaderRejectedClientId,callback);
    },
    KYCFileUploadForLoanSanctionModel : function(clientid,formType,mifosCustomerId,docLanguage,bcOfficeId,ImageArray,ImagePathArray,callback) {
        this.dataModel.KYCFileUploadForLoanSanctionDatamodel(clientid,formType,mifosCustomerId,docLanguage,bcOfficeId,ImageArray,ImagePathArray,callback);
    },
    /*  Added on 10/7/17  */
    generateDocumentModel: function(params, callback){
        this.dataModel.generateDocumentDataModel(params, callback);
    },
    /**/
    generateLegalFormModel : function(mifosGlobalAccountNo,callback){
        this.dataModel.generateLegalFormForGroupDataModel(mifosGlobalAccountNo,callback);
    },
    generateMASLegalFormModel : function(mifosGlobalAccountNo,callback){
        this.dataModel.generateMASLegalFormForGroupDataModel(mifosGlobalAccountNo,callback);
    },
    groupAndClientsLoanScheduleModel : function(mifosCustomerId,globalAccountNum,clientid, includePremiumCalc, callBack){
        this.dataModel.groupAndClientsLoanScheduleDataModel(mifosCustomerId,globalAccountNum,clientid, includePremiumCalc, callBack);
    },
    getIklantGroupIdFromCustomerIdModel : function(mifosCustomerId,callback){
        this.dataModel.getIklantGroupIdFromCustomerIdDataModel(mifosCustomerId,callback);
    },
    insertFieldVerificationDetailsModel: function(fieldVerificationObj,prospectClientHouseDetailToUpdate,latValue,longValue,isLeader,callback) {
        this.dataModel.insertFieldVerificationDetails(fieldVerificationObj,prospectClientHouseDetailToUpdate,latValue,longValue,isLeader,callback);
    },
    getClientNamesAfterModel: function(groupId,callback) {
        this.dataModel.getClientNamesForFieldVerification(groupId,callback);
    },
    //Adarsh-retrieve DocType
    retrieveDocTypeListModel : function(tenantId,callback){
        this.dataModel.retrieveDocTypeListDataModel(tenantId,callback);
    },
    retrieveDocTypeListForMFINChange : function(clientNameID,callback){
        this.dataModel.retrieveDocTypeListForMFINChangeDataModel(clientNameID,callback);
    },
    ccaModel1: function(tenantId,groupId,callback) {
        this.dataModel.cca1AfterCheck(tenantId,groupId,callback);
    },
    retrieveIdleClientsModel: function(tenantId, groupId, statusId, callback) {
        this.dataModel.retrieveIdleClientsDataModel(tenantId, groupId, statusId, callback);
    },
    rejectActiveGroupModel: function(groupId,statusId,remarks,callback){
        this.dataModel.rejectActiveGroup(groupId,statusId,remarks,callback);
    },

    getActiveClientsModel : function(groupId, callback){
        this.dataModel.getActiveClients(groupId, callback);
    },
    uploadNOCclientModel:function(clientId, callback){
        this.dataModel.uploadNOCclientDataModel(clientId, callback);
    },
    backtoRmModel:function(clientId,clientName,groupName,centerName,callback){
        this.dataModel.backtoRmDataModel(clientId,clientName,groupName,centerName,callback);
    },
    rejectedClientDetailsModel : function(tenantId,clientId,callback){
        this.dataModel.rejectedClientDetailsDataModel(tenantId,clientId,callback);
    },
    needClarificationDetailsModel : function(clientId,remarks,callback) {
        this.dataModel.needClarificationDetails(clientId,remarks,callback);
    },
    groupAuthorizationClientCalculationModel : function(tenantId,groupId,callback) {
        this.dataModel.groupAuthorizationClientCalculationDataModel(tenantId,groupId,callback);
    },
    //Baskar
    getClientNamesModel: function(groupId,callback) {
        this.dataModel.getClientNamesForFieldVerification(groupId,callback);
    },
    getFieldVerificationDetailsModel: function(clientId,callback) {
        this.dataModel.getFieldVerificationDetails(clientId,callback);
    },
    groupDetailsAuthorizationModel: function(tenantId,branchId,groupId,clientId,callback) {
        this.dataModel.groupDetailsAuthorizationDatamodel(tenantId,branchId,groupId,clientId,callback);
    },
    listQuestionsCCACallModel: function(tenantId,clientId,clientLoanCount,callback) {
        this.dataModel.listQuestionsCCACallDataModel(tenantId,clientId,clientLoanCount,callback);
    },
    updateClientStatusModel : function(clientIdListArray,clientIds,overdues,callback){
        this.dataModel.updateClientStatusDataModel(clientIdListArray,clientIds,overdues,callback);
    },
    removableDocumentAvailabilityModel : function(isAvailableSize,isDelete,checkingType,callback){
     console.log("Model : removableDocumentAvailabilityModel entry");
     this.dataModel.removableDocumentAvailabilityDataModel(isAvailableSize,isDelete,checkingType,callback)
    },
    archeivedFlagUpdateClientDocModel : function(clientDocId,clientId,type,callback){
        console.log("Model : archeivedFlagUpdateClientDocModel entry");
        this.dataModel.archeivedFlagUpdateClientDoc(clientDocId,clientId,type,callback)
    },
    getGroupsForPartnership: function(tenantId,userId,reqOfficeId,callback){
        this.dataModel.getGroupsForPartnershipDataModel(tenantId,userId,reqOfficeId,callback);
    },
    listGroupDetailsHighMarkResultsModel :function(tenantId,userId,roleId,officeId,callback){
        this.dataModel.listGroupDetailsforHighMarkDetailsDataModel(tenantId,userId,roleId,officeId,callback);
    },
    calculateInsurancePremiumModel : function(disbursementDate,clientDOBList,totalInstallments,clientAmount,guarantorDOBList,clientListArray,callback){
        this.dataModel.calculateInsurancePremiumDataModel(disbursementDate,clientDOBList,totalInstallments,clientAmount,guarantorDOBList,clientListArray,callback);
    },
    getGuarantorDOBListModel : function(clientGlobalNumberList,callback){
        this.dataModel.getGuarantorDOBListDataModel(clientGlobalNumberList,callback);
    },
    getClientBankAccountDetailsModel : function(clientList,callback){
        this.dataModel.getClientBankAccountDetailsDataModel(clientList,callback);
    },

    groupCountKYCDashBoardModel : function(officeId,roleId,callback){
        this.dataModel.groupCountKYCDashBoardDataModel(officeId,roleId,callback);
    },
    groupListForKycUpdating : function(officeId,roleId,userId,roleIdslength,callback){
        this.dataModel.groupListForKycUpdatingDataModel(officeId,roleId,userId,roleIdslength,callback);
    },
    getOperationNameModel : function(roleId,callback){
        this.dataModel.getOperationNameDataModel(roleId,callback);
    },
    getGroupsForBankDetailsModel : function(officeId,currentOperationIndex,callback){
        this.dataModel.getGroupsForBankDetailsDataModel(officeId,currentOperationIndex,callback);
    },
    getClientDetailsModel : function(groupId, currentOperationIndex,callback){
        this.dataModel.getClientDetailsDataModel(groupId,currentOperationIndex,callback);
    },
    groupsForLoanAmtUpdationModel : function(officeId,currentOperationIndex,callback){
        this.dataModel.groupsForLoanAmtUpdationDataModel(officeId,currentOperationIndex,callback);
    },
    getAllGroupsModel: function(userId,callback){
        this.dataModel.getAllGroupsDataModel(userId,callback);
    },
    getLDModel: function(userId,callback){
        this.dataModel.getLDDataModel(userId,callback);
    },
    assignGroupsModel: function(userId,groupId,callback){
        this.dataModel.assignGroupsDataModel(userId,groupId,callback);
    },
    getDetailsModel: function(groupId,clientId,accountId,callback){
        this.dataModel.getDetailsDataModel(groupId,clientId,accountId,callback);
    },
    getHighmarkResultsModel : function(type, id, callback){
        this.dataModel.getHighmarkResultsDataModel(type, id, callback);
    },
    saveCBAModel : function(activeAccountInfo,userId,callback){
        this.dataModel.saveCBADataModel(activeAccountInfo,userId,callback);
    },
    saveGroupCBAModel : function(groupId){
        this.dataModel.saveGroupCBADataModel(groupId);
    },
     getCallHistoryModel : function(groupId,clientId,callback){
     this.dataModel.getCallHistoryDataModel(groupId,clientId,callback);
     },
     getAddressDetailsModel : function(groupId,flag,callback){
     this.dataModel.getAddressDetailsDataModel(groupId,flag,callback);
     },
    getImageForReportsModel : function(clientIdArray,callback){
        this.dataModel.getImageForReportsDataModel(clientIdArray,callback);
    },
    getEquifaxModel:function(params,callback){
        this.dataModel.getEquifaxDataModel(params, callback);
    },
    getReleaseGroupsModel : function(req,res,callback){
        this.dataModel.getReleaseGroupsDataModel(req,res,callback);
    },
    updateLeaderAndSubLeader:function(iklantGroupId){
        this.dataModel.updateLeaderAndSubLeaderDataModel(iklantGroupId);
    },
    getClientDocsModel : function(clientId,tenantId,groupId,callback){
        this.dataModel.getClientDocsDataModel(clientId,tenantId,groupId,callback);
    },
    getClientInfoModel : function(groupId,officeId,operationId,callback){
        this.dataModel.getClientInfoDataModel(groupId,officeId,operationId,callback);
    },
    getBCOfficeListModel : function(callback){
        this.dataModel.getBCOfficeListDataModel(callback);
    },
    getOfficeListModel : function(bcId,callback){
        this.dataModel.getOfficeListDataModel(bcId,callback);
    },
    getGroupsForBCModel : function(officeId,operationId,callback){
        this.dataModel.getGroupsForBCDataModel(officeId,operationId,callback);
    },
    getEmailIdForNotificationModel : function(callback){
        this.dataModel.getEmailIdForNotificationDataModel(callback);
    },
    getMailIdForRoleModel: function(officeId,roleId,callback){
        this.dataModel.getMailIdForRoleDataModel(officeId,roleId,callback);
    },
    //Added By Vishesh to get status list
    getLoanStatusList: function(callback){
        this.dataModel.getLoanStatusListDataModel(callback);
    }
};