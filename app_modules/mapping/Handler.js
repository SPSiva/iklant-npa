function handler(server) {
    var path = require('path');
    var applicationHome = path.dirname(process.mainModule.filename);
    var props = require(path.join(applicationHome, "properties.json"));

    var constantsRequire = require(path.join(applicationHome, "app_modules/dto/common/Constants"));
    var constants = new constantsRequire();
    var authenticationRouter = new (require(path.join(applicationHome, "/app_modules/router/AuthenticationRouter")))(constants);
    var commonRouter = new (require(path.join(applicationHome, "/app_modules/router/CommonRouting")))(constants);
    var userManagementRouter = new (require(path.join(applicationHome, "/app_modules/router/UserManagementRouter")))(constants);
    var groupManagementRouter = new (require(path.join(applicationHome, "/app_modules/router/GroupManagementRouter")))(constants);
    var reportManagementRouter = new (require(path.join(applicationHome, "/app_modules/router/ReportManagementRouter")))(constants);

    authenticationRouter.commonRouter = commonRouter;
    userManagementRouter.commonRouter = commonRouter;
    groupManagementRouter.commonRouter = commonRouter;
    reportManagementRouter.commonRouter = commonRouter;
    reportManagementRouter.groupManagementRouter = groupManagementRouter;

    /**
     * Authentication process
     */
    server.get(props.contextPath + '/client/ci/login', authenticationRouter.loginPage.bind(authenticationRouter));
    server.post(props.contextPath + '/client/ci/auth', authenticationRouter.authLogin.bind(authenticationRouter));
    server.get(props.contextPath + '/client/ci/menu', authenticationRouter.getMenu.bind(authenticationRouter));
    server.get(props.contextPath + '/client/ci/logout', authenticationRouter.logoutPage.bind(authenticationRouter));
    server.get(props.contextPath + '/client/ci/empty', authenticationRouter.showMenu.bind(authenticationRouter));

    /**
     * Used for Menu
     */
    server.get(props.contextPath + '/client/ci/getGroups', commonRouter.getBranches.bind(commonRouter));
    server.get(props.contextPath + '/client/ci/getGroups/:new', commonRouter.getBranches.bind(commonRouter));
    server.get(props.contextPath + '/client/ci/groups', commonRouter.listGroups.bind(commonRouter));
    server.get(props.contextPath + '/client/ci/groups/:new', commonRouter.listGroups.bind(commonRouter));
    server.post(props.contextPath + '/client/ci/groups/operation/:operationId', commonRouter.listGroupsOperation.bind(commonRouter));

    /**
     * User management related calls
     */
    server.post(props.contextPath + '/client/ci/getNewPassword', userManagementRouter.getNewPassword.bind(userManagementRouter));
    server.get(props.contextPath + '/client/ci/changePassword', userManagementRouter.loadChangePassword.bind(userManagementRouter));
    server.post(props.contextPath + '/client/ci/submitChangePassword', userManagementRouter.submitChangePassword.bind(userManagementRouter));
    server.get(props.contextPath + '/client/ci/forgotPassword', userManagementRouter.loadForgotPassword.bind(userManagementRouter));
    server.post(props.contextPath + '/client/ci/generatePassword', userManagementRouter.generatePassword.bind(userManagementRouter));

    /**
     * Used for NPA related screens - Web
     */
    server.get(props.contextPath + '/client/ci/searchnpa', reportManagementRouter.searchNPA.bind(reportManagementRouter));
    server.post(props.contextPath + '/client/ci/listsearchednpa', reportManagementRouter.listSearchedNPA.bind(reportManagementRouter));
    server.get(props.contextPath + '/client/ci/NPALRGroups/assignROLoad', reportManagementRouter.assignROLoad.bind(reportManagementRouter));
    server.post(props.contextPath + '/client/ci/NPALRGroups/assignROLoad', reportManagementRouter.assignROLoad.bind(reportManagementRouter));
    server.post(props.contextPath + '/client/ci/NPALRGroups/assignRO', reportManagementRouter.assignRO.bind(reportManagementRouter));
    server.get(props.contextPath + '/client/ci/NPALRGroups/todo/:taskType', reportManagementRouter.NPALRTodo.bind(reportManagementRouter));
    server.post(props.contextPath + '/client/ci/NPALRGroups/todo/:taskType', reportManagementRouter.NPALRTodo.bind(reportManagementRouter));
    server.post(props.contextPath + '/client/ci/NPALRGroups/submittask', reportManagementRouter.submitNpaCase.bind(reportManagementRouter));
    server.post(props.contextPath + '/client/ci/loanOfficers', reportManagementRouter.loanOfficers.bind(reportManagementRouter));
    server.post(props.contextPath + '/client/ci/:accountId/getGroupDetailsForWebModel/:backFlag', reportManagementRouter.getGroupDetailsForWeb.bind(reportManagementRouter));
    /**
     * Used for NPA related screens - Mobile
     */
    server.get(props.contextPath + '/client/ci/getGroupsForRecovery', reportManagementRouter.getGroupsForRecovery.bind(reportManagementRouter));
    server.post(props.contextPath + '/client/ci/:accountId/retrieveGroupDetails/:backFlag', reportManagementRouter.retrieveGroupDetails.bind(reportManagementRouter));
    server.post(props.contextPath + '/client/ci/npaloans/:accountId/updateVerifiedInformation', reportManagementRouter.updateVerifiedInformation.bind(reportManagementRouter));
    server.post(props.contextPath + '/client/ci/retrieveClientDetails', reportManagementRouter.retrieveClientDetails.bind(reportManagementRouter));
    server.post(props.contextPath + '/client/ci/npaloans/:accountId/:clientId/uploadFile', reportManagementRouter.uploadFile.bind(reportManagementRouter));
    
    server.post(props.contextPath + '/client/ci/submitNpaReviewDetails', reportManagementRouter.submitNPAReviewDetails.bind(reportManagementRouter));
    server.post(props.contextPath + '/client/ci/addActionPlans', reportManagementRouter.addActionPlans.bind(reportManagementRouter));
    server.post(props.contextPath + '/client/ci/getofficelistajaxcall', reportManagementRouter.getOfficeListAjaxcall.bind(reportManagementRouter));
    
    server.post(props.contextPath + '/client/ci/retrieveUploadedDocs', reportManagementRouter.retrieveUploadedDocs.bind(reportManagementRouter));
    server.post(props.contextPath + '/client/ci/groups/member/docVerification', reportManagementRouter.docVerification.bind(reportManagementRouter));
    server.post(props.contextPath + '/client/ci/downloadDocs', reportManagementRouter.downloadDocs.bind(reportManagementRouter));
    //document verification group list- report management
    server.post(props.contextPath + '/client/ci/reportManagement/docVerificationGroupList', reportManagementRouter.generateDocVerificationGroups.bind(reportManagementRouter));
    
    server.get(props.contextPath + '/migrateCustomerData', reportManagementRouter.migrateCustomerData.bind(reportManagementRouter));
    
    server.get('/', function (req, res) {
        res.send('Server is running..');
    });
    server.get('/*', function (req, res) {
        res.redirect(props.contextPath + '/client/ci/login');
    });
    server.post('/*', function (req, res) {
        res.redirect(props.contextPath + '/client/ci/login');
    });
    return server;
}
module.exports = handler;
