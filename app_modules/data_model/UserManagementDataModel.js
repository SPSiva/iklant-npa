module.exports = userManagementDataModel;

var path = require('path');
var rootPath = path.dirname(process.mainModule.filename);
var lookupEntityObj;

var dbTableName = require(path.join(rootPath,"properties.json"));

var connectionDataSource = require(path.join(rootPath,"app_modules/data_model/DataSource"));
var customlog = require(path.join(rootPath,"logger/loggerConfig.js"))('UserManagementDataModel.js');
var commonDTO = path.join(rootPath,"app_modules/dto/common");
var dateUtils = require('../utils/DateUtils');

//Business Layer
function userManagementDataModel(constants) {
    customlog.debug("Inside User Management Data Access Layer");
    this.constants = constants;
}

userManagementDataModel.prototype = {
    getUsersDatamodel: function (tenantId, callback) {
        var constantsObj = this.constants;
        var getUsersQuery = "SELECT p.display_name,u.user_id,u.user_name,o.office_name,contact_number,u.email_id, imei_number " +
            "FROM "+dbTableName.iklantUsers+"  u " +
            "INNER JOIN  "+dbTableName.iklantOffice+"  o ON o.office_id = u.office_id " +
            "INNER JOIN  personnel  p ON p.personnel_id = u.user_id " +
            "INNER JOIN  "+dbTableName.mfiPersonnelRole+"  pr ON pr.personnel_id = u.user_id " +
            "WHERE u.tenant_id=" + tenantId + " AND " +
            "u.active_indicator =" + constantsObj.getActiveIndicatorTrue() + " AND u.user_id <> 1 " +
            "GROUP BY u.`user_id` ORDER BY o.office_id,u.user_id ";
        var userIdArray = new Array();
        var userNameArray = new Array();
        var office_NameArray = new Array();
        var contactNumberArray = new Array();
        var emailIDArray = new Array();
        var userRoleIdArray = new Array();
        var imeiArray = new Array();
        var userNameAllArray = new Array();
        var userCodeArray = new Array();
        customlog.info("GetUsers Query : " + getUsersQuery);
        connectionDataSource.getConnection(function (clientConnect) {
            clientConnect.query(getUsersQuery, function(err, results, fields) {
                if (!err) {
                    for (var i in results) {
                        userIdArray[i] = results[i].user_id;
                        userNameArray[i] = results[i].display_name;
                        office_NameArray[i] = results[i].office_name;
                        contactNumberArray[i] = results[i].contact_number;
                        emailIDArray[i] = results[i].email_id;
                        userRoleIdArray[i] = results[i].role_id;
                        userCodeArray[i] = results[i].user_name;
                        if(results[i].imei_number != null){
                            imeiArray.push(results[i].imei_number);
                        }
                    }
                    var getAllUsersQuery = "SELECT u.user_id,u.user_name,o.office_name,contact_number,email_id, imei_number " +
                        "FROM "+dbTableName.iklantUsers+"  u " +
                        "INNER JOIN  "+dbTableName.iklantOffice+"  o ON o.office_id = u.office_id " +
                        "INNER JOIN  "+dbTableName.mfiPersonnelRole+"  pr ON pr.personnel_id = u.user_id " +
                        "WHERE u.tenant_id=" + tenantId + " ORDER BY o.office_id,u.user_id ";
                    customlog.info("GetAllUsers Query : " + getAllUsersQuery);
                    clientConnect.query(getAllUsersQuery, function(err, results, fields) {
                        connectionDataSource.releaseConnectionPool(clientConnect);
                        if (!err) {
                            for (var i in results) {
                                userNameAllArray[i] = results[i].user_name;
                            }
                            callback(userIdArray, userNameArray, office_NameArray, contactNumberArray, emailIDArray,userRoleIdArray,imeiArray,userNameAllArray,userCodeArray);
                        }
                        else
                        {
                            customlog.error(err);
                            callback(userIdArray, userNameArray, office_NameArray, contactNumberArray, emailIDArray,userRoleIdArray,imeiArray,userNameAllArray,userCodeArray);
                        }
                    });
                } else {
                    connectionDataSource.releaseConnectionPool(clientConnect);
                    customlog.error(err);
                    callback(userIdArray, userNameArray, office_NameArray, contactNumberArray, emailIDArray,userRoleIdArray,imeiArray,userNameAllArray,userCodeArray);
                }
            });
        });
    },
    getRolesDatamodel: function (tenantId, callback) {
        var constantsObj = this.constants;
        var getRolesQuery = "SELECT role_id,role_name,role_description FROM " +
            ""+dbTableName.iklantRole+"  WHERE tenant_id=" + tenantId + " " +
            "AND active_indicator=" + constantsObj.getActiveIndicatorTrue() + " " +
            "AND role_id NOT IN (" + constantsObj.getAdminroleId() + ") ";

        var roleIdArray = new Array();
        var roleNameArray = new Array();
        var roleDescriptionArray = new Array();
        customlog.info("GetRoles Query : " + getRolesQuery);
        connectionDataSource.getConnection(function (clientConnect) {
            clientConnect.query(getRolesQuery, function(err, results, fields) {
                connectionDataSource.releaseConnectionPool(clientConnect);
                if (!err) {
                    for (var i in results) {
                        roleIdArray[i] = results[i].role_id;
                        roleNameArray[i] = results[i].role_name;
                        roleDescriptionArray[i] = results[i].role_description;
                    }
                    callback(roleIdArray, roleNameArray, roleDescriptionArray);
                } else {
                    customlog.error(err);
                    callback(roleIdArray, roleNameArray, roleDescriptionArray);
                }
            });
        });
    },
    saveUserDatamodel: function (tenantId, officeId, userName, password, contactNumber, emailId, userId, imeiNumberId,bcId, roleIds, callback) {
        var self = this;
        imeiNumberId = (imeiNumberId) ? imeiNumberId : "";
        var manageUsersQuery = "INSERT INTO " + dbTableName.iklantUsers + " (tenant_id,office_id,bc_office_id,user_name,password,contact_number,email_id,created_by,created_date,imei_number) VALUES " +
            "(" + tenantId + ", " + officeId + "," + bcId + ",'" + userName + "', '" + password + "', '" + contactNumber + "', " +
            "'" + emailId + "', " + userId + ", NOW() + INTERVAL 5 HOUR + INTERVAL 30 MINUTE, '" + imeiNumberId + "')";
            
        customlog.info("Save User Query : " + manageUsersQuery);
        connectionDataSource.getConnection(function (clientConnect) {
            clientConnect.query(manageUsersQuery, function(err,result) {
                if (err) {
                    connectionDataSource.releaseConnectionPool(clientConnect);
                    customlog.error(err);
                    callback(false);
                }else{
                    self.updateUserRole(clientConnect, roleIds, tenantId, result.insertId, bcId, callback);
                }
            });
        });
    },
    
    updateUserRole: function(clientConnect, roleIds, tenantId ,userId, bcId, callback){
        var count = 0;
        var userRoleDeleteQuery = "DELETE FROM "+dbTableName.iklantUserRole+" WHERE user_id = "+userId;
        var bcUpdateQuery = "UPDATE personnel SET bc_office_id = "+bcId+" WHERE personnel_id = "+userId;
        clientConnect.query(userRoleDeleteQuery);
        clientConnect.query(bcUpdateQuery);
        for(count; count < roleIds.length; count++){
            var assignRoleQuery = "INSERT INTO "+dbTableName.iklantUserRole+" (tenant_id,user_id,role_id) " +
                "VALUES(" + tenantId + "," + userId + "," + roleIds[count] + ")";
                customlog.info("Assign Role Query : " + assignRoleQuery);
                clientConnect.query(assignRoleQuery, function(err) {
                if (err) {
                    customlog.error(err);
                }
            });
        }
        if(count == roleIds.length){
            connectionDataSource.releaseConnectionPool(clientConnect);
            callback(true);
        }
    },

    updateUserDatamodel: function (tenantId, currentUserId, officeId, userName, password, contactNumber, emailId, roleId, userId, imeiNumberId,bcId,callback) {
        var self = this;
        var constantsObj = this.constants;
        connectionDataSource.getConnection(function (clientConnect) {
            var updateUserQuery = "UPDATE " + dbTableName.iklantUsers + "  SET office_id = " + officeId + ",user_name = '" + userName + "', " +"bc_office_id = "+bcId+", " +
                "contact_number = '" + contactNumber + "',email_id = '" + emailId + "', updated_by = " + userId + ",updated_date = NOW() + INTERVAL 5 HOUR + INTERVAL 30 MINUTE";
            var retrieveUserQuery;
            if(roleId == constantsObj.getBDEroleId()  || roleId == constantsObj.getFOroleId()) {
                retrieveUserQuery = "SELECT * FROM " + dbTableName.iklantUsers + " WHERE imei_number = '" + imeiNumberId + "' AND user_name NOT IN ('" + userName + "')";
                updateUserQuery += ",imei_number = '" + imeiNumberId +"'";
            }
            if(password != ""){
                updateUserQuery += ",password = '" + password +"'";
            }
            updateUserQuery += " WHERE user_id = " + currentUserId + " and tenant_id=" + tenantId;
            if(roleId == constantsObj.getBDEroleId()  || roleId == constantsObj.getFOroleId()) {
                clientConnect.query(retrieveUserQuery,function(err,result){
                    if(result.length>0){
                        connectionDataSource.releaseConnectionPool(clientConnect);
                        callback(false,"IMEI number already exists for "+result[0].user_name);
                    }else{
                        self.updateUserAndRoleDataModel(clientConnect,updateUserQuery,function(status,message){
                            self.updateUserRole(clientConnect, roleId, tenantId, currentUserId, bcId, callback);
                        });
                    }
                });
            }
            else{
                self.updateUserAndRoleDataModel(clientConnect,updateUserQuery,function(status,message){
                    self.updateUserRole(clientConnect, roleId, tenantId, currentUserId, bcId, callback);
                });
            }
        });
    },

    updateUserAndRoleDataModel : function(clientConnect,updateUserQuery,callback){
        clientConnect.query(updateUserQuery, function postCreate(err) {
            if (err) {
                customlog.info(err+" "+updateUserQuery);
                connectionDataSource.releaseConnectionPool(clientConnect);
                callback(false,"User not updated properly. Please try later");
            }
            else{
                callback(true,"");
            }
        });
    },

    populateUserDetailsDatamodel: function (tenantId, userId, callback) {
        var getUserDetailsQuery = "SELECT u.bc_office_id,u.office_id,u.password,u.user_id,u.user_name,u.password,u.contact_number, " +
            "u.email_id,u.imei_number,pr.role_id FROM "+dbTableName.iklantUsers+"  u " +
            "INNER JOIN "+dbTableName.mfiPersonnelRole+"  pr ON pr.personnel_id = u.user_id " +
            "WHERE u.tenant_id=" + tenantId + " AND u.user_id = " + userId + "";
        var self = this;
        var officeId;
        var userId;
        var userName;
        var password;
        var contactNumber;
        var emailId;
        var roleId=new Array();
        var imeiNumber;
        var bcId;
        var roleIdArray= new Array();
        customlog.info("Get UserDetails Query : " + getUserDetailsQuery);
        connectionDataSource.getConnection(function (clientConnect) {
            clientConnect.query(getUserDetailsQuery, function selectCb(err, results, fields) {
                connectionDataSource.releaseConnectionPool(clientConnect);
                if (!err) {
                    for (var i in results) {
                        officeId = results[i].office_id;
                        userId = results[i].user_id;
                        userName = results[i].user_name;
                        password = results[i].password;
                        contactNumber = results[i].contact_number;
                        emailId = results[i].email_id;
                        roleIdArray.push(results[i].role_id);
                        imeiNumber = results[i].imei_number;
                        bcId = results[i].bc_office_id;
                    }
                    roleId = roleIdArray;
                    callback(contactNumber,imeiNumber,roleId,bcId);
                } else {
                    customlog.error(err);
                    callback(contactNumber);
                }
            });
        });
    },

    deleteUserDataModel: function (userid, tenantId, callback) {
        var self = this;
        var deleteUserQuery = "UPDATE "+dbTableName.iklantUsers+"  SET active_indicator = " + this.constants.getActiveIndicatorFalse() + ",imei_number='' " +
            "where user_id = " + userid + " ;";
        customlog.info("deleteUserQuery:" + deleteUserQuery);
        connectionDataSource.getConnection(function (clientConnect) {
            clientConnect.query(deleteUserQuery, function postCreate(err) {
                connectionDataSource.releaseConnectionPool(clientConnect);
                if (!err) {
                    callback();
                } else {
                    customlog.error(err);
                    callback();
                }
            });
        });
    },

    //Manage Office
    saveOfficeDatamodel: function (tenantId, officeName, officeShortName, officeAddress, userId, stateId,bcId,callback) {
        var selectOfficeQuery = "SELECT * FROM "+dbTableName.iklantOffice+" WHERE office_name = '" + officeName + "' OR office_short_name = '"+officeShortName+"'";
        var manageOfficeQuery = "INSERT INTO "+dbTableName.iklantOffice+" (tenant_id,office_name,office_short_name,office_address,state_id,created_by,bc_id,created_date) " +
            "VALUES(" + tenantId + ",'" + officeName + "','" + officeShortName + "','" + officeAddress + "'," + stateId+  "," + userId + "," + bcId + ",NOW() + INTERVAL 5 HOUR + INTERVAL 30 MINUTE)";

        customlog.info("Save Office Query : " + manageOfficeQuery);
        connectionDataSource.getConnection(function (clientConnect) {
            clientConnect.query(selectOfficeQuery,function(error,results){
                if(error){
                    connectionDataSource.releaseConnectionPool(clientConnect);
                    callback(false,false);
                }
                else if(results.length>0){
                    connectionDataSource.releaseConnectionPool(clientConnect);
                    callback(true,false);
                }else{
                    clientConnect.query(manageOfficeQuery, function(err,result) {
                        connectionDataSource.releaseConnectionPool(clientConnect);
                        if (!err) {
                            callback(true,false,result.insertId);
                        } else {
                            customlog.error(err);
                            callback(false,false);
                        }
                    });
                }
            });
        });
    },

    updateOfficeDatamodel: function (tenantId, officeId, officeName, officeShortName, officeAddress, bcId, officeLevel,userId, parentOffice,ldCallTrackingEnabled,callback) {
        var self = this;
        var updateOfficeQuery = "UPDATE "+dbTableName.iklantOffice+"  SET office_address = '" + officeAddress + "', bc_id = "+bcId+"," +
            "updated_by = " + userId + ",updated_date = NOW() + INTERVAL 5 HOUR + INTERVAL 30 MINUTE,pre_ld_call_tracking = "+ldCallTrackingEnabled;
        if(parentOffice != 0){
            updateOfficeQuery += ",parent_office_id = "+parentOffice;
        }
        updateOfficeQuery += " WHERE office_id = " + officeId + " and tenant_id=" + tenantId;
        customlog.info("Update Office Query : " + updateOfficeQuery);
        connectionDataSource.getConnection(function (clientConnect) {
            clientConnect.query(updateOfficeQuery, function(err) {
                if (err) {
                    customlog.error(err);
                    connectionDataSource.releaseConnectionPool(clientConnect);
                    callback();
                } else {
                    var updateOfficeQuery = "UPDATE office SET updated_by = " + userId + ",bc_id="+bcId+",updated_date = NOW() + INTERVAL 5 HOUR + INTERVAL 30 MINUTE";
                    if(officeLevel != 0){
                        updateOfficeQuery += ",office_level_id = "+officeLevel;
                    }
                    if(parentOffice != 0){
                        updateOfficeQuery += ",parent_office_id = "+parentOffice;
                    }
                    updateOfficeQuery += " WHERE office_id = " + officeId;
                    clientConnect.query(updateOfficeQuery, function(err) {
                        connectionDataSource.releaseConnectionPool(clientConnect);
                        if (err) {
                            customlog.error(err);
                        }
                        callback();
                    });
                }
            });
        });
    },

    populateOfficeDetailsDatamodel: function (tenantId, officeId, callback) {
        var getOfficeDetailsQuery = "SELECT o.pre_ld_call_tracking,off.parent_office_id,off.office_level_id,o.bc_id,o.office_id,o.office_name,o.office_short_name,o.office_address,o.state_id FROM "+dbTableName.iklantOffice+" o  " +
            "INNER JOIN office off ON off.office_id = o.office_id " +
            "WHERE o.office_id = " + officeId + " AND o.tenant_id = " + tenantId + "";

        var officeId;
        var officeName;
        var officeShortName;
        var officeAddress;
        var stateName;
        var bcId;
        var officeLevel;
        var parentOfficeId;
        var ldCallTracking;
        customlog.info("Get OfficeDetails Query : " + getOfficeDetailsQuery);
        connectionDataSource.getConnection(function (clientConnect) {
            clientConnect.query(getOfficeDetailsQuery, function selectCb(err, results, fields) {
                connectionDataSource.releaseConnectionPool(clientConnect);
                if (!err) {
                    for (var i in results) {
                        officeId = results[i].office_id;
                        officeName = results[i].office_name;
                        officeShortName = results[i].office_short_name;
                        officeAddress = results[i].office_address;
                        stateName = results[i].state_id;
                        officeLevel = results[i].office_level_id;
                        bcId = results[i].bc_id;
                        parentOfficeId = results[i].parent_office_id;
                        ldCallTracking = results[i].pre_ld_call_tracking;
                    }
                    callback(officeId, officeName, officeAddress, officeShortName,stateName,bcId,officeLevel,parentOfficeId,ldCallTracking);
                } else {
                    customlog.error(err);
                    callback(officeId, officeName, officeAddress, officeShortName,bcId,officeLevel,parentOfficeId,ldCallTracking);
                }
            });
        });
    },

    getBCDetailsDataModel : function(bcId,callback){
        var bcDetailsJson = {};
        var getBCByIdQuery = " SELECT * FROM business_correspondent_office WHERE bc_id="+bcId;
        customlog.info("getBCByIdQuery "+getBCByIdQuery);
        connectionDataSource.getConnection(function (clientConnect) {
            clientConnect.query(getBCByIdQuery, function (err,results) {
                connectionDataSource.releaseConnectionPool(clientConnect);
                if (err) {
                    customlog.error(" Error in getBCByIdQuery "+err);
                    callback(false,bcDetailsJson);
                } else {
                    bcDetailsJson.bcId = results[0].bc_id;
                    bcDetailsJson.bcName = results[0].bc_name;
                    bcDetailsJson.bcShortName = results[0].bc_short_name;
                    bcDetailsJson.bcAddress = results[0].bc_address;
                    bcDetailsJson.glCode = results[0].glcode_id;
                    bcDetailsJson.workFlow = results[0].workflow_type_id;
                    callback(true,bcDetailsJson);
                }
            });
        });
    },
    getParentOfficeListDataModel : function(levelId,callback){
        var parentOfficeQuery = "SELECT office_id, display_name AS office_name FROM office WHERE `office_level_id` = "+levelId;
        customlog.info("parentOfficeQuery "+parentOfficeQuery);
        connectionDataSource.getConnection(function (clientConnect) {
            clientConnect.query(parentOfficeQuery, function (err,results) {
                connectionDataSource.releaseConnectionPool(clientConnect);
                if (err) {
                    customlog.error(" Error in parentOfficeQuery "+err);
                    callback(new Array());
                } else {
                    callback(results);
                }
            });
        });
    },
    saveOrUpdateBCDetailsDataModel : function(bcDetails,callback){
        var self = this;
        var getBCQuery = "SELECT * FROM business_correspondent_office WHERE active_indicator = 1 AND (bc_name =  '"+bcDetails.bcOfficeName+"' OR bc_short_name = '"+bcDetails.bcShortName+"') AND bc_id <> "+bcDetails.bcId;
        customlog.info("getBCQuery "+getBCQuery);
        connectionDataSource.getConnection(function (clientConnect) {
            clientConnect.query(getBCQuery, function (err,results) {
                if(err){
                    connectionDataSource.releaseConnectionPool(clientConnect);
                    customlog.error(" Error in getBCQuery "+err);
                    callback(false,false);
                }
                else {
                    if(results.length > 0){
                        connectionDataSource.releaseConnectionPool(clientConnect);
                        callback(true,true);
                    }else{
                        var saveOrUpdateQuery = "";
                        if(bcDetails.bcId == 0){
                            saveOrUpdateQuery = "INSERT INTO business_correspondent_office (bc_name,bc_short_name,bc_address,workflow_type_id,glcode_id,created_by,created_date)"+
                                "VALUES('"+bcDetails.bcOfficeName+"','"+bcDetails.bcShortName+"','"+bcDetails.bcAddress+"','"+bcDetails.workflowId+"','"+bcDetails.glCode+"',"+bcDetails.createdBy+",NOW() + INTERVAL 5 HOUR + INTERVAL 30 MINUTE)";
                            self.saveFundDetails(clientConnect, bcDetails.bcOfficeName);
                        }else{
                            saveOrUpdateQuery = "UPDATE business_correspondent_office SET bc_name = '"+bcDetails.bcOfficeName+"',bc_short_name = '"+bcDetails.bcShortName+"',bc_address = '"+bcDetails.bcAddress+"',workflow_type_id = "+
                                "'"+bcDetails.workflowId+"',glcode_id = '"+bcDetails.glCode+"',updated_by = "+bcDetails.createdBy+",updated_date = NOW() + INTERVAL 5 HOUR + INTERVAL 30 MINUTE WHERE bc_id = "+bcDetails.bcId;
                        }
                        customlog.info("saveOrUpdateQuery "+saveOrUpdateQuery);
                        clientConnect.query(saveOrUpdateQuery, function (err) {
                            connectionDataSource.releaseConnectionPool(clientConnect);
                            if (err) {
                                customlog.error(" Error in saveOrUpdateQuery "+err);
                                callback(false,false);
                            } else {
                                callback(true,false);
                            }
                        });
                    }
                }
            });
        });
    },
    saveFundDetails: function(clientConnect,fundName){
        
                var saveFundQuery = "INSERT INTO `fund` (`fund_name`,`version_no`,`fundcode_id`) VALUES('"+fundName+"',0,1)";        
                clientConnect.query(saveFundQuery, function(){});
            
    },
    deleteBCDetailsDataModel: function (bcId, callback) {
        var deleteBCQuery = "UPDATE business_correspondent_office SET active_indicator=" + this.constants.getActiveIndicatorFalse() + " WHERE bc_id = " + bcId;
        customlog.info("deleteBCQuery:" + deleteBCQuery);
        connectionDataSource.getConnection(function (clientConnect) {
            clientConnect.query(deleteBCQuery, function(err) {
                connectionDataSource.releaseConnectionPool(clientConnect);
                if (!err) {
                    callback(true);
                } else {
                    customlog.error(err);
                    callback(false);
                }
            });
        });
    },
    deleteOfficeDataModel: function (officeid, tenantId, callback) {
        var self = this;
        var deleteOfficeQuery = "UPDATE "+dbTableName.iklantOffice+"  SET active_indicator=" + this.constants.getActiveIndicatorFalse() + " " +
            "WHERE office_id = " + officeid + " ;";
        customlog.info("deleteOfficeQuery:" + deleteOfficeQuery);
        connectionDataSource.getConnection(function (clientConnect) {
            clientConnect.query(deleteOfficeQuery, function postCreate(err) {
                connectionDataSource.releaseConnectionPool(clientConnect);
                if (!err) {
                    callback();
                } else {
                    customlog.error(err);
                    callback();
                }
            });
        });
    },
    populateRoleDetailsDatamodel: function (tenantId, roleId, callback) {
        var self = this;
        var selectedOperationIdArray = new Array();
        var selectedOperationNameArray = new Array();
        var roleName;
        var roleDescription;
        var roleOperationsQuery = "SELECT r.role_name,r.role_description,o.operation_id,o.operation_name " +
            "FROM "+dbTableName.iklantRole+"  r " +
            "LEFT JOIN "+dbTableName.iklantRoleOperation+"  ro ON ro.role_id = r.role_id " +
            "LEFT JOIN "+dbTableName.iklantOperation+"  o ON o.operation_id = ro.operation_id " +
            "WHERE r.role_id = " + roleId + " ORDER BY o.operation_id";
        customlog.info("Get Role Operation Details Query : " + roleOperationsQuery);
        connectionDataSource.getConnection(function (clientConnect) {
            clientConnect.query(roleOperationsQuery, function selectCb(err, results, fields) {
                connectionDataSource.releaseConnectionPool(clientConnect);
                if (!err) {
                    for (var i in results) {
                        selectedOperationIdArray[i] = results[i].operation_id;
                        selectedOperationNameArray[i] = results[i].operation_name;
                        roleName = results[i].role_name;
                        roleDescription = results[i].role_description;
                    }
                    self.getRolePrevilegeDetails(tenantId, roleId, function (selectedRolePrevilegeIdArray, selectedRolePrevilegeNameArray) {
                        callback(roleId, roleName, roleDescription, selectedOperationIdArray, selectedOperationNameArray,
                            selectedRolePrevilegeIdArray, selectedRolePrevilegeNameArray);
                    });
                } else {
                    customlog.error(err);
                    callback(roleId, roleName, roleDescription, selectedOperationIdArray, selectedOperationNameArray);
                }
            });
        });
    },
    getRolePrevilegeDetails: function (tenant_id, role_id, callback) {
        var self = this;
        var selectedRolePrevilegeIdArray = new Array();
        var selectedRolePrevilegeNameArray = new Array();
        var rolePrevilegeQuery = "SELECT o.operation_id,o.operation_name FROM "+dbTableName.iklantRolePrevilege+"  rp " +
            "INNER JOIN "+dbTableName.iklantOperation+"  o ON o.operation_id = rp.operation_id " +
            "WHERE rp.role_id = " + role_id + " AND rp.tenant_id = " + tenant_id + " " +
            "ORDER BY o.operation_id";
        customlog.info("Get Role Previlege Details Query : " + rolePrevilegeQuery);
        connectionDataSource.getConnection(function (clientConnect) {
            clientConnect.query(rolePrevilegeQuery, function selectCb(err, results, fields) {
                connectionDataSource.releaseConnectionPool(clientConnect);
                if (!err) {
                    for (var i in results) {
                        selectedRolePrevilegeIdArray[i] = results[i].operation_id;
                        selectedRolePrevilegeNameArray[i] = results[i].operation_name;
                    }
                    callback(selectedRolePrevilegeIdArray, selectedRolePrevilegeNameArray);
                } else {
                    customlog.error(err);
                    callback(selectedRolePrevilegeIdArray, selectedRolePrevilegeNameArray);
                }
            });
        });
    },
    updateRoleDatamodel: function (tenantId, userId, roleId, roleName, roleDescription, insertFlag, deleteFlag, previouslySelectedOperationlist, selectedOperation, callback) {
        var self = this;
        var updateRoleQuery = "UPDATE "+dbTableName.iklantRole+"  SET role_name = '" + roleName + "', " +
            "role_description = '" + roleDescription + "',updated_by=" + userId + ", " +
            "updated_date =NOW() + INTERVAL 5 HOUR + INTERVAL 30 MINUTE WHERE role_id = " + roleId + " AND tenant_id = " + tenantId + "";
        customlog.info("updateRoleQuery:::" + updateRoleQuery);
        connectionDataSource.getConnection(function (clientConnect) {
            clientConnect.query(updateRoleQuery, function postCreate(err) {
                if (err) {
                    customlog.error(err);
                    connectionDataSource.releaseConnectionPool(clientConnect);
                }
            });

            if (insertFlag.length != 0 | deleteFlag.length != 0) {
                for (var i = 0; i < insertFlag.length; i++) {
                    if (insertFlag[i] == 1) {
                        var insertNewTaskForRole = "INSERT INTO "+dbTableName.iklantRoleOperation+"  (role_id, operation_id) " +
                            "VALUES(" + roleId + "," + selectedOperation[i] + "); ";
                        customlog.info("insertNewTaskForRole:" + insertNewTaskForRole);
                        clientConnect.query(insertNewTaskForRole, function postCreate(err) {
                            if (err) {
                                customlog.error(err);
                                connectionDataSource.releaseConnectionPool(clientConnect);
                            }
                        });
                    }
                }
                for (var i = 0; i < deleteFlag.length; i++) {
                    if (deleteFlag[i] == 1) {
                        var deletetask = "DELETE FROM "+dbTableName.iklantRoleOperation+"  WHERE role_id = " + roleId + " AND " +
                            "operation_id = " + previouslySelectedOperationlist[i] + " ";
                        customlog.info("deletetask:" + deletetask);
                        clientConnect.query(deletetask, function postCreate(err) {
                            if (err) {
                                customlog.error(err);
                                connectionDataSource.releaseConnectionPool(clientConnect);
                            }
                        });
                    }
                }
                connectionDataSource.releaseConnectionPool(clientConnect);
                callback();
            }
            else {
                connectionDataSource.releaseConnectionPool(clientConnect);
                callback();
            }
        });
    },
    checkForRoleIsAssignedDatamodel: function (tenantId, roleId, callback) {
        var self = this;
        var constantsObj = this.constants;
        var noOfUsers;
        var checkForRoleIsAssignedQuery = "SELECT COUNT(u.user_id) as noofusers FROM "+dbTableName.mfiPersonnelRole+"  pr " +
            "INNER JOIN "+dbTableName.iklantRole+"  r ON r.role_id = pr.role_id " +
            "INNER JOIN "+dbTableName.iklantUsers+"  u ON u.user_id = pr.personnel_id " +
            "WHERE r.role_id = " + roleId + " AND u.tenant_id = " + tenantId + " " +
            "AND u.active_indicator = " + constantsObj.getActiveIndicatorTrue() + "";
        customlog.info("checkForRoleIsAssignedQuery:" + checkForRoleIsAssignedQuery);
        connectionDataSource.getConnection(function (clientConnect) {
            clientConnect.query(checkForRoleIsAssignedQuery, function selectCb(err, results, fields) {
                connectionDataSource.releaseConnectionPool(clientConnect);
                if (!err) {
                    for (var i in results) {
                        noOfUsers = results[i].noofusers;
                    }
                    callback(noOfUsers);
                } else {
                    customlog.error(err);
                    callback(noOfUsers);
                }
            });
        });
    },
    manageUsers: function (manageUsersObject, callback) {
        //INSERT QUERY FOR MANAGE USERS
        var self = this;
        var manageUsersQuery = "INSERT INTO "+dbTableName.iklantUsers+"  (tenant_id,office_id,user_name,password,contact_number,email_id,created_date) VALUES " +
            "('" + manageUsersObject.getTenant_id() + "', '" + manageUsersObject.getOffice_id() + "','" + manageUsersObject.getUser_name() + "', " +
            "'" + manageUsersObject.getPassword() + "', '" + manageUsersObject.getContact_number() + "'," +
            "'" + manageUsersObject.getEmail_id() + "',NOW() + INTERVAL 5 HOUR + INTERVAL 30 MINUTE) ";


        customlog.info("Query== " + manageUsersQuery);
        connectionDataSource.getConnection(function (clientConnect) {
            clientConnect.query(manageUsersQuery,
                function postCreate(err) {
                    connectionDataSource.releaseConnectionPool(clientConnect);
                    if (!err) {
                        callback();
                    } else {
                        callback();
                        customlog.error(err);
                    }
                }
            );
        });
    },

    assignRoles: function (tenantID, callback) {
        //select query to fetch user details from users table
        var self = this;
        var assignRoles = require(path.join(commonDTO,"/assignRoles"));
        var assignRolesObj = new assignRoles();
        assignRolesObj.clearAll();
        var fetchUsersQuery = "select user_id,user_name from "+dbTableName.iklantUsers+"  where user_id not in (select personnel_id from "+dbTableName.mfiPersonnelRole+" ) AND tenant_id =" + tenantID;
        var user_id = new Array();
        var userName = new Array();
        connectionDataSource.getConnection(function (clientConnect) {
            clientConnect.query(fetchUsersQuery,
                function selectCb(err, results, fields) {
                    if (err) {
                        connectionDataSource.releaseConnectionPool(clientConnect);
                        customlog.error(err);
                    }
                    else {
                        for (var i in results) {
                            user_id[i] = results[i].user_id;
                            userName[i] = results[i].user_name;
                        }
                        assignRolesObj.setUser_id(user_id);
                        assignRolesObj.setUser_name(userName);
                    }
                }
            );
            //select query to fetch roles from role table
            var fetchRolesQuery = "SELECT * FROM "+dbTableName.iklantRole+"  WHERE tenant_id =" + tenantID;
            var role_id = new Array();
            var role_name = new Array();
            clientConnect.query(fetchRolesQuery,
                function selectCb(err, results, fields) {
                    if (err) {
                        connectionDataSource.releaseConnectionPool(clientConnect);
                        customlog.error(err);
                        callback(assignRolesObj);
                    }
                    else {
                        for (var i in results) {
                            role_id[i] = results[i].role_id;
                            role_name[i] = results[i].role_name;
                        }
                        assignRolesObj.setRole_id(role_id);
                        assignRolesObj.setRole_name(role_name);
                        connectionDataSource.releaseConnectionPool(clientConnect);
                        callback(assignRolesObj);
                    }
                });
        });
    },

    saveAssignRoles: function (assignRolesObject, callback) {
        //insert query to insert users n roles into user_role table
        var self = this;
        var insertUserRoleQuery = "insert into "+dbTableName.mfiPersonnelRole+" (role_id,personnel_id) values('" + assignRolesObject.getSelected_role_id() + "','" + assignRolesObject.getSelected_user_id() + "')";
        connectionDataSource.getConnection(function (clientConnect) {
            clientConnect.query(insertUserRoleQuery,
                function postCreate(err) {
                    connectionDataSource.releaseConnectionPool(clientConnect);
                    if (!err) {
                        customlog.info("userID and RoleID inserted successfully");
                        callback();
                    } else {
                        customlog.error(err);
                        callback();
                    }
                }
            );
        });
    },

    saveManageRoles: function (manageRolesObj, callback) {
        var self = this;
        var insertRolesQuery = "INSERT INTO "+dbTableName.iklantRole+" (tenant_id,role_name,role_description,created_date) VALUES('" + manageRolesObj.getTenantId() + "','" + manageRolesObj.getRoleName() + "','" + manageRolesObj.getRoleDescName() + "',NOW() + INTERVAL 5 HOUR + INTERVAL 30 MINUTE)";
        connectionDataSource.getConnection(function (clientConnect) {
            clientConnect.query(insertRolesQuery,
                function postCreate(err) {
                    if (err) {
                        connectionDataSource.releaseConnectionPool(clientConnect);
                        customlog.error(err);
                    }
                }
            );

            var selectMaxRoleIdQuery = "select max(role_id) as currentRoleId from "+dbTableName.iklantRole+"  where tenant_id=" + manageRolesObj.getTenantId();
            var MaxRoleId = 0;
            var operationsChecked = new Array();
            operationsChecked = manageRolesObj.getCheckedValues().split(",");
            clientConnect.query(selectMaxRoleIdQuery,
                function selectCb(err, results, fields) {
                    if (err) {
                        connectionDataSource.releaseConnectionPool(clientConnect);
                        customlog.error(err);
                    }
                    else {
                        MaxRoleId = results[0].currentRoleId;
                        customlog.info("MAx = " + results[0].currentRoleId);
                        for (i = 0; i < operationsChecked.length; i++) {
                            var insertRoleOperationQuery = "insert into "+dbTableName.iklantRoleOperation+" (role_id,operation_id) values('" + MaxRoleId + "','" + operationsChecked[i] + "') ";
                            clientConnect.query(insertRoleOperationQuery, function postCreate(err) {
                                if (err){
                                    connectionDataSource.releaseConnectionPool(clientConnect);
                                    customlog.error(err);
                                    callback();
                                }
                            });
                        }
                        connectionDataSource.releaseConnectionPool(clientConnect);
                        callback();
                    }
                }
            );
        });
    },

    retrieveUserDetailsDataModel: function (userName, emailId, callback) {
        var userDetailsQuery = "SELECT * FROM " + dbTableName.iklantUsers + " WHERE user_name = '" + userName + "' AND email_id = '" + emailId + "' AND active_indicator = 1";
        connectionDataSource.getConnection(function (clientConnect) {
            clientConnect.query(userDetailsQuery,function (err, clientDetails) {
                connectionDataSource.releaseConnectionPool(clientConnect);
                if(err){
                    callback("failure");
                }
                else{
                    callback("success",clientDetails);
                }
            });
        });
    },
    encryptUserDetailsDataModel: function (userName,callback) {
        var userDetailsQuery = "SELECT * FROM " + dbTableName.iklantUsers +" WHERE user_name = '"+userName+"'";
        connectionDataSource.getConnection(function (clientConnect) {
            clientConnect.query(userDetailsQuery,function (err, clientDetails) {
                connectionDataSource.releaseConnectionPool(clientConnect);
                if(err){
                    callback("failure",clientDetails);
                }
                else{
                    callback("success",clientDetails);
                }
            });
        });
    },

    updateUserDetailsDataModel: function (user_id, userName, oldPassword, newPassword, callback) {
        var userQuery = "UPDATE " + dbTableName.iklantUsers + " SET password = '" + newPassword + "', password_changed = 0 WHERE user_name = '" + userName + "' AND user_id = " + user_id;
        connectionDataSource.getConnection(function (clientConnect) {
            clientConnect.query(userQuery,function (err) {
                connectionDataSource.releaseConnectionPool(clientConnect);
                if(err){
                    callback("failure");
                }
                else{
                    callback("success");
                }
            });
        });
    },

    updateCustomUserDetailsDataModel: function (user_id, userName, newPassword, callback) {
        var userQuery = "UPDATE " + dbTableName.iklantUsers + " SET password = '" + newPassword + "', user_name = '"+userName+"' WHERE user_id = " + user_id;
        connectionDataSource.getConnection(function (clientConnect) {
            clientConnect.query(userQuery,function (err) {
                connectionDataSource.releaseConnectionPool(clientConnect);
                if(err){
                    callback("failure");
                }
                else{
                    callback("success");
                }
            });
        });
    },

    validateoldPasswordDatamodel : function(userId,encrptedOldPassword,callback){
        var validateOldPasswordQuery = "SELECT password from "+ dbTableName.iklantUsers + " WHERE user_id = "+userId+" AND password = '"+encrptedOldPassword+"'";
        customlog.info(validateOldPasswordQuery);
        connectionDataSource.getConnection(function (clientConnect) {
            clientConnect.query(validateOldPasswordQuery,function (err,result) {
                connectionDataSource.releaseConnectionPool(clientConnect);
                if (err) {
                    customlog.error(err);
                    callback("error");
                }
                else{
                    if(result.length == 0){
                        callback("old password failure");
                    }else if(result.length == 1){
                        callback("old password success");
                    }
                }
            });
        });
    },

    //Added by sathishKumar 008 for Change Password
    changePasswordDataModelCall : function(userId,userName,encyptedOldPassword,encyptedNewPassword,callback){
        var constantsObj = this.constants;
        var updateQuery = "UPDATE " + dbTableName.iklantUsers + " SET password = '" + encyptedNewPassword + "', password_changed = 1 WHERE user_id = " + userId ;
        customlog.info(updateQuery);
        connectionDataSource.getConnection(function (clientConnect) {
            clientConnect.beginTransaction(function(err){
                if(err){
                    throw err;
                }
                clientConnect.query(updateQuery,function (err,result) {
                    if (err) {
                        clientConnect.rollback(function(){
                            throw err;
                            customlog.error(err);
                            connectionDataSource.releaseConnectionPool(clientConnect);
                            callback("failure");
                        });
                    }
                    else{
                        clientConnect.commit(function (err){
                           if(err){
                               clientConnect.rollback(function(){
                                    throw err;
                               });
                           }
                            console.log("Transaction Has been completed");
                            connectionDataSource.releaseConnectionPool(clientConnect);
                            callback("success");
                        });
                    }
                });
            });
        });
    },
    listExistingReportsDataModel:function(callback){
        var self = this;
        var retrieveListReport  = " SELECT ir.`report_id`, ir.`report_name`,rcc.`report_category_name`,rcc.`report_category_id`"+
            " FROM iklant_reports ir"+
            " INNER JOIN `report_category_custom` rcc ON rcc.`report_category_id` = ir.`report_category` "+
            " WHERE report_state = 1"+
            " ORDER BY `report_category_id` ";
        connectionDataSource.getConnection(function (clientConnect) {
            clientConnect.query(retrieveListReport,
                function selectCb(err, results, fields) {
                    connectionDataSource.releaseConnectionPool(clientConnect);
                    if (err) {
                        customlog.error(err);
                        callback(results,"failure");
                    }
                    else {
                        callback(results,"success");
                    }
                }
            );
        });
    },
    showAddReportViewDataModel:function(callback){
        var self = this;
        var retrieveAddReportParamsQuery  = "SELECT `entity_id` ,`lookup_id`, lookup_value FROM `iklant_lookup_value` WHERE `entity_id` = 33 AND lookup_id != 108";
        var retrieveAddReportRolesQuery  = "SELECT role_id,role_name FROM `iklant_role`";
        var retrieveReportCategoryQuery  = "SELECT `report_category_id`,`report_category_name` FROM `report_category_custom`";
        var ReportParams;
        var ReportRoles;
        var reportCategory;
        connectionDataSource.getConnection(function (clientConnect) {
            clientConnect.query(retrieveAddReportParamsQuery,
                function selectCb(err, results, fields) {
                    ReportParams = results;
                    if (err) {
                        connectionDataSource.releaseConnectionPool(clientConnect);
                        customlog.error(err);
                        callback(ReportParams,ReportRoles,reportCategory,"failure");
                    }
                    else {
                        clientConnect.query(retrieveAddReportRolesQuery,
                            function selectCb(err, results, fields) {
                                ReportRoles = results;
                                if (err) {
                                    connectionDataSource.releaseConnectionPool(clientConnect);
                                    customlog.error(err);
                                    callback(ReportParams,ReportRoles,reportCategory,"failure");
                                }
                                else {
                                    clientConnect.query(retrieveReportCategoryQuery,
                                        function selectCb(err, results, fields) {
                                            reportCategory = results;
                                            connectionDataSource.releaseConnectionPool(clientConnect);
                                            if (err) {
                                                customlog.error(err);
                                                callback(ReportParams,ReportRoles,reportCategory,"failure");
                                            }
                                            else {
                                                callback(ReportParams,ReportRoles,reportCategory,"success");
                                            }
                                        }
                                    );
                                }
                            }
                        );
                    }
                }
            );
        });
    },

    createDynamicReportDataModel : function(reportData,callback){
        var self = this;
        var createReportQuery = "INSERT INTO iklant_reports(`report_name`,`procedure_name`,`report_category`,`report_filters`,`report_fields`,`role_id`,`report_state`,`is_background`) VALUES('"+reportData[0]+"','"+reportData[1]+"',"+reportData[2]+",'"+reportData[3]+"','"+reportData[3]+"','"+reportData[4]+"',1,0)";
        connectionDataSource.getConnection(function (clientConnect) {
            connectionDataSource.releaseConnectionPool(clientConnect);
            clientConnect.query(createReportQuery, function postCreate(err) {
                if (err) {
                    callback("failure");
                }else{
                    callback("success");
                }
            });

        });
    },
    getWorkFlowTypeDataModel : function(callback){
        var workFlowTypeJson = {};
        var workFlowId = new Array();
        var workFlowName = new Array();
        var getWorkFlowQuery = " SELECT * FROM "+dbTableName.iklantWorkflowType;
        customlog.info("getWorkFlowQuery "+getWorkFlowQuery);
        connectionDataSource.getConnection(function (clientConnect) {
            clientConnect.query(getWorkFlowQuery, function (err, results) {
                connectionDataSource.releaseConnectionPool(clientConnect);
                if(err) {
                    customlog.error(" Error in getWorkFlowQuery "+err);
                    workFlowTypeJson.workFlowId = workFlowId;
                    workFlowTypeJson.workFlowName = workFlowName;
                    callback(workFlowTypeJson);
                } else {
                    for(var i=0;i<results.length;i++){
                        workFlowId[i] = results[i].iklant_workflow_type_id;
                        workFlowName[i] = results[i].workflow_name;
                    }
                    workFlowTypeJson.workFlowId = workFlowId;
                    workFlowTypeJson.workFlowName = workFlowName;
                    callback(workFlowTypeJson);
                }
            });
        });
    },
    getLedgerAccountsDataModel : function(callback){
        var ledgerQuery = "SELECT gl.`glcode_id` AS gl_code,c.`coa_name` AS gl_code_value FROM `coahierarchy` co "+
            "INNER JOIN coa c ON c.`coa_id` = co.`coa_id` "+
            "INNER JOIN gl_code gl ON gl.`glcode_id` = c.`glcode_id` "+
            "INNER JOIN `lookup_value` lv ON lv.`lookup_name` = gl.`glcode_value` "+
            "WHERE co.parent_coaid  = 6 AND lv.`entity_id` = 103";
        customlog.info("ledgerQuery "+ledgerQuery);
        connectionDataSource.getConnection(function (clientConnect) {
            clientConnect.query(ledgerQuery, function (err, results) {
                connectionDataSource.releaseConnectionPool(clientConnect);
                if(err) {
                    customlog.error(" Error in ledgerQuery "+err);
                }
                callback(results);
            });
        });
    }
};