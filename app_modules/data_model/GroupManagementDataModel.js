module.exports = groupManagementDataModel;

var path = require('path');
var rootPath = path.dirname(process.mainModule.filename);
var dbTableName = require(path.join(rootPath,"properties.json"));
var fs = require('fs');
var connectionDataSource = require(path.join(rootPath,"app_modules/data_model/DataSource"));
var customLog = require(path.join(rootPath,"logger/loggerConfig.js"))('GroupManagementDataModel.js');
var commonDTO = path.join(rootPath,"app_modules/dto/common");
var groupManagementDTO = path.join(rootPath,"app_modules/dto/group_management");
var dateUtils = require(path.join(rootPath,"app_modules/utils/DateUtils"));
var ComDataModel = require(path.join(rootPath,"app_modules/data_model/CommonDataModel"));

//Business Layer
function groupManagementDataModel(constants) {
    customLog.debug("Inside Group Management Data Access Layer");
    this.constants = constants;
}

groupManagementDataModel.prototype = {

    getKYCUploadStatusDataModel: function(groupId, callback){
        var self = this;
        var retrieveKYCStatusQuery =
            " SELECT  "+
                " `clientId`, "+
                "     SUM(CASE  "+
                "         WHEN dc.doc_type_id = 3 THEN "+
                "             CASE `clientId` "+
                "             WHEN 'client_id' THEN dc.client_id "+
                "         END "+
                "         ELSE  '' "+
                "     END)   AS 'ApplicationForm', "+
                "     SUM(CASE  "+
                "         WHEN dc.doc_type_id = 5 THEN "+
                "             CASE `clientId` "+
                "             WHEN 'client_id' THEN dc.client_id "+
                "         END "+
                "         ELSE  '' "+
                "     END)   AS 'Photo', "+
                "     SUM(CASE  "+
                "         WHEN dc.doc_type_id = 6 THEN "+
                "             CASE `clientId` "+
                "             WHEN 'client_id' THEN dc.client_id "+
                "         END "+
                "         ELSE  '' "+
                "     END)   AS 'MemID', "+
                "     SUM(CASE  "+
                "         WHEN dc.doc_type_id = 7 THEN "+
                "             CASE `clientId` "+
                "             WHEN 'client_id' THEN dc.client_id "+
                "         END "+
                "         ELSE  '' "+
                "     END)   AS 'MemAddress', "+
                "     SUM(CASE  "+
                "         WHEN dc.doc_type_id = 8 THEN "+
                "             CASE `clientId` "+
                "             WHEN 'client_id' THEN dc.client_id "+
                "         END "+
                "         ELSE  '' "+
                "     END)   AS 'GuarantorID', "+
                "     SUM(CASE  "+
                "         WHEN dc.doc_type_id = 9 THEN "+
                "             CASE `clientId` "+
                "             WHEN 'client_id' THEN dc.client_id "+
                "         END "+
                "         ELSE  '' "+
                "     END)   AS 'GuarantorAddress', "+
                "     SUM(CASE  "+
                "         WHEN dc.doc_type_id = 12 THEN "+
                "             CASE `clientId` "+
                "             WHEN 'client_id' THEN dc.client_id "+
                "         END "+
                "         ELSE  '' "+
                "     END)   AS 'OwnHouseReceipt' "+
                "  "+
                "     FROM "+
                "      (SELECT * "+
                " 	 FROM "+dbTableName.iklantClientDoc +"  "+
                " 	 WHERE client_id IN (SELECT client_id FROM "+dbTableName.iklantProspectClient +" WHERE group_id = "+groupId+") "+
                " 	 GROUP BY client_id,doc_type_id)dc "+
                "  INNER JOIN ( "+
                "        SELECT 'client_id' AS `clientId` "+
                "       ) AS value_columns "+
                "     GROUP BY `clientId`,dc.client_id ";
        connectionDataSource.getConnection(function (clientConnect) {
            clientConnect.query(retrieveKYCStatusQuery, function (err, results, fields) {
                connectionDataSource.releaseConnectionPool(clientConnect);
                if (err) {
                    customLog.error(err);
                }
                callback(results);
            });
        });
    },

    moveForDataEntryDataModel: function(groupId, callback){
        var self = this;
        var moveForDEQuery = " UPDATE `"+dbTableName.iklantProspectGroup+"` SET `status_id`=3 WHERE `group_id`="+groupId;
        connectionDataSource.getConnection(function (clientConnect) {
            clientConnect.query(moveForDEQuery, function (err, results, fields) {
                connectionDataSource.releaseConnectionPool(clientConnect);
                if (err) {
                    callback("failure");
                    customLog.error(err);
                }
                else{
                    callback("success");
                }
            });
        });
    },

    getGroupRecognitionTestDetailsDataModel: function(groupId,callback) {
        var self = this;
        var categoryId = new Array();
        var categoryDesc = new Array();
        var questionCategoryId = new Array();
        var question = new Array();
        var questionId = new Array();
        var constantsObj = this.constants;
        connectionDataSource.getConnection(function (clientConnect) {
            var retrieveQuery = "SELECT c_id, `description` FROM `" + dbTableName.iklantGrtCategories + "` ORDER BY c_id ASC";
            clientConnect.query(retrieveQuery, function (err, result) {
                if (err) {
                    customLog.info(retrieveQuery);
                    connectionDataSource.releaseConnectionPool(clientConnect);
                    callback("failure");
                } else {
                    for (var i = 0; i < result.length; i++) {
                        categoryId[i] = result[i].c_id;
                        categoryDesc[i] = result[i].description;
                    }
                    retrieveQuery = "SELECT `pt_id`,`category`,`check_point_ques` FROM `" + dbTableName.iklantGrtCheckPts + "` ORDER BY category ASC";
                    clientConnect.query(retrieveQuery, function (err, result) {
                        if (err) {
                            customLog.info(retrieveQuery);
                            connectionDataSource.releaseConnectionPool(clientConnect);
                            callback("failure");
                        } else {
                            for (var i = 0; i < result.length; i++) {
                                questionCategoryId[i] = result[i].category;
                                question[i] = result[i].check_point_ques;
                                questionId[i] = result[i].pt_id;
                            }
                            retrieveQuery = "SELECT COUNT(*) AS no_of_clients FROM " + dbTableName.iklantProspectClient + " WHERE group_id = "+groupId+" AND `status_id`="+constantsObj.getAppraisedStatus();
                            customLog.info(retrieveQuery);
                            clientConnect.query(retrieveQuery, function (err, result) {
                                connectionDataSource.releaseConnectionPool(clientConnect);
                                if (err) {
                                    customLog.error("error in "+retrieveQuery);
                                    callback("failure");
                                } else {
                                    var noOfClients = "";
                                    if(result.length > 0)
                                        noOfClients = result[0].no_of_clients;
                                    customLog.info("noOfClients in appraisal state: "+noOfClients);
                                    callback('success',categoryId,categoryDesc,questionCategoryId,question,questionId,noOfClients);
                                }
                            });
                        }
                    });
                }
            });
        });
    },

    insertRemarksForGRT: function(connectionDataSource,clientConnect,groupId,remarks,totalRating,isMoved,callback) {
        var remarksQuery = "INSERT INTO `" + dbTableName.iklantGrtGroupRemarks + "` (`group_id`,`created_date`,`remarks`,`total_rate`) VALUES ("+groupId+",NOW() + INTERVAL 5 HOUR + INTERVAL 30 MINUTE,'"+remarks+"',"+totalRating+");"
        customLog.info(remarksQuery);
        clientConnect.query(remarksQuery, function (err, result) {
            connectionDataSource.releaseConnectionPool(clientConnect);
            if (err) {
                callback("failure");
            } else {
                callback("success", isMoved);
            }
        });
    },

    saveRatingForGRTDataModel: function(groupId,totalRating,remarks, flowId, callback) {
        var self = this;
        var constantsObj = this.constants;
        if(remarks == undefined){
            remarks='';
        }
        self.ComDataModel = new ComDataModel(constantsObj);
        customLog.info(totalRating);
        connectionDataSource.getConnection(function (clientConnect) {
            /**
             *  As per client requirement, storing each question's answer is not required.
             *  But it might be used in the future.
             */
            if(totalRating > constantsObj.getMinimumRatingRequiredForGroupRecognitionTest()){
                self.ComDataModel.updateGroupStatusIdForNextOperation(groupId, constantsObj.getAppraisedStatus(), flowId, clientConnect, "", false, function(groupStatus){
                    if(groupStatus == 'success'){
                        self.ComDataModel.updateClientStatusIdForNextOperation("", groupId, constantsObj.getAppraisedStatus(), flowId, clientConnect, "", false, function(status){
                            if (status == 'success') {
                                self.insertRemarksForGRT(connectionDataSource,clientConnect,groupId,remarks,totalRating,1,callback);
                            } else {
                                callback(status);
                            }
                        });
                    }else{
                        callback(groupStatus);
                    }
                });
            } else {
                self.insertRemarksForGRT(connectionDataSource,clientConnect,groupId,remarks,totalRating,0,callback);
            }
        });
    },
    submitGrtLeaderDataModel :function(grtleaderdetails, callback){
        var self=this;
        var constantsObj = this.constants;
        var obj = JSON.parse(grtleaderdetails);
        customLog.info("grtleaderdetails......"+obj);
        connectionDataSource.getConnection(function (clientConnect) {
            for(var i=0;i<obj.length;i++){
                var grtLeaderQuery = "UPDATE iklant_prospect_client SET leader_id ="+obj[i].leader_name+" WHERE client_id = "+obj[i].client_id+" AND group_id ="+ obj[i].group_id;
                clientConnect.query(grtLeaderQuery, function (err, result) {
                    if (err) {
                        callback("failure");
                        connectionDataSource.releaseConnectionPool(clientConnect);
                    } else {
                        callback("success");
                    }
                });
            }
        });
    },
//Modified by Anitha Thilagar
    getClientNamesForLoanSanction: function (groupId, mifosCustomerId, callback) {
        var self=this;
        var constantsObj = this.constants;
        var clientNameArray = new Array();
        var clientIdArray = new Array();
        var subLeaderNameArray = new Array();
        var clientCodeArray = new Array();
        var groupNameForLoanSanction;
        var centerName;
        var thisclientId = 0;
        var disbDate;
        var globalAccountNum;

        var retrieveClientListQuery = "SELECT pg.group_name,pg.center_name,ipc.client_id,ipc.client_name,ipc.client_global_number " +
            " FROM "+dbTableName.iklantProspectClient+" ipc "+
            "INNER JOIN "+dbTableName.iklantMifosMapping+" imm ON imm.group_id = ipc.group_id "+
            "INNER JOIN "+dbTableName.iklantProspectGroup+" pg ON pg.group_id = ipc.group_id "+
            "INNER JOIN account acc ON acc.customer_id = imm.mifos_client_customer_id "+
            "INNER JOIN loan_account la ON la.account_id = acc.account_id "+
            "WHERE la.parent_account_id = (SELECT MAX(account_id) FROM account WHERE customer_id = "+mifosCustomerId+" GROUP BY customer_id) " +
            "AND mifos_customer_id = "+mifosCustomerId+" AND ipc.status_id IN (" + constantsObj.getSynchronizedGroupsStatus() + "," + constantsObj.getAuthorizedStatus()+") GROUP BY ipc.client_id ORDER BY client_global_number";
        customLog.info("retrieveClientListQuery : " + retrieveClientListQuery);
        connectionDataSource.getConnection(function (clientConnect) {
            clientConnect.query(retrieveClientListQuery, function selectCb(err, results, fields) {
                if (err) {
                    connectionDataSource.releaseConnectionPool(clientConnect);
                    customLog.error(err);
                } else {
                    var subLeaderQuery = " SELECT sub_leader_global_number,client_name FROM "+dbTableName.iklantProspectClient+" pc WHERE group_id = "+groupId+" AND sub_leader_global_number IS NOT NULL  AND pc.status_id NOT IN ("+constantsObj.getRejectedPriliminaryVerification()+","+constantsObj.getRejectedAppraisal()+","+constantsObj.getRejectedFieldVerification()+","+constantsObj.getRejectedLoanSanction()+","+constantsObj.getRejectedCreditBureauAnalysisStatusId()+","+
                        constantsObj.getRejectedInNextLoanPreCheck()+","+constantsObj.getRejectedWhileIdleGroupsStatusId()+") ORDER BY sub_leader_global_number";
                    customLog.info("subLeaderQuery : " + subLeaderQuery);
                    clientConnect.query(subLeaderQuery, function selectCb(error, leaderResults, fields) {
                        if(error){
                            connectionDataSource.releaseConnectionPool(clientConnect);
                            customLog.error(err);
                        } else{
                            for (var i in results) {
                                var fieldName = results[i];
                                groupNameForLoanSanction = fieldName.group_name;
                                centerName = fieldName.center_name;
                                var clientname = fieldName.client_name;
                                var clientId = fieldName.client_id;
                                clientNameArray.push(clientname);
                                clientIdArray.push(clientId);
                                clientCodeArray.push(fieldName.client_global_number);
                                var subLeader = "";
                                for (var j in leaderResults) {
                                    var clientGlobalCode = fieldName.client_global_number;
                                    if(clientGlobalCode.match(leaderResults[j].sub_leader_global_number)){
                                        subLeader = leaderResults[j].client_name;
                                    }
                                }
                                subLeaderNameArray.push(subLeader);
                            }
                            var retrieveProductCategoryQuery = "SELECT prd_offering_id,prd_offering_name FROM prd_offering " +
                                "WHERE prd_offering_id= (SELECT MAX(prd_offering_id) FROM loan_account lo " +
                                "INNER JOIN account ac ON lo.account_id = ac.account_id INNER JOIN customer cu ON cu.customer_id = ac.customer_id " +
                                "WHERE cu.customer_id=" + mifosCustomerId + ")";

                            var productCategoryId = new Array();
                            var ProductCategoryType = new Array();
                            customLog.info("retrieveProductCategoryQuery" + retrieveProductCategoryQuery);
                            clientConnect.query(retrieveProductCategoryQuery,function selectCb(err, results, fields) {
                                if (err) {
                                    connectionDataSource.releaseConnectionPool(clientConnect);
                                    customLog.error(err);
                                } else {
                                    for (var i in results) {
                                        var fieldName = results[i];
                                        productCategoryId[i] = fieldName.prd_offering_id;
                                        ProductCategoryType[i] = fieldName.prd_offering_name;
                                    }
                                }
                                customLog.info("productCategoryId[i]" + productCategoryId);
                                customLog.info("ProductCategoryType[i]" + ProductCategoryType);
                                var disbAmount, interestRateValue, recurrenceType;
                                var disbQuery = "SELECT `disbursement_date`,a.global_account_num,la.loan_amount,la.interest_rate,rt.recurrence_name FROM `loan_account` la " +
                                    "INNER JOIN account a ON la.account_id = a.account_id INNER JOIN recurrence_detail rd ON rd.meeting_id = la.meeting_id " +
                                    "INNER JOIN recurrence_type rt ON rt.recurrence_id = rd.recurrence_id WHERE  a.customer_id = " + mifosCustomerId + " AND account_type_id = 1 AND a.account_state_id IN (5,9) order by a.account_id desc";
                                clientConnect.query(disbQuery,function selectCb(err, results, fields) {
                                    connectionDataSource.releaseConnectionPool(clientConnect);
                                    if (err) {
                                        customLog.error(err);
                                    } else {
                                        if(results.length>0){
                                            disbDate = results[0].disbursement_date;
                                            globalAccountNum = results[0].global_account_num;
                                            disbAmount = results[0].loan_amount;
                                            interestRateValue = results[0].interest_rate;
                                            recurrenceType = results[0].recurrence_name;
                                            callback(groupId, thisclientId, clientNameArray, groupNameForLoanSanction, clientIdArray,
                                                productCategoryId, ProductCategoryType, disbDate, globalAccountNum, disbAmount, interestRateValue, recurrenceType,subLeaderNameArray,clientCodeArray,centerName);
                                        }
                                        else{
                                            callback(groupId, thisclientId, clientNameArray, groupNameForLoanSanction, clientIdArray,
                                                productCategoryId, ProductCategoryType, disbDate, globalAccountNum, disbAmount, interestRateValue, recurrenceType,subLeaderNameArray,clientCodeArray,centerName);
                                        }
                                    }
                                });
                            });
                        }
                    });
                }
            });
        });
    },

    loanSummaryDataModel: function(mifosCustomerId, callback){
        var self=this;
        var constantsObj = this.constants;
        var accountId = new Array();
        var origPrincipal = new Array();
        var origInterest = new Array();
        var principalPaid = new Array();
        var interestPaid = new Array();
        var principalOutstanding = new Array();
        var interestOutstanding = new Array();
        var disbursementDate = new Array();
        var interestRate = new Array();
        var noOfInstallments = new Array();
        var moment = require('moment');
        var loanSummaryQuery = " SELECT la.parent_account_id,ls.account_id,ROUND(SUM(ls.orig_principal)) AS orig_principal,ROUND(SUM(ls.orig_interest)) AS orig_interest,ROUND(SUM(ls.principal_paid)) AS principal_paid,"+
            "ROUND(SUM(ls.interest_paid)) AS interest_paid,ROUND(SUM(ls.orig_principal-ls.principal_paid)) AS principal_outstanding,ROUND(SUM(ls.orig_interest-ls.interest_paid)) AS interest_outstanding,"+
            "la.disbursement_date,la.interest_rate,la.no_of_installments FROM loan_account la INNER JOIN loan_summary ls ON ls.`account_id`=la.`account_id`"+
            "INNER JOIN account a ON a.`account_id`= la.`parent_account_id` WHERE a.`customer_id`="+mifosCustomerId+" AND a.`account_state_id` IN (5,6,9) GROUP BY la.`parent_account_id` ORDER BY la.`parent_account_id` DESC;";
        customLog.info("loanSummaryQuery"+loanSummaryQuery);
        connectionDataSource.getConnection(function(clientConnect){
            clientConnect.query(loanSummaryQuery,function selectCb(err,results,fields){
                connectionDataSource.releaseConnectionPool(clientConnect);
                if(err){
                    customLog.error(err);
                } else {
                    for (var i in results) {
                        accountId[i] = results[i].account_id;
                        origPrincipal[i] = results[i].orig_principal;
                        origInterest[i] = results[i].orig_interest;
                        principalPaid[i] = results[i].principal_paid;
                        interestPaid[i] = results[i].interest_paid;
                        principalOutstanding[i] = results[i].principal_outstanding;
                        interestOutstanding[i] = results[i].interest_outstanding;
                        disbursementDate[i] = moment(results[i].disbursement_date).format('MMMM Do YYYY');
                        //groupListDashboardJsonObj.createdDate = moment(results[i].created_date).format('MMMM Do YYYY, h:mm:ss');
                        interestRate[i] = results[i].interest_rate;
                        noOfInstallments[i] = results[i].no_of_installments;
                    }
                    callback(accountId,origPrincipal,origInterest,principalPaid,interestPaid,principalOutstanding,interestOutstanding,disbursementDate,
                    interestRate,noOfInstallments);
                }
            });
        });
    },

    getClientNamesForRejectedGroups: function (groupId, callback) {
        var self=this;
        var constantsObj = this.constants;
        var clientNameArray = new Array();
        var clientIdArray = new Array();
        var groupNameForRejectedGroups;
        var rejectedStage;
        var centername;
        var active_clients;
        var thisclientId = 0;
        var retrieveClientListForRejectedGroups = "SELECT gp.*,ac.* FROM " +
            "(SELECT pg.group_name,pc.client_id,pc.client_name,IF(ps.`status_id`= "+constantsObj.getRejectedAppraisal()+",IF(pg.`rejection_remarks` = 'Rejected in Loan Authorization', " +
            "'Rejected in Loan Authorization',ps.`status_desc`),ps.`status_desc`) AS status_desc,"+
            "pg.center_name,pg.group_id FROM "+dbTableName.iklantProspectGroup+" pg " +
            "LEFT JOIN "+dbTableName.iklantProspectClient+" pc ON pc.group_id = pg.group_id  " +
            "INNER JOIN "+dbTableName.iklantProspectStatus+" ps ON ps.status_id = pg.status_id " +
            "WHERE pg.group_id = " + groupId + " " +
            /*"and pc.status_id "+
             "NOT IN("+constantsObj.getRejectedCreditBureauAnalysisStatusId()+") "+*/
            "GROUP BY pc.client_id)gp " +
            "LEFT JOIN " +
            "(SELECT COUNT(pc.client_id) AS active_clients ,pc.group_id  FROM "+dbTableName.iklantProspectClient+" pc " +
            "INNER JOIN "+dbTableName.iklantProspectGroup+" pg ON pg.group_id = pc.group_id " +
            "WHERE pc.status_id NOT IN (" + constantsObj.getRejectedPriliminaryVerification() + ", " +
            "" + constantsObj.getRejectedCreditBureauAnalysisStatusId() + ", " +
            "" + constantsObj.getRejectedFieldVerification() + "," + constantsObj.getRejectedAppraisal() + ") " +
            "GROUP BY pc.group_id) ac ON " +
            "ac.group_id = gp.group_id";

        customLog.info("retrieveClientListForRejectedGroups : " + retrieveClientListForRejectedGroups);
        connectionDataSource.getConnection(function (clientConnect) {
            clientConnect.query(retrieveClientListForRejectedGroups,
                function selectCb(err, results, fields) {
                    connectionDataSource.releaseConnectionPool(clientConnect);
                    if (err) {
                        customLog.error(err);
                    } else {
                        for (var i in results) {
                            var fieldName = results[i];
                            groupNameForRejectedGroups = fieldName.group_name;
                            var clientname = fieldName.client_name;
                            var clientId = fieldName.client_id;
                            centername = fieldName.center_name;
                            rejectedStage = fieldName.status_desc;
                            active_clients = fieldName.active_clients;
                            clientNameArray.push(clientname);
                            clientIdArray.push(clientId);
                        }
                        callback(thisclientId, groupNameForRejectedGroups, clientNameArray, clientIdArray, rejectedStage, centername, active_clients);
                    }

                }
            );
            }
        );
    },

    updateRejectedClientStatus: function(groupId, clientId, clientStatus, roleId, remarksForReintiate, clientConnect, callback){
        var self=this;
        var constantsObj = this.constants;
        if(roleId == constantsObj.getSMHroleId() && dbTableName.isRMApprovalRequiredForReinitiate == true) {
            var reinitiateClientQuery = "UPDATE " + dbTableName.iklantRejectedClientStatus + " SET is_rm_reinitiated = 1 " +
                "WHERE group_id = " + groupId + " AND client_id = " + clientId;
        }
        else{
            var reinitiateClientQuery = "DELETE FROM "+ dbTableName.iklantRejectedClientStatus + " WHERE client_id = "+clientId;
        }
        if(clientConnect !=null){
            clientConnect.query(reinitiateClientQuery, function postCreate(err) {
                if (err) {
                    clientConnect.rollback(function(){
                        connectionDataSource.releaseConnectionPool(clientConnect);
                        customLog.error(err);
                        callback();
                    });
                }
                else {
                    if(roleId == constantsObj.getSMHroleId()){
                        connectionDataSource.releaseConnectionPool(clientConnect);
                        callback();
                    } else {
                        var insertClientQuery = "INSERT INTO " + dbTableName.iklantRejectedClientStatus + " (`group_id`,`client_id`,`rejected_status_id`," +
                            "`is_bm_reinitiated`) VALUES (" + groupId + "," + clientId + "," + clientStatus + ",1);";
                        clientConnect.query(insertClientQuery, function postCreate(err) {
                            if (err) {
                                clientConnect.rollback(function(){
                                    connectionDataSource.releaseConnectionPool(clientConnect);
                                    customLog.error(err);
                                    callback();
                                });
                            } else {
                                connectionDataSource.releaseConnectionPool(clientConnect);
                                callback();
                            }
                        });
                    }
                }
            });
        }
    },

    //To get the client,group and status info for the mail notification to the reinitiated client
    getReinitiatedClientDetailsDataModel: function(officeId,clientId,callback){
        var reinitiatedClientDetailsQuery = " SELECT io.`office_name`,ipg.`group_name`,ipg.`center_name`,CONCAT(ipc.`client_name`,' ',ipc.`client_last_name`) AS client_name,"+
            " IF(ipc.`status_id` = 17 AND ipc.`remarks_for_rejection` = 'Rejected in Loan Authorization','Rejected in Loan Authorization',ips.`status_name`) AS rejected_status,"+
            " ips2.`status_desc` AS current_status,ipc.`remarks_for_reintiate` AS bm_remarks FROM "+dbTableName.iklantProspectClient+" ipc INNER JOIN "+dbTableName.iklantProspectGroup+" ipg ON ipg.`group_id` = ipc.`group_id`"+
            " INNER JOIN "+dbTableName.iklantRejectedClientStatus+" rcs ON rcs.`client_id` = ipc.`client_id` INNER JOIN "+dbTableName.iklantProspectStatus+" ips ON ips.`status_id` = rcs.`rejected_status_id`"+
            " INNER JOIN "+dbTableName.iklantProspectStatus+" ips2 ON ips2.`status_id` = ipc.`status_id` INNER JOIN "+dbTableName.iklantOffice+" io ON io.office_id = ipg.office_id WHERE ipc.`client_id` = "+clientId+" AND io.`office_id`="+officeId+";";
        customLog.info("reinitiatedClientDetailsQuery: "+reinitiatedClientDetailsQuery);
        connectionDataSource.getConnection(function(clientConnect){
            clientConnect.query(reinitiatedClientDetailsQuery,function(err, results){
                connectionDataSource.releaseConnectionPool(clientConnect);
                if (err) {
                    customLog.error(err);
                    callback(results);
                } else {
                    callback(results);
                }
            });
        });
    },

    //The rejected clients will be reinitiated by BM first
    //Then the RM can either reject or reinitiate the clients
    //Reinitiate the clients to the group status only if the group status is in FV (status = 7) or before that
    //If the group status is after the FV (status = 7) then move the rejected clients and the active clients to FV (status = 6)
    //If the group is rejected then the group must be reinitiated before reinitiating the client

    reintiateClientDataModel: function(tenantId, clientId, remarksForReintiate, groupStatusID, clientStatus, groupId, roleId, callback){
        var self = this;
        var constantsObj = this.constants;

        var preliminaryVerified = constantsObj.getPreliminaryVerified(); //2
        var preliminaryRejected = constantsObj.getRejectedPriliminaryVerification(); //14

        //Status Id before Field Verification
        var cgtStatus = constantsObj.getCGTStatus();                                               //30
        var creditBureauStatus = constantsObj.getCreditBureauAnalysedStatus();                     //5
        var assignedFOStatus = constantsObj.getAssignedFO();                                       //6
        var rejectedInCreditBureauStatus = constantsObj.getRejectedCreditBureauAnalysisStatusId(); //15
        var rejectedInFieldVerificationStatus = constantsObj.getRejectedFieldVerification();       //16

        //Status Id after Field Verification
        var bankDetailsUpdateStatus = constantsObj.getBankDetailsUpdateStatus();                   //31
        var grtStatus = constantsObj.getGroupRecognitionTested();                                  //20
        var appraisalStatus = constantsObj.getFieldVerified();                                     //7
        var rejectedInAppraisal = constantsObj.getRejectedAppraisal();                             //17
        var reinitiatedStatusDisplay,statusId;

        connectionDataSource.getConnection(function(clientConnect){
            clientConnect.beginTransaction(function(err){
                if(err){
                    customLog.error(err);
                }
                if((roleId == constantsObj.getSMHroleId() && dbTableName.isRMApprovalRequiredForReinitiate == true) || (roleId == constantsObj.getBMroleId() && dbTableName.isRMApprovalRequiredForReinitiate == false)){
                    if(((groupStatusID == cgtStatus || groupStatusID == creditBureauStatus || groupStatusID == assignedFOStatus)) || ((groupStatusID == appraisalStatus || groupStatusID == grtStatus || groupStatusID == bankDetailsUpdateStatus)
                        && (clientStatus == rejectedInAppraisal || clientStatus == rejectedInFieldVerificationStatus))){
                        reinitiatedStatusDisplay = groupStatusID ==  cgtStatus ? "Client reinitiated successfully and moved to CGT" :
                            groupStatusID == creditBureauStatus ? "Client reinitiated successfully and moved to Assigning FO" :
                                groupStatusID == appraisalStatus ? "Client reinitiated successfully and moved to appraisal" :
                                groupStatusID == grtStatus ? "Client reinitiated successfully and moved to GRT" :
                                groupStatusID == bankDetailsUpdateStatus ? "Client reinitiated successfully and moved to bank details update"
                                    : "Client reinitiated successfully and moved to Field Verification";
                        statusId = groupStatusID;
                        var reintiateClientQuery = "UPDATE " + dbTableName.iklantProspectClient + " pc " +
                            "SET pc.status_id = "+ statusId + "," +
                            "pc.is_overdue = " + constantsObj.getActiveIndicatorFalse() + ", " +
                            "remarks_for_reintiate = '" + remarksForReintiate + "', " +
                            "pc.updated_date = NOW() + INTERVAL 5 HOUR + INTERVAL 30 MINUTE WHERE  pc.client_id = " + clientId + "";
                        clientConnect.query(reintiateClientQuery,function(err,results){
                            if (err) {
                                clientConnect.rollback(function(){
                                    connectionDataSource.releaseConnectionPool(clientConnect);
                                    customLog.error(err);
                                    clientConnect = null;
                                    callback("Unable to proceed");
                                });
                            } else {
                                self.updateRejectedClientStatus(groupId, clientId, clientStatus, roleId, remarksForReintiate, clientConnect, function(){
                                    clientConnect.commit(function(err){
                                        if(err){
                                            clientConnect.rollback(function(){
                                                connectionDataSource.releaseConnectionPool(clientConnect);
                                            });
                                        }
                                        callback(reinitiatedStatusDisplay);
                                    });
                                });
                            }
                        });
                    } else if(groupStatusID == appraisalStatus){
                        statusId = assignedFOStatus;
                        reinitiatedStatusDisplay = "Client reinitiated successfully and moved to Field verification";
                        var reintiateClientQuery = "UPDATE " + dbTableName.iklantProspectClient + " pc " +
                            "SET pc.status_id = "+ statusId + "," +
                            "pc.is_overdue = " + constantsObj.getActiveIndicatorFalse() + ", " +
                            "remarks_for_reintiate = '" + remarksForReintiate + "', " +
                            "pc.updated_date = NOW() + INTERVAL 5 HOUR + INTERVAL 30 MINUTE WHERE  pc.client_id = " + clientId + "";
                        clientConnect.query(reintiateClientQuery,function(err,results){
                            if (err) {
                                clientConnect.rollback(function(){
                                    connectionDataSource.releaseConnectionPool(clientConnect);
                                    customLog.error(err);
                                    clientConnect = null;
                                    callback("Unable to proceed");
                                });
                            } else {
                                var reintiategroupquery = "UPDATE "+dbTableName.iklantProspectGroup+" SET status_id="+statusId+" WHERE group_id=" + groupId;
                                console.log(reintiategroupquery);
                                clientConnect.query(reintiategroupquery, function (err, results) {
                                    if (err) {
                                        connectionDataSource.releaseConnectionPool(clientConnect);
                                        customLog.error(err);
                                        callback("Unable to proceed");
                                    }
                                    else {
                                        self.updateRejectedClientStatus(groupId, clientId, clientStatus, roleId, remarksForReintiate, clientConnect, function(){
                                            clientConnect.commit(function(err){
                                                if(err){
                                                    clientConnect.rollback(function(){
                                                        connectionDataSource.releaseConnectionPool(clientConnect);
                                                    });
                                                }
                                                callback(reinitiatedStatusDisplay);
                                            });
                                        });
                                    }
                                });
                            }
                        });
                    } else {
                        reinitiatedStatusDisplay = clientStatus == preliminaryRejected ? "Client reinitiated successfully and moved to preliminary verification":
                            clientStatus == rejectedInFieldVerificationStatus ? "Client reinitiated successfully and moved to field verification" :
                                clientStatus == rejectedInAppraisal ? "Client reinitiated successfully and moved to appraisal" :
                                    clientStatus == rejectedInCreditBureauStatus ? "Client reinitiated successfully and moved to credit Bureau Analysis" :
                                        "Client reinitiated successfully";
                        var reintiateClientQuery = "UPDATE " + dbTableName.iklantProspectClient + " pc " +
                            "SET pc.status_id = IF(pc.status_id=" + preliminaryRejected + "," + preliminaryVerified + ", " +
                            "IF(pc.status_id=" + rejectedInFieldVerificationStatus + "," + assignedFOStatus + ", " +
                            "IF(pc.status_id=" + rejectedInAppraisal + "," + appraisalStatus + ", " +
                            "IF(pc.status_id=" + rejectedInCreditBureauStatus + "," + creditBureauStatus + ", " +
                            "IF(pc.status_id=" + constantsObj.getRejectedInNextLoanPreCheck() + "," + preliminaryVerified + ", " +
                            "IF(pc.status_id=" + constantsObj.getRejectedWhileIdleGroupsStatusId() + "," + rejectedInCreditBureauStatus + ",pc.status_id)))))), " +
                            "pc.is_overdue=" + constantsObj.getActiveIndicatorFalse() + ", " +
                            "remarks_for_reintiate = '" + remarksForReintiate + "', " +
                            "pc.updated_date = NOW() + INTERVAL 5 HOUR + INTERVAL 30 MINUTE WHERE  pc.client_id = " + clientId + "";
                        customLog.info("reintiateClientQuery in else" + reintiateClientQuery);
                        if(clientConnect !=null) {
                            clientConnect.query(reintiateClientQuery, function postCreate(err) {
                                if (err) {
                                    clientConnect.rollback(function () {
                                        connectionDataSource.releaseConnectionPool(clientConnect);
                                        customLog.error(err);
                                        clientConnect = null;
                                    });
                                }
                                else {
                                    self.updateRejectedClientStatus(groupId, clientId, clientStatus, roleId, remarksForReintiate, clientConnect, function () {
                                        clientConnect.commit(function (err) {
                                            if (err) {
                                                clientConnect.rollback(function () {
                                                    connectionDataSource.releaseConnectionPool(clientConnect);
                                                });
                                            }
                                            callback(reinitiatedStatusDisplay);
                                        });

                                    });
                                }
                            });
                        }
                    }
                } else{
                    self.updateRejectedClientStatus(groupId, clientId, clientStatus, roleId, remarksForReintiate, clientConnect, function(){
                        var reintiateClientQuery = "UPDATE " + dbTableName.iklantProspectClient + " pc " +
                            "SET remarks_for_reintiate = '" + remarksForReintiate + "', " +
                            "pc.updated_date = NOW() + INTERVAL 5 HOUR + INTERVAL 30 MINUTE WHERE  pc.client_id = " + clientId + "";
                        if(clientConnect !=null){
                            clientConnect.query(reintiateClientQuery, function postCreate(err) {
                                if (err) {
                                    clientConnect.rollback(function(){
                                        connectionDataSource.releaseConnectionPool(clientConnect);
                                        customLog.error(err);
                                        clientConnect = null;
                                    });
                                }
                                else {
                                    clientConnect.commit(function(err){
                                        if(err){
                                            clientConnect.rollback(function(){
                                                connectionDataSource.releaseConnectionPool(clientConnect);
                                            });
                                        }
                                        callback("Client re-initiated & Moved to Regional Manager approval");
                                    });
                                }
                            });
                        }
                    });
                }
            });
        });
    },

    /*reintiateClientDataModel: function (tenantId, clientId, remarksForReintiate, groupStatusID, clientStatus, groupId, roleId, callback) {
        var self=this;
        var constantsObj = this.constants;
        var preliminaryVerified = constantsObj.getPreliminaryVerified(); //2
        var preliminaryRejected = constantsObj.getRejectedPriliminaryVerification(); //14
        var fieldVerified = constantsObj.getFieldVerified(); //7
        var fieldRejected = constantsObj.getRejectedFieldVerification(); //16
        var appraisalRejected = constantsObj.getRejectedAppraisal(); //17
        var appraisedStatus = constantsObj.getAppraisedStatus();//7
        var creditBureauAnalysed = constantsObj.getCreditBureauAnalysedStatus(); //5
        var creditBureauRejected = constantsObj.getRejectedCreditBureauAnalysisStatusId(); //15
        var compulsoryGroupTraining = constantsObj.getCGTStatus(); //30
        var foAssigned = constantsObj.getAssignedFO(); //6

        var reinitiatedStatusDisplay;
        if (groupStatusID == preliminaryVerified) {
            reinitiatedStatusDisplay = "Client Reinitiated Succesfully and Moved To KYC Uploading";
        } else if (groupStatusID == constantsObj.getFieldVerified()) {
            reinitiatedStatusDisplay = "Client Reinitiated Succesfully and Moved To Appraisal";
        } else if (groupStatusID == constantsObj.getAppraisedStatus()) {
            reinitiatedStatusDisplay = "Client Reinitiated Succesfully  and Moved To Group recognition test";
        } else if (groupStatusID == constantsObj.getCreditBureauAnalysedStatus()) {
            reinitiatedStatusDisplay = "Client Reinitiated Succesfully  and Moved To Assign FO";
        } else if (groupStatusID == constantsObj.getAssignedFO()) {
            reinitiatedStatusDisplay = "Client Reinitiated Succesfully and Moved To Field Verification";
        } else if(groupStatusID == constantsObj.getPreliminaryVerified()){
            reinitiatedStatusDisplay = "Client Reinitiated Succesfully  and Moved To KYC uploading";
        } else if(groupStatusID == constantsObj.getDataVerificationOperationId()){
            reinitiatedStatusDisplay = "Client Reinitiated Succesfully  and Moved To Credit bureau analysis";
        } else if(groupStatusID == compulsoryGroupTraining){
            reinitiatedStatusDisplay = "Client Reinitiated Successfully and Moved To CGT";
        } else{
            reinitiatedStatusDisplay = "Client Reinitiated Succesfully  and Moved To Group recognition test";
        }

        connectionDataSource.getConnection(function (clientConnect) {
            clientConnect.beginTransaction(function(err){
               if(err){
                   customLog.error(err);
               }
                if(roleId == constantsObj.getSMHroleId()) {
                    var assessmentquery="SELECT * FROM "+dbTableName.iklantClientAssessment+" WHERE client_id="+clientId;
                    clientConnect.query(assessmentquery,function(err,results)
                    {
                       if(err)
                       {
                           connectionDataSource.releaseConnectionPool(clientConnect);
                           customLog.error(err);
                           callback("Unable to proceed");
                       }

                        else if(results.length == 0 && groupStatusID > fieldVerified)
                       {
                           //var statusId = groupStatusID == compulsoryGroupTraining ? compulsoryGroupTraining : constantsObj.getAssignedFO();
                           var statusId = foAssigned;
                           var reintiateClientQuery = "UPDATE " + dbTableName.iklantProspectClient + " pc " +
                               //"SET pc.status_id ="+constantsObj.getAssignedFO() + "," +
                               "SET pc.status_id = "+ statusId + "," +
                               "pc.is_overdue = " + constantsObj.getActiveIndicatorFalse() + ", " +
                               "remarks_for_reintiate = '" + remarksForReintiate + "', " +
                               "pc.updated_date = NOW() + INTERVAL 5 HOUR + INTERVAL 30 MINUTE WHERE  pc.client_id = " + clientId + "";
                           console.log(reintiateClientQuery);
                           clientConnect.query(reintiateClientQuery,function(err,results) {
                               if (err) {
                                   connectionDataSource.releaseConnectionPool(clientConnect);
                                   customLog.error(err);
                                   callback("Unable to proceed");
                               }
                               else {
                                   var reintiategroupquery = "UPDATE "+dbTableName.iklantProspectGroup+" SET status_id="+statusId+" WHERE group_id=" + groupId;
                                   console.log(reintiategroupquery);
                                   clientConnect.query(reintiategroupquery, function (err, results) {
                                       if (err) {
                                           connectionDataSource.releaseConnectionPool(clientConnect);
                                           customLog.error(err);
                                           callback("Unable to proceed");
                                       }
                                       else {
                                           var reintiateactivequery="SELECT GROUP_CONCAT(client_id) as client_id FROM "+dbTableName.iklantProspectClient +" WHERE group_id="+groupId
                                               +" AND status_id IN("+constantsObj.getAssignedFO()+","
                                               +constantsObj.getFieldVerified()+","
                                           +constantsObj.getNeedInformation()+","
                                           +constantsObj.getAppraisedStatus()+","
                                           +constantsObj. getGroupRecognitionTested()+","
                                           +constantsObj.getAuthorizedStatus()+")";
                                           console.log(reintiateactivequery);
                                           clientConnect.query(reintiateactivequery, function (err,results) {
                                               if(err)
                                               {
                                                   connectionDataSource.releaseConnectionPool(clientConnect);
                                                   customLog.error(err);
                                                   callback("Unable to proceed");
                                               }
                                               else{
                                                   var reintiateactiveclient="UPDATE "+dbTableName.iklantProspectClient+" SET status_id="+statusId+" WHERE client_id IN("+results[0].client_id+")";
                                                   console.log(reintiateactiveclient);
                                                   clientConnect.query(reintiateactiveclient, function (err) {
                                                       if (err) {
                                                           connectionDataSource.releaseConnectionPool(clientConnect);
                                                           customLog.error(err);
                                                           callback("Unable to proceed");
                                                       }
                                                       else
                                                       {
                                                           self.updateRejectedClientStatus(groupId, clientId, clientStatus, roleId, remarksForReintiate, clientConnect, function(){
                                                               clientConnect.commit(function(err){
                                                                   if(err){
                                                                       clientConnect.rollback(function(){

                                                                       });
                                                                   }
                                                                   callback(reinitiatedStatusDisplay);
                                                               });
                                                           });
                                                       }

                                                   });
                                               }
                                           });
                                       }
                                   });
                               }
                           });
                       }
                        else
                       {
                           if (groupStatusID == fieldVerified) {
                               var reintiateClientQuery = "UPDATE " + dbTableName.iklantProspectClient + " pc " +
                                   "SET pc.status_id = IF(pc.status_id=" + preliminaryRejected + "," + preliminaryVerified + ", " +
                                   "IF(pc.status_id=" + fieldRejected + "," + fieldVerified + ", " +
                                   "IF(pc.status_id=" + creditBureauRejected + "," + fieldVerified + ", " +
                                   "IF(pc.status_id=" + appraisalRejected + "," + fieldVerified + ", pc.status_id)))), " +
                                   "pc.is_overdue = " + constantsObj.getActiveIndicatorFalse() + ", " +
                                   "remarks_for_reintiate = '" + remarksForReintiate + "', " +
                                   "pc.updated_date = NOW() + INTERVAL 5 HOUR + INTERVAL 30 MINUTE WHERE  pc.client_id = " + clientId + "";
                               customLog.info("reintiateClientQuery in if" + reintiateClientQuery);
                               if(clientConnect !=null){
                                   clientConnect.query(reintiateClientQuery, function postCreate(err) {
                                       if (err) {
                                           clientConnect.rollback(function(){
                                               connectionDataSource.releaseConnectionPool(clientConnect);
                                               customLog.error(err);
                                               clientConnect = null;
                                           });
                                       }
                                       else{
                                           self.updateRejectedClientStatus(groupId, clientId, clientStatus, roleId, remarksForReintiate, clientConnect, function(){
                                               clientConnect.commit(function(err){
                                                   if(err){
                                                       clientConnect.rollback(function(){

                                                       });
                                                   }
                                                   callback(reinitiatedStatusDisplay);
                                               });
                                           });
                                       }
                                   });
                               }
                           } else {
                               var reintiateClientQuery = "UPDATE " + dbTableName.iklantProspectClient + " pc " +
                                   "SET pc.status_id = IF(pc.status_id=" + preliminaryRejected + "," + preliminaryVerified + ", " +
                                   "IF(pc.status_id=" + fieldRejected + "," + fieldVerified + ", " +
                                   "IF(pc.status_id=" + appraisalRejected + "," + appraisedStatus + ", " +
                                   "IF(pc.status_id=" + creditBureauRejected + "," + creditBureauAnalysed + ", " +
                                   "IF(pc.status_id=" + constantsObj.getRejectedInNextLoanPreCheck() + "," + preliminaryVerified + ", " +
                                   "IF(pc.status_id=" + constantsObj.getRejectedWhileIdleGroupsStatusId() + "," + creditBureauRejected + ",pc.status_id)))))), " +
                                   "pc.is_overdue=" + constantsObj.getActiveIndicatorFalse() + ", " +
                                   "remarks_for_reintiate = '" + remarksForReintiate + "', " +
                                   "pc.updated_date = NOW() + INTERVAL 5 HOUR + INTERVAL 30 MINUTE WHERE  pc.client_id = " + clientId + "";
                               customLog.info("reintiateClientQuery in else" + reintiateClientQuery);
                               if(clientConnect !=null){
                                   clientConnect.query(reintiateClientQuery, function postCreate(err) {
                                       if (err) {
                                           clientConnect.rollback(function(){
                                               connectionDataSource.releaseConnectionPool(clientConnect);
                                               customLog.error(err);
                                               clientConnect = null;
                                           });
                                       }
                                       else{
                                           self.updateRejectedClientStatus(groupId, clientId, clientStatus, roleId, remarksForReintiate, clientConnect, function(){
                                               clientConnect.commit(function(err){
                                                   if(err){
                                                       clientConnect.rollback(function(){

                                                       });
                                                   }
                                                   connectionDataSource.releaseConnectionPool(clientConnect);
                                                   callback(reinitiatedStatusDisplay);
                                               });

                                           });
                                       }
                                   });
                               }
                           }
                       }
                    });


                }
                else{
                    self.updateRejectedClientStatus(groupId, clientId, clientStatus, roleId, remarksForReintiate, clientConnect, function(){
                        var reintiateClientQuery = "UPDATE " + dbTableName.iklantProspectClient + " pc " +
                            "SET remarks_for_reintiate = '" + remarksForReintiate + "', " +
                            "pc.updated_date = NOW() + INTERVAL 5 HOUR + INTERVAL 30 MINUTE WHERE  pc.client_id = " + clientId + "";
                        if(clientConnect !=null){
                            clientConnect.query(reintiateClientQuery, function postCreate(err) {
                                if (err) {
                                    clientConnect.rollback(function(){
                                        connectionDataSource.releaseConnectionPool(clientConnect);
                                        customLog.error(err);
                                        clientConnect = null;
                                    });
                                }
                                else {
                                    clientConnect.commit(function(err){
                                        if(err){
                                            clientConnect.rollback(function(){

                                            });
                                        }
                                        connectionDataSource.releaseConnectionPool(clientConnect);
                                        callback("Client re-initiated & Moved to Regional Manager approval");
                                    });
                                }
                            });
                        }
                    });
                }
            });
        });
    },*/
    //sendBackClientDataModel:function(clientId,tenantId,roleId,groupStatus,clientStatus,groupId,callback){
    sendBackClientDataModel:function(clientId,tenantId,roleId,groupId,callback){
        var self=this;
        var constantsObj=this.constants;
        var sendbackselectQuery="select * from iklant_rejected_client_status where client_id="+clientId;
        connectionDataSource.getConnection(function (clientConnect) {
        clientConnect.query(sendbackselectQuery,
            function selectCb(err, results, fields) {
                //connectionDataSource.releaseConnectionPool(clientConnect);
                if (err) {
                    customLog.error(err);
                    callback("Update rejected status failed");
                } else if (results.length > 0) {
                    //callback("Client Moved to uploadNOC");
                    var sendbackQuery = "UPDATE iklant_rejected_client_status SET is_rm_reinitiated=-1 WHERE client_id=" + clientId;
                    customLog.info("sendbackQuery" + sendbackQuery);
                    clientConnect.query(sendbackQuery,
                        function selectCb(err, results, fields) {
                            connectionDataSource.releaseConnectionPool(clientConnect);
                            if (err) {
                                customLog.error(err);
                                callback("Update rejected status failed");
                            } else {
                                callback("Client Moved to uploadNOC");
                            }
                        });
                }else{
                    connectionDataSource.releaseConnectionPool(clientConnect);
                    callback("Update rejected status failed");
                }
                /*else{
                    var sendbackinsertQuery = "insert into iklant_rejected_client_status(group_id,client_id,is_bm_reinitiated,is_rm_reinitiated)" +
                        "values("+groupId+","+clientId+","+ 1+","+ -1+")";
                    customLog.info("sendbackinsertQuery" + sendbackQuery);
                    clientConnect.query(sendbackinsertQuery,
                        function selectCb(err, results, fields) {
                            //connectionDataSource.releaseConnectionPool(clientConnect);
                            if (err) {
                                customLog.error(err);
                            } else {
                                callback("Client Moved to uploadNOC");
                            }
                        });

                }*/
            });

        });
    },
    uploadnocdetailsmodelDataModel:function(groupId,requestedOperationId,callback){
        var clientCount;
        var constantsObj=this.constants;
        if(requestedOperationId == constantsObj.getAuthorizeGroupOperationId()) {
            var clientDetailsQuery = " SELECT COUNT(pc.client_id)AS upload_noc_client_count " +
                " FROM `iklant_rejected_client_status` ics " +
                " INNER JOIN iklant_prospect_client pc ON pc.client_id=ics.`client_id`" +
                " WHERE ics.`is_rm_reinitiated` =-1 AND ics.group_id=" + groupId +" AND pc.status_id = " +constantsObj.getGroupRecognitionTested();
            customLog.info('clientDetailsQuery: ' + clientDetailsQuery);
            connectionDataSource.getConnection(function (clientConnect) {
                clientConnect.query(clientDetailsQuery, function (err, results, fields) {
                    if (err) {
                        customLog.error(err);
                        connectionDataSource.releaseConnectionPool(clientConnect);
                        callback(clientCount);
                    } else {
                        connectionDataSource.releaseConnectionPool(clientConnect);
                        if (results.length > 0) {
                            clientCount = results[0].upload_noc_client_count;
                            callback(clientCount);
                        } else {
                            callback(clientCount);
                        }
                    }
                });
            });
        }
        else if(requestedOperationId == constantsObj.getAppraisalOperationId()){
            var appraisalclientDetailsQuery = " SELECT COUNT(pc.client_id)AS upload_noc_client_count " +
                " FROM `iklant_rejected_client_status` ics " +
                " INNER JOIN  iklant_prospect_client pc ON pc.client_id=ics.`client_id`" +
                " WHERE ics.`is_rm_reinitiated` = -1 AND ics.group_id = " + groupId +" AND" +
                " pc.status_id IN("+constantsObj.getRejectedAppraisal()+","+constantsObj.getRejectedFieldVerification()+","+constantsObj.getRejectedCreditBureauAnalysisStatusId()+")";
            customLog.info('appraisalclientDetailsQuery: ' + appraisalclientDetailsQuery);
            connectionDataSource.getConnection(function (clientConnect) {
                clientConnect.query(appraisalclientDetailsQuery, function (err, results, fields) {
                    if (err) {
                        customLog.error(err);
                        connectionDataSource.releaseConnectionPool(clientConnect);
                        callback(clientCount);
                    } else {
                        connectionDataSource.releaseConnectionPool(clientConnect);
                        if (results.length > 0) {
                            clientCount = results[0].upload_noc_client_count;
                            callback(clientCount);
                        } else {
                            callback(clientCount);
                        }
                    }
                });
            });
        }
    },
    populateGroupsDataModel: function (tenantId, officeId, userId, statusid, callback) {
        var self=this;
        var constantsObj = this.constants;
        var groupNameArray = new Array();
        var centerNameArray = new Array();
        connectionDataSource.getConnection(function (clientConnect) {
            if (statusid == constantsObj.getNewGroup() || statusid == constantsObj.getPreliminaryVerified() ) {
                var retrievePopulateGroupsQuery = "SELECT group_name,center_name FROM "+dbTableName.iklantProspectGroup+" " +
                    "WHERE office_id =" + officeId + " AND created_by =" + userId + " " +
                    "AND status_id = " + statusid + " ";
                clientConnect.query(retrievePopulateGroupsQuery, function selectCb(err, results, fields) {
                    connectionDataSource.releaseConnectionPool(clientConnect);
                    customLog.info(retrievePopulateGroupsQuery);
                    if (err) {
                        customLog.error(err);
                    } else {
                        for (var i in results) {
                            groupNameArray[i] = results[i].group_name;
                            centerNameArray[i] = results[i].center_name;
                        }
                        callback(groupNameArray, centerNameArray);
                    }
                });
            }else if (statusid == constantsObj.getAssignedFO()) {
                var retrievePopulateGroupsQuery = "SELECT group_name,center_name FROM "+dbTableName.iklantProspectGroup+" " +
                    "WHERE office_id =" + officeId + " AND assigned_to =" + userId + " " +
                    "AND status_id = " + statusid + " ";
                clientConnect.query(retrievePopulateGroupsQuery, function selectCb(err, results, fields) {
                    customLog.info(retrievePopulateGroupsQuery);
                    connectionDataSource.releaseConnectionPool(clientConnect);
                    if (err) {
                        customLog.error(err);
                    } else {
                        for (var i in results) {
                            groupNameArray[i] = results[i].group_name;
                            centerNameArray[i] = results[i].center_name;
                        }
                        callback(groupNameArray, centerNameArray);
                    }
                });
            }else {
                var retrievePopulateGroupsQuery = "SELECT group_name,center_name FROM "+dbTableName.iklantProspectGroup+" " +
                    "WHERE office_id =" + officeId + " AND status_id IN(" + statusid + ")  ";
                clientConnect.query(retrievePopulateGroupsQuery, function selectCb(err, results, fields) {
                    connectionDataSource.releaseConnectionPool(clientConnect);
                    if (err) {
                        customLog.error(err);
                    } else {
                        for (var i in results) {
                            groupNameArray[i] = results[i].group_name;
                            centerNameArray[i] = results[i].center_name;
                        }
                        callback(groupNameArray, centerNameArray);
                    }
                });
            }
        });
    },

    generateGroupsDataModel: function (tenantId, officeId, statusid, callback) {
        var self=this;
        var constantsObj = this.constants;
        var groupNameArray = new Array();
        var centerNameArray = new Array();
        var groupStatusArray = new Array();
        var clientCountArray = new Array();
        connectionDataSource.getConnection(function (clientConnect) {
            if (statusid == constantsObj.getNewGroup() || statusid == constantsObj.getPreliminaryVerified() ) {
                var retrievePopulateGroupsQuery = "SELECT pg.group_name,pg.center_name,IF(pg.is_idle=0,'Active','Idle') AS group_status,COUNT(pc.group_id) AS no_of_clients FROM "+dbTableName.iklantProspectGroup+" " +
                    " pg LEFT JOIN "+dbTableName.iklantProspectClient+" pc ON pg.group_id=pc.group_id WHERE pg.office_id =" + officeId + " AND pg.status_id = " + statusid + " " +
                    " GROUP BY pg.group_id";
                customLog.info("retrievePopulateGroupsQuery : " + retrievePopulateGroupsQuery);
                clientConnect.query(retrievePopulateGroupsQuery, function selectCb(err, results, fields) {
                    connectionDataSource.releaseConnectionPool(clientConnect);

                    if (err) {
                        customLog.error(err);
                    } else {
                        for (var i in results) {
                            groupNameArray[i] = results[i].group_name;
                            centerNameArray[i] = results[i].center_name;
                            groupStatusArray[i] = results[i].group_status;
                            clientCountArray[i] = results[i].no_of_clients;
                        }
                        callback(groupNameArray, centerNameArray,groupStatusArray,clientCountArray);
                    }
                });
            }else if (statusid == constantsObj.getAssignedFO()) {
                var retrievePopulateGroupsQuery = "SELECT pg.group_name,pg.center_name,IF(pg.is_idle=0,'Active','Idle') AS group_status,COUNT(pc.group_id) AS no_of_clients FROM "+dbTableName.iklantProspectGroup+" " +
                    " pg LEFT JOIN "+dbTableName.iklantProspectClient+" pc ON pg.group_id=pc.group_id WHERE pg.office_id =" + officeId + " AND pc.`status_id` NOT IN (14,15,16,17,18,21,25,26,29) AND pg.status_id = " + statusid + " GROUP BY pg.group_id";
                clientConnect.query(retrievePopulateGroupsQuery, function selectCb(err, results, fields) {
                    customLog.info("retrievePopulateGroupsQuery : " + retrievePopulateGroupsQuery);
                    connectionDataSource.releaseConnectionPool(clientConnect);
                    if (err) {
                        customLog.error(err);
                    } else {
                        for (var i in results) {
                            groupNameArray[i] = results[i].group_name;
                            centerNameArray[i] = results[i].center_name;
                            groupStatusArray[i] = results[i].group_status;
                            clientCountArray[i] = results[i].no_of_clients;
                        }
                        callback(groupNameArray, centerNameArray,groupStatusArray,clientCountArray);
                    }
                });
            }else {
                var retrievePopulateGroupsQuery = "SELECT pg.group_name,pg.center_name,IF(pg.is_idle=0,'Active','Idle') AS group_status,COUNT(pc.group_id) AS no_of_clients FROM "+dbTableName.iklantProspectGroup+" " +
                    " pg LEFT JOIN "+dbTableName.iklantProspectClient+" pc ON pg.group_id=pc.group_id WHERE pg.office_id =" + officeId + " AND pc.`status_id` NOT IN (14,15,16,17,18,21,25,26,29)  AND pg.status_id IN(" + statusid + ")  GROUP BY pg.group_id";
                clientConnect.query(retrievePopulateGroupsQuery, function selectCb(err, results, fields) {
                    customLog.info("retrievePopulateGroupsQuery : " + retrievePopulateGroupsQuery);
                    connectionDataSource.releaseConnectionPool(clientConnect);
                    if (err) {
                        customLog.error(err);
                    } else {
                        for (var i in results) {
                            groupNameArray[i] = results[i].group_name;
                            centerNameArray[i] = results[i].center_name;
                            groupStatusArray[i] = results[i].group_status;
                            clientCountArray[i] = results[i].no_of_clients;
                        }
                        callback(groupNameArray, centerNameArray,groupStatusArray,clientCountArray);
                    }
                });
            }
        });
    },

    populateRejectedGroupsDataModel: function (tenantId, officeId, userId, statusid, callback) {
        var self=this;
        var constantsObj = this.constants;
        var groupNameArray = new Array();
        var centerNameArray = new Array();
        var statusDescArray = new Array();
        var status = new Array();
        var retrievePopulateGroupsQuery = "SELECT pg.group_name,pg.center_name,ps.status_desc FROM "+dbTableName.iklantProspectGroup+" pg " +
            "INNER JOIN "+dbTableName.iklantProspectStatus+" ps ON ps.status_id = pg.status_id " +
            "WHERE pg.office_id =" + officeId + " AND pg.created_by = " + userId + " AND pg.status_id IN " ;
        if(statusid == 0){
            retrievePopulateGroupsQuery += "(" + constantsObj.getRejectedPriliminaryVerification() + ", " +
            "" + constantsObj.getRejectedFieldVerification() + "," + constantsObj.getRejectedAppraisal() + ", " + constantsObj.getRejectedLoanSanction() +", "+
            "" + constantsObj.getRejectedCreditBureauAnalysisStatusId() + ")";
        }
        if(statusid == 1){
            retrievePopulateGroupsQuery += "(" + constantsObj.getCreditBureauAnalysedStatus() + ", " +
                "" + constantsObj.getAssignedFO() + "," + constantsObj.getFieldVerified() + ", " + constantsObj.getAppraisedStatus() +", "+
                "" + constantsObj.getGroupRecognitionTested() + ","+ constantsObj.getAuthorizedStatus()+") and pg.is_idle =1";
        }
        connectionDataSource.getConnection(function (clientConnect) {
            clientConnect.query(retrievePopulateGroupsQuery, function selectCb(err, results, fields) {
                connectionDataSource.releaseConnectionPool(clientConnect);
                if (err) {
                    customLog.error(err);
                } else {
                    for (var i in results) {
                        groupNameArray[i] = results[i].group_name;
                        centerNameArray[i] = results[i].center_name;
                        statusDescArray[i] = results[i].status_desc;
                    }
                    callback(groupNameArray, centerNameArray, statusDescArray);
                }
            });
        });
    },

    generateRejectedGroupsDataModel: function (tenantId, officeId, statusid, callback) {
        var self=this;
        var constantsObj = this.constants;
        var groupNameArray = new Array();
        var centerNameArray = new Array();
        var statusDescArray = new Array();
        var status = new Array();
        var clientCountArray = new Array();
        var retrievePopulateGroupsQuery = "SELECT COUNT(pc.group_id) AS no_of_clients,pg.group_name,pg.center_name,ps.status_desc FROM "+dbTableName.iklantProspectGroup+" pg " +
            "LEFT JOIN "+dbTableName.iklantProspectClient+" pc ON pc.group_id = pg.group_id INNER JOIN "+dbTableName.iklantProspectStatus+" ps ON ps.status_id = pg.status_id " +
            "WHERE pg.office_id =" + officeId + " AND pg.status_id IN (" + constantsObj.getRejectedPriliminaryVerification() + ", " + constantsObj.getRejectedCreditBureauAnalysisStatusId()+
            "," + constantsObj.getRejectedFieldVerification() + "," + constantsObj.getRejectedAppraisal() + ", " + constantsObj.getRejectedLoanSanction() +", "+
            constantsObj.getRejectedInNextLoanPreCheck() +","+ constantsObj.getRejectedKYCDataVerificationStatusId()+","+constantsObj.getRejectedPreviousLoanStatusId()+","
            +constantsObj.getRejectedKYCByRM()+") GROUP BY pg.group_id";
        connectionDataSource.getConnection(function (clientConnect) {
            clientConnect.query(retrievePopulateGroupsQuery, function selectCb(err, results, fields) {
                connectionDataSource.releaseConnectionPool(clientConnect);
                if (err) {
                    customLog.error(err);
                } else {
                    for (var i in results) {
                        groupNameArray[i] = results[i].group_name;
                        centerNameArray[i] = results[i].center_name;
                        statusDescArray[i] = results[i].status_desc;
                        clientCountArray[i] = results[i].no_of_clients;
                    }
                    callback(groupNameArray, centerNameArray, statusDescArray,clientCountArray);
                }
            });
        });
    },

    showDashBoardDataModel: function (tenantId, officeId, callback) {
        var self=this;
        var dashBoard = require(groupManagementDTO+"/dashboard");
        var dashBoardObject = new dashBoard();
        var constantsObj = this.constants;
        var apexHeadOffice = constantsObj.getApexHeadOffice();
        var userIdArray = new Array();
        var userNameArray = new Array();
        var officeIdArray = new Array();
        var officeNameArray = new Array();
        var pvCount = new Array();
        var kycUploadCount = new Array();
        var kycUpdatingCount = new Array();
        var dataVerificationCount = new Array();
        var creditCheckCount = new Array();
        var rejectedCount = new Array();
        var assignFoCount = new Array();
        var fvCount = new Array();
        var appraisalCount = new Array();
        var loanAuthorizeCount = new Array();
        var loanSanctionCount = new Array();
        var roleIdArray = new Array();
        var roleNameArray = new Array();
        var noOfGroupsArray = new Array();
        var grtCountArray = new Array();
        var idleCountArray = new Array();
        var retrieveDashBoardQueryDetails = "SELECT user_id,user_name,role_id,IF(role_id = "+constantsObj.getBDEroleId()+",'BDE','FO') AS role_name,office_id,office_name,IF((pv_count IS NULL OR pv_count = 0),'-',pv_count) AS pv_count,IF((kyc_upload_count IS NULL OR kyc_upload_count = 0),'-',kyc_upload_count) AS kyc_upload_count," +
            " IF((fv_count IS NULL OR fv_count = 0),'-',fv_count) AS fv_count,IF((rejected_count IS NULL OR rejected_count = 0),'-',rejected_count) AS rejected_count,IF((kyc_updating_count IS NULL OR kyc_updating_count = 0),'-',kyc_updating_count) AS kyc_updating_count," +
            " IF((dv_count IS NULL OR dv_count = 0),'-',dv_count) AS dv_count,IF((cba_count IS NULL OR cba_count = 0),'-',cba_count) AS cba_count,IF((assigned_fo_count IS NULL OR assigned_fo_count = 0),'-',assigned_fo_count) AS assigned_fo_count,IF((appraisal_count IS NULL OR appraisal_count = 0),'-',appraisal_count) AS appraisal_count," +
            " IF((la_count IS NULL OR la_count = 0),'-',la_count) AS la_count,IF((ls_count IS NULL OR ls_count = 0),'-',ls_count) AS ls_count," +
            " IF((grt_count IS NULL OR grt_count = 0),'-',grt_count) AS grt_count,IF((idle_count IS NULL OR idle_count = 0),'-',idle_count) AS idle_count  FROM ( SELECT u.user_id,u.user_name,pr.role_id,(SELECT role_name FROM iklant_role role WHERE role.role_id = pr.role_id) AS role_name, " +
            " u.office_id,(SELECT office_name FROM "+dbTableName.iklantOffice+" WHERE office_id = u.office_id) AS office_name," +
            " (SELECT COUNT(group_id) FROM "+dbTableName.iklantProspectGroup+" pg1 WHERE pg1.created_by = pr.personnel_id AND status_id = "+constantsObj.getNewGroup()+" GROUP BY pg1.created_by) AS pv_count," +
            " (SELECT COUNT(group_id) FROM "+dbTableName.iklantProspectGroup+" pg1 WHERE pg1.created_by = pr.personnel_id AND status_id = "+constantsObj.getPreliminaryVerified()+" GROUP BY pg1.created_by) AS kyc_upload_count," +
            " (SELECT COUNT(group_id) FROM "+dbTableName.iklantProspectGroup+" pg1 WHERE pg1.assigned_to = pr.personnel_id AND status_id = "+constantsObj.getAssignedFO()+" AND `is_idle` =0 GROUP BY pg1.assigned_to) AS fv_count," +
            " (SELECT COUNT(group_id) FROM "+dbTableName.iklantProspectGroup+" pg1 WHERE pg1.created_by = pr.personnel_id AND status_id IN("+constantsObj.getRejectedPriliminaryVerification()+","+constantsObj.getRejectedAppraisal()+","+constantsObj.getRejectedLoanSanction()+","+constantsObj.getRejectedFieldVerification()+","+constantsObj.getRejectedCreditBureauAnalysisStatusId()+","+constantsObj.getArchived() +","+ constantsObj.getRejectedInNextLoanPreCheck()+","+ constantsObj.getRejectedWhileIdleGroupsStatusId()+","+constantsObj.getRejectedKYCDataVerificationStatusId() +","+ constantsObj.getRejectedPreviousLoanStatusId()+") GROUP BY pg1.created_by) AS rejected_count," +
            " (SELECT COUNT(group_id) FROM "+dbTableName.iklantProspectGroup+" pg1 WHERE status_id = "+constantsObj.getKYCUploaded()+" AND office_id IN(" + officeId + ")) AS kyc_updating_count," +
            " (SELECT COUNT(group_id) FROM "+dbTableName.iklantProspectGroup+" pg1 WHERE status_id = "+constantsObj.getKYCCompleted()+" AND office_id IN(" + officeId + ")) AS dv_count," +
            " (SELECT COUNT(group_id) FROM "+dbTableName.iklantProspectGroup+" pg1 WHERE status_id = "+constantsObj.getDataVerificationOperationId()+" AND office_id IN(" + officeId + ")) AS cba_count," +
            " (SELECT COUNT(group_id) FROM "+dbTableName.iklantProspectGroup+" pg1 WHERE status_id = "+constantsObj.getCreditBureauAnalysedStatus()+" AND office_id IN(" + officeId + ") AND `is_idle` =0) AS assigned_fo_count," +
            " (SELECT COUNT(group_id) FROM "+dbTableName.iklantProspectGroup+" pg1 WHERE status_id = "+constantsObj.getFieldVerified()+" AND office_id IN(" + officeId + ") AND `is_idle` =0) AS appraisal_count," +
            " (SELECT COUNT(group_id) FROM "+dbTableName.iklantProspectGroup+" pg1 WHERE status_id = "+constantsObj.getGroupRecognitionTested()+" AND office_id IN("+ officeId + ") AND `is_idle` =0 ) AS la_count," +
            " (SELECT COUNT(group_id) FROM "+dbTableName.iklantProspectGroup+" pg1 WHERE status_id = "+constantsObj.getAuthorizedStatus()+" AND office_id IN("+ officeId + ") AND `is_idle` =0 ) AS ls_count," +
            " (SELECT COUNT(group_id) FROM "+dbTableName.iklantProspectGroup+" pg1 WHERE status_id = "+constantsObj.getAppraisedStatus()+" AND office_id IN("+ officeId + ") AND `is_idle` =0 ) AS grt_count," +
            " (SELECT COUNT(group_id) FROM "+dbTableName.iklantProspectGroup+" pg1 WHERE pg1.created_by = pr.personnel_id AND pg1.status_id IN("+constantsObj.getCreditBureauAnalysedStatus()+","+constantsObj.getAssignedFO()+","+constantsObj.getFieldVerified()+","+constantsObj.getAppraisedStatus()+","+constantsObj.getGroupRecognitionTested()+","+constantsObj.getAuthorizedStatus()+") AND pg1.is_idle = 1 GROUP BY pg1.created_by) AS idle_count "+
            " FROM "+dbTableName.iklantUsers+" u,"+dbTableName.mfiPersonnelRole+" pr" +
            " LEFT JOIN "+dbTableName.iklantProspectGroup+" pg ON pr.personnel_id = pg.created_by" +
            " WHERE u.user_id = pr.personnel_id AND u.office_id IN(" + apexHeadOffice + "," + officeId + ") AND pr.role_id IN ("+constantsObj.getBDEroleId()+","+constantsObj.getFOroleId()+")" +
            " GROUP BY u.user_id ORDER BY pr.role_id,u.office_id)temp";
        customLog.info("retrieveDashBoardQueryDetails : " + retrieveDashBoardQueryDetails);
        connectionDataSource.getConnection(function (clientConnect) {
            clientConnect.query(retrieveDashBoardQueryDetails, function selectCb(err, results, fields) {
                if (err) {
                    connectionDataSource.releaseConnectionPool(clientConnect);
                    customLog.error(err);
                } else {
                    for (var i in results) {
                        userIdArray [i] = results[i].user_id;
                        userNameArray[i] = results[i].user_name;
                        officeIdArray[i] = results[i].office_id;
                        officeNameArray[i] = results[i].office_name;
                        roleIdArray[i] = results[i].role_id;
                        roleNameArray[i] = results[i].role_name;
                        pvCount[i] = results[i].pv_count;
                        kycUploadCount[i] = results[i].kyc_upload_count;
                        kycUpdatingCount[i] = results[i].kyc_updating_count;
                        dataVerificationCount[i] = results[i].dv_count;
                        creditCheckCount[i] = results[i].cba_count;
                        assignFoCount[i] = results[i].assigned_fo_count;
                        fvCount[i] = results[i].fv_count;
                        rejectedCount[i] = results[i].rejected_count;
                        appraisalCount[i] = results[i].appraisal_count;
                        loanAuthorizeCount[i] = results[i].la_count;
                        loanSanctionCount[i] = results[i].ls_count;
                        grtCountArray[i] = results[i].grt_count;
                        idleCountArray[i] = results[i].idle_count;
                    }
                    dashBoardObject.setUserId(userIdArray);
                    dashBoardObject.setUserName(userNameArray);
                    dashBoardObject.setOfficeId(officeIdArray);
                    dashBoardObject.setofficeName(officeNameArray);
                    dashBoardObject.setRoleId(roleIdArray);
                    dashBoardObject.setRoleName(roleNameArray);
                    dashBoardObject.setPvCount(pvCount);
                    dashBoardObject.setKycUploadCount(kycUploadCount);
                    dashBoardObject.setKycUpdatingCount(kycUpdatingCount);
                    dashBoardObject.setRejectedCount(rejectedCount);
                    dashBoardObject.setDataVerificationCount(dataVerificationCount);
                    dashBoardObject.setCreditCheckCount(creditCheckCount);
                    dashBoardObject.setAssignFoCount(assignFoCount);
                    dashBoardObject.setFvCount(fvCount);
                    dashBoardObject.setAppraisalCount(appraisalCount);
                    dashBoardObject.setLoanAuthorizeCount(loanAuthorizeCount);
                    dashBoardObject.setLoanSanctionCount(loanSanctionCount);
                    dashBoardObject.setGrtCount(grtCountArray);
                    dashBoardObject.setIdleCount(idleCountArray);
                    connectionDataSource.releaseConnectionPool(clientConnect);
                    callback(dashBoardObject);
                }
            });
        });
    },


    showRejectionCountStageWiseDashBoardDataModel : function (tenantId, officeId, bcOfficeId,roleId,officeLevelId,fromDate,toDate,callback) {
        var self=this;
        var dashBoardObject ={};
        var dashBoardArrayObject = new Array();
        var constantsObj = this.constants;
        var groupByColumn;
        var retrieveDashBoardQueryDetails = "SELECT  "+
            "bo.head_office_id,bo.head_office_name,bo.head_office_level_id,  "+
            "bo.regional_office_id,bo.regional_office_name,bo.regional_office_level_id,  "+
            "bo.divisional_office_id,bo.divisional_office_name,bo.divisional_office_level_id,  "+
            "bo.area_office_id,bo.area_office,bo.area_office_level_id,  "+
            "bo.branch_office_id,bo.bo_office_level_id,bo.branch_name, "+
            "SUM(off.rejected_in_kyc_data_verification) AS rejected_in_kyc_data_verification, "+
            "SUM(off.rejected_in_KYC_by_RM) AS rejected_in_KYC_by_RM, "+
            "SUM(off.rejected_in_cba) AS rejected_in_cba, "+
            "SUM(off.rejected_in_field_verification) AS rejected_in_field_verification, "+
            "SUM(off.rejected_in_appraisal) AS rejected_in_appraisal, "+
            "SUM(off.rejected_in_loan_sanction) AS rejected_in_loan_sanction, "+
            "SUM(off.rejected_in_idle_stage) AS rejected_in_idle_stage "+
            "FROM "+
            " (SELECT ipg.`office_id`,io.`office_name`, "+
            " SUM(CASE WHEN ipc.`status_id` = 25 THEN 1 ELSE 0 END) AS 'rejected_in_kyc_data_verification', "+
            " SUM(CASE WHEN ipc.`status_id` = 29 THEN 1 ELSE 0 END) AS 'rejected_in_KYC_by_RM', "+
            " SUM(CASE WHEN ipc.`status_id` = 15 THEN 1 ELSE 0 END) AS 'rejected_in_cba', "+
            " SUM(CASE WHEN ipc.`status_id` = 16 THEN 1 ELSE 0 END) AS 'rejected_in_field_verification', "+
            " SUM(CASE WHEN ipc.`status_id` = 17 THEN 1 ELSE 0 END) AS 'rejected_in_appraisal', "+
            " SUM(CASE WHEN ipc.`status_id` = 18 THEN 1 ELSE 0 END) AS 'rejected_in_loan_sanction', "+
            " SUM(CASE WHEN ipc.`status_id` = 22 THEN 1 ELSE 0 END) AS 'rejected_in_idle_stage' "+
            " FROM iklant_prospect_group ipg "+
            " INNER JOIN iklant_prospect_client ipc ON ipc.`group_id` = ipg.`group_id` "+
            " INNER JOIN iklant_office io ON io.`office_id` = ipg.`office_id` "+
            "WHERE DATE(ipg.`created_date`) BETWEEN '"+fromDate+"' AND '"+toDate+"'  AND ipc.`status_id` NOT IN (21,26) "+
            //"WHERE DATE(ipg.`created_date`) BETWEEN '2015-06-01' AND '2015-08-31'  AND ipc.`status_id` NOT IN (21,26) "+
            " GROUP BY ipg.`office_id`) off "+
            "LEFT JOIN (    "+
            "SELECT  ho.`office_id`  AS head_office_id,ho.`display_name` AS head_office_name,ho.`office_level_id` AS head_office_level_id,    "+
            "ro.`office_id` AS regional_office_id,IFNULL(ro.`display_name`,'NA') AS regional_office_name,ro.`office_level_id` AS  "+ " regional_office_level_id,   "+
            "divo.`office_id` AS divisional_office_id,IFNULL(divo.`display_name`,'NA') AS divisional_office_name,divo.`office_level_id` AS  "+ " divisional_office_level_id,  "+
            " ao.`office_id` AS area_office_id,IFNULL(ao.`display_name`,'NA') AS area_office,ao.`office_level_id` AS area_office_level_id,   "+
            "  bo.`office_id` AS branch_office_id, bo.`office_level_id` AS bo_office_level_id, bo.`display_name` AS branch_name   "+
            "  FROM office ho   "+
            " LEFT  JOIN office ro ON  ro.`parent_office_id` = ho.`office_id`  AND ro.`office_level_id` =2   "+
            " LEFT  JOIN office divo ON  (divo.`parent_office_id` = ro.`office_id`) AND divo.`office_level_id` = 3    "+
            " LEFT  JOIN office ao ON  (ao.`parent_office_id` = divo.`office_id`)  OR (ao.`parent_office_id` = ro.`office_id`) AND "+ " divo.`office_level_id` = 4   "+
            " LEFT JOIN office bo ON  (bo.`parent_office_id` = ao.`office_id`) OR (bo.`parent_office_id` = divo.`office_id`) OR   "+ " (bo.`parent_office_id` = ro.`office_id`) "+
            "  OR (bo.`parent_office_id` =  ho.`office_id` ) AND bo.`office_level_id` = 5  ";
        if(dbTableName.headOfficeLeveId == officeLevelId){
            retrieveDashBoardQueryDetails = retrieveDashBoardQueryDetails + "WHERE   ho.`office_id` = "+officeId+"  AND bo.`office_level_id` = "+dbTableName.branchOfficeLeveId+" ";
            if(dbTableName.regionalOfficeLevelApplicable){
                dashBoardObject.officeLabelColumn = "regional_office_name";
                dashBoardObject.officeIdColumn = "regional_office_id";
                dashBoardObject.officeLevelIdColumn = dbTableName.regionalOfficeLeveId;
                groupByColumn = "regional_office_id";
            }else if(dbTableName.divsionalOfficeLevelApplicable) {
                dashBoardObject.officeLabelColumn = "divisional_office_name";
                dashBoardObject.officeIdColumn = "divisional_office_id";
                dashBoardObject.officeLevelIdColumn = dbTableName.divisionalOfficeLeveId;
                groupByColumn = "divisional_office_id";
            }else if(dbTableName.areaOfficeLevelApplicable) {
                dashBoardObject.officeLabelColumn = "area_office";
                dashBoardObject.officeIdColumn = "area_office_id";
                dashBoardObject.officeLevelIdColumn = dbTableName.areaOfficeLeveId;
                groupByColumn = "area_office_id";
            }else{
                dashBoardObject.officeLabelColumn = "branch_name";
                dashBoardObject.officeIdColumn = "branch_office_id";
                dashBoardObject.officeLevelIdColumn = dbTableName.branchOfficeLeveId;
                groupByColumn = "branch_office_id";
            }

        }
        else if(dbTableName.regionalOfficeLeveId == officeLevelId) {
            retrieveDashBoardQueryDetails = retrieveDashBoardQueryDetails + " WHERE   ro.`office_id` = "+officeId+"  AND bo.`office_level_id` = " + dbTableName.branchOfficeLeveId + " ";
            if(dbTableName.divsionalOfficeLevelApplicable) {
                dashBoardObject.officeLabelColumn = "divisional_office_name";
                dashBoardObject.officeIdColumn = "divisional_office_id";
                dashBoardObject.officeLevelIdColumn = dbTableName.divisionalOfficeLeveId;
                groupByColumn = "divisional_office_id";
            }else if(dbTableName.areaOfficeLevelApplicable) {
                dashBoardObject.officeLabelColumn = "area_office";
                dashBoardObject.officeIdColumn = "area_office_id";
                dashBoardObject.officeLevelIdColumn = dbTableName.areaOfficeLeveId;
                groupByColumn = "area_office_id";
            }else{
                dashBoardObject.officeLabelColumn = "branch_name";
                dashBoardObject.officeIdColumn = "branch_office_id";
                dashBoardObject.officeLevelIdColumn = dbTableName.branchOfficeLeveId;
                groupByColumn = "branch_office_id";
            }
        }
        else if(dbTableName.divisionalOfficeLeveId == officeLevelId) {
            retrieveDashBoardQueryDetails = retrieveDashBoardQueryDetails +  " WHERE   divo.`office_id` = "+officeId+" AND bo.`office_level_id` = " + dbTableName.branchOfficeLeveId + " ";
            if(dbTableName.areaOfficeLevelApplicable) {
                dashBoardObject.officeLabelColumn = "area_office";
                dashBoardObject.officeIdColumn = "area_office_id";
                dashBoardObject.officeLevelIdColumn = dbTableName.areaOfficeLeveId;
                groupByColumn = "area_office_id";
            }else{
                dashBoardObject.officeLabelColumn = "branch_name";
                dashBoardObject.officeIdColumn = "branch_office_id";
                dashBoardObject.officeLevelIdColumn = dbTableName.branchOfficeLeveId;
                groupByColumn = "branch_office_id";
            }
        }
        else if(dbTableName.areaOfficeLeveId == officeLevelId) {
            retrieveDashBoardQueryDetails = retrieveDashBoardQueryDetails + " WHERE   ao.`office_id` = "+officeId+"  AND bo.`office_level_id` = " + dbTableName.branchOfficeLeveId + " ";
            dashBoardObject.officeLabelColumn = "branch_name";
            dashBoardObject.officeIdColumn = "branch_office_id";
            dashBoardObject.officeLevelIdColumn = dbTableName.branchOfficeLeveId;
            groupByColumn = "branch_office_id";
        }else{
            dashBoardObject.officeLabelColumn = "branch_name";
            dashBoardObject.officeIdColumn = "branch_office_id";
            dashBoardObject.officeLevelIdColumn = dbTableName.branchOfficeLeveId;
            groupByColumn = "branch_office_id";;
        }
        retrieveDashBoardQueryDetails = retrieveDashBoardQueryDetails + " GROUP BY bo.`office_id`) "+
            " bo ON bo.branch_office_id = off.office_id ";
        if(roleId == constantsObj.getBcCoordinatorRoleId()){
            retrieveDashBoardQueryDetails = retrieveDashBoardQueryDetails + " WHERE grp.bc_id = " +bcOfficeId+" AND bo.head_office_id IS NOT NULL ";
        }
        else if(dbTableName.branchOfficeLeveId == officeLevelId) {
            retrieveDashBoardQueryDetails = retrieveDashBoardQueryDetails + " WHERE bo.branch_office_id = "+officeId+" OR "+officeId+" = -1 AND bo.head_office_id IS NOT NULL ";
        }
        else{
            retrieveDashBoardQueryDetails = retrieveDashBoardQueryDetails + " WHERE bo.branch_office_id = -1 OR -1 = -1 AND bo.head_office_id IS NOT NULL ";
        }
        retrieveDashBoardQueryDetails = retrieveDashBoardQueryDetails +  " GROUP BY "+groupByColumn+"; ";

        customLog.info("retrieveDashBoardQueryDetails : " + retrieveDashBoardQueryDetails);
        connectionDataSource.getConnection(function (clientConnect) {
            clientConnect.query(retrieveDashBoardQueryDetails, function selectCb(err, results, fields) {
                connectionDataSource.releaseConnectionPool(clientConnect);
                if (err) {
                    customLog.error(err);
                } else {
                    var totalClientsinDV= 0;
                    var totalClientsInRejectedByRM= 0;
                    var totalClientsInRejectedInCC= 0;
                    var totalClientsInRejectedInFV= 0;
                    var totalClientsInRejectedInCCA= 0;
                    var totalClientsInRejectedInLS= 0;
                    var totalClientsRejected = 0;

                    for(var i=0;i<results.length;i++) {
                        var dashBoardDetailsObject = {};
                        dashBoardDetailsObject.displayOfficeName = results[i][dashBoardObject.officeLabelColumn];
                        dashBoardDetailsObject.displayOfficeId = results[i][dashBoardObject.officeIdColumn];
                        dashBoardDetailsObject.displayOfficeLevelId = dashBoardObject.officeLevelIdColumn;

                        dashBoardDetailsObject.rejectedInDataVerification = results[i].rejected_in_kyc_data_verification;
                        totalClientsinDV = totalClientsinDV + results[i].rejected_in_kyc_data_verification;

                        dashBoardDetailsObject.rejectedKYCByAreaManager = results[i].rejected_in_KYC_by_RM;
                        totalClientsInRejectedByRM = totalClientsInRejectedByRM + results[i].rejected_in_KYC_by_RM;

                        dashBoardDetailsObject.rejectedInCreditCheck =  results[i].rejected_in_cba;
                        totalClientsInRejectedInCC = totalClientsInRejectedInCC + results[i].rejected_in_cba;

                        dashBoardDetailsObject.rejectedInFieldVerification =  results[i].rejected_in_field_verification;
                        totalClientsInRejectedInFV = totalClientsInRejectedInFV + results[i].rejected_in_field_verification;

                        dashBoardDetailsObject.rejectedInAppraisal =  results[i].rejected_in_appraisal;
                        totalClientsInRejectedInCCA =  totalClientsInRejectedInCCA + results[i].rejected_in_appraisal;

                        dashBoardDetailsObject.rejectedInLoanSanction =  results[i].rejected_in_loan_sanction;
                        totalClientsInRejectedInLS = totalClientsInRejectedInLS + results[i].rejected_in_loan_sanction;

                        dashBoardDetailsObject.rejectedInIdleStage=  results[i].rejected_in_idle_stage;

                        dashBoardDetailsObject.totalClientsRejected = (results[i].rejected_in_kyc_data_verification + results[i].rejected_in_KYC_by_RM + results[i].rejected_in_cba + results[i].rejected_in_field_verification + results[i].rejected_in_appraisal + results[i].rejected_in_loan_sanction );
                        totalClientsRejected = totalClientsRejected + dashBoardDetailsObject.totalClientsRejected;
                        if(i == results.length-1){
                            dashBoardDetailsObject.totalClientsinDV = totalClientsinDV;
                            dashBoardDetailsObject.totalRejectedClientsinDVPerc = ((totalClientsinDV / totalClientsRejected) * 100).toFixed(2);
                            dashBoardDetailsObject.totalClientsInRejectedByRM = totalClientsInRejectedByRM;
                            dashBoardDetailsObject.totalRejectedClientsinByRM = ((totalClientsInRejectedByRM / totalClientsRejected) * 100).toFixed(2);
                            dashBoardDetailsObject.totalClientsInRejectedInCC = totalClientsInRejectedInCC;
                            dashBoardDetailsObject.totalRejectedClientsinCC = ((totalClientsInRejectedInCC / totalClientsRejected) * 100).toFixed(2);
                            dashBoardDetailsObject.totalClientsInRejectedInFV = totalClientsInRejectedInFV;
                            dashBoardDetailsObject.totalRejectedClientsinFV = ((totalClientsInRejectedInFV / totalClientsRejected) * 100).toFixed(2);
                            dashBoardDetailsObject.totalClientsInRejectedInCCA = totalClientsInRejectedInCCA;
                            dashBoardDetailsObject.totalRejectedClientsinCCA= ((totalClientsInRejectedInCCA / totalClientsRejected) * 100).toFixed(2);
                            dashBoardDetailsObject.totalClientsInRejectedInLS = totalClientsInRejectedInLS;
                            dashBoardDetailsObject.totalRejectedClientsinLS = ((totalClientsInRejectedInLS / totalClientsRejected) * 100).toFixed(2);
                            dashBoardDetailsObject.totalClientsRejectedForAllStages = totalClientsRejected;
                        }
                        dashBoardArrayObject.push(dashBoardDetailsObject);
                    }
                    console.log(dashBoardArrayObject);
                    callback(dashBoardArrayObject);
                }
            });
        });
    },

    showRejectionCountDashBoardDataModel: function (tenantId, officeId, bcOfficeId,roleId,officeLevelId,fromDate,toDate,callback) {
        var self=this;
        var dashBoardObject ={};
        var dashBoardArrayObject = new Array();
        var constantsObj = this.constants;
        var groupByColumn;
        var retrieveDashBoardQueryDetails = "SELECT  "+
            "bo.head_office_id,bo.head_office_name,bo.head_office_level_id,  "+
            "bo.regional_office_id,bo.regional_office_name,bo.regional_office_level_id,  "+
            "bo.divisional_office_id,bo.divisional_office_name,bo.divisional_office_level_id,  "+
            "bo.area_office_id,bo.area_office,bo.area_office_level_id,  "+
            "bo.branch_office_id,bo.bo_office_level_id,bo.branch_name, "+
            "SUM(off.total_no_of_sourced) AS total_no_of_sourced,SUM(total_no_of_rejected) AS total_no_of_rejected "+
            "FROM "+
            "(SELECT io.`office_id`,io.`office_name`, "+
            "COUNT(ipc.`client_id`) AS 'total_no_of_sourced', "+
            "SUM(CASE WHEN ipc.`status_id` IN (15,16,17,18,25,29) THEN 1 ELSE 0 END) AS 'total_no_of_rejected' "+
            "FROM iklant_prospect_group ipg "+
            "INNER JOIN iklant_prospect_client ipc ON ipc.`group_id` = ipg.`group_id` "+
            "INNER JOIN iklant_office io ON io.`office_id` = ipg.`office_id` "+
            "WHERE DATE(ipg.`created_date`) BETWEEN '"+fromDate+"' AND '"+toDate+"'  AND ipc.`status_id` NOT IN (21,26) "+
            //"WHERE DATE(ipg.`created_date`) BETWEEN '2015-06-01' AND '2015-08-31'  AND ipc.`status_id` NOT IN (21,26) "+
            "GROUP BY ipg.`office_id`) off "+
            "LEFT JOIN (    "+
            "SELECT  ho.`office_id`  AS head_office_id,ho.`display_name` AS head_office_name,ho.`office_level_id` AS head_office_level_id,    "+
            "ro.`office_id` AS regional_office_id,IFNULL(ro.`display_name`,'NA') AS regional_office_name,ro.`office_level_id` AS  "+ " regional_office_level_id,   "+
            "divo.`office_id` AS divisional_office_id,IFNULL(divo.`display_name`,'NA') AS divisional_office_name,divo.`office_level_id` AS  "+ " divisional_office_level_id,  "+
            " ao.`office_id` AS area_office_id,IFNULL(ao.`display_name`,'NA') AS area_office,ao.`office_level_id` AS area_office_level_id,   "+
            "  bo.`office_id` AS branch_office_id, bo.`office_level_id` AS bo_office_level_id, bo.`display_name` AS branch_name   "+
            "  FROM office ho   "+
            " LEFT  JOIN office ro ON  ro.`parent_office_id` = ho.`office_id`  AND ro.`office_level_id` =2   "+
            " LEFT  JOIN office divo ON  (divo.`parent_office_id` = ro.`office_id`) AND divo.`office_level_id` = 3    "+
            " LEFT  JOIN office ao ON  (ao.`parent_office_id` = divo.`office_id`)  OR (ao.`parent_office_id` = ro.`office_id`) AND "+ " divo.`office_level_id` = 4   "+
            " LEFT JOIN office bo ON  (bo.`parent_office_id` = ao.`office_id`) OR (bo.`parent_office_id` = divo.`office_id`) OR   "+ " (bo.`parent_office_id` = ro.`office_id`) "+
            "  OR (bo.`parent_office_id` =  ho.`office_id` ) AND bo.`office_level_id` = 5  ";
        if(dbTableName.headOfficeLeveId == officeLevelId){
            retrieveDashBoardQueryDetails = retrieveDashBoardQueryDetails + "WHERE   ho.`office_id` = "+officeId+"  AND bo.`office_level_id` = "+dbTableName.branchOfficeLeveId+" ";
            if(dbTableName.regionalOfficeLevelApplicable){
                dashBoardObject.officeLabelColumn = "regional_office_name";
                dashBoardObject.officeIdColumn = "regional_office_id";
                dashBoardObject.officeLevelIdColumn = dbTableName.regionalOfficeLeveId;
                groupByColumn = "regional_office_id";
            }else if(dbTableName.divsionalOfficeLevelApplicable) {
                dashBoardObject.officeLabelColumn = "divisional_office_name";
                dashBoardObject.officeIdColumn = "divisional_office_id";
                dashBoardObject.officeLevelIdColumn = dbTableName.divisionalOfficeLeveId;
                groupByColumn = "divisional_office_id";
            }else if(dbTableName.areaOfficeLevelApplicable) {
                dashBoardObject.officeLabelColumn = "area_office";
                dashBoardObject.officeIdColumn = "area_office_id";
                dashBoardObject.officeLevelIdColumn = dbTableName.areaOfficeLeveId;
                groupByColumn = "area_office_id";
            }else{
                dashBoardObject.officeLabelColumn = "branch_name";
                dashBoardObject.officeIdColumn = "branch_office_id";
                dashBoardObject.officeLevelIdColumn = dbTableName.branchOfficeLeveId;
                groupByColumn = "branch_office_id";
            }

        }
        else if(dbTableName.regionalOfficeLeveId == officeLevelId) {
            retrieveDashBoardQueryDetails = retrieveDashBoardQueryDetails + " WHERE   ro.`office_id` = "+officeId+"  AND bo.`office_level_id` = " + dbTableName.branchOfficeLeveId + " ";
            if(dbTableName.divsionalOfficeLevelApplicable) {
                dashBoardObject.officeLabelColumn = "divisional_office_name";
                dashBoardObject.officeIdColumn = "divisional_office_id";
                dashBoardObject.officeLevelIdColumn = dbTableName.divisionalOfficeLeveId;
                groupByColumn = "divisional_office_id";
            }else if(dbTableName.areaOfficeLevelApplicable) {
                dashBoardObject.officeLabelColumn = "area_office";
                dashBoardObject.officeIdColumn = "area_office_id";
                dashBoardObject.officeLevelIdColumn = dbTableName.areaOfficeLeveId;
                groupByColumn = "area_office_id";
            }else{
                dashBoardObject.officeLabelColumn = "branch_name";
                dashBoardObject.officeIdColumn = "branch_office_id";
                dashBoardObject.officeLevelIdColumn = dbTableName.branchOfficeLeveId;
                groupByColumn = "branch_office_id";
            }
        }
        else if(dbTableName.divisionalOfficeLeveId == officeLevelId) {
            retrieveDashBoardQueryDetails = retrieveDashBoardQueryDetails +  " WHERE   divo.`office_id` = "+officeId+" AND bo.`office_level_id` = " + dbTableName.branchOfficeLeveId + " ";
            if(dbTableName.areaOfficeLevelApplicable) {
                dashBoardObject.officeLabelColumn = "area_office";
                dashBoardObject.officeIdColumn = "area_office_id";
                dashBoardObject.officeLevelIdColumn = dbTableName.areaOfficeLeveId;
                groupByColumn = "area_office_id";
            }else{
                dashBoardObject.officeLabelColumn = "branch_name";
                dashBoardObject.officeIdColumn = "branch_office_id";
                dashBoardObject.officeLevelIdColumn = dbTableName.branchOfficeLeveId;
                groupByColumn = "branch_office_id";
            }
        }
        else if(dbTableName.areaOfficeLeveId == officeLevelId) {
            retrieveDashBoardQueryDetails = retrieveDashBoardQueryDetails + " WHERE   ao.`office_id` = "+officeId+"  AND bo.`office_level_id` = " + dbTableName.branchOfficeLeveId + " ";
            dashBoardObject.officeLabelColumn = "branch_name";
            dashBoardObject.officeIdColumn = "branch_office_id";
            dashBoardObject.officeLevelIdColumn = dbTableName.branchOfficeLeveId;
            groupByColumn = "branch_office_id";
        }else{
            dashBoardObject.officeLabelColumn = "branch_name";
            dashBoardObject.officeIdColumn = "branch_office_id";
            dashBoardObject.officeLevelIdColumn = dbTableName.branchOfficeLeveId;
            groupByColumn = "branch_office_id";
        }
        retrieveDashBoardQueryDetails = retrieveDashBoardQueryDetails + " GROUP BY bo.`office_id`) "+
            " bo ON bo.branch_office_id = off.office_id ";
        if(roleId == constantsObj.getBcCoordinatorRoleId()){
            retrieveDashBoardQueryDetails = retrieveDashBoardQueryDetails + " WHERE grp.bc_id = " +bcOfficeId+" AND bo.head_office_id IS NOT NULL ";
        }
        else if(dbTableName.branchOfficeLeveId == officeLevelId) {
            retrieveDashBoardQueryDetails = retrieveDashBoardQueryDetails + " WHERE bo.branch_office_id = "+officeId+" OR "+officeId+" = -1 AND bo.head_office_id IS NOT NULL ";
        }
        else{
            retrieveDashBoardQueryDetails = retrieveDashBoardQueryDetails + " WHERE bo.branch_office_id = -1 OR -1 = -1 AND bo.head_office_id IS NOT NULL ";
        }
        retrieveDashBoardQueryDetails = retrieveDashBoardQueryDetails +  " GROUP BY "+groupByColumn+"; ";

        customLog.info("retrieveDashBoardQueryDetails : " + retrieveDashBoardQueryDetails);
        connectionDataSource.getConnection(function (clientConnect) {
            clientConnect.query(retrieveDashBoardQueryDetails, function selectCb(err, results, fields) {
                connectionDataSource.releaseConnectionPool(clientConnect);
                if (err) {
                    customLog.error(err);
                } else {
                    var totalClientsSourced = 0;
                    var totalClientsRejected = 0;

                    for(var i=0;i<results.length;i++) {
                        var dashBoardDetailsObject = {};
                        dashBoardDetailsObject.displayOfficeName = results[i][dashBoardObject.officeLabelColumn];
                        dashBoardDetailsObject.displayOfficeId = results[i][dashBoardObject.officeIdColumn];
                        dashBoardDetailsObject.displayOfficeLevelId = dashBoardObject.officeLevelIdColumn;

                        dashBoardDetailsObject.totalClientSourcedForOffice = results[i].total_no_of_sourced;
                        dashBoardDetailsObject.totalClientRejectedForOffice = results[i].total_no_of_rejected;
                        dashBoardDetailsObject.rejectionPercentageForOffice = (results[i].total_no_of_rejected / results[i].total_no_of_sourced) * 100;
                        totalClientsSourced = totalClientsSourced + results[i].total_no_of_sourced;
                        totalClientsRejected = totalClientsRejected + results[i].total_no_of_rejected;
                        if(i == results.length-1){
                            dashBoardDetailsObject.totalClientsSourced = totalClientsSourced;
                            dashBoardDetailsObject.totalClientsRejected = totalClientsRejected;
                            dashBoardDetailsObject.totalClientsRejectedPercentage = (totalClientsRejected/totalClientsSourced) * 100;
                        }
                        dashBoardArrayObject.push(dashBoardDetailsObject);
                    }
                    callback(dashBoardArrayObject);
                }
            });
        });
    },

    showLDDashBoardDataModel: function (tenantId, officeId, bcOfficeId,roleId,officeLevelId,callback) {
        var self=this;
        var dashBoard = require(groupManagementDTO+"/dashboard");
        var dashBoardArrayObject = new Array();
        var dashBoardObject ={};
        var constantsObj = this.constants;
        var groupByColumn;
        var kycUploading,imageClarification,kycUploaded,dataVerified,CBA,assignedFo,fieldVerified,appraised,authorized,kycDV,GRT,rejectedClients,officeId;
        /*var retrieveDashBoardQueryDetails = " SELECT office_id, SUM(status_id=" +constantsObj.getPreliminaryVerified()+
            ") AS kycUploading,SUM(status_id="+constantsObj.getKYCVerificationStatusId() +") AS image_clarification, SUM(status_id="+constantsObj.getKYCUploaded()+") AS kycUploaded, SUM(status_id="+constantsObj.getKYCCompleted()+") AS dataVerified, SUM(status_id="+constantsObj.getLeaderSubleaderVerificationCompletedStatusId()
            +") AS CBA,SUM(status_id="+constantsObj.getCreditBureauAnalysedStatus()+") AS assignedFo, SUM(status_id="+constantsObj.getAssignedFO()+") AS fieldVerified, SUM(status_id="+constantsObj.getFieldVerified()
        +") AS appraised, SUM(status_id="+constantsObj.getGroupRecognitionTested()+") AS authorized, SUM(status_id="+constantsObj.getAuthorizedStatus()+") AS loanSanctioned, SUM(status_id="
        +constantsObj.getAppraisedStatus()+") AS GRT, SUM(status_id IN("+constantsObj.getRejectedPriliminaryVerification()+","+constantsObj.getRejectedCreditBureauAnalysisStatusId()+","+constantsObj.getRejectedFieldVerification()+","+constantsObj.getRejectedAppraisal()+","+
        constantsObj.getRejectedLoanSanction()+","+constantsObj.getRejectedInNextLoanPreCheck()+","+constantsObj.getRejectedKYCDataVerificationStatusId()+","+
        constantsObj.getRejectedPreviousLoanStatusId()+","+constantsObj.getRejectedKYCByRM()+")) AS rejectedClients from "+dbTableName.iklantProspectGroup+" WHERE office_id="+officeId;
        */
        var retrieveDashBoardQueryDetails = "SELECT  "+
            "bo.head_office_id,bo.head_office_name,bo.head_office_level_id, "+
            "bo.regional_office_id,bo.regional_office_name,bo.regional_office_level_id, "+
            "bo.divisional_office_id,bo.divisional_office_name,bo.divisional_office_level_id, "+
            "bo.area_office_id,bo.area_office,bo.area_office_level_id, "+
            "bo.branch_office_id,bo.bo_office_level_id,bo.branch_name, "+
            "grp.bc_id, "+
            "SUM(grp.g_kycUploading) AS g_kycUploading, "+
            "SUM(grp.g_image_clarification) AS g_image_clarification, "+
            "SUM(grp.g_kycUploaded) AS g_kycUploaded, "+
            "SUM(grp.g_dataVerified) AS g_dataVerified, "+
            "SUM(grp.g_CBA) AS g_CBA, "+
            "SUM(grp.g_CGT) AS g_CGT, "+
            "SUM(grp.g_assignedFo) AS g_assignedFo, "+
            "SUM(grp.g_fieldVerified) AS g_fieldVerified, "+
            "SUM(grp.g_appraised) AS g_appraised, "+
            "SUM(grp.g_authorized) AS g_authorized, "+
            "SUM(grp.g_loanSanctioned) AS g_loanSanctioned, "+
            "SUM(grp.g_GRT) AS g_GRT, " +
            "SUM(grp.g_BDU) AS g_BDU, " +
            "SUM(mem.m_kycUploading) AS m_kycUploading, "+
            "SUM(mem.m_image_clarification) AS m_image_clarification, "+
            "SUM(mem.m_kycUploaded) AS m_kycUploaded, "+
            "SUM(mem.m_dataVerified) AS m_dataVerified, "+
            "SUM(mem.m_CBA) AS m_CBA, "+
            "SUM(mem.m_CGT) AS m_CGT, "+
            "SUM(mem.m_assignedFo) AS m_assignedFo, "+
            "SUM(mem.m_fieldVerified) AS m_fieldVerified, "+
            "SUM(mem.m_appraised) AS m_appraised, "+
            "SUM(mem.m_authorized) AS m_authorized, "+
            "SUM(mem.m_loanSanctioned) AS m_loanSanctioned, "+
            "SUM(mem.m_GRT) AS m_GRT, "+
            "SUM(mem.m_BDU) AS m_BDU "+
        " FROM  "+
            "( SELECT "+
            "	pg.office_id, o.bc_id, "+
            "	SUM(pg.status_id=" +constantsObj.getPreliminaryVerified()+") AS g_kycUploading, "+
            "	SUM(pg.status_id="+constantsObj.getKYCVerificationStatusId() +") AS g_image_clarification,  "+
            "	SUM(pg.status_id="+constantsObj.getKYCUploaded()+") AS g_kycUploaded,  "+
            "	SUM(pg.status_id="+constantsObj.getKYCCompleted() +") AS g_dataVerified,  "+
            "	SUM(pg.status_id="+constantsObj.getLeaderSubleaderVerificationCompletedStatusId()+") AS g_CBA, "+
            "	SUM(pg.status_id="+constantsObj.getCGTStatus()+") AS g_CGT, "+
            "	SUM(pg.status_id="+constantsObj.getCreditBureauAnalysedStatus()+") AS g_assignedFo,  "+
            "	SUM(pg.status_id="+constantsObj.getAssignedFO()+") AS g_fieldVerified,  "+
            "	SUM(pg.status_id="+constantsObj.getFieldVerified()+") AS g_appraised,  "+
            "	SUM(pg.status_id="+constantsObj.getGroupRecognitionTested()+") AS g_authorized,  "+
            "	SUM(pg.status_id="+constantsObj.getAuthorizedStatus()+") AS g_loanSanctioned,  "+
            "	SUM(pg.status_id="+constantsObj.getAppraisedStatus()+") AS g_GRT, "+
            "	SUM(pg.status_id="+constantsObj.getBankDetailsUpdateStatus()+") AS g_BDU "+
            "  FROM iklant_prospect_group  pg "+
            "  INNER JOIN office o ON o.office_id = pg.office_id " +
            "  GROUP BY pg.`office_id`) grp "+
            "  LEFT JOIN   "+
            "  (SELECT "+
            "  pg.office_id, "+
            "  SUM(pc.status_id=" +constantsObj.getPreliminaryVerified()+") AS m_kycUploading, "+
            "  SUM(pc.status_id=" +constantsObj.getKYCVerificationStatusId() + ") AS m_image_clarification,  "+
            "  SUM(pc.status_id="+constantsObj.getKYCUploaded()+") AS m_kycUploaded,  "+
            "  SUM(pc.status_id="+constantsObj.getKYCCompleted() +") AS m_dataVerified,  "+
            "  SUM(pc.status_id="+constantsObj.getLeaderSubleaderVerificationCompletedStatusId()+") AS m_CBA, "+
            "  SUM(pc.status_id="+constantsObj.getCGTStatus()+") AS m_CGT, "+
            "  SUM(pc.status_id="+constantsObj.getCreditBureauAnalysedStatus()+") AS m_assignedFo,  "+
            "  SUM(pc.status_id="+constantsObj.getAssignedFO()+") AS m_fieldVerified,  "+
            "  SUM(pc.status_id="+constantsObj.getFieldVerified()+") AS m_appraised,  "+
            "  SUM(pc.status_id="+constantsObj.getGroupRecognitionTested()+") AS m_authorized,  "+
            "  SUM(pc.status_id="+constantsObj.getAppraisedStatus()+") AS m_GRT, "+
            "  SUM(pc.status_id="+constantsObj.getBankDetailsUpdateStatus()+") AS m_BDU, "+
            "  SUM(pc.status_id="+constantsObj.getAuthorizedStatus()+") AS m_loanSanctioned "+
            "  FROM iklant_prospect_group  pg "+
            "  INNER JOIN iklant_prospect_client pc ON pc.`group_id` = pg.`group_id` "+
            "  WHERE  pc.`status_id` NOT IN (14,15,16,17,18,21,25,26,29) AND pg.`status_id` NOT IN (12,13,14,15,16,17,18,21,22,25,26,29) "+
            "  GROUP BY pg.`office_id`) mem ON mem.office_id = grp.office_id "+
            "  LEFT JOIN ( "+
            "  SELECT  ho.`office_id`  AS head_office_id,ho.`display_name` AS head_office_name,ho.`office_level_id` AS head_office_level_id, "+
            "  ro.`office_id` AS regional_office_id,IFNULL(ro.`display_name`,'NA') AS regional_office_name,ro.`office_level_id` AS "+
            " regional_office_level_id, "+
            " divo.`office_id` AS divisional_office_id,IFNULL(divo.`display_name`,'NA') AS divisional_office_name,divo.`office_level_id` AS "+
            " divisional_office_level_id, "+
            " ao.`office_id` AS area_office_id,IFNULL(ao.`display_name`,'NA') AS area_office,ao.`office_level_id` AS area_office_level_id, "+
            " bo.`office_id` AS branch_office_id, bo.`office_level_id` AS bo_office_level_id, bo.`display_name` AS branch_name "+
            " FROM office ho "+
            " LEFT  JOIN office ro ON  ro.`parent_office_id` = ho.`office_id`  AND ro.`office_level_id` =2 "+
            " LEFT  JOIN office divo ON  (divo.`parent_office_id` = ro.`office_id`) AND divo.`office_level_id` = 3  "+
            " LEFT  JOIN office ao ON  (ao.`parent_office_id` = divo.`office_id`)  OR (ao.`parent_office_id` = ro.`office_id`) AND divo.`office_level_id` = 4 "+

            " LEFT JOIN office bo ON  (bo.`parent_office_id` = ao.`office_id`) OR (bo.`parent_office_id` = divo.`office_id`) OR  "+
            " (bo.`parent_office_id` = ro.`office_id`) OR (bo.`parent_office_id` =  ho.`office_id` ) AND bo.`office_level_id` = 5 ";
        if(dbTableName.headOfficeLeveId == officeLevelId){
            retrieveDashBoardQueryDetails = retrieveDashBoardQueryDetails + "WHERE   ho.`office_id` = "+officeId+"  AND bo.`office_level_id` = "+dbTableName.branchOfficeLeveId+" ";
            if(dbTableName.regionalOfficeLevelApplicable){
                dashBoardObject.officeLabelColumn = "regional_office_name";
                dashBoardObject.officeIdColumn = "regional_office_id";
                dashBoardObject.officeLevelIdColumn = dbTableName.regionalOfficeLeveId;
                groupByColumn = "regional_office_id";
            }else if(dbTableName.divsionalOfficeLevelApplicable) {
                dashBoardObject.officeLabelColumn = "divisional_office_name";
                dashBoardObject.officeIdColumn = "divisional_office_id";
                dashBoardObject.officeLevelIdColumn = dbTableName.divisionalOfficeLeveId;
                groupByColumn = "divisional_office_id";
            }else if(dbTableName.areaOfficeLevelApplicable) {
                dashBoardObject.officeLabelColumn = "area_office";
                dashBoardObject.officeIdColumn = "area_office_id";
                dashBoardObject.officeLevelIdColumn = dbTableName.areaOfficeLeveId;
                groupByColumn = "area_office_id";
            }else{
                dashBoardObject.officeLabelColumn = "branch_name";
                dashBoardObject.officeIdColumn = "branch_office_id";
                dashBoardObject.officeLevelIdColumn = dbTableName.branchOfficeLeveId;
                groupByColumn = "branch_office_id";
            }

        }
        else if(dbTableName.regionalOfficeLeveId == officeLevelId) {
            retrieveDashBoardQueryDetails = retrieveDashBoardQueryDetails + " WHERE   ro.`office_id` = "+officeId+"  AND bo.`office_level_id` = " + dbTableName.branchOfficeLeveId + " ";
            if(dbTableName.divsionalOfficeLevelApplicable) {
                dashBoardObject.officeLabelColumn = "divisional_office_name";
                dashBoardObject.officeIdColumn = "divisional_office_id";
                dashBoardObject.officeLevelIdColumn = dbTableName.divisionalOfficeLeveId;
                groupByColumn = "divisional_office_id";
            }else if(dbTableName.areaOfficeLevelApplicable) {
                dashBoardObject.officeLabelColumn = "area_office";
                dashBoardObject.officeIdColumn = "area_office_id";
                dashBoardObject.officeLevelIdColumn = dbTableName.areaOfficeLeveId;
                groupByColumn = "area_office_id";
            }else{
                dashBoardObject.officeLabelColumn = "branch_name";
                dashBoardObject.officeIdColumn = "branch_office_id";
                dashBoardObject.officeLevelIdColumn = dbTableName.branchOfficeLeveId;
                groupByColumn = "branch_office_id";
            }
        }
        else if(dbTableName.divisionalOfficeLeveId == officeLevelId) {
            retrieveDashBoardQueryDetails = retrieveDashBoardQueryDetails +  " WHERE   divo.`office_id` = "+officeId+" AND bo.`office_level_id` = " + dbTableName.branchOfficeLeveId + " ";
            if(dbTableName.areaOfficeLevelApplicable) {
                dashBoardObject.officeLabelColumn = "area_office";
                dashBoardObject.officeIdColumn = "area_office_id";
                dashBoardObject.officeLevelIdColumn = dbTableName.areaOfficeLeveId;
                groupByColumn = "area_office_id";
            }else{
                dashBoardObject.officeLabelColumn = "branch_name";
                dashBoardObject.officeIdColumn = "branch_office_id";
                dashBoardObject.officeLevelIdColumn = dbTableName.branchOfficeLeveId;
                groupByColumn = "branch_office_id";
            }
        }
        else if(dbTableName.areaOfficeLeveId == officeLevelId) {
            retrieveDashBoardQueryDetails = retrieveDashBoardQueryDetails + " WHERE   ao.`office_id` = "+officeId+"  AND bo.`office_level_id` = " + dbTableName.branchOfficeLeveId + " ";
            dashBoardObject.officeLabelColumn = "branch_name";
            dashBoardObject.officeIdColumn = "branch_office_id";
            dashBoardObject.officeLevelIdColumn = dbTableName.branchOfficeLeveId;
            groupByColumn = "branch_office_id";
        }else{
            dashBoardObject.officeLabelColumn = "branch_name";
            dashBoardObject.officeIdColumn = "branch_office_id";
            dashBoardObject.officeLevelIdColumn = dbTableName.branchOfficeLeveId;
            groupByColumn = "branch_office_id";
        }
           retrieveDashBoardQueryDetails = retrieveDashBoardQueryDetails + " GROUP BY bo.`office_id`) "+
                     " bo ON bo.branch_office_id = grp.office_id ";
                    if(roleId == constantsObj.getBcCoordinatorRoleId()){
                        retrieveDashBoardQueryDetails = retrieveDashBoardQueryDetails + " WHERE grp.bc_id = " +bcOfficeId+" AND bo.head_office_id IS NOT NULL ";
                    }
                    else if(dbTableName.branchOfficeLeveId == officeLevelId) {
                        retrieveDashBoardQueryDetails = retrieveDashBoardQueryDetails + " WHERE bo.branch_office_id = "+officeId+" OR "+officeId+" = -1 AND bo.head_office_id IS NOT NULL ";
                    }
                    else{
                        retrieveDashBoardQueryDetails = retrieveDashBoardQueryDetails + " WHERE bo.branch_office_id = -1 OR -1 = -1 AND bo.head_office_id IS NOT NULL ";
                    }
                retrieveDashBoardQueryDetails = retrieveDashBoardQueryDetails +  " GROUP BY "+groupByColumn+"; ";

        customLog.info("retrieveDashBoardQueryDetails : " + retrieveDashBoardQueryDetails);
        connectionDataSource.getConnection(function (clientConnect) {
            clientConnect.query(retrieveDashBoardQueryDetails, function selectCb(err, results, fields) {
                connectionDataSource.releaseConnectionPool(clientConnect);
                if (err) {
                    customLog.error(err);
                } else {
                    var totalClientsInSourcingProcess = 0;
                    var totalClientsInApprovalProcess = 0;

                    for(var i=0;i<results.length;i++) {
                        var dashBoardDetailsObject = {};
                        dashBoardDetailsObject.displayOfficeName = results[i][dashBoardObject.officeLabelColumn];
                        dashBoardDetailsObject.displayOfficeId = results[i][dashBoardObject.officeIdColumn];
                        dashBoardDetailsObject.displayOfficeLevelId = dashBoardObject.officeLevelIdColumn;
                        //groupDetails
                        dashBoardDetailsObject.groupKycUploaded = results[i].g_kycUploaded;
                        dashBoardDetailsObject.groupImageClarification = results[i].g_image_clarification;
                        dashBoardDetailsObject.groupKycUploading = results[i].g_kycUploading;
                        dashBoardDetailsObject.groupDataVerified = results[i].g_dataVerified;
                        dashBoardDetailsObject.groupCBA = results[i].g_CBA;
                        dashBoardDetailsObject.groupCGT = results[i].g_CGT;
                        dashBoardDetailsObject.groupAssignedFo = results[i].g_assignedFo;
                        dashBoardDetailsObject.groupFieldVerified = results[i].g_fieldVerified;
                        dashBoardDetailsObject.groupAppraised = results[i].g_appraised;
                        dashBoardDetailsObject.groupAuthorized = results[i].g_authorized;
                        dashBoardDetailsObject.groupLoanSanctioned = results[i].g_loanSanctioned;
                        dashBoardDetailsObject.groupGRT = results[i].g_GRT;
                        dashBoardDetailsObject.groupBDU = results[i].g_BDU;
                        //ClientDetails
                        totalClientsInSourcingProcess = totalClientsInSourcingProcess + (results[i].m_kycUploaded + results[i].m_image_clarification + results[i].m_kycUploading + results[i].m_dataVerified + results[i].m_CBA)
                        totalClientsInApprovalProcess = totalClientsInApprovalProcess + (results[i].m_CGT + results[i].m_BDU + results[i].m_assignedFo + results[i].m_fieldVerified + results[i].m_appraised + results[i].m_authorized + results[i].m_loanSanctioned + results[i].m_GRT)
                        dashBoardDetailsObject.memberKycUploaded = results[i].m_kycUploaded;
                        dashBoardDetailsObject.memberImageClarification = results[i].m_image_clarification;
                        dashBoardDetailsObject.memberKycUploading = results[i].m_kycUploading;
                        dashBoardDetailsObject.memberDataVerified = results[i].m_dataVerified;
                        dashBoardDetailsObject.memberCBA = results[i].m_CBA;
                        dashBoardDetailsObject.memberCGT = results[i].m_CGT;
                        dashBoardDetailsObject.memberAssignedFo = results[i].m_assignedFo;
                        dashBoardDetailsObject.memberFieldVerified = results[i].m_fieldVerified;
                        dashBoardDetailsObject.memberAppraised = results[i].m_appraised;
                        dashBoardDetailsObject.memberAuthorized = results[i].m_authorized;
                        dashBoardDetailsObject.memberLoanSanctioned = results[i].m_loanSanctioned;
                        dashBoardDetailsObject.memberGRT = results[i].m_GRT;
                        dashBoardDetailsObject.memberBDU = results[i].m_BDU;
                        dashBoardDetailsObject.officeId = results[i].branch_office_id;
                        if(i == results.length-1){
                            dashBoardDetailsObject.totalClientsInQueue = totalClientsInSourcingProcess + totalClientsInApprovalProcess;
                            dashBoardDetailsObject.totalClientsInSourcingProcess = totalClientsInSourcingProcess;
                            dashBoardDetailsObject.totalClientsInApprovalProcess = totalClientsInApprovalProcess;

                        }
                        dashBoardArrayObject.push(dashBoardDetailsObject);
                    }
                    callback(dashBoardArrayObject);
                }
            });
        });
    },

    saveGroup: function (userId,officeId,areaCodeId,prosGroup,callback) {
        var self=this;
        var prosClient = require(commonDTO +"/prospectClient");
        var constantsObj = this.constants;
        var group_id_for_client = 0;
        var prosGroup = prosGroup;
        var prosClient = new prosClient();
        var overdue = 0;
        var new_group_global_number;
        var query1 = "select grp.*,grp_id.* " +
            "from " +
            "(SELECT IFNULL(MAX(group_global_number)+1,0) AS group_global_number , " +
            "IFNULL(MAX(group_id),0)AS group_id,us.office_id FROM "+dbTableName.iklantUsers+" us  " +
            "left JOIN "+dbTableName.iklantProspectGroup+" pg  ON  pg.office_id  = us.office_id " +
            "GROUP BY pg.office_id " +
            ")grp " +
            "left join " +
            "(select COUNT(prg.group_id) AS glo_acc_num,prg.office_id from "+dbTableName.iklantProspectGroup+" prg " +
            "group BY prg.office_id) grp_id " +
            "on grp_id.office_id = grp.office_id " +
            "GROUP by grp_id.office_id ";
        connectionDataSource.getConnection(function (clientConnect) {
            clientConnect.query(query1,function selectCb(err, results, fields) {
                var group_id = 0;
                var group_global_number = 0;
                var temp_count;
                var office_id;
                var officeIdName;
                if (err) {
                    connectionDataSource.releaseConnectionPool(clientConnect);
                    customLog.error(err);
                } else {
                    for (var i in results) {
                        var fieldName = results[i];
                        if(fieldName.office_id != null){
                            officeIdName = fieldName.office_id;
                            if (officeIdName == officeId) {
                                group_global_number = fieldName.group_id;
                                group_id = fieldName.group_id;
                                temp_count = fieldName.glo_acc_num;
                            }
                            var temp = fieldName.glo_acc_num;
                            group_id_for_client = group_id_for_client + temp;
                        }
                    }
                }
                office_id = officeId;
                var group_id_for_client_local;
                if (group_id_for_client == 0) {
                    group_id_for_client_local = 1;
                }
                if (group_global_number == 0) {
                    new_group_global_number = "00" + office_id + "-00001";
                }
                else if (temp_count >= 1 && temp_count < 9) {
                    new_group_global_number = "00" + office_id + "-0000" + (temp_count + 1);
                    customLog.info("new_group_global_number : " + new_group_global_number);
                }
                else if (temp_count >= 9 && temp_count < 100) {
                    new_group_global_number = "00" + office_id + "-000" + (temp_count + 1);
                }
                else if (temp_count >= 100 && temp_count < 1000) {
                    new_group_global_number = "00" + office_id + "-00" + (temp_count);
                }
                else if (temp_count >= 1000 && temp_count < 10000) {
                    new_group_global_number = "00" + office_id + "-0" + (temp_count);
                }
                else if (temp_count >= 10000) {
                    new_group_global_number = "00" + office_id + "-" + (temp_count);
                }

                var mobileGroupName = prosGroup.getGroup_name().replace("-","-"+areaCodeId+"-");
                if (areaCodeId >= 1 && areaCodeId < 9) {
                    mobileGroupName = prosGroup.getGroup_name().replace("-","-0000"+areaCodeId+"-");
                }else if (areaCodeId >= 9 && areaCodeId < 100) {
                    mobileGroupName = prosGroup.getGroup_name().replace("-","-000"+areaCodeId+"-");
                }else if (areaCodeId >= 100 && areaCodeId < 1000) {
                    mobileGroupName = prosGroup.getGroup_name().replace("-","-00"+areaCodeId+"-");
                }else if (areaCodeId >= 1000 && areaCodeId < 10000) {
                    mobileGroupName = prosGroup.getGroup_name().replace("-","-0"+areaCodeId+"-");
                }else if (areaCodeId >= 10000) {
                    mobileGroupName = prosGroup.getGroup_name().replace("-","-"+areaCodeId+"-");
                }

                var groupStatusId = "";
                if (prosGroup.getLoan_type_id() == constantsObj.getLoanTypeIdJLG()) {
                    groupStatusId = constantsObj.getPreliminaryVerified();
                }
                else if (prosGroup.getLoan_type_id() == constantsObj.getLoanTypeIdSHG()) {
                    groupStatusId = constantsObj.getNewGroup();
                }


                var query2 = "INSERT INTO "+dbTableName.iklantProspectGroup+" (tenant_id,group_global_number,group_name, " +
                    "center_name,office_id,area_code_id, loan_type_id,status_id,group_created_date,created_by,created_date,updated_date,mobile_group_name) " +
                    "VALUES ('" + prosGroup.getTenant_id() + "','" + new_group_global_number + "', " +
                    "'" + prosGroup.getGroup_name() + "','" + prosGroup.getCenter_name() + "', " +
                    "" + prosGroup.getOffice_id() + "," + areaCodeId + "," + prosGroup.getLoan_type_id() + "," + groupStatusId + ", " +
                    "'" + prosGroup.getGroup_created_date() + "'," + userId + ",NOW() + INTERVAL 5 HOUR + INTERVAL 30 MINUTE,NOW() + INTERVAL 5 HOUR + INTERVAL 30 MINUTE,'" + mobileGroupName + "')";
                clientConnect.query(query2,function postCreate(err) {
                    if (err) {
                        connectionDataSource.releaseConnectionPool(clientConnect);
                        customLog.error(err);
                    }else{
                        if (prosGroup.getWeekradio() == 1) {
                            var insertMeetingQuery = "INSERT INTO "+dbTableName.iklantMeeting+" (meeting_type_id, meeting_place, start_date, meeting_time) " +
                                "VALUES(4, '" + prosGroup.getWeeklocation() + "','" + prosGroup.getGroup_created_date() + "', '" + prosGroup.getMeetingTime() + "') ";
                            clientConnect.query(insertMeetingQuery,
                                function postCreate(err) {
                                    if (err) {
                                        customLog.error(err);
                                        connectionDataSource.releaseConnectionPool(clientConnect);
                                    }
                                });
                        }
                        else if (prosGroup.getWeekradio() == 2) {
                            var insertMeetingQuery = "INSERT INTO "+dbTableName.iklantMeeting+" (meeting_type_id, meeting_place, start_date, meeting_time) " +
                                "VALUES(4, '" + prosGroup.getMonthlocation() + "','" + prosGroup.getGroup_created_date() + "', '" + prosGroup.getMeetingTime() + "') ";
                            clientConnect.query(insertMeetingQuery,
                                function postCreate(err) {
                                    if (err) {
                                        customLog.error(err);
                                        connectionDataSource.releaseConnectionPool(clientConnect);
                                    }
                                });
                        }
                        var retrieveMeetingIdQuery = "SELECT MAX(meeting_id) AS meeting_id FROM "+dbTableName.iklantMeeting;
                        var meeting_id;
                        clientConnect.query(retrieveMeetingIdQuery,function selectCb(err, results, fields) {
                            if (err) {
                                connectionDataSource.releaseConnectionPool(clientConnect);
                                customLog.error(err);
                            } else {
                                for (var i in results) {
                                    var fieldName = results[i];
                                    meeting_id = fieldName.meeting_id;
                                }
                                if (prosGroup.getWeekradio() == 1) {
                                    var insertRecurrenceDetailQuery = "INSERT INTO "+dbTableName.iklantRecurrenceDetail+" ( meeting_id, recurrence_id, recur_after) " +
                                        "VALUES(" + meeting_id + "," + prosGroup.getWeekradio() + "," + prosGroup.getRecurweek() + " );	";
                                    clientConnect.query(insertRecurrenceDetailQuery,
                                        function postCreate(err) {
                                            if (err) {
                                                customLog.error(err);
                                                connectionDataSource.releaseConnectionPool(clientConnect);
                                            }
                                        });
                                }
                                else if (prosGroup.getWeekradio() == 2) {

                                    var insertRecurrenceDetailQuery = "INSERT INTO "+dbTableName.iklantRecurrenceDetail+" ( meeting_id, recurrence_id, recur_after) " +
                                        "VALUES(" + meeting_id + "," + prosGroup.getWeekradio() + "," + prosGroup.getEverymonth() + " );	";
                                    clientConnect.query(insertRecurrenceDetailQuery,
                                        function postCreate(err) {
                                            if (err) {
                                                customLog.error(err);
                                                connectionDataSource.releaseConnectionPool(clientConnect);
                                            }
                                        });
                                }
                                var retrieveDetailsIdQuery = "SELECT MAX(details_id) AS details_id FROM "+dbTableName.iklantRecurrenceDetail;
                                var details_id;
                                clientConnect.query(retrieveDetailsIdQuery,
                                    function selectCb(err, results, fields) {
                                        if (err) {
                                            connectionDataSource.releaseConnectionPool(clientConnect);
                                            customLog.error(err);
                                        } else {
                                            for (var i in results) {
                                                var fieldName = results[i];
                                                details_id = fieldName.details_id;
                                            }
                                            if (prosGroup.getWeekradio() == 1) {
                                                var insertRecurOnDayQuery = "INSERT INTO "+dbTableName.iklantRecurOnDay+" (details_id, days) " +
                                                    "VALUES(" + details_id + ", " + prosGroup.getDayorder() + "); ";
                                                clientConnect.query(insertRecurOnDayQuery,
                                                    function postCreate(err) {
                                                        if (err) {
                                                            customLog.error(err);
                                                            connectionDataSource.releaseConnectionPool(clientConnect);
                                                        }
                                                    });
                                            }
                                            else if (prosGroup.getWeekradio() == 2) {

                                                var insertRecurOnDayQuery = "INSERT INTO "+dbTableName.iklantRecurOnDay+" (details_id, day_number) " +
                                                    "VALUES(" + details_id + ", " + prosGroup.getMonthday() + "); ";
                                                clientConnect.query(insertRecurOnDayQuery,
                                                    function postCreate(err) {
                                                        if (err) {
                                                            customLog.error(err);
                                                            connectionDataSource.releaseConnectionPool(clientConnect);
                                                        }
                                                    });
                                            }
                                            var retrieveGroupIdQuery = "SELECT MAX(group_id) AS group_id FROM "+dbTableName.iklantProspectGroup;
                                            var group_id;
                                            clientConnect.query(retrieveGroupIdQuery,
                                                function selectCb(err, results, fields) {
                                                    if (err) {
                                                        connectionDataSource.releaseConnectionPool(clientConnect);
                                                        customLog.error(err);
                                                    } else {
                                                        for (var i in results) {
                                                            var fieldName = results[i];
                                                            group_id = fieldName.group_id;
                                                        }

                                                        var insertProspectGroupQuery = "INSERT INTO "+dbTableName.iklantProspectGroupMeeting+" ( group_id, meeting_id) " +
                                                            "VALUES(" + group_id + ", " + meeting_id + ");";
                                                        clientConnect.query(insertProspectGroupQuery,
                                                            function postCreate(err) {
                                                                if (err) {
                                                                    customLog.error(err);
                                                                }
                                                                connectionDataSource.releaseConnectionPool(clientConnect);
                                                                callback();
                                                            });
                                                    }
                                                }
                                            );
                                        }
                                    }
                                );
                            }
                        });
                    }
                });
            });
        });
    },

    showPreliminaryVerification: function (groupId, callback) {
        var prospectGroup = require(commonDTO+"/prospectGroup");
        var prosClient = require(commonDTO+"/prospectClient");
        var officeRequire = require(commonDTO+"/office");
        var self=this;
        var constantsObj = this.constants;
        var prosGroup = new prospectGroup();
        var prosClient = new prosClient();
        var office = new officeRequire();
        var clientId = new Array();
        var memberName = new Array();
        var docTypeIdArray = new Array();
        var docTypeNameArray = new Array();
        office.clearAll();

        var groupDetailsQuery = "SELECT lt.*,grp_det.group_id,grp_det.group_name,grp_det.center_name,grp_det.group_created_date, " +
            "grp_det.office_id,o.office_name FROM "+dbTableName.iklantProspectGroup+" grp_det INNER JOIN "+dbTableName.iklantOffice+" o ON grp_det.office_id=o.office_id " +
            "LEFT JOIN "+dbTableName.iklantLoanType+" lt ON lt.loan_type_id = grp_det.loan_type_id WHERE group_id=" + groupId + "";
        connectionDataSource.getConnection(function (clientConnect) {
            clientConnect.query(groupDetailsQuery,function selectCb(err, results, fields) {
                if (err) {
                    connectionDataSource.releaseConnectionPool(clientConnect);
                    customLog.error(err);
                } else {
                    for (var i in results) {
                        var fieldName = results[i];
                        prosGroup.setGroup_id(fieldName.group_id);
                        prosGroup.setGroup_name(fieldName.group_name);
                        prosGroup.setCenter_name(fieldName.center_name);
                        prosGroup.setGroup_created_date(dateUtils.formatDateForUI(fieldName.group_created_date));
                        prosGroup.setLoan_type(fieldName.loan_type);
                        office.setOfficeName(fieldName.office_name);
                    }
                }
            });

            var query2 = "SELECT doc_id,doc_name FROM "+dbTableName.iklantDocType+" where doc_entity_id=" + constantsObj.getGroupDocsEntity() + "";
            clientConnect.query(query2,function selectCb(err, results, fields) {
                if (err) {
                    customLog.error(err);
                } else {
                    for (var i in results) {
                        var fieldName = results[i];
                        docTypeIdArray[i] = fieldName.doc_id;
                        docTypeNameArray[i] = fieldName.doc_name;
                    }
                }
                customLog.info("docTypeIdArray : " + docTypeIdArray);
                connectionDataSource.releaseConnectionPool(clientConnect);
                callback(prosGroup, office, prosClient, docTypeIdArray, docTypeNameArray);
            });

            /*var groupMembersDetailsQuery="SELECT client_id,client_name FROM "+dbTableName.iklantProspectClient+" WHERE group_id="+groupId;
             client.query(groupMembersDetailsQuery,
             function selectCb(err, results, fields) {
             if (err) {
             customlog.error(err);
             }else{
             for (var i in results) {
             var fieldName=results[i];
             clientId[i]=fieldName.client_id;
             memberName[i]=fieldName.client_name;
             }
             prosClient.setClientIds(clientId);
             prosClient.setMemberNames(memberName);
             customlog.info("clientId ="+clientId);
             }
             callback(prosGroup,office,prosClient,docTypeIdArray,docTypeNameArray);
             });*/
        });
    },

    showPreliminaryVerificationUpload: function (groupId, callback) {
        var self = this;
        var constantsObj = this.constants;
        var docTypeIdArray = new Array();
        var docTypeNameArray = new Array();
        var query2 = "SELECT doc_id,doc_name FROM "+dbTableName.iklantDocType+" where doc_entity_id=" + constantsObj.getGroupDocsEntity() + "";
        connectionDataSource.getConnection(function (clientConnect) {
            clientConnect.query(query2,function selectCb(err, results, fields) {
                connectionDataSource.releaseConnectionPool(clientConnect);
                if (err) {
                    customLog.error(err);
                } else {
                    for (var i in results) {
                        var fieldName = results[i];
                        docTypeIdArray[i] = fieldName.doc_id;
                        docTypeNameArray[i] = fieldName.doc_name;
                    }
                }
                customLog.info("docTypeIdArray : " + docTypeIdArray);
                callback(docTypeIdArray, docTypeNameArray);
            });
        });
    },

    preVerificationDocumentUpload: function (groupId, fileName, docTypeId, callback) {
        var self=this;
        if (fileName != "") {
            for (var i = 0; i < fileName.length; i++) {
                var fileLoacation = "documents/group_documents/" + fileName[i];
                var query = "INSERT INTO "+dbTableName.iklantGroupDoc+"(image_location,group_id,doc_type_id,doc_name)VALUES('" + fileLoacation + "'," + groupId + "," + docTypeId + ",'" + fileName[i] + "')";
                connectionDataSource.getConnection(function (clientConnect) {
                    clientConnect.query(query,function selectCb(err, results, fields) {
                        connectionDataSource.releaseConnectionPool(clientConnect);
                        if (err) {
                            customLog.error(err);
                        } else {
                            customLog.info("Image Location Inserted..!");
                            if (fileName.length - 1 == i)
                                callback();
                        }
                    });
                });
            }
        }
    },

    KYC_Uploading: function (group_id, callback) {
        var self=this;
        var constantsObj = this.constants;
        var docTypeIdArray = new Array();
        var docTypeNameArray = new Array();
        var memberIdArray = new Array();
        var memberNameArray = new Array();

        var query1 = "SELECT client_id,client_name FROM "+dbTableName.iklantProspectClient+" WHERE " +
            "group_id=" + group_id + " and is_overdue = 0";
        connectionDataSource.getConnection(function (clientConnect) {
            clientConnect.query(query1,function selectCb(err, results, fields) {
                if (err) {
                    connectionDataSource.releaseConnectionPool(clientConnect);
                    customLog.error(err);
                } else {
                    for (var i in results) {
                        var fieldName = results[i];
                        memberIdArray[i] = fieldName.client_id;
                        memberNameArray[i] = fieldName.client_name;
                    }
                }
                customLog.info("memberNameArray : " + memberNameArray);
            });
            var query2 = "SELECT doc_id,doc_name FROM "+dbTableName.iklantDocType+" where " +
                "doc_entity_id=" + constantsObj.getClientDocsEntity() + "";
            clientConnect.query(query2,function selectCb(err, results, fields) {
                if (err) {
                    connectionDataSource.releaseConnectionPool(clientConnect);
                    customLog.error(err);
                } else {
                    for (var i in results) {
                        var fieldName = results[i];
                        docTypeIdArray[i] = fieldName.doc_id;
                        docTypeNameArray[i] = fieldName.doc_name;
                    }
                    connectionDataSource.releaseConnectionPool(clientConnect);
                }
                customLog.info("docTypeIdArray : " + docTypeIdArray);
                callback(group_id, docTypeIdArray, docTypeNameArray, memberIdArray, memberNameArray);
            });
        });
    },

    KYC_UploadingImage: function (client_id, doc_type_id, fileName, callback) {
        //customlog.info("Before captured image insert");
        var self=this;
        var groupId;
        for (var i = 0; i < fileName.length; i++) {
            customLog.info(fileName[i]);
            var fileLoacation = "documents/client_documents/" + fileName[i];
            var query = "INSERT INTO "+dbTableName.iklantClientDoc+"(Captured_image,client_id,doc_type_id,doc_name)VALUES('" + fileLoacation + "'," + client_id + "," + doc_type_id + ",'" + fileName[i] + "')";
            connectionDataSource.getConnection(function (clientConnect) {
                clientConnect.query(query,function selectCb(err, results, fields) {
                    connectionDataSource.releaseConnectionPool(clientConnect);
                    if (err) {
                        customLog.error(err);
                    } else {
                        customLog.info("Image Inserted..!");
                        if (fileName.length - 1 == i)
                            callback();
                    }
                });
            });
        }
    },

    storeCapturedImage: function (client_id, doc_type_id, image, fileName, callback) {
        customLog.info("Before captured image insert");
        var self=this;
        var groupId;
        var fileLoacation = "documents/client_documents/" + fileName + ".png"
        var query = "INSERT INTO "+dbTableName.iklantClientDoc+"(Captured_image,client_id,doc_type_id,doc_name)VALUES('" + fileLoacation + "'," + client_id + "," + doc_type_id + ",'" + fileName + "')";
        var fs = require('fs');
        fs.writeFile(fileLoacation, new Buffer(image.replace(/^data:image\/png;base64,/, ""), "base64"), function (err) {
        });

        connectionDataSource.getConnection(function (clientConnect) {
            clientConnect.query(query,function selectCb(err, results, fields) {
                if (err) {
                    connectionDataSource.releaseConnectionPool(clientConnect);
                    customLog.error(err);
                }
            });
            var getGroupIdQuery = "SELECT group_id FROM "+dbTableName.iklantProspectClient+" WHERE client_id = " + client_id + " ";
            customLog.info("getGroupIdQuery:====>" + getGroupIdQuery);
            clientConnect.query(getGroupIdQuery, function selectCb(err, results, fields) {
                if (err) {
                    connectionDataSource.releaseConnectionPool(clientConnect);
                    customLog.error(err);
                } else {
                    for (var i in results) {
                        groupId = results[i].group_id;
                    }
                    customLog.info("groupId : " + groupId);
                    connectionDataSource.releaseConnectionPool(clientConnect);
                    callback(groupId);
                }
            });
        });
    },

    storePreliminaryVerificationCapturedImage: function (groupId, doc_type_id, image, fileName, callback) {
        var self=this;
        var fileLoacation = "documents/group_documents/" + fileName + ".png"
        var query = "INSERT INTO "+dbTableName.iklantGroupDoc+"(image_location,group_id,doc_type_id,doc_name)VALUES('" + fileLoacation + "'," + groupId + "," + doc_type_id + ",'" + fileName + "')";
        var fs = require('fs');
        fs.writeFile(fileLoacation, new Buffer(image.replace(/^data:image\/png;base64,/, ""), "base64"), function (err) {
        });

        customLog.info(query);
        connectionDataSource.getConnection(function (clientConnect) {
            clientConnect.query(query,function selectCb(err, results, fields) {
                connectionDataSource.releaseConnectionPool(clientConnect);
                if (err) {
                    customLog.error(err);
                } else {
                    customLog.info("Image Location Inserted..");
                    callback(groupId);
                }
            });
        });
    },

    groupDetails: function (tenant_id, office_id, callback) {
        var groupIdArray = new Array();
        var groupNameArray = new Array();
        var self=this;
        var constantsObj = this.constants;
        var query = "SELECT group_id,group_name FROM "+dbTableName.iklantProspectGroup+" WHERE status_id " +
            "IN(" + constantsObj.getNewGroup() + "," + constantsObj.getPreliminaryVerified() + ", " +
            "" + constantsObj.getKYCUploaded() + "," + constantsObj.getAssignedFO() + ") and " +
            "tenant_id=" + tenant_id + " AND office_id=" + office_id + "";
        customLog.info("Sync Group Query : " + query);
        connectionDataSource.getConnection(function (clientConnect) {
                clientConnect.query(query,
                    function selectCb(err, results, fields) {
                        connectionDataSource.releaseConnectionPool(clientConnect);
                        if (err) {
                            customLog.error(err);
                        } else {
                            for (var i in results) {
                                var fieldName = results[i];
                                groupIdArray[i] = fieldName.group_id;
                                groupNameArray[i] = fieldName.group_name;
                            }
                        }
                        customLog.info(groupNameArray);
                        callback(groupIdArray, groupNameArray);
                    }
                );
            }
        );
    },

    documentDetails: function (tenant_id, callback) {
        var doc_id = new Array();
        var doc_type = new Array();
        var self= this;
        var query = "SELECT doc_id,doc_name FROM "+dbTableName.iklantDocType+" WHERE tenant_id=" + tenant_id + "";
        connectionDataSource.getConnection(function (clientConnect) {
                clientConnect.query(query,
                    function selectCb(err, results, fields) {
                        connectionDataSource.releaseConnectionPool(clientConnect);
                        if (err) {
                            customLog.error(err);
                        } else {
                            for (var i in results) {
                                doc_id[i] = results[i].doc_id;
                                doc_type[i] = results[i].doc_name;
                            }
                        }
                        customLog.info(doc_type);
                        callback(doc_id, doc_type);
                    }
                );
            }
        );
    },

    memberDetails: function (tenant_id, office_id, callback) {
        var groupIdArray = new Array();
        var memberIdArray = new Array();
        var memberNameArray = new Array();
        var self=this;
        var constantsObj = this.constants;
        var query = "SELECT pc.group_id,pc.client_id,pc.client_name FROM "+dbTableName.iklantProspectClient+" pc " +
            "INNER JOIN "+dbTableName.iklantProspectGroup+" pg ON pg.group_id=pc.group_id WHERE pg.tenant_id=" + tenant_id + " " +
            "AND pc.status_id IN (" + constantsObj.getNewGroup() + "," + constantsObj.getPreliminaryVerified() + ", " +
            "" + constantsObj.getKYCUploaded() + "," + constantsObj.getAssignedFO() + ") AND pg.status_id " +
            "IN (" + constantsObj.getNewGroup() + "," + constantsObj.getPreliminaryVerified() + ", " +
            "" + constantsObj.getKYCUploaded() + "," + constantsObj.getAssignedFO() + ") AND pg.office_id=" + office_id + " ";
        customLog.info("Sync Clients Query : " + query);
        connectionDataSource.getConnection(function (clientConnect) {
                clientConnect.query(query,
                    function selectCb(err, results, fields) {
                        connectionDataSource.releaseConnectionPool(clientConnect);
                        if (err) {
                            customLog.error(err);
                        } else {
                            for (var i in results) {
                                groupIdArray[i] = results[i].group_id;
                                memberIdArray[i] = results[i].client_id;
                                memberNameArray[i] = results[i].client_name;
                            }
                        }
                        callback(groupIdArray, memberIdArray, memberNameArray);
                    }
                );
            }
        );
    },

    availableDocumentDetailsDatamodel: function (tenant_id, office_id, callback) {
        var groupIdArray = new Array();
        var memberIdArray = new Array();
        var docNameArray = new Array();
        var docTypeArray = new Array();
        var self=this;
        var constantsObj = this.constants;
        var preliminaryVerified = constantsObj.getPreliminaryVerified();
        var query = "SELECT pg.group_id,pc.client_id,cd.doc_name,cd.doc_type_id FROM "+dbTableName.iklantProspectClient+" pc " +
            "INNER JOIN "+dbTableName.iklantClientDoc+" cd ON cd.client_id = pc.client_id " +
            "INNER JOIN "+dbTableName.iklantProspectGroup+" pg ON pg.group_id = pc.group_id " +
            "WHERE pc.status_id=" + preliminaryVerified + " AND pg.status_id=" + preliminaryVerified + " " +
            "AND pg.tenant_id = " + tenant_id + " AND pg.office_id = " + office_id + " order by client_id";
        customLog.info("Sync available Document details Query : " + query);
        connectionDataSource.getConnection(function (clientConnect) {
            clientConnect.query(query,function selectCb(err, results, fields) {
                connectionDataSource.releaseConnectionPool(clientConnect);
                if (err) {
                    customLog.error(err);
                } else {
                    for (var i in results) {
                        groupIdArray[i] = results[i].group_id;
                        memberIdArray[i] = results[i].client_id;
                        docNameArray[i] = results[i].doc_name;
                        docTypeArray[i] = results[i].doc_type_id;
                    }
                }
                callback(groupIdArray, memberIdArray, docNameArray, docTypeArray);
            });
        });
    },

    //Sindhu
    saveKycUpload: function (groupId, callback) {
        var self=this;
        var constantsObj = this.constants;
        var groupIds = new Array();
        var clientDocCount;
        var noOfClients;
        var errorMsg = "All the client documents not yet uploaded";
        var clientDoc_query = "SELECT * FROM(SELECT COUNT(DISTINCT cd.client_id)AS client_doc_count " +
            "FROM "+dbTableName.iklantClientDoc+" cd INNER JOIN "+dbTableName.iklantProspectClient+" pc ON  pc.client_id=cd.client_id " +
            "WHERE cd.doc_type_id=" + constantsObj.getApplicationFormDocId() + " " +
            "AND pc.group_id=" + groupId + ")aa " +
            "JOIN(SELECT COUNT(client_id) AS no_of_clients FROM "+dbTableName.iklantProspectClient+" " +
            "WHERE group_id=" + groupId + " AND is_overdue=0)bb";
        connectionDataSource.getConnection(function (clientConnect) {
            clientConnect.query(clientDoc_query,function selectCb(err, results, fields) {
                if (err) {
                    connectionDataSource.releaseConnectionPool(clientConnect);
                    customLog.error(err);
                } else {
                    for (var i in results) {
                        var fieldName = results[i];
                        clientDocCount = fieldName.client_doc_count;
                        noOfClients = fieldName.no_of_clients;
                    }
                }
                customLog.info("clientDocCount : " + clientDocCount);
                customLog.info("noOfClients : " + noOfClients);
                if (clientDocCount == noOfClients) {
                    errorMsg = "";
                    var groupInsertQuery = "UPDATE "+dbTableName.iklantProspectGroup+" pg, "+dbTableName.iklantProspectClient+" pc " +
                        "SET pg.status_id=" + constantsObj.getKYCUploaded() + ", " +
                        "pc.status_id=if(pc.is_overdue=0," + constantsObj.getKYCUploaded() + ",pc.status_id), " +
                        "pg.updated_date=NOW() + INTERVAL 5 HOUR + INTERVAL 30 MINUTE,pc.updated_date=NOW() + INTERVAL 5 HOUR + INTERVAL 30 MINUTE WHERE pg.group_id = " + groupId + " AND pc.group_id = " + groupId + " ";
                    customLog.info("groupInsertQuery : " + groupInsertQuery);
                    clientConnect.query(groupInsertQuery, function postCreate(err) {
                        if (err) {
                            connectionDataSource.releaseConnectionPool(clientConnect);
                            customLog.error(err);
                        }
                    });
                }
                connectionDataSource.releaseConnectionPool(clientConnect);
                customLog.error("errorMsg : " + errorMsg);
                callback(errorMsg);
            });
        });
    },

    saveAssignFO: function (foName, assignGroupIds, callback) {
        var self = this;
        var constantsObj = this.constants;
        self.ComDataModel = new ComDataModel(constantsObj);
        var flowId = constantsObj.getNextFlowId();
        var otherParams = ",pg.is_credit_check_completed = 0,pg.assigned_to='" + foName + "'";
        connectionDataSource.getConnection(function (clientConnect) {
            var getClientListQuery = "SELECT * FROM "+dbTableName.iklantProspectClient+" ipc JOIN iklant_prospect_group ipg ON ipg.group_id = ipc.group_id " +
                " WHERE ipg.group_id IN ("+assignGroupIds+") AND ipg.status_id = ipc.status_id";
            customLog.info("getClientListQuery "+getClientListQuery);
            clientConnect.query(getClientListQuery, function (err, results) {
                if(err){
                    connectionDataSource.releaseConnectionPool(clientConnect);
                    customLog.error("Error in getClientListQuery "+err);
                    callback('failure');
                } else {
                    var clientId = new Array();
                    for(var i in results){
                        clientId[i] = results[i].client_id;
                    }
                    clientId = results.length > 0 ? clientId : 0;
                    self.ComDataModel.updateClientStatusIdForNextOperation(clientId, "", constantsObj.getCreditBureauAnalysedStatus(), flowId, clientConnect, "", false, function(status){
                        if(status == 'success'){
                            self.ComDataModel.updateGroupStatusIdForNextOperation(assignGroupIds, constantsObj.getCreditBureauAnalysedStatus(), flowId, clientConnect, otherParams, false, function(groupStatus){
                                if(groupStatus == 'success'){
                                    connectionDataSource.releaseConnectionPool(clientConnect);
                                }
                                callback(groupStatus);
                            });
                        }else{
                            callback(status);
                        }
                    });
                }
            });
        });
    },
    saveAssignDEODataModel:function(deoName, assignGroupIds,officeValue, callback){
        var self = this;
        var constantsObj = this.constants;
        var groupIds=new Array();
        groupIds=assignGroupIds;
        var groupId=groupIds.split(",");
        connectionDataSource.getConnection(function (clientConnect) {
        clientConnect.beginTransaction(function (err) {
            for(var i=0;i<groupId.length;i++) {
                var assignDEOQuery = "INSERT INTO iklant_assign_deo(group_id,assigned_to,created_date,updated_date) VALUES (" + groupId[i] +","+ deoName + ",NOW() + INTERVAL 5 HOUR + INTERVAL 30 MINUTE,NOW() + INTERVAL 5 HOUR + INTERVAL 30 MINUTE" + ")";
                    customLog.info("assignDEOQuery" + assignDEOQuery);
                        if (err) {
                            customLog.error(err)
                        }
                        clientConnect.query(assignDEOQuery, function postCreate(err) {
                            if (err) {
                                clientConnect.rollback(function () {
                                    customLog.error(err);
                                    callback('failure');
                                });
                            }
                            else {
                                clientConnect.commit(function (err) {
                                    if (err) {
                                        clientConnect.rollback(function () {

                                        });
                                    }
                                });
                                if(i == groupId.length){
                                    connectionDataSource.releaseConnectionPool(clientConnect);
                                    callback('success');
                                }
                            }
                        });
            }

        });

        });
    },
    cca1RejectClients: function (rejectedClientName, remarksToReject, roleId, callback) {
        var self = this;
        var constantsObj = this.constants;
        self.ComDataModel = new ComDataModel(constantsObj);
        var groupId,branchId;
        var getGroupIdQuery = "SELECT pg.group_id,pg.office_id FROM "+dbTableName.iklantProspectClient+" pc " +
            "INNER JOIN "+dbTableName.iklantProspectGroup+" pg ON pg.group_id = pc.group_id " +
            "WHERE pc.client_id IN (" + rejectedClientName + ")";
        
        connectionDataSource.getConnection(function (clientConnect) {
            clientConnect.query(getGroupIdQuery,function(error, groupResults) {
                if (error) {
                    connectionDataSource.releaseConnectionPool(clientConnect);
                    customLog.error(error);
                    callback(groupId, branchId);
                } else {
                    var flowId = constantsObj.getRejectFlowId();
                    var otherParams = ",pc.remarks_for_rejection='" + remarksToReject + "'";
                    groupId = groupResults[0].group_id;
                    branchId = groupResults[0].office_id;
                    if(roleId == constantsObj.getSMHroleId()){
                        self.ComDataModel.updateClientStatusIdForNextOperation(rejectedClientName, "", constantsObj.getGroupRecognitionTested(), flowId, clientConnect, otherParams, false, function(status){
                            if(status == 'success'){
                                clientConnect.query("DELETE FROM " + dbTableName.iklantRejectedClientStatus + " WHERE client_id = "+rejectedClientName,function(err) {
                                    connectionDataSource.releaseConnectionPool(clientConnect);
                                    if (err) {
                                        customLog.error(err);
                                    }
                                    callback(groupId, branchId);
                                });
                            }else{
                                callback("", "");
                            }
                        });
                    }
                    else {
                        var clients = rejectedClientName.split(",");
                        var groupIdForUpdate = (clients.length > 1) ? groupId : "";
                        self.ComDataModel.updateClientStatusIdForNextOperation(rejectedClientName,groupIdForUpdate, constantsObj.getFieldVerified(), flowId, clientConnect, otherParams, false, function(status){
                            if(status == 'success'){
                                connectionDataSource.releaseConnectionPool(clientConnect);
                                callback(groupId, branchId);
                            }else{
                                callback("", "");
                            }
                        });
                    }
                }
            });
        });
    },

    updateLoanAmountDataModel : function(clientIds,loanAmountArray,callback){
        var self = this;
        if(loanAmountArray.length > 0){
            connectionDataSource.getConnection(function (clientConnect) {
                self.updateLoanAmountInternally(0, clientIds, loanAmountArray, clientConnect, function(status){
                    connectionDataSource.releaseConnectionPool(clientConnect);
                    callback(status);
                });
            });
        } else {
            callback("failure");
        }
    },
    
    updateLoanAmountInternally: function(count, clientIds, loanAmountArray, clientConnect, callback){
        var self = this;
        var updateLoanAmount = " UPDATE "+dbTableName.iklantProspectClient+" SET loan_amount_sanctioned="+loanAmountArray[count]+" WHERE client_id ="+clientIds[count]+";";
        customLog.info("updateLoanAmount "+updateLoanAmount);
        clientConnect.query(updateLoanAmount, function(err, results) {
            if(err){
                customLog.error("Error in updateLoanAmount " + err);
                callback("failure");
            }else{
                count++;
                if(count == loanAmountArray.length){
                    callback("success");
                }else{
                    self.updateLoanAmountInternally(count, clientIds, loanAmountArray, clientConnect, callback);
                }
            }
        });
    },

    cca1approvedGroup: function (rejectedClientName, approvedGroupName, callback) {
        var self = this;
        var constantsObj = this.constants;
        self.ComDataModel = new ComDataModel(constantsObj);
        var flowId = constantsObj.getNextFlowId();
        
        connectionDataSource.getConnection(function (clientConnect) {
            self.ComDataModel.updateClientStatusIdForNextOperation("", approvedGroupName, constantsObj.getFieldVerified(), flowId, clientConnect, "", false, function(status){
                if(status == 'success'){
                    self.ComDataModel.updateGroupStatusIdForNextOperation(approvedGroupName, constantsObj.getFieldVerified(), flowId, clientConnect, "", false, function(groupStatus){
                        if(groupStatus == 'success'){
                            self.getBranchId(clientConnect, approvedGroupName, callback);
                        }else{
                            callback();
                        }
                    });
                }else{
                    callback();
                }
            });
        });
    },

    cca1rejectedGroup: function (approvedGroupName,remarks,approveOrRejectFlag,currentStatusId, callback) {
        var self = this;
        var constantsObj = this.constants;
        self.ComDataModel = new ComDataModel(constantsObj);
        var flowId = constantsObj.getRejectFlowId();
        var activClientQuery = "SELECT COUNT(pc.client_id) AS active_clients FROM "+dbTableName.iklantProspectClient+" pc WHERE pc.group_id='" + approvedGroupName + "' AND pc.status_id =" + constantsObj.getFieldVerified();
        
        connectionDataSource.getConnection(function (clientConnect) {
            clientConnect.query(activClientQuery,function(err, activClientsResult) {
                if (err) {
                    connectionDataSource.releaseConnectionPool(clientConnect);
                    customLog.error(err);
                    callback();
                }else{
                    var otherParams = (activClientsResult.length > 0 && activClientsResult[0].active_clients >= constantsObj.getMinimumNumberOfClients()) ? ",pg.rejected_less_no_of_clients = 0" : ",pg.rejected_less_no_of_clients = 1";
                    if(remarks != null){
                        otherParams += ",rejection_remarks='"+remarks+"'";
                    }
                    self.ComDataModel.updateGroupStatusIdForNextOperation(approvedGroupName, currentStatusId, flowId, clientConnect, otherParams, false, function(groupStatus){
                        if(groupStatus == 'success'){
                            flowId = approveOrRejectFlag == 1 ? constantsObj.getNextFlowId() : constantsObj.getCurrentFlowId();
                            self.ComDataModel.updateClientStatusIdForNextOperation("", approvedGroupName, constantsObj.getFieldVerified(), flowId, clientConnect, "", false, function(status){
                                if(status == 'success'){
                                    self.getBranchId(clientConnect, approvedGroupName, callback);
                                }else{
                                    callback();
                                }
                            });
                        }else{
                            callback();
                        }
                    });
                }
            });
        });
    },

    getBranchId: function(clientConnect, approvedGroupName, callback){
        var getGroupIdQuery = "SELECT office_id FROM "+dbTableName.iklantProspectGroup+" WHERE group_id =" + approvedGroupName;
        clientConnect.query(getGroupIdQuery,function(err, results, fields) {
            connectionDataSource.releaseConnectionPool(clientConnect);
            if (err) {
                callback();
                customLog.error(err);
            } else {
                callback(approvedGroupName, results[0].office_id);
            }
        });
    },
    
    toInsertGroup: function (groupId, callback) {
        var constantsObj = this.constants;
        var group_id = groupId;
        connectionDataSource.getConnection(function (clientConnect) {
            var ciQuery = "SELECT pg.`group_id`,pg.group_name,pg.`center_name` AS displayName, pg.loan_count, "+
                "   pg.`created_by` AS loanOfficerId, imm.mifos_customer_id,"+
                "   DATE_FORMAT(NOW() + INTERVAL 5 HOUR + INTERVAL 30 MINUTE,'%Y-%m-%d') AS mfiJoiningDate, "+
                "   DATE_FORMAT(NOW() + INTERVAL 5 HOUR + INTERVAL 30 MINUTE,'%Y-%m-%d') AS activationDate, "+
                "   pg.`office_id` AS officeId, "+
                "   IFNULL(pcp.`line1`,'') AS line1,IFNULL(pcp.`line2`,'') AS line2,IFNULL(pcp.`line3`,'') AS line3,"+
                "   IFNULL(pcp.`city`,'') AS city,"+
                "   st.state_name AS state, "+
                "   'India' AS country, "+
                "   pcp.pincode AS zip, "+
                "   pcp.`mobile_number` AS phoneNumber, "+
                "   pcp.`landline_number` AS landlineNumber, "+
                "   IF(rd.`recurrence_id` = 1,rod.days,IF(rod.day_number = 0,1,rod.day_number)) AS dayNumber, "+
                "   rd.`recur_after` AS recureAfter, "+
                "   m.`meeting_place` AS meetingPlace, "+
                "   m.`meeting_time` AS meetingTime, "+
                "   rd.`recurrence_id` AS recurrenceType "+
                "   FROM "+dbTableName.iklantProspectGroup+" pg "+
                "   INNER JOIN "+dbTableName.iklantProspectClient+" pc ON pc.`group_id` = pg.`group_id` "+
                "   INNER JOIN "+dbTableName.iklantProspectClientPersonal+" pcp ON pcp.`client_id` = pc.`client_id` "+
                "   INNER JOIN "+dbTableName.iklantOffice+" o ON o.office_id = pg.office_id "+
                "   INNER JOIN iklant_state_list st ON st.state_id = o.state_id "+
                "   LEFT JOIN "+dbTableName.iklantProspectGroupMeeting+" pgm ON pgm.`group_id` = pg.`group_id` "+
                "   LEFT JOIN "+dbTableName.iklantMeeting+" m ON m.`meeting_id` = pgm.`meeting_id` "+
                "   LEFT JOIN "+dbTableName.iklantRecurrenceDetail+" rd ON rd.`meeting_id` = m.`meeting_id` "+
                "   LEFT JOIN "+dbTableName.iklantRecurOnDay+" rod ON rod.`details_id` = rd.`details_id` "+
                "   LEFT JOIN "+dbTableName.iklantMifosMapping+" imm ON imm.group_id = pg.group_id "+
                "   WHERE CASE WHEN (SELECT client_id FROM iklant_prospect_client WHERE sub_leader_global_number  = (SELECT leader_global_number FROM "+dbTableName.iklantProspectGroup+" WHERE group_id =  " + group_id + ") AND status_id = " + constantsObj.getGroupRecognitionTested() + " ) IS NOT NULL THEN"+
                "   pg.group_id = " + group_id + " AND pc.client_id = (SELECT client_id FROM " +dbTableName.iklantProspectClient+ " WHERE sub_leader_global_number  = (SELECT leader_global_number FROM "+dbTableName.iklantProspectGroup+" WHERE group_id =  "+ group_id +") AND status_id = " + constantsObj.getGroupRecognitionTested() + " )"+
                "   ELSE  pc.client_id = (SELECT client_id FROM "+dbTableName.iklantProspectClient+" WHERE group_id = "+ group_id +" AND status_id = " + constantsObj.getGroupRecognitionTested() + " LIMIT 1 ) END "+
                "   LIMIT 1";
            customLog.info("ciQuery "+ciQuery);
            clientConnect.query(ciQuery, function selectCb(err, groupDetailsResultSet, fields) {
                if (err) {
                    connectionDataSource.releaseConnectionPool(clientConnect);
                    customLog.error(err);
                } else{
                    var lookupValues = require(path.join(rootPath,"app_modules/utils/lookupValues"));
                    var clientQuery =  "SELECT 	pc.client_id,pc.client_global_number, pc.`client_name` AS firstName,   "+
                        "   pc.`client_last_name` AS lastName,pc.loan_count,IFNULL(imm.`mifos_client_customer_id`,0) AS mifos_client_customer_id, "+
                        "   DATE_FORMAT(pcp.`date_of_birth`,'%Y%m%d') AS dateOfBirth, "+
                        "   IFNULL(pcp.`line1`,'') AS line1,IFNULL(pcp.`line2`,'') AS line2,IFNULL(pcp.`city`,'') AS city,"+
                        "   st.state_name AS state, "+
                        "   pcp.pincode AS zip, "+
                        "   pcp.`mobile_number` AS phoneNumber, "+
                        "   pc.`created_by` AS formedBy, "+
                        "   IF(pcp.`gender` = "+lookupValues.iklantGenderMale+","+lookupValues.mifosSaluationMr+",IF(pcp.`marital_status` = "+lookupValues.iklantMaritalStatusUnmarried+","+lookupValues.mifosSaluationMs+","+lookupValues.mifosSaluationMrs+")) AS salutation, "+
                        "   IF(pcp.`gender` = "+lookupValues.iklantGenderMale+","+lookupValues.mifosGenderMale+","+lookupValues.mifosGenderFemale+") AS gender, "+
                        "   IF(pcp.`marital_status` = "+lookupValues.iklantMaritalStatusMarried+","+lookupValues.mifosMaritalStatusMarried+","+
                        "   IF(pcp.`marital_status` = "+lookupValues.iklantMaritalStatusUnmarried+","+lookupValues.mifosMaritalStatusUnmarried+","+
                        "   IF(pcp.`marital_status` = "+lookupValues.iklantMaritalStatusDivorced+","+lookupValues.mifosMaritalStatusDivorced+","+
                        "   IF(pcp.`marital_status` = "+lookupValues.iklantMaritalStatusWidow+","+lookupValues.mifosMaritalStatusWidow+","+lookupValues.mifosMaritalStatusMarried+")))) AS maritialStatus,"+
                        "   IF(pcp.`religion` = "+lookupValues.iklantReligionHindu+","+lookupValues.mifosReligionHindu+","+
                        "   IF(pcp.`religion` = "+lookupValues.iklantReligionChristian+","+lookupValues.mifosReligionChristian+","+
                        "   IF(pcp.`religion` = "+lookupValues.iklantReligionMuslim+","+lookupValues.mifosReligionMuslim+","+lookupValues.mifosReligionOthers+"))) AS religion, "+
                        "   IF(pcp.`educational_details` = "+lookupValues.iklantEducationalDetailsAboveSSLC+","+lookupValues.mifosEducationalDetailsAboveSSLC+","+
                        "   IF(pcp.`educational_details` = "+lookupValues.iklantEducationalDetailsAnyDegree+","+lookupValues.mifosEducationalDetailsAnyDegree+","+lookupValues.mifosEducationalDetailsBelowSSLC+")) AS educationalQualification, "+
                        "   IF(pcp.`nationality` = "+lookupValues.iklantNationalityIndian+","+lookupValues.mifosNationalityIndian+","+lookupValues.mifosNationalityOthers+") AS nationality, "+
                        "   IF(pc.`family_monthly_income`< 5000,"+lookupValues.mifosPovertyStatusVPoor+",IF(pc.`family_monthly_income` > 5000 && pc.`family_monthly_income` <= 10000,"+lookupValues.mifosPovertyStatusPoor+","+lookupValues.mifosPovertyStatusNPoor+")) AS povertyStatus, "+
                        "   IFNULL(pcp.`guardian_name`,pcg.`guarantor_name`) AS spouseFatherFirstName, "+
                        "   IFNULL(pcp.`guardian_lastname`,pcg.`guarantor_name`) AS spouseFatherLastName, "+
                        "   IF(pcg.`guarantor_relationship` = "+lookupValues.iklantGuarantorRelationshipFather+","+lookupValues.mifosGuarantorRelationshipFather+","+
                        "   IF(pcg.`guarantor_relationship` = "+lookupValues.iklantGuarantorRelationshipMother+","+lookupValues.mifosGuarantorRelationshipMother+","+
                        "   IF(pcg.`guarantor_relationship` = "+lookupValues.iklantGuarantorRelationshipHusband+","+lookupValues.mifosGuarantorRelationshipHusband+","+
                        "   IF(pcg.`guarantor_relationship` = "+lookupValues.iklantGuarantorRelationshipFIL+","+lookupValues.mifosGuarantorRelationshipFIL+","+
                        "   IF(pcg.`guarantor_relationship` = "+lookupValues.iklantGuarantorRelationshipMIL+","+lookupValues.mifosGuarantorRelationshipMIL+","+
                        "   IF(pcg.`guarantor_relationship` = "+lookupValues.iklantGuarantorRelationshipSon+","+lookupValues.mifosGuarantorRelationshipSon+","+
                        "   IF(pcg.`guarantor_relationship` = "+lookupValues.iklantGuarantorRelationshipDIL+","+lookupValues.mifosGuarantorRelationshipDIL+","+
                        "   IF(pcg.`guarantor_relationship` = "+lookupValues.iklantGuarantorRelationshipSIL+","+lookupValues.mifosGuarantorRelationshipSIL+","+
                        "   IF(pcg.`guarantor_relationship` = "+lookupValues.iklantGuarantorRelationshipBrother+","+lookupValues.mifosGuarantorRelationshipBrother+","+
                        "   IF(pcg.`guarantor_relationship` = "+lookupValues.iklantGuarantorRelationshipDaughter+","+lookupValues.mifosGuarantorRelationshipDaughter+","+lookupValues.mifosGuarantorRelationshipOthers+")))))))))) AS spouseFatherNameType, "+
                        "   pg.`created_by` AS loanOfficerId, "+
                        "   pg.`office_id` AS officeId, "+
                        "   pcp.`ration_card_number` AS rationCardNumber, "+
                        "   pcp.`voter_id` AS voterId, "+
                        "   IF(pcp.`caste` = "+lookupValues.iklantCasteOC+",'OC', "+
                        "   IF(pcp.`caste` = "+lookupValues.iklantCasteBC+",'BC', "+
                        "   IF(pcp.`caste` = "+lookupValues.iklantCasteMBC+",'MBC', "+
                        "   IF(pcp.`caste` = "+lookupValues.iklantCasteSC+",'SC', "+
                        "   IF(pcp.`caste` = "+lookupValues.iklantCasteST+",'ST','Others'))))) AS caste, "+
                        "   IF(pcb.`is_bank_account` = 1,'Yes','No') AS isBankAccountAvailable, "+
                        "   IF(pcb.`is_insurance_lifetime` IS NOT NULL,'Yes','No') AS isInsuranceAvailable, "+
                        "   pch.`vehicle_details` AS asset, "+
                        "   IF(pch.house_type = "+lookupValues.iklantHouseHypeOwn+",'Yes','No') ownHouse, "+
                        "   IF(pc.`family_monthly_income` < 5000,'Less than 5000',IF(pc.`family_monthly_income` > 5000 && pc.`family_monthly_income` <= 10000,'Up to Rs.10000','Above Rs.10000')) AS borrowersHouseholdIncome, "+
                        "   (SELECT COUNT(pcf.`client_id`) FROM "+dbTableName.iklantProspectClientFamilyDetail+" pcf WHERE pcf.`client_id` = pc.`client_id`) AS earningMembersInTheBorrowerFamily, "+
                        "   IF(pc.`loan_repayment_track_record` = "+lookupValues.iklantLoanRepaymentTrackRecordNew+",'New', "+
                        "   IF(pc.`loan_repayment_track_record` = "+lookupValues.iklantLoanRepaymentTrackRecordVGood+",'Very Good', "+
                        "   IF(pc.`loan_repayment_track_record` = "+lookupValues.iklantLoanRepaymentTrackRecordGood+",'Good','Average'))) AS borrowersLoanRepaymentTrackRecord, "+
                        "   pcp.`aadhaar_number` AS aadhaarNumber, "+
                        "   pcp.`gas_number` AS gasNumber, "+
                        "   pcp.`other_id1` AS otherId1, "+
                        "   pcp.`other_id2` AS otherId2 "+
                        "   FROM "+dbTableName.iklantProspectGroup+" pg "+
                        "   INNER JOIN "+dbTableName.iklantProspectClient+" pc ON pc.`group_id` = pg.`group_id` "+
                        "   INNER JOIN "+dbTableName.iklantProspectClientPersonal+" pcp ON pcp.`client_id` = pc.`client_id` "+
                        "   INNER JOIN "+dbTableName.iklantOffice+" o ON o.office_id = pg.office_id "+
                        "   INNER JOIN iklant_state_list st ON st.state_id = o.state_id "+
                        "   LEFT JOIN "+dbTableName.iklantProspectClientGuarantor+" pcg ON pcg.`client_id` = pc.`client_id` "+
                        "   LEFT JOIN "+dbTableName.iklantprospectClientBankDetail+" pcb ON pcb.`client_id` = pc.`client_id` "+
                        "   LEFT JOIN "+dbTableName.iklantProspectClientHouseDetail+" pch ON pch.`client_id` = pc.`client_id` "+
                        "   LEFT JOIN "+dbTableName.iklantMifosMapping+" imm ON imm.client_id = pc.client_id "+
                        "   WHERE pc.`group_id` = " + group_id+" AND pc.status_id = " + constantsObj.getGroupRecognitionTested() + " GROUP BY pc.client_id "+
                        "   ORDER BY pc.client_id ";
                    customLog.info("clientQuery:" + clientQuery);
                    clientConnect.query(clientQuery, function selectCb(err, clientDetailsResultSet) {
                        customLog.info("In Client Detail");
                        if (err) {
                            connectionDataSource.releaseConnectionPool(clientConnect);
                            customLog.error(err);
                        } else{
                            var mifosClientStatus = 1;
                            for(var i=0; i<clientDetailsResultSet.length; i++){
                                if(clientDetailsResultSet[i].mifos_client_customer_id == null  && clientDetailsResultSet[i].loan_count > 1){
                                    mifosClientStatus = 0;
                                }
                            }
                            var mifosRejectedCustomerIdQuery = "SELECT c.customer_id FROM customer c " +
                                "INNER JOIN iklant_mifos_mapping imm ON imm.mifos_client_customer_id = c.customer_id " +
                                "INNER JOIN iklant_prospect_group ipg ON ipg.`group_id` = imm.`group_id` "+
                                "INNER JOIN iklant_prospect_client ipc ON ipc.client_id = imm.client_id " +
                                "WHERE ipc.status_id NOT IN (" + constantsObj.getGroupRecognitionTested() + ") AND ipc.group_id = " + group_id + " AND imm.mifos_client_customer_id IS NOT NULL "+
                                "AND ipc.`loan_count` = ipg.`loan_count` ";
                            var rejectedClientIds = new Array();
                            console.log("mifosRejectedCustomerIdQuery:"+mifosRejectedCustomerIdQuery);
                            clientConnect.query(mifosRejectedCustomerIdQuery, function selectCb(error, rejectedClientDetails) {
                                connectionDataSource.releaseConnectionPool(clientConnect);
                                if(!error){
                                    for(var i=0;i<rejectedClientDetails.length;i++){
                                        rejectedClientIds[i] = rejectedClientDetails[i].customer_id;
                                    }
                                    callback(groupDetailsResultSet,clientDetailsResultSet,mifosClientStatus,rejectedClientIds);
                                }
                                else{
                                    callback(groupDetailsResultSet,clientDetailsResultSet,mifosClientStatus,rejectedClientIds);
                                }
                            });
                        }
                    });
                }
            });
        });
    },

    rejectIdleClientsDataModel : function(clientId, callback){
        var constantsObj = this.constants;
        var updateClientQuery = "UPDATE " + dbTableName.iklantProspectClient + " SET status_id=" + constantsObj.getRejectedWhileIdleGroupsStatusId() + ", " +
            "updated_date=NOW() + INTERVAL 5 HOUR + INTERVAL 30 MINUTE, remarks_for_rejection = 'Rejected while idle stage' WHERE client_id='" + clientId + "'";
        connectionDataSource.getConnection(function (clientConnect) {
            clientConnect.query(updateClientQuery, function selectCb(err) {
                connectionDataSource.releaseConnectionPool(clientConnect);
                if(err){
                    callback('failure');
                }
                else{
                    callback('success');
                }
            })
        })
    },

    rejectIdleGroupDataModel : function(groupId, callback){
        var constantsObj = this.constants;
        var updateGroupQuery = "UPDATE " + dbTableName.iklantProspectGroup + " SET status_id=" + constantsObj.getRejectedWhileIdleGroupsStatusId() + ", " +
            "updated_date = NOW() + INTERVAL 5 HOUR + INTERVAL 30 MINUTE, is_idle = 0, last_credit_check_date = NULL, remarks = 'Rejected while idle stage' WHERE group_id=" + groupId;
        connectionDataSource.getConnection(function (clientConnect) {
            clientConnect.query(updateGroupQuery, function(err) {
                connectionDataSource.releaseConnectionPool(clientConnect);
                if(err){
                    callback('failure');
                }
                else{
                    callback('success');
                }
            })
        })
    },

 //Modified by Anitha Thilagar

    approveIdleGroupDataModel : function(groupId, statusId, stageFlag, callback){
        var constantsObj = this.constants;
        if(stageFlag == 'sendCBA'){
            var updateClientQuery = "UPDATE " + dbTableName.iklantProspectClient + " SET status_id=" + constantsObj.getDataVerificationOperationId() + ", " +
                "updated_date = NOW() + INTERVAL 5 HOUR + INTERVAL 30 MINUTE WHERE group_id=" + groupId + " AND status_id = "+statusId;
        } else if(stageFlag == 'sendCurrentStage'){
            var updateClientQuery = " UPDATE "+dbTableName.iklantProspectClient+" SET updated_date = NOW() + INTERVAL 5 HOUR + INTERVAL 30 MINUTE WHERE group_id="+ groupId+" AND status_id="+statusId;
        }
        customLog.info("updateIdleClientQuery: "+updateClientQuery);
        connectionDataSource.getConnection(function (clientConnect) {
            clientConnect.query(updateClientQuery, function(err) {
                if(err){
                    connectionDataSource.releaseConnectionPool(clientConnect);
                    callback('failure');
                }
                else{
                    if(stageFlag == 'sendCBA'){
                        var updateGroupQuery = "UPDATE " + dbTableName.iklantProspectGroup + " SET status_id=" + constantsObj.getDataVerificationOperationId() + ", " +
                            "updated_date = NOW() + INTERVAL 5 HOUR + INTERVAL 30 MINUTE, last_credit_check_date = NULL, is_idle = 0, remarks = " + statusId + " WHERE group_id=" + groupId;
                    } else if(stageFlag == 'sendCurrentStage'){
                        var updateGroupQuery = " UPDATE "+dbTableName.iklantProspectGroup+" SET updated_date = NOW() + INTERVAL 5 HOUR + INTERVAL 30 MINUTE, is_idle = 0 WHERE group_id="+groupId;
                    }
                    customLog.info("updateIdleGroupQuery: "+updateGroupQuery);
                    clientConnect.query(updateGroupQuery, function(err) {
                        connectionDataSource.releaseConnectionPool(clientConnect);
                        if (err) {
                            callback('failure');
                        } else {
                            callback('success');
                        }
                    });
                }
            })
        })
    },

    sendRMDataModel: function(groupId, statusId, callback){
        var constantsObj = this.constants;
        var updateGroupQuery = "UPDATE " + dbTableName.iklantProspectGroup + " SET updated_date = NOW() + INTERVAL 5 HOUR + INTERVAL 30 MINUTE, is_idle = -1 WHERE group_id=" + groupId;
        connectionDataSource.getConnection(function (clientConnect) {
            clientConnect.query(updateGroupQuery, function(err) {
                connectionDataSource.releaseConnectionPool(clientConnect);
                if (err) {
                    callback('failure');
                } else {
                    callback('success');
                }
            });
        });
    },

    updateClientStatusDataModel: function (clientIdListArray, clientIds, overdues, callback) {
        var self = this;
        var constantsObj = this.constants;
        var newGroupStatus = constantsObj.getNewGroup();
        var preliminaryVerifiedStatus = constantsObj.getPreliminaryVerified();
        var rejectedPriliminaryVerificationStatus = constantsObj.getRejectedPriliminaryVerification();
        var assignedFOStatus = constantsObj.getAssignedFO();
        var fieldVerifiedStatus = constantsObj.getFieldVerified();
        var rejectedFieldVerifiedStatus = constantsObj.getRejectedFieldVerification();
        var appraisedStatus = constantsObj.getAppraisedStatus();
        var rejectedAppraisal = constantsObj.getRejectedAppraisal();
        connectionDataSource.getConnection(function (clientConnect) {
            for (var j = 0; j < clientIdListArray.length; j++) {
                for (var i = 0; i < clientIds.length; i++) {
                    if (clientIdListArray[j] == clientIds[i]) {
                        var updateClientStatusForRejectedClient = "UPDATE "+dbTableName.iklantProspectClient+" pc  " +
                            "SET pc.status_id = IF (pc.status_id IN " +
                            "(" + newGroupStatus + "," + preliminaryVerifiedStatus + ", " +
                            "" + rejectedPriliminaryVerificationStatus + ")," + rejectedPriliminaryVerificationStatus + ", " +
                            "IF (pc.status_id IN (" + assignedFOStatus + "," + fieldVerifiedStatus + ", " +
                            "" + rejectedFieldVerifiedStatus + ")," + rejectedFieldVerifiedStatus + ", " +
                            "IF (pc.status_id IN (" + fieldVerifiedStatus + "," + appraisedStatus + ", " +
                            "" + rejectedAppraisal + ")," + rejectedAppraisal + ", pc.status_id))), " +
                            "pc.is_overdue = " + constantsObj.getActiveIndicatorTrue() + ", " +
                            "pc.remarks_for_rejection = '" + overdues[i] + "', pc.updated_date = NOW() + INTERVAL 5 HOUR + INTERVAL 30 MINUTE " +
                            "WHERE  pc.client_id = " + clientIdListArray[j] + "";
                        customLog.info("updateClientStatusForRejectedClient" + updateClientStatusForRejectedClient);
                        clientConnect.query(updateClientStatusForRejectedClient, function postCreate(err) {
                            if (err) {
                                customLog.error(err);
                                connectionDataSource.releaseConnectionPool(clientConnect);
                            }
                        });
                        break;
                    }
                    else {
                        customLog.info("clientIdListArray In else" + clientIdListArray[j]);
                        var updateClientStatusForReintiatedClient = "UPDATE "+dbTableName.iklantProspectClient+" pc " +
                            "SET pc.status_id = IF (pc.status_id IN (" + newGroupStatus + ", " +
                            "" + preliminaryVerifiedStatus + "," + rejectedPriliminaryVerificationStatus + "), " +
                            "" + preliminaryVerifiedStatus + ", " +
                            "IF (pc.status_id IN (" + assignedFOStatus + "," + fieldVerifiedStatus + ", " +
                            "" + rejectedFieldVerifiedStatus + ")," + fieldVerifiedStatus + ", " +
                            "IF (pc.status_id IN (" + fieldVerifiedStatus + "," + appraisedStatus + "," + rejectedAppraisal + "), " +
                            "" + appraisedStatus + ", pc.status_id))), " +
                            "pc.is_overdue=" + constantsObj.getActiveIndicatorFalse() + ", " +
                            "pc.updated_date = NOW() + INTERVAL 5 HOUR + INTERVAL 30 MINUTE WHERE  pc.client_id = " + clientIdListArray[j] + "";

                        customLog.info("updateClientStatusForReintiatedClient" + updateClientStatusForReintiatedClient);
                        clientConnect.query(updateClientStatusForReintiatedClient, function postCreate(err) {
                            if (err) {
                                customLog.error(err);
                                connectionDataSource.releaseConnectionPool(clientConnect);
                            }
                        });
                    }
                }
            }
            connectionDataSource.releaseConnectionPool(clientConnect);
            callback();
        });
    },

    reinitiateGroupDatamodel: function (groupId, remarks, callback) {
        var self=this;
        var constantsObj = this.constants;
        var reinitiatedStatus = "";
        var previousStatus = 0;
        var previousStatusQuery = "SELECT remarks AS previousStatus FROM "+dbTableName.iklantProspectGroup+" WHERE group_id = "+ groupId;
        connectionDataSource.getConnection(function (clientConnect) {
            clientConnect.query(previousStatusQuery, function selectCb(err, results, fields) {
                if(!err && results.length>0){
                    var status = (results[0].previousStatus)?results[0].previousStatus:0;
                    if(status !=0 && (status == constantsObj.getCreditBureauAnalysedStatus() || status == constantsObj.getAssignedFO())){
                        previousStatus = constantsObj.getCreditBureauAnalysedStatus();
                    }
                    else if(status != 0 && (status == constantsObj.getFieldVerified() || status == constantsObj.getGroupRecognitionTested() || status == constantsObj.getAppraisedStatus() || status == constantsObj.getAuthorizedStatus())){
                        previousStatus = constantsObj.getFieldVerified();
                    }
                    else{
                        previousStatus = 0;
                    }

                    var statusAfterRejectedInCBA = dbTableName.isCGTApplicable == true ? constantsObj.getCGTStatus() : constantsObj.getCreditBureauAnalysedStatus();

                    var reinitiateQuery = "UPDATE "+dbTableName.iklantProspectGroup+" pg, "+dbTableName.iklantProspectClient+" pc " +
                        "SET pg.status_id = IF (pg.status_id=" + constantsObj.getRejectedPriliminaryVerification() + ", " +
                        "" + constantsObj.getPreliminaryVerified() + ", " +
                        //"IF (pg.status_id=" + constantsObj.getRejectedCreditBureauAnalysisStatusId() + " AND "+previousStatus+" <> 0, " +
                        //"IF (pg.status_id=" + constantsObj.getRejectedCreditBureauAnalysisStatusId() + " , " +
                        //"" + previousStatus + ", " +
                        //"IF (pg.status_id=" + constantsObj.getRejectedCreditBureauAnalysisStatusId() + " AND "+previousStatus+" = 0, " +
                        "IF (pg.status_id=" + constantsObj.getRejectedCreditBureauAnalysisStatusId() + " , " +
                        "" + statusAfterRejectedInCBA + ", " +
                        "IF (pg.status_id=" + constantsObj.getRejectedFieldVerification() + ", " +
                        "" + constantsObj.getFieldVerified() + ",IF (pg.status_id=" + constantsObj.getRejectedAppraisal() + ", " +
                        "" + constantsObj.getAppraisedStatus() + "," + constantsObj.getNewGroup() + ")))), " +
                        "pg.remarks = '" + remarks + "', pg.updated_date = NOW() + INTERVAL 5 HOUR + INTERVAL 30 MINUTE " +
                        "WHERE pg.group_id = " + groupId + "";// AND pc.group_id = "+groupId+"";
                    customLog.info("reinitiateQuery : "+reinitiateQuery);
                    clientConnect.query(reinitiateQuery, function selectCb(err, results, fields) {
                        if (err) {
                            connectionDataSource.releaseConnectionPool(clientConnect);
                            customLog.error(err);
                            callback(reinitiatedStatus);
                        }
                        else{
                            var getStatusQuery = "SELECT ps.status_name FROM "+dbTableName.iklantProspectGroup+" pg " +
                                "INNER JOIN "+dbTableName.iklantProspectStatus+" ps ON ps.status_id = pg.status_id " +
                                "WHERE pg.group_id = " + groupId + "";
                            clientConnect.query(getStatusQuery, function selectCb(err, results, fields) {
                                connectionDataSource.releaseConnectionPool(clientConnect);
                                if (err) {
                                    customLog.error(err);
                                    callback(reinitiatedStatus);
                                } else {
                                    reinitiatedStatus = results[0].status_name;
                                    callback(reinitiatedStatus);
                                }
                            });
                        }
                    });
                }
                else{
                    connectionDataSource.releaseConnectionPool(clientConnect);
                    callback(reinitiatedStatus);
                }
            });
        });
    },

    addQuestionsDataModel: function (tenantId, callback) {
        var questionId = new Array();
        var questionsNonDefault = new Array();
        var self=this;
        var questionsNonDefaultQuery = "SELECT * FROM "+dbTableName.iklantQuestions+" WHERE is_default=1 AND tenant_id=" + tenantId + " ";
        connectionDataSource.getConnection(function (clientConnect) {
            clientConnect.query(questionsNonDefaultQuery, function selectCb(err, results, fields) {
                connectionDataSource.releaseConnectionPool(clientConnect);
                if (err) {
                    customLog.error(err);
                } else {
                    for (var i in results) {
                        questionId[i] = results[i].question_Id;
                        questionsNonDefault[i] = results[i].question_Name;
                        customLog.info("inside for" + questionId, questionsNonDefault);
                    }
                    callback(questionId, questionsNonDefault);
                }
            });
        });
    },

    questionsSelectDataModel: function (tenantId, selectedQuestionId, callback) {
        var questionsRequire = require(commonDTO+"/questions");
        var questionsObj = new questionsRequire();
        var choice_id = new Array();
        var choice_name = new Array();
        var marks = new Array();
        var self=this;
        var answersFetchQuery = "SELECT q.question_id,q.question_Name,q.display_name,q.weightage, " +
            "c.choice_id,c.choice_name,c.choice_marks " +
            "FROM "+dbTableName.iklantQuestions+" q " +
            "INNER JOIN "+dbTableName.iklantChoices+" c on c.question_id = q.question_id " +
            "WHERE q.question_Id = '" + selectedQuestionId + "' AND q.is_default=1 " +
            "AND q.tenant_id=" + tenantId + " ";
        connectionDataSource.getConnection(function (clientConnect) {
            clientConnect.query(answersFetchQuery, function selectCb(err, results, fields) {
                connectionDataSource.releaseConnectionPool(clientConnect);
                if (err) {
                    customLog.error(err);
                } else {
                    questionsObj.clearAll();
                    var questionsName = questionsObj.setQuestion(results[0].question_Name);
                    var displayName = questionsObj.setDisplaytext(results[0].display_name);
                    var weightage = questionsObj.setWeightage(results[0].weightage);
                    for (var i in results) {
                        choice_id[i] = results[i].choice_id;
                        choice_name[i] = results[i].choice_name;
                        marks[i] = results[i].choice_marks;
                    }
                    questionsObj.setChoiceId(choice_id);
                    questionsObj.setChoiceName(choice_name);
                    questionsObj.setMarks(marks);
                    callback(questionsObj);
                }
            });
        });
    },

    saveQuestionDataModel: function (tenantId, submitId, callback) {
        var choicesRequire = require(commonDTO+"/choices");
        var questionsRequire = require(commonDTO+"/questions");
        var questionsObj = new questionsRequire();
        var choiceObj = new choicesRequire();
        var self=this;
        customLog.info("submitId DM== " + submitId);
        connectionDataSource.getConnection(function (clientConnect) {
            if (submitId == 0) {
                var question_Name = questionsObj.getQuestion();
                var display_name = questionsObj.getDisplaytext();
                var weightage = questionsObj.getWeightage();
                var currentQuestionId;
                var insertQuestionQuery = "INSERT INTO "+dbTableName.iklantQuestions+" (tenant_id,question_Name,display_name,weightage, " +
                    "questionnaire_version,is_default,created_date) " +
                    "VALUES(" + tenantId + ",'" + question_Name + "','" + display_name + "', " +
                    "" + weightage + "," + 1.0 + "," + 1 + ", NOW() + INTERVAL 5 HOUR + INTERVAL 30 MINUTE)";
                customLog.info("insertQuestionQuery : " + insertQuestionQuery);
                clientConnect.query(insertQuestionQuery, function postCreate(err) {
                    if (err) {
                        customLog.error(err);
                        connectionDataSource.releaseConnectionPool(clientConnect);
                    }
                });

                var currentQuestionIdQuery = "SELECT MAX(question_Id) AS current_question FROM "+dbTableName.iklantQuestions+
                    " where tenant_id=" + tenantId + " ";
                clientConnect.query(currentQuestionIdQuery, function selectCb(err, results, fields) {
                    if (err) {
                        connectionDataSource.releaseConnectionPool(clientConnect);
                        customLog.error(err);
                    }
                    else {
                        for (var i in results) {
                            currentQuestionId = results[i].current_question;
                        }
                        var choiceArray = new Array();
                        var marksArray = new Array();
                        choiceArray = choiceObj.getChoice();
                        customLog.info(choiceArray);
                        marksArray = choiceObj.getMarks();
                        for (var i = 0; i < choiceArray.length; i++) {
                            var choice_id = i + 1;
                            var insertChoicesQuery = "INSERT INTO "+dbTableName.iklantChoices+" (question_id,choice_id,choice_name,choice_marks) " +
                                "VALUES(" + currentQuestionId + "," + choice_id + ",'" + choiceArray[i] + "'," + marksArray[i] + " );"
                            clientConnect.query(insertChoicesQuery, function postCreate(err) {
                                if (err) {
                                    customLog.error(err);
                                    connectionDataSource.releaseConnectionPool(clientConnect);
                                }

                            });
                        }
                        connectionDataSource.releaseConnectionPool(clientConnect);
                        callback();
                    }
                });
            }
            else if (submitId == 1) {
                //query to update questions
                var updateEditedQuestionsQuery = "UPDATE "+dbTableName.iklantQuestions+" SET question_Name='" + questionsObj.getQuestionEdit() + "', " +
                    "display_name='" + questionsObj.getDisplayEdit() + "', " +
                    "weightage='" + questionsObj.getWeightageEdit() + "' ,updated_date= NOW() + INTERVAL 5 HOUR + INTERVAL 30 MINUTE " +
                    "WHERE tenant_id=" + tenantId + " AND question_Id=" + questionsObj.getQuestionIDEdit();

                clientConnect.query(updateEditedQuestionsQuery,
                    function postCreate(err) {
                        if (err) {
                            customLog.error(err);
                            connectionDataSource.releaseConnectionPool(clientConnect);
                        }
                    });
                //query to update answers & marks
                var choiceIdArray = questionsObj.getChoice_ID().split(",");
                var answersArray = questionsObj.getAnswersEdit().split(",");
                var marksArray = questionsObj.getMarksEdit().split(",");

                customLog.info("A= " + answersArray);
                customLog.info("M= " + marksArray);

                for (var i = 0; i < answersArray.length; i++) {
                    var updateEditedAnswersQuery = "UPDATE "+dbTableName.iklantChoices+" SET choice_name ='" + answersArray[i] + "', " +
                        "choice_marks='" + marksArray[i] + "' " +
                        "WHERE question_Id='" + questionsObj.getQuestionIDEdit() + "' " +
                        "AND choice_id = " + choiceIdArray[i];
                    clientConnect.query(updateEditedAnswersQuery,function postCreate(err) {
                        if (err) {
                            connectionDataSource.releaseConnectionPool(clientConnect);
                            customLog.error(err);
                        }
                    });
                }
                connectionDataSource.releaseConnectionPool(clientConnect);
                callback();
            }
        });
    },

    calculateSecondaryAppraisalDataModel: function (tenantId, clientId, secondaryQuestionId, selectedAnswerArray, callback) {
        var self=this;
        var questionId = new Array();
        var choice_marks = new Array();
        var weightage = new Array();
        var secondaryRating;
        var totalRatingWeightage = new Array();
        var groupId;
        var clientAppraisalRating;
        var forIteration;
        var secQueForInsert = new Array();
        if (isNaN(secondaryQuestionId - 1)) {
            forIteration = secondaryQuestionId.length;
            for (var i = 0; i < secondaryQuestionId.length; i++) {
                secQueForInsert[i] = secondaryQuestionId[i];
            }
        }
        else {
            forIteration = 1;
            secQueForInsert[0] = secondaryQuestionId;
        }
        connectionDataSource.getConnection(function (clientConnect) {
            for (var i = 0; i < forIteration; i++) {
                var secondaryAppraisalQuery = "INSERT INTO "+dbTableName.iklantClientAssessment+" (client_id,question_id,answer_id,created_date) " +
                    "VALUES(" + clientId + "," + secQueForInsert[i] + "," + selectedAnswerArray[i] + ",NOW() + INTERVAL 5 HOUR + INTERVAL 30 MINUTE)";
                customLog.info("secondaryAppraisalQuery : " + secondaryAppraisalQuery);
                clientConnect.query(secondaryAppraisalQuery, function selectCb(err, results, fields) {
                    if (err) {
                        connectionDataSource.releaseConnectionPool(clientConnect);
                        customLog.error(err);
                    }
                });
            }
            var calculateSecondaryAppraisalQuery = "SELECT pc.group_id,c.question_id,c.choice_name, " +
                "c.choice_marks,q.weightage,cr.appraisal_rating FROM "+dbTableName.iklantChoices+" c " +
                "INNER JOIN "+dbTableName.iklantProspectClient+" pc ON pc.client_id = " + clientId + " " +
                "INNER JOIN "+dbTableName.iklantQuestions+" q ON q.is_default = 1 " +
                "INNER JOIN "+dbTableName.iklantClientRating+" cr ON cr.client_id = " + clientId + " " +
                "INNER JOIN "+dbTableName.iklantClientAssessment+" ca ON ca.question_id = q.question_Id " +
                "WHERE ca.client_id = " + clientId + " AND c.choice_id = ca.answer_id " +
                "AND c.question_id = ca.question_id AND q.tenant_id=" + tenantId + "";

            customLog.info("calculateSecondaryAppraisalQuery : " + calculateSecondaryAppraisalQuery);
            clientConnect.query(calculateSecondaryAppraisalQuery, function selectCb(err, results, fields) {
                if (err) {
                    connectionDataSource.releaseConnectionPool(clientConnect);
                    customLog.error(err);
                }
                else {
                    for (var i in results) {
                        clientAppraisalRating = results[i].appraisal_rating;
                        groupId = results[i].group_id;
                        questionId[i] = results[i].question_id;
                        choice_marks[i] = results[i].choice_marks;
                        weightage[i] = results[i].weightage;
                    }
                    secondaryRating = calculateCCAForClient(choice_marks, weightage);
                    totalRatingWeightage = calculateTotalCCAForClient(choice_marks, weightage, clientAppraisalRating)
                    customLog.info("totalRatingWeightage " + totalRatingWeightage);
                }
                var updateSecondaryAppraisalQuery = "UPDATE "+dbTableName.iklantClientRating+" SET appraisal_rating = " + totalRatingWeightage[0] + ", secondary_rating = " + secondaryRating + ",total_weightage_obtained=" + totalRatingWeightage[1] + ",total_weightage_required=" + totalRatingWeightage[2] + " WHERE client_id= " + clientId + "";
                clientConnect.query(updateSecondaryAppraisalQuery, function selectCb(err, results, fields) {
                    connectionDataSource.releaseConnectionPool(clientConnect);
                    if (err) {
                        customLog.error(err);
                    }
                    else {
                        callback(groupId, secondaryRating, totalRatingWeightage[2]);
                    }
                });
            });
        });
    },

    skipKycUploadDatamodel: function (groupId, callback) {
        var self=this;
        var constantsObj = this.constants;
        var groupInsertQuery = "UPDATE "+dbTableName.iklantProspectGroup+" pg, "+dbTableName.iklantProspectClient+" pc " +
            "SET pg.status_id=" + constantsObj.getKYCUploaded() + ", " +
            "pc.status_id=if(pc.is_overdue=0," + constantsObj.getKYCUploaded() + ",pc.status_id), " +
            "pg.updated_date=NOW() + INTERVAL 5 HOUR + INTERVAL 30 MINUTE,pc.updated_date=NOW() + INTERVAL 5 HOUR + INTERVAL 30 MINUTE WHERE " +
            "pg.group_id = " + groupId + " AND pc.group_id = " + groupId + " ";
        customLog.info("groupInsertQuery : " + groupInsertQuery);
        connectionDataSource.getConnection(function (clientConnect) {
            clientConnect.query(groupInsertQuery, function postCreate(err) {
                connectionDataSource.releaseConnectionPool(clientConnect);
                if (err) {
                    customLog.error(err);
                }
                callback();
            });
        });
    },

    // Ended by SathishKumar M
    getActiveOrRejectedClientsDataModel: function (group_id, callBack) {
        var self = this;
        var clientDetails = new Array();
        var reinitiatedDetails = new Array();
        var rejectedDetails = new Array();
        var reintiatedClients = new Array();
        var lastCreditCheckDate = new Array();
        var clientsInUploadNOC = new Array();
        var constantsObj = this.constants;
        var status = true;
        var activeOrRejectedDetailsQuery = "SELECT 	pc.client_id,pc.client_name,IF(pg.status_id = pc.status_id,0,1) AS is_rejected, " +
            "IF(irc.is_rm_reinitiated = 1 && irc.is_bm_reinitiated = 1,1,0) AS is_reinitiated, " +
            "IFNULL(irc.is_rm_reinitiated,0) AS is_rm_reinitiated,IFNULL(irc.is_bm_reinitiated,0) AS is_bm_reinitiated," +
            "IF(pc.status_id = "+constantsObj.getRejectedCreditBureauAnalysisStatusId()+",pc.updated_date,ps.status_name) AS last_credit_date " +
            "FROM iklant_prospect_client pc " +
            "INNER JOIN iklant_prospect_group pg ON pg.group_id = pc.group_id " +
            "INNER JOIN iklant_prospect_status ps ON ps.status_id = pc.status_id " +
            "LEFT JOIN iklant_rejected_client_status irc ON irc.client_id = pc.client_id " +
            "WHERE  pg.group_id = " + group_id + " AND (irc.`rejected_client_id` = "+
            "(SELECT MAX(rejected_client_id) FROM `iklant_rejected_client_status` WHERE client_id = pc.`client_id` GROUP BY client_id) OR 1=1) GROUP BY pc.client_id ORDER BY pc.status_id";
        customLog.info("activeOrRejectedDetailsQuery: "+activeOrRejectedDetailsQuery);
        connectionDataSource.getConnection(function (clientConnect) {
            clientConnect.query(activeOrRejectedDetailsQuery, function (error, activeOrRejectedClients) {
                connectionDataSource.releaseConnectionPool(clientConnect);
                if (error) {
                    callBack(false);
                }
                else {
                    var j= 0,uploadNOC = 0;
                    for(var i = 0; i < activeOrRejectedClients.length; i++){
                        if(activeOrRejectedClients[i].is_bm_reinitiated == 1 && (activeOrRejectedClients[i].is_rm_reinitiated == 0 && dbTableName.isRMApprovalRequiredForReinitiate == true)){
                            status = false;
                            reintiatedClients[j] = activeOrRejectedClients[i].client_name;
                            j++;
                        }
                        if(activeOrRejectedClients[i].is_bm_reinitiated == 1 && (activeOrRejectedClients[i].is_rm_reinitiated == -1 && dbTableName.isRMApprovalRequiredForReinitiate == true)){
                            status = false;
                            clientsInUploadNOC[uploadNOC] = activeOrRejectedClients[i].client_name;
                            uploadNOC++;
                        }
                        clientDetails[i] = activeOrRejectedClients[i].client_name;
                        rejectedDetails[i] = activeOrRejectedClients[i].is_rejected;
                        reinitiatedDetails[i] = (activeOrRejectedClients[i].is_rejected == 0 && activeOrRejectedClients[i].is_bm_reinitiated == 1 && dbTableName.isRMApprovalRequiredForReinitiate == false) ? '' : activeOrRejectedClients[i].is_reinitiated;
                        lastCreditCheckDate[i] = activeOrRejectedClients[i].last_credit_date.toString();
                    }
                    callBack(status,clientDetails,rejectedDetails,reinitiatedDetails,reintiatedClients,lastCreditCheckDate,clientsInUploadNOC);
                }
            });
        });
    },

    downloadRequstedImageDataModel: function (tenantId, clientId, docId, callback) {
        var self = this;
        var fileLocation = new Array();
        var storageLocationIndicatorArray = new Array();
        customLog.info("clientId" + clientId);
        customLog.info("docId" + docId);
        var downloadRequstedImageQuery = "select cd.Captured_image,cd.android_docname,cd.s3_key from "+dbTableName.iklantClientDoc+" cd INNER JOIN "+
            dbTableName.iklantProspectClient+" ipc ON ipc.`client_id`=cd.`client_id` AND ipc.`loan_count`=cd.`loan_count` where cd.client_id = " + clientId + " and cd.doc_type_id = " + docId + " ";
        customLog.info("downloadRequstedImageQuery " + downloadRequstedImageQuery);
        connectionDataSource.getConnection(function (clientConnect) {
            clientConnect.query(downloadRequstedImageQuery,
                function selectCb(err, results, fields) {
                    connectionDataSource.releaseConnectionPool(clientConnect);
                    if (err) {
                        customLog.error(err);
                        callback(fileLocation);
                    }
                    else {
                        var fileLocationAlt = new Array();
                        for (var i in results) {
                            if( results[i].s3_key == 1){
                                fileLocationAlt[i] = results[i].android_docname;
                            } else {
                                fileLocationAlt[i] = results[i].Captured_image;
                            }

                        }
                        customLog.info("fileLocationDatamodel All Images" + fileLocationAlt);
                        for(var j=0;j<fileLocationAlt.length;j++){
                            if( results[j].s3_key == 1){
                                fileLocation.push(fileLocationAlt[j]);
                                storageLocationIndicatorArray[j] = 's3';
                            } else {
                                fileLocation.push(rootPath+"/"+fileLocationAlt[j]);
                                storageLocationIndicatorArray[j] = 'ebs';
                            }
                        }
                        customLog.info("fileLocationDatamodel Only Available Images" + fileLocation);
                        callback(fileLocation,storageLocationIndicatorArray);
                    }
                }
            );
        });
    },

    approveOrRejectClientForNextLoanDataModel: function (iklantGroupId,userId,callback){
        var self = this;
        var constantsObj = this.constants;
        var clientsIdArray = new Array();
        var retriveClientDetails = "SELECT ipc.client_id,ipc.status_id FROM iklant_prospect_group ipg "+
            " LEFT JOIN iklant_prospect_client ipc ON ipg.group_id = ipc.group_id "+
            " WHERE ipg.group_id = "+ iklantGroupId +" AND ipc.status_id NOT IN (21,2);"
        customLog.info("retriveClientDetails :"+retriveClientDetails);
        connectionDataSource.getConnection(function (clientConnect) {
            clientConnect.query(retriveClientDetails, function selectCb(err, results) {
                if (err) {
                    connectionDataSource.releaseConnectionPool(clientConnect);
                    customLog.error("error occured in approveOrRejectClientForNextLoanDataModel(): ", err);
                    callback("failure");
                }
                else {
                    for (var i in results) {
                        clientsIdArray.push(results[i].client_id);
                    }
                    if (clientsIdArray.length > 0) {
                        var updateQuery = "UPDATE iklant_prospect_client SET status_id = "+ constantsObj.getRejectedPreviousLoanStatusId() +" WHERE client_id IN (" + clientsIdArray + ");"
                        console.log(updateQuery);
                        clientConnect.query(updateQuery, function selectCb(err, result) {
                            connectionDataSource.releaseConnectionPool(clientConnect);
                            if (err) {
                                customLog.error("error occured in approveOrRejectClientForNextLoanDataModel(): ", err);
                                callback("failure");
                            }
                            else{
                                callback("success");
                            }
                        });
                    }
                    else{
                        callback("success");
                    }
                }
            });
        });
    },

    updatePreviousCycleRejectedClientStatusDataModel: function (iklantGroupId,userId,callback){
        var constantsObj = this.constants;
        var updatePreviousClientStatus = "UPDATE iklant_prospect_client SET status_id = "+constantsObj.getRejectedPreviousLoanStatusId()+", updated_by = " + userId +", updated_date = current_timestamp WHERE group_id = "+ iklantGroupId +" AND status_id NOT IN (2,10,21)";
        customLog.info("updatePreviousClientStatus: "+updatePreviousClientStatus);
        connectionDataSource.getConnection(function (clientConnect) {
            clientConnect.query(updatePreviousClientStatus, function selectCb(err) {
                connectionDataSource.releaseConnectionPool(clientConnect);
                if (err) {
                    customLog.error("error occured in updatePreviousCycleRejectedClientStatusDataModel: ", err);
                    callback("failure");
                }
                else {
                    callback("success");
                }
            });
        });
    },
    
    retrieveGroupsForCustomerProfileDataModel: function(officeId,offset,limit,searchKey,callback){
        var groupIdArray = new Array();
        var groupNameArray = new Array();
        var centerNameArray = new Array();
        var activeClients = new Array();
        var totalClients = new Array();
        var mifosCustomerId = new Array();
        var constantsObj = this.constants;
        var foNames = new Array();
        connectionDataSource.getConnection(function(clientConnect){
            var retrieveGroupQuery = "SELECT * FROM (SELECT pg.group_name,pg.center_name,(SELECT `mifos_customer_id` FROM iklant_mifos_mapping WHERE group_id = pg.group_id GROUP BY group_id) AS mifos_customer_id,pg.group_id,(SELECT COUNT(client_id) FROM "+dbTableName.iklantProspectClient+" WHERE "+
                "group_id = pg.group_id) AS total_clients,(SELECT COUNT(client_id) FROM "+dbTableName.iklantProspectClient+" WHERE group_id = pg.group_id AND status_id NOT IN("+constantsObj.getRejectedStatusIds()+")) AS active_clients,"+
                "IF(pg.assigned_to IS NULL OR pg.assigned_to = '',(SELECT display_name FROM personnel WHERE personnel_id = pg.created_by),(SELECT display_name FROM personnel WHERE personnel_id = pg.assigned_to)) AS fo "+
                "FROM "+dbTableName.iklantProspectGroup+" pg WHERE office_id = "+officeId+
                " AND pg.status_id IN("+constantsObj.getCreditBureauAnalysedStatus()+","+constantsObj.getCGTStatus()+","+constantsObj.getAssignedFO()+","+constantsObj.getFieldVerified()+","+constantsObj.getAppraisedStatus()+
                ","+constantsObj.getAuthorizedStatus()+","+constantsObj.getSynchronizedGroupsStatus()+","+constantsObj.getGroupRecognitionTested()+","+constantsObj.getBankDetailsUpdateStatus()+","+constantsObj.getFundTransferStatus()+","+
                constantsObj.getLoanAmountUpdateStatusId()+","+constantsObj.getLoanAmountSanctionedStatusId()+") ORDER BY pg.group_id) temp ";
            if(searchKey != ""){
                retrieveGroupQuery += " WHERE (group_name LIKE '%"+searchKey+"%' OR center_name LIKE '%"+searchKey+"%' OR total_clients LIKE '%"+searchKey+"%' OR active_clients LIKE '%"+searchKey+"%' OR (total_clients - active_clients) LIKE '%"+searchKey+"%' OR fo LIKE '%"+searchKey+"%')";
            }
            customLog.info("retrieveGroupQuery: "+retrieveGroupQuery);
            clientConnect.query(retrieveGroupQuery,function(err, results, fields){
                if(err){
                    customLog.error("retrieveGroupQuery error:",err);
                    callback(groupIdArray,groupNameArray,centerNameArray,mifosCustomerId,totalClients,activeClients,foNames,0);
                }else{
                    var totalRecords = results.length;
                    retrieveGroupQuery += " LIMIT "+offset+", "+limit;
                    clientConnect.query(retrieveGroupQuery,function(err, results, fields){
                        connectionDataSource.releaseConnectionPool(clientConnect);
                        if(err){
                            customLog.info("retrieveGroupQuery error: ",err);
                            callback(groupIdArray,groupNameArray,centerNameArray,mifosCustomerId,totalClients,activeClients,foNames,totalRecords);
                        }else{
                            for( var i in results){
                                groupIdArray[i] = results[i].group_id;
                                groupNameArray[i] = results[i].group_name;
                                centerNameArray[i] = results[i].center_name;
                                totalClients[i] = results[i].total_clients;
                                activeClients[i] = results[i].active_clients;
                                foNames[i] = results[i].fo;
                                mifosCustomerId[i] = results[i].mifos_customer_id;
                            }
                            callback(groupIdArray,groupNameArray,centerNameArray,mifosCustomerId,totalClients,activeClients,foNames,totalRecords);
                        }
                    });
                }
            });
        });
    },
    retrieveGroupProfileDetailsDataModel: function(groupId,mifosCustomerId,callback){
        var groupDetails = new Array();
        var clientDetails = new Array();
        var cgtDetails = new Array();
        var loanSummary = new Array();
        var constantsObj = this.constants;
        var self = this;
        connectionDataSource.getConnection(function(clientConnect){
            var groupBasicInfoQuery = "SELECT pg.`group_name`,pg.`center_name` ,p.`display_name` as created_by,p1.`display_name` as assigned,"+
                "(select response from iklant_area_questions_response where area_code_id = ac.`area_code_id` and question_id = 1) as area_name,"+
                "IFNULL((SELECT operation_name FROM `iklant_operation` WHERE operation_id = os.operation_id),ps.`status_name`) AS status_name,"+
                "DATE_FORMAT(pg.`group_created_date`,'%d-%m-%Y') AS group_created_date,(SELECT meeting_place FROM `iklant_meeting` WHERE meeting_id = pm.`meeting_id`) AS meeting_place,"+
                "IF((select meeting_time from `iklant_meeting` where meeting_id = pm.`meeting_id`) > '11:59',CONCAT((select meeting_time from `iklant_meeting` where meeting_id = pm.`meeting_id`),' PM'),"+
                "CONCAT((select meeting_time from `iklant_meeting` where meeting_id = pm.`meeting_id`),' AM')) as meeting_time, "+
                "IFNULL((SELECT ROUND(SUM(cr.appraisal_rating)/COUNT(pc.client_id),2) FROM iklant_client_rating cr "+
                "INNER JOIN iklant_prospect_client pc ON pc.client_id = cr.client_id WHERE pc.group_id = "+groupId+" GROUP BY pc.`group_id`),'-') AS rating, "+
                "IFNULL((SELECT total_rate FROM `iklant_grt_group_remarks` WHERE group_id = "+groupId+" ORDER BY `id` DESC LIMIT 1),'-') AS grt_rating "+
                "FROM iklant_prospect_group pg  inner join iklant_prospect_status ps on ps.`status_id` = pg.`status_id` "+
                "LEFT JOIN iklant_operation_status os ON os.status_id = ps.`status_id` "+
                "left join personnel p on p.`personnel_id` = pg.`created_by` LEFT JOIN personnel p1 ON p1.`personnel_id` = pg.`assigned_to` "+
                "left join iklant_area_code ac on ac.`area_code_id` = pg.`area_code_id` left join `iklant_prospect_group_meeting`  pm on pm.`group_id` = pg.`group_id` "+
                "WHERE pg.group_id = "+groupId;
            customLog.info("groupBasicInfoQuery: "+groupBasicInfoQuery);
            clientConnect.query(groupBasicInfoQuery,function(err, groupInfo, fields){
                if(err){
                    self.sendFailureCallback(clientConnect,"groupBasicInfoQuery",err,callback);
                }else{
                    groupDetails = groupInfo;
                    var clientQuery = "SELECT pc.group_id,pc.client_id,pc.`client_name`,pc.`client_last_name`,pc.latitude,pc.longitude,"+
                        "CONCAT(pcp.`line1`,IF(pcp.`line2` IS NULL,'',CONCAT(',',pcp.`line2`)),IF(pcp.`line3` IS NULL,'',CONCAT(',',pcp.`line3`)),IF(pcp.`city` IS NULL,'',CONCAT(',',pcp.`city`)),IF(pcp.`state` IS NULL,'',CONCAT(',',pcp.`state`)),IF(pcp.`pincode` IS NULL,'',CONCAT(',',pcp.`pincode`))) AS address,"+
                        "(SELECT mifos_client_customer_id FROM iklant_mifos_mapping WHERE client_id = pc.client_id) AS mifos_customer_id, "+
                        "IF(pc.status_id NOT IN ("+constantsObj.getCreditBureauAnalysedStatus()+","+constantsObj.getAssignedFO()+","+constantsObj.getCGTStatus()+"),IF(pc.is_leader=1,'Leader',IF(pc.is_leader=-1,'Sub Leader','Member')),'-') AS role "+
                        "FROM iklant_prospect_client pc "+
                        "INNER JOIN iklant_prospect_group pg ON pg.`group_id` = pc.`group_id` "+
                        "INNER JOIN iklant_prospect_client_personal pcp ON pcp.`client_id` = pc.`client_id` "+
                        "WHERE pc.`group_id` = "+groupId+" AND pc.`status_id` NOT IN ("+constantsObj.getRejectedStatusIds()+") ";
                    customLog.info("clientQuery: "+clientQuery);
                    clientConnect.query(clientQuery,function(err, clientInfo, fields){
                        if(err){
                            self.sendFailureCallback(clientConnect,"clientQuery",err,callback);
                        }else{
                            clientDetails = clientInfo;
                            var cgtQuery = "SELECT gd.doc_name,gd.`etag`,DATE_FORMAT(cgt.`meeting_date`,'%d-%m-%Y') AS meeting_date,cgt.`no_of_presence`,cgt.`no_of_absence`,cgt.`latitude`,cgt.`longitude` FROM iklant_prospect_cgt cgt "+
                                "INNER JOIN iklant_group_doc gd ON gd.`group_doc_id` = cgt.`doc_id`  WHERE cgt.group_id = "+groupId;
                            customLog.info("cgtQuery: "+cgtQuery);
                            clientConnect.query(cgtQuery,function(err, cgtInfo, fields){
                                if(err){
                                    self.sendFailureCallback(clientConnect,"cgtQuery",err,callback);
                                }else{
                                    cgtDetails = cgtInfo;
                                    if(mifosCustomerId != "NULL"){
                                        var lsQuery = "SELECT a.global_account_num AS account_no,a.account_id,DATE_FORMAT(la.disbursement_date,'%d-%m-%Y') AS disbursement_date,TRUNCATE(la.`interest_rate`,2) AS interest_rate,la.`no_of_installments`,"+
                                            "ROUND(ls.`orig_principal`,2) AS principal,ROUND(ls.`orig_interest`,2) AS interest,"+
                                            "ROUND(ls.`principal_paid`,2) AS principal_paid,ROUND(ls.`interest_paid`,2) AS interest_paid,"+
                                            "ROUND(ls.`orig_principal`-ls.`principal_paid`,2) AS principal_outstanding,"+
                                            "ROUND(ls.`orig_interest`-ls.`interest_paid`,2) AS interest_outstanding, "+
                                            "(SELECT display_name FROM personnel WHERE personnel_id = a.personnel_id) AS loan_officer "+
                                            "FROM account a INNER JOIN loan_account la ON la.`account_id` = a.`account_id` "+
                                            "INNER JOIN loan_summary ls ON ls.`account_id` = a.`account_id` "+
                                            "WHERE a.customer_id = "+mifosCustomerId+" AND a.account_state_id IN (5,6,9,19)";
                                        clientConnect.query(lsQuery,function(err, lsResult, fields){
                                            if(err){
                                                self.sendFailureCallback(clientConnect,"lsQuery",err,callback);
                                            }else{
                                                loanSummary = lsResult;
                                                var notesQuery = "SELECT DATE_FORMAT(c.comment_date,'%d-%m-%Y') AS commented_date,(SELECT display_name FROM personnel WHERE personnel_id = c.field_officer_id) AS commented_by, c.`comment` FROM customer_note c WHERE c.customer_id = "+mifosCustomerId;
                                                clientConnect.query(notesQuery,function(err, notes, fields){
                                                    if(err){
                                                        self.sendFailureCallback(clientConnect,"notesQuery",err,callback);
                                                    }else{
                                                        connectionDataSource.releaseConnectionPool(clientConnect);
                                                        callback(true,groupDetails,clientDetails,cgtDetails,loanSummary,notes);
                                                    }
                                                });
                                            }
                                        });
                                    }else{
                                        connectionDataSource.releaseConnectionPool(clientConnect);
                                        callback(true,groupDetails,clientDetails,cgtDetails,loanSummary);
                                    }
                                }
                            });
                        }
                    });
                }
            });
        });
    },
    retrieveClientProfileDetailsDataModel: function(groupId,clientId,mifosCustomerId,callback){
        var profileDetails = new Array(),
            personalDetails = new Array(),
            guarantorDetails = new Array(),
            demographicDetails = new Array(),
            bankDetails = new Array(),
            docDetails = new Array(),
            loanSummary = new Array(),
            familyInfo = new Array(),
            self = this;
        var constantsObj = this.constants;
        connectionDataSource.getConnection(function(clientConnect){
            var clientBasicInfoQuery = "SELECT pc.latitude,pc.longitude,pc.`other_mfi_loan_count`,pc.`other_mfi_balance_amount`,pc.`other_mfi_written_off_amount`,pc.client_name,pc.`client_last_name`,DATE_FORMAT(cp.`date_of_birth`,'%m-%d-%Y') AS dob,cp.`mobile_number`,cp.`landline_number`,"+
                //"cp.`address`,cp.`pincode`," +
                "IFNULL(CONCAT(cp.`line1`,IF(cp.`line2` IS NULL,'',CONCAT(',',cp.`line2`)),IF(cp.`line3` IS NULL,'',CONCAT(',',cp.`line3`)),IF(cp.`city` IS NULL,'',CONCAT(',',cp.`city`)),IF(cp.`state` IS NULL,'',CONCAT(',',cp.`state`))),'No Address') AS address,cp.pincode,"+
                "`ration_card_number`,cp.`aadhaar_number`,cp.`voter_id`,cp.job_card_number,"+
                "IF(pc.status_id NOT IN ("+constantsObj.getCreditBureauAnalysedStatus()+","+constantsObj.getAssignedFO()+","+constantsObj.getCGTStatus()+"),IF(pc.is_leader=1,'Leader',IF(pc.is_leader=-1,'Sub Leader','Member')),'-') AS role, "+
                "cp.`gas_number`,(SELECT ROUND(appraisal_rating,2) FROM iklant_client_rating WHERE client_id = pc.`client_id`) AS rating FROM iklant_prospect_client pc "+
                "INNER JOIN iklant_prospect_client_personal cp ON cp.`client_id` = pc.client_id WHERE pc.client_id = "+clientId+" AND pc.`group_id` = "+groupId;
            customLog.info("clientBasicInfoQuery: "+clientBasicInfoQuery);
            clientConnect.query(clientBasicInfoQuery,function(err, clientInfo, fields){
                if(err){
                    self.sendFailureCallback(clientConnect,"clientBasicInfoQuery",err,callback);
                }else{
                    personalDetails = clientInfo;
                    var familyQuery = "SELECT fd.member_name,(SELECT lookup_value FROM iklant_lookup_value WHERE lookup_id = fd.`member_gender`) AS gender,"+
                        "IF(fd.member_relationship = 85,fd.`other_family_relationship_name`,(SELECT lookup_value FROM iklant_lookup_value WHERE lookup_id = fd.`member_relationship`)) AS relationship,"+
                        "DATE_FORMAT(fd.`member_dob`,'%d-%m-%Y') AS member_dob,(SELECT lookup_value FROM iklant_lookup_value WHERE lookup_id = fd.`member_education`) AS education,"+
                        "(SELECT lookup_value FROM iklant_lookup_value WHERE lookup_id = fd.`member_occupation`) AS occupation,"+
                        "fd.`member_income` FROM `iklant_prospect_client_family_detail` fd WHERE fd.client_id = "+clientId;
                    customLog.info("familyQuery: "+familyQuery);
                    clientConnect.query(familyQuery,function(err, familyResult, fields){
                        if(err){
                            self.sendFailureCallback(clientConnect,"familyQuery",err,callback);
                        }else{
                            familyInfo = familyResult;
                            var guarantorQuery = "SELECT pg.`guarantor_name`,pg.`guarantor_lastname`,DATE_FORMAT(pg.`guarantor_dob`,'%d-%m-%Y') AS dob,"+ //pg.`guarantor_address`,
                                "IFNULL(CONCAT(pg.`line1`,IF(pg.`line2` IS NULL,'',CONCAT(',',pg.`line2`)),IF(pg.`line3` IS NULL,'',CONCAT(',',pg.`line3`)),IF(pg.`city` IS NULL,'',CONCAT(',',pg.`city`)),IF(pg.`state` IS NULL,'',CONCAT(',',pg.`state`))),'No Address') AS guarantor_address,"+
                                "pg.`guarantor_ration_card_number`,pg.`guarantor_aadhaar_card_number`,pg.`guarantor_voter_id`,pg.`landline_number`,pg.`mobile_number`,pg.guarantor_job_card_number AS job_card_number,"+
                                "IF(pg.`guarantor_relationship` = 85,pg.other_guarantor_relationship_name,(SELECT lookup_value FROM iklant_lookup_value WHERE lookup_id = pg.`guarantor_relationship`)) AS relationship,"+
                                "(SELECT lookup_value FROM iklant_lookup_value WHERE lookup_id = pg.`marital_status`) AS marital_status,"+
                                "(SELECT lookup_value FROM iklant_lookup_value WHERE lookup_id = pg.`religion`) AS religion,"+
                                "(SELECT lookup_value FROM iklant_lookup_value WHERE lookup_id = pg.`caste`) AS caste,"+
                                "(SELECT lookup_value FROM iklant_lookup_value WHERE lookup_id = pg.`education_status`) AS education_status,"+
                                "(SELECT lookup_value FROM iklant_lookup_value WHERE lookup_id = pg.`gender`) AS gender,"+
                                "(SELECT lookup_value FROM iklant_lookup_value WHERE lookup_id = pg.`nationality`) AS nationality "+
                                "FROM iklant_prospect_client_guarantor pg WHERE pg.client_id = "+clientId;
                            customLog.info("guarantorQuery: "+guarantorQuery);
                            clientConnect.query(guarantorQuery,function(err, guarantorInfo, fields){
                                if(err){
                                    self.sendFailureCallback(clientConnect,"guarantorQuery",err,callback);
                                }else{
                                    guarantorDetails = guarantorInfo;
                                    var demographicQuery = "SELECT  (SELECT lookup_value FROM iklant_lookup_value WHERE lookup_id = pc.`marital_status`) AS marital_status,"+
                                        "(SELECT lookup_value FROM iklant_lookup_value WHERE lookup_id = pc.`religion`) AS religion,"+
                                        "(SELECT lookup_value FROM iklant_lookup_value WHERE lookup_id = pc.`caste`) AS caste,"+
                                        "(SELECT lookup_value FROM iklant_lookup_value WHERE lookup_id = pc.`educational_details`) AS education_status,"+
                                        "(SELECT lookup_value FROM iklant_lookup_value WHERE lookup_id = pc.`gender`) AS gender,"+
                                        "(SELECT lookup_value FROM iklant_lookup_value WHERE lookup_id = pc.`nationality`) AS nationality,"+
                                        "(SELECT lookup_value FROM iklant_lookup_value WHERE lookup_id = hd.`house_type`) AS house_type,"+
                                        "(SELECT lookup_value FROM iklant_lookup_value WHERE lookup_id = hd.`house_ceiling_type`) AS house_ceiling_type,"+
                                        "(SELECT lookup_value FROM iklant_lookup_value WHERE lookup_id = hd.`house_wall_type`) AS house_wall_type,"+
                                        "(SELECT lookup_value FROM iklant_lookup_value WHERE lookup_id = hd.`house_flooring_detail`) AS house_flooring_detail,"+
                                        "(SELECT lookup_value FROM iklant_lookup_value WHERE lookup_id = hd.`house_toilet`) AS house_toilet,"+
                                        "(SELECT lookup_value FROM iklant_lookup_value WHERE lookup_id = pc.`loan_purpose`) AS loan_purpose,"+
                                        "(SELECT lookup_value FROM iklant_lookup_value WHERE lookup_id = pc.`business_category`) AS business_category,"+
                                        "hd.house_sqft,hd.house_room_detail,hd.vehicle_details FROM `iklant_prospect_client_personal` pc  "+
                                        "LEFT JOIN `iklant_prospect_client_house_detail` hd ON pc.`client_id` = hd.`client_id` WHERE pc.`client_id` = "+clientId;
                                    customLog.info("demographicQuery: "+demographicQuery);
                                    clientConnect.query(demographicQuery,function(err, demographicInfo, fields){
                                        if(err){
                                            self.sendFailureCallback(clientConnect,"demographicQuery",err,callback);
                                        }else{
                                            demographicDetails = demographicInfo;
                                            var bankQuery = "SELECT (SELECT SUM(member_income) FROM `iklant_prospect_client_family_detail` WHERE client_id = pc.`client_id` GROUP BY pc.`client_id`) AS family_monthly_income,"+
                                                "pc.`family_monthly_expense`,pcd.`account_number`,bd.`branch_name`,bd.`IFSC_code` FROM `iklant_prospect_client` pc  "+
                                                "LEFT JOIN `iklant_prospect_client_bank_detail` pcd ON pcd.`client_id` = pc.`client_id` "+
                                                "LEFT JOIN bank_details bd ON bd.`bank_detail_id` = pcd.`bank_detail_id` WHERE pc.client_id = "+clientId;
                                            customLog.info("bankQuery: "+bankQuery);
                                            clientConnect.query(bankQuery,function(err, bankInfo, fields){
                                                if(err){
                                                    self.sendFailureCallback(clientConnect,"bankQuery",err,callback);
                                                }else{
                                                    bankDetails = bankInfo;
                                                    var docQuery = "SELECT cd.`android_docname`,dt.`doc_name` FROM iklant_client_doc cd "+
                                                        "INNER JOIN iklant_doc_type dt ON dt.`doc_id` = cd.`doc_type_id` "+
                                                        "WHERE cd.client_id = "+clientId+" ORDER BY dt.`doc_name`";
                                                    customLog.info("docQuery: "+docQuery);
                                                    clientConnect.query(docQuery,function(err, docInfo, fields){
                                                        if(err){
                                                            self.sendFailureCallback(clientConnect,"docQuery",err,callback);
                                                        }else{
                                                            docDetails = docInfo;
                                                            var profileInfoQuery = "SELECT cd.`android_docname` AS profilePicture,(SELECT doc_name FROM `iklant_group_doc` WHERE group_id = "+groupId+
                                                                " ORDER BY group_doc_id DESC LIMIT 1) AS coverPhoto FROM iklant_client_doc cd WHERE cd.`doc_type_id` =  5 AND cd.client_id = "+clientId+" ORDER BY client_doc_id DESC LIMIT 1";
                                                            customLog.info("profileInfoQuery: "+profileInfoQuery);
                                                            clientConnect.query(profileInfoQuery,function(err, profileInfo, fields){
                                                                if(err){
                                                                    self.sendFailureCallback(clientConnect,"profileInfoQuery",err,callback);
                                                                }else{
                                                                    profileDetails = profileInfo;
                                                                    if(mifosCustomerId != "NULL"){
                                                                        var lsQuery = "SELECT a.global_account_num AS account_no,a.account_id,DATE_FORMAT(la.disbursement_date,'%d-%m-%Y') AS disbursement_date,TRUNCATE(la.`interest_rate`,2) AS interest_rate,la.`no_of_installments`,"+
                                                                            "ROUND(ls.`orig_principal`,2) AS principal,ROUND(ls.`orig_interest`,2) AS interest,"+
                                                                            "ROUND(ls.`principal_paid`,2) AS principal_paid,ROUND(ls.`interest_paid`,2) AS interest_paid,"+
                                                                            "ROUND(ls.`orig_principal`-ls.`principal_paid`,2) AS principal_outstanding,"+
                                                                            "ROUND(ls.`orig_interest`-ls.`interest_paid`,2) AS interest_outstanding "+
                                                                            "FROM account a INNER JOIN loan_account la ON la.`account_id` = a.`account_id` "+
                                                                            "INNER JOIN loan_summary ls ON ls.`account_id` = a.`account_id` "+
                                                                            "WHERE a.customer_id = "+mifosCustomerId+" AND a.account_state_id IN (5,6,9,19)";
                                                                            customLog.info("lsQuery: "+lsQuery);
                                                                        clientConnect.query(lsQuery,function(err, lsResult, fields){
                                                                            if(err){
                                                                                self.sendFailureCallback(clientConnect,"lsQuery",err,callback);
                                                                            }else{
                                                                                loanSummary = lsResult;
                                                                                var notesQuery = "SELECT DATE_FORMAT(c.comment_date,'%d-%m-%Y') AS commented_date,(SELECT display_name FROM personnel WHERE personnel_id = c.field_officer_id) AS commented_by, c.`comment` FROM customer_note c WHERE c.customer_id = "+mifosCustomerId;
                                                                                clientConnect.query(notesQuery,function(err, notes, fields){
                                                                                    if(err){
                                                                                        self.sendFailureCallback(clientConnect,"notesQuery",err,callback);
                                                                                    }else{
                                                                                        var paymentVerifiedQuery = "SELECT COUNT(1) AS paymentVerified,(SELECT COUNT(1) FROM loan_custom_detail WHERE account_id IN (SELECT account_id FROM account WHERE customer_id = "+mifosCustomerId+") AND is_insurance_claimed IN (-1,1,2)) AS claimStatus FROM payment_collection pc "+
                                                                                            "INNER JOIN account a ON a.`account_id` = pc.`account_id` "+
                                                                                            "INNER JOIN customer c ON c.`parent_customer_id` = a.`customer_id` "+
                                                                                            "WHERE c.`customer_id` = "+mifosCustomerId+" AND pc.`is_verified` = 0 AND (pc.`is_match` = 0 OR pc.`is_match` IS NULL)";
                                                                                        customLog.info("paymentVerifiedQuery: "+paymentVerifiedQuery);
                                                                                        clientConnect.query(paymentVerifiedQuery,function(err, paymentVerifiedResult){
                                                                                            if(err){
                                                                                                self.sendFailureCallback(clientConnect,"paymentVerifiedQuery",err,callback);
                                                                                            }else{
                                                                                                connectionDataSource.releaseConnectionPool(clientConnect);
                                                                                                var paymentVerified = (paymentVerifiedResult.length > 0) ? (paymentVerifiedResult[0].paymentVerified > 0) ? false : true : true;
                                                                                                var claimStatus = (paymentVerifiedResult.length > 0) ? (paymentVerifiedResult[0].claimStatus > 0) ? true : false : false;
                                                                                                callback(true,profileDetails, personalDetails,guarantorDetails,demographicDetails,bankDetails,docDetails,loanSummary,familyInfo,notes,paymentVerified,claimStatus);
                                                                                            }
                                                                                        });
                                                                                    }
                                                                                });
                                                                            }
                                                                        });
                                                                    }else{
                                                                        connectionDataSource.releaseConnectionPool(clientConnect);
                                                                        callback(true,profileDetails, personalDetails,guarantorDetails,demographicDetails,bankDetails,docDetails,loanSummary,familyInfo,[],true,false);
                                                                    }
                                                                }
                                                            });
                                                        }
                                                    });
                                                }
                                            });
                                        }
                                    });
                                }
                            });
                        }
                    });
                    
                }
            });
        });
    },
    sendFailureCallback:function(clientConnect,typeOfQuery,error,callback){
        connectionDataSource.releaseConnectionPool(clientConnect);
        customLog.error(typeOfQuery+" error:",error);
        callback(false);
    },
    retrieveRepaymentScheduleDataModel: function(accountId, callback){
        var installmentScheduleQuery = "SELECT DATE_FORMAT(ls.action_date,'%d-%m-%Y') AS due_date,IFNULL(DATE_FORMAT(ls.payment_date,'%d-%m-%Y'),'-') AS payment_date,ls.installment_id,"+
            "ROUND(ls.principal,2) principal_demd,ROUND(ls.interest,2) interest_demd,"+
            "CASE WHEN ls.principal_paid =  0.0000 THEN '-' ELSE ROUND(ls.principal_paid,2) END AS principal_paid,CASE WHEN ls.interest_paid = 0.0000 THEN '-' ELSE ROUND(ls.interest_paid,2) END AS interest_paid, "+
            "IF(ls.payment_status = 1,'Paid','Not Paid') AS paymentStatus "+
            "FROM loan_schedule ls INNER JOIN loan_account la ON ls.account_id = la.account_id "+
            "INNER JOIN account a ON la.account_id = a.account_id "+
            "WHERE a.account_state_id IN (5,6,9,19) AND a.account_id = "+accountId+" ORDER BY ls.installment_id"
        customLog.info("installmentScheduleQuery: "+installmentScheduleQuery); 
        
        connectionDataSource.getConnection(function(clientConnect){
            clientConnect.query(installmentScheduleQuery,function(err, installmentShedule, fields){
                connectionDataSource.releaseConnectionPool(clientConnect);
                if(err){
                    customLog.error(" installmentScheduleQuery error:",err);
                    callback(false);
                }else{
                    callback(true,installmentShedule);
                }
            });
        });
    },
    
    saveCommentsDataModel: function(customerId, comments, userId,callback){
        var commentsQuery = "INSERT INTO customer_note(customer_id,field_officer_id,comment_date,comment) VALUES("+customerId+","+userId+",CURDATE(),'"+comments+"')";
        customLog.info("commentsQuery: "+commentsQuery); 
        
        connectionDataSource.getConnection(function(clientConnect){
            clientConnect.query(commentsQuery,function(err){
                if(err){
                    customLog.error(" commentsQuery error:",err);
                    callback(false);
                }else{
                    var notesQuery = "SELECT DATE_FORMAT(c.comment_date,'%d-%m-%Y') AS commented_date,(SELECT display_name FROM personnel WHERE personnel_id = c.field_officer_id) AS commented_by, c.`comment` FROM customer_note c WHERE c.customer_id = "+customerId+" ORDER BY comment_id ASC";
                    clientConnect.query(notesQuery,function(err, notes, fields){
                        connectionDataSource.releaseConnectionPool(clientConnect);
                        if(!err){
                            callback(true,notes);
                        }
                    });
                }
            });
        });
    },
    
    saveClaimStatusDataModel: function(accountId, claimStatus, callback){
        var updateStatusQuery = "UPDATE loan_custom_detail SET is_insurance_claimed = "+claimStatus+", claim_updated_date = NOW() + INTERVAL 5 HOUR + INTERVAL 30 MINUTE"+
            " WHERE is_insurance_claimed IN (-1,1,2) AND account_id = "+accountId;
        customLog.info("updateStatusQuery: "+updateStatusQuery); 
        
        connectionDataSource.getConnection(function(clientConnect){
            clientConnect.query(updateStatusQuery,function(err){
                if(err){
                    connectionDataSource.releaseConnectionPool(clientConnect);
                    customLog.error(" updateStatusQuery error:",err);
                    callback(false);
                }else{
                    var retrieveStatusQuery = "SELECT DATE_FORMAT(claim_updated_date,'%d-%m-%Y %h:%i:%s') AS updatedDate FROM loan_custom_detail WHERE account_id = "+accountId;
                    clientConnect.query(retrieveStatusQuery,function(err,result){
                        connectionDataSource.releaseConnectionPool(clientConnect);
                        if(err){
                            customLog.error(" retrieveStatusQuery error:",err);
                            callback(false);
                        }else{
                            callback(true,result[0].updatedDate);
                        }
                    });
                }
            });
        });
    },
    
    retrieveAllClaimStatusDataModel: function(customerId, callback){
        var claimStatusQuery = "SELECT lcd.account_id,IF(lcd.claim_updated_date IS NULL,'-',DATE_FORMAT(lcd.claim_updated_date,'%d-%m-%Y %h:%i:%s')) AS claimedDate,lcd.is_insurance_claimed,a.global_account_num,(SELECT lookup_value FROM iklant_lookup_value WHERE lookup_id = lcd.remarks) claimReason FROM loan_custom_detail lcd INNER JOIN account a on a.account_id = lcd.account_id WHERE lcd.is_insurance_claimed IN (-1,1,2) AND a.account_id IN (SELECT account_id FROM account WHERE customer_id = "+customerId+")";
        customLog.info("claimStatusQuery: "+claimStatusQuery); 
        
        connectionDataSource.getConnection(function(clientConnect){
            clientConnect.query(claimStatusQuery,function(err,result){
                connectionDataSource.releaseConnectionPool(clientConnect);
                if(err){
                    customLog.error(" claimStatusQuery error:",err);
                    callback(false,result);
                }else{
                    callback(true,result);
                }
            });
        });
    }
};
