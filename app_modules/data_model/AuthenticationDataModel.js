module.exports = authenticationDataModel;

var path = require('path');
var rootPath = path.dirname(process.mainModule.filename);
var dbTableName = require(path.join(rootPath,"properties.json"));

var connectionDataSource = require(path.join(rootPath,"app_modules/data_model/DataSource"));


var customlog = require(path.join(rootPath,"logger/loggerConfig.js"))('authenticationDataModel.js');

function authenticationDataModel(constants) {
    this.constants = constants;
    customlog.debug("Inside Authentication Data Access Layer");




}

authenticationDataModel.prototype = {
    authLoginAccess: function (userName, password, callback) {
        var constantsObj = this.constants;
        var userDetailsQuery = "SELECT us.*,p.display_name AS login_name,r.role_id,r.role_name,r.role_description,io.office_level_id,io.display_name as office_name,isl.doc_language,"+
            "(SELECT `default` FROM tenant_settings WHERE settings_params = 'MFI_ID' AND bc_id = bco.`bc_id`) AS ownMFIId " +
            "FROM "+dbTableName.iklantUsers+" us " +
            "INNER JOIN personnel p ON p.personnel_id = us.user_id " +
            "INNER JOIN "+dbTableName.mfiPersonnelRole+" ur ON ur.personnel_id = us.user_id " +
            "INNER JOIN "+dbTableName.iklantRole+" r ON r.role_id = ur.role_id " +
            "LEFT JOIN office io ON io.`office_id` = us.`office_id` " +
            "LEFT JOIN iklant_office o ON o.`office_id` = us.`office_id` " +
            "LEFT JOIN business_correspondent_office bco ON bco.`bc_id` = us.`bc_office_id` " +
            "LEFT JOIN iklant_state_list isl ON isl.`state_id` = o.`state_id` " +
            "WHERE us.user_name = '" + userName + "' and us.password = '" + password + "' " +
            "and us.active_indicator = " + this.constants.getActiveIndicatorTrue() + "";
        customlog.info("Login Query : " + userDetailsQuery);
        connectionDataSource.getConnection(function (clientConnect, err) {
            if (!err){
                customlog.info('Inside Connection');
                clientConnect.query(userDetailsQuery, function selectCb(err, results, fields) {
                    customlog.info('Inside Query execution');
                    if (err) {
                        customlog.error(err);
                        connectionDataSource.releaseConnectionPool(clientConnect);
                        callback(results);
                    }
                    else if (results !=null && results.length !=0){
                        var menuId = new Array(),menuName = new Array(),menuUrl = new Array(), imgLocation = new Array();
                        var roleIdArray = new Array();
                        for(var i=0;i<results.length;i++){
                            roleIdArray.push(results[i].role_id);
                        }
                        var menuDetailsQuery = "SELECT mr.menu_id,ml.menu_name,ml.img_location,ml.menu_url " +
                            "FROM " + dbTableName.iklantMenuRoleMapping + " mr INNER JOIN " + dbTableName.iklantMenuLevels + " ml ON (ml.menu_id = mr.menu_id) " +
                            "WHERE mr.role_id IN (" + roleIdArray + ") AND ml.parent_menu_id IS NULL  AND ml.depth_level=0 AND currently_in_use = 1 AND ml.menu_id IN ("+constantsObj.getNPAMenus()+") GROUP BY mr.menu_id ORDER BY ml.order_no ";
                          
                        customlog.info(menuDetailsQuery);
                        clientConnect.query(menuDetailsQuery, function selectCb(err, resultsMenu, fields) {
                            if (err) {
                                customlog.error(err);
                                connectionDataSource.releaseConnectionPool(clientConnect);
                                callback(results,resultsMenu);
                            }
                            else{
                                for(var i=0;i<resultsMenu.length;i++){
                                    menuId[i] = resultsMenu[i].menu_id;
                                    menuName[i] = resultsMenu[i].menu_name;
                                    menuUrl[i] = resultsMenu[i].menu_url;
                                    imgLocation[i] = resultsMenu[i].img_location;
                                }
                                var resultArray = new Array({menu_id:menuId, menu_name:menuName, menu_url:menuUrl, img_location:imgLocation});

                                var grantedPermissionsQuery =   " SELECT  r.`role_id`, "+
                                        " IFNULL(SUBSTRING(SUBSTRING_INDEX(laaa.`lookup_name`,'.',2),13),SUBSTRING(SUBSTRING_INDEX(laa.`lookup_name`,'.',2),13)) AS main_menu, "+
                                        " IFNULL(SUBSTRING(SUBSTRING_INDEX(laa.`lookup_name`,'.',2),13),SUBSTRING(SUBSTRING_INDEX(la.`lookup_name`,'.',2),13)) AS sub_menu, "+
                                        " GROUP_CONCAT(SUBSTRING(SUBSTRING_INDEX(la.`lookup_name`,'.',2),13)) AS activity, "+
                                        " IFNULL(paa.activity_id,pa.activity_id) AS activity_id "+
                                        " FROM role r "+
                                        " INNER JOIN personnel_role pr ON pr.`role_id` = r.`role_id` "+
                                        " INNER JOIN roles_activity  ra ON ra.`role_id` = r.`role_id` "+
                                        " INNER JOIN activity a ON a.`activity_id` = ra.`activity_id` "+
                                        " LEFT JOIN activity  pa ON pa.`activity_id` =  a.`parent_id` "+
                                        " LEFT JOIN activity  paa ON paa.`activity_id` =  pa.`parent_id` "+
                                        " INNER JOIN lookup_value la ON la.`lookup_id` = a.`description_lookup_id` "+
                                        " LEFT JOIN lookup_value laa ON laa.`lookup_id` = pa.`description_lookup_id`"+
                                        " LEFT JOIN lookup_value laaa ON laaa.`lookup_id` = paa.`description_lookup_id` "+
                                        " WHERE pr.`personnel_id` = "+results[0].user_id +" "+
                                        " GROUP BY main_menu,sub_menu "+
                                        " ORDER BY main_menu,sub_menu; ";


                                clientConnect.query(grantedPermissionsQuery, function selectCb(err, permissionList, fields) {
                                    connectionDataSource.releaseConnectionPool(clientConnect);
                                    if (err) {
                                        customlog.error(err);
                                        callback(results, resultsMenu);
                                    }
                                    else {
                                        var permissionAccessJsonObj = {};
                                        var menuAccessArray = new Array();
                                        var subMenuAccessArray = new Array();
                                        var grantedAccessArray = new Array();
                                        for (var k = 0; k < permissionList.length; k++) {
                                            menuAccessArray.push(permissionList[k].main_menu);
                                            subMenuAccessArray.push(permissionList[k].sub_menu);
                                            grantedAccessArray.push(permissionList[k].activity);
                                        }
                                        permissionAccessJsonObj.menuAccessList = menuAccessArray;
                                        permissionAccessJsonObj.subMenuAccessList = subMenuAccessArray;
                                        permissionAccessJsonObj.activityAccessList = grantedAccessArray;
                                        callback(results, resultArray, permissionAccessJsonObj);
                                    }
                                });
                            }
                        });
                }
                else{
                    connectionDataSource.releaseConnectionPool(clientConnect);
                    callback(results);
                }
                });
            }else {
                connectionDataSource.releaseConnectionPool(clientConnect);
                customlog.error("Login query " + err);
            }
        });
    }
};