module.exports = reportManagementDataModel;

var path = require('path');
var rootPath = path.dirname(process.mainModule.filename);
var dbTableName = require(path.join(rootPath, "properties.json"));
var fs = require('fs');
var connectionDataSource = require(path.join(rootPath, "app_modules/data_model/DataSource"));
var customlog = require(path.join(rootPath, "logger/loggerConfig.js"))('ReportManagementDataModel.js');
var commonDTO = path.join(rootPath, "app_modules/dto/common");
var groupManagementDTO = path.join(rootPath, "app_modules/dto/report_management");
var dateUtils = require(path.join(rootPath, "app_modules/utils/DateUtils"));
var ciDB = dbTableName.database;
var async = require('async');
var format = require('dateformat');

//Business Layer
function reportManagementDataModel(constants) {
    customlog.debug("Inside Report Management Data Access Layer");
    this.constants = constants;
}

reportManagementDataModel.prototype = {
    retrieveClientDetailsForVerificationPageDataModel: function (parent_customer_id, callback) {
        var self = this;
        var retrieveClientDetailsQuery = "select la.account_id,loan_amount,c.display_name AS client_name,ccd.phone_number " +
                " FROM loan_account la INNER JOIN account a ON a.account_id = la.account_id " +
                " INNER JOIN customer c ON c.customer_id = a.customer_id INNER JOIN office o ON o.office_id = a.office_id " +
                " INNER JOIN loan_custom_detail lcd ON lcd.account_id = la.account_id LEFT JOIN customer_address_detail ccd ON c.customer_id = ccd.customer_id " +
                " WHERE parent_account_id IS not NULL AND is_loan_disbursal_verified = 0 AND parent_account_id = " + parent_customer_id;
        connectionDataSource.getConnection(function (clientConnect) {
            clientConnect.query(retrieveClientDetailsQuery, function (err, results, fields) {
                connectionDataSource.releaseConnectionPool(clientConnect);
                if (err) {
                }
                callback(results);
            });
        });
    },

    assignGroupToRODataModel: function (customerId, accountId, roId, callback) {
        var self = this;
        customlog.info("accountId" + accountId);
        customlog.info("roId========================" + roId);
        var accountExistCheckQuery = 'SELECT * FROM ' + dbTableName.tenantPrefixForNPA + 'npa_util_loan_detail WHERE account_id=?';
        connectionDataSource.getConnection(function (clientConnect) {
            async.eachOfSeries(accountId, function (list, index, a_callback) {
                clientConnect.query(accountExistCheckQuery, [accountId[index]], function (err, rows) {
                    if (err) {
                        customlog.error(err);
                        callback();
                    } else if (rows.length === 0) {
                        var insertNPALoansDetail = "INSERT INTO " + dbTableName.tenantPrefixForNPA + "npa_util_loan_detail " +
                                "(account_id, recovery_officer_id, allocated_date, last_updated_date) " +
                                " VALUES(" + accountId[index] + "," + roId + ",NOW() + INTERVAL 5 HOUR + INTERVAL 30 MINUTE,NOW() + INTERVAL 5 HOUR + INTERVAL 30 MINUTE)";
                        customlog.info("insertNPALoansDetail :" + insertNPALoansDetail);
                        clientConnect.query(insertNPALoansDetail, function (err) {
                            if (err) {
                                customlog.error(err);
                                callback();
                            }
                            a_callback();
                        });
                    } else {
                        var updateQuery = 'UPDATE ' + dbTableName.tenantPrefixForNPA + 'npa_util_loan_detail SET `recovery_officer_id`=' + roId + ', ' +
                                '`allocated_date`=NOW() + INTERVAL 5 HOUR + INTERVAL 30 MINUTE, ' +
                                '`last_updated_date`=NOW() + INTERVAL 5 HOUR + INTERVAL 30 MINUTE ' +
                                'WHERE `account_id`=' + accountId[index] + '';
                        customlog.info("UpdateNPALoansDetail :" + updateQuery);
                        clientConnect.query(updateQuery, function (err) {
                            if (err) {
                                customlog.error(err);
                                callback();
                            }
                            a_callback();
                        });
                    }
                });
                if(index == (accountId.length)-1){
                     callback();
                }
            });
            connectionDataSource.releaseConnectionPool(clientConnect);
        });
    },

    assignGroupToROLogDataModel: function (userId, groupName, officer, userName, customerId, accountId, roId, callback) {
        var self = this;
        customlog.info("Officer: " + officer + ", UserName: " + userName);
        customlog.info("GroupName========================" + roId);
        connectionDataSource.getConnection(function (clientConnect) {
            for (var i in accountId) {
                var comment = "The group " + groupName[i] + " has been assigned to " + officer + " by " + userName;
                customlog.info("comment========================" + comment);
                var insertNPALoansDetail = "INSERT INTO " + dbTableName.tenantPrefixForNPA + "npa_loan_activity_logs " +
                        "(account_id, assigned_to, assigned_by, assigned_date, comments) " +
                        " VALUES(" + accountId[i] + "," + roId + "," + userId + ",NOW() + INTERVAL 5 HOUR + INTERVAL 30 MINUTE,'" + comment + "')";
                customlog.info("insertNPALoansDetail :" + insertNPALoansDetail);
                clientConnect.query(insertNPALoansDetail, function (err) {
                    if (err) {
                        customlog.error(err);
                        callback();
                    }
                });
            }
            connectionDataSource.releaseConnectionPool(clientConnect);
        });
        callback();
    },

    NPAReviewDetailsDataModel: function (status, remarks, accountId, callback) {
        connectionDataSource.getConnection(function (clientConnect) {
            var insertNPAReviewDetail = "UPDATE " + dbTableName.tenantPrefixForNPA + "npa_util_loan_detail SET " +
                    " status_id=" + status + ",remarks='" + remarks + "' where account_id=" + accountId;
            customlog.info("insertNPAReviewDetail: " + insertNPAReviewDetail);
            clientConnect.query(insertNPAReviewDetail, function (err) {
                connectionDataSource.releaseConnectionPool(clientConnect);
                if (err) {
                    customlog.error(err);
                }
            });
        });
        callback();
    },

    listNPASearchGroupDatamodel: function (tenantId, recoveryOfficer, capabilityPercentage, isLeaderTraceableID, reasonForNPA, overdueDurationFrom, overdueDurationTo, amountFrom, amountTo, branch, callback) {
        var self = this;
        var constantsObj = this.constants;

        var recoveryOfficerNameArray = new Array();
        var accountIdArray = new Array();
        var customerNameArray = new Array();
        var overDueAmountArray = new Array();
        var daysInArrearsArray = new Array();
        var expectedCompletionDateArray = new Array();
        var groupName = new Array();
        var npaSearchResultsQuery = "";
        if (recoveryOfficer != 0) {
            npaSearchResultsQuery = "SELECT nld.account_id,nl.customer, " +
                    "(nl.actual_principal_overdue+nl.actual_interest_overdue) AS overdue_amount, " +
                    "nl.days_in_arrears,us.user_name,nur.response_txt,IFNULL (pg.group_name,'Created in mifos') as group_name " +
                    "FROM " + dbTableName.tenantPrefixForNPA + "npa_util_loan_detail nld " +
                    "INNER JOIN " + dbTableName.tenantPrefixForNPA + "npa_util_loan nl ON nl.account_id = nld.account_id " +
                    "INNER JOIN " + dbTableName.iklantUsers + " us ON us.user_id = nld.recovery_officer_id " +
                    "INNER JOIN " + dbTableName.tenantPrefixForNPA + "npa_util_response nur ON nur.account_id = nld.account_id " +
                    " LEFT JOIN  `iklant_mifos_mapping`  imm ON  imm.`mifos_customer_id`= nl.`customer_id` LEFT JOIN iklant_prospect_group pg ON pg.`group_id`=imm.`group_id` " +
                    "WHERE nld.recovery_officer_id IN(" + recoveryOfficer + ") " +
                    "AND nl.npa_indicator <> 'N' AND nld.status_id IS NOT NULL GROUP BY nld.`account_id` ORDER BY overdue_amount DESC";
        } else if (capabilityPercentage != 0) {
            if (capabilityPercentage == 100) {
                npaSearchResultsQuery = "SELECT nld.account_id,nl.customer, " +
                        "(nl.actual_principal_overdue+nl.actual_interest_overdue) AS overdue_amount, " +
                        "nl.days_in_arrears,us.user_name,nur.response_txt,IFNULL (pg.group_name,'Created in mifos') as group_name " +
                        "FROM " + dbTableName.tenantPrefixForNPA + "npa_util_loan_detail nld " +
                        "INNER JOIN " + dbTableName.tenantPrefixForNPA + "npa_util_loan nl ON nl.account_id = nld.account_id " +
                        "INNER JOIN " + dbTableName.iklantUsers + " us ON us.user_id = nld.recovery_officer_id " +
                        "INNER JOIN " + dbTableName.tenantPrefixForNPA + "npa_util_response nr ON nr.account_id = nld.account_id " +
                        "INNER JOIN " + dbTableName.tenantPrefixForNPA + "npa_util_response nur ON nur.account_id = nld.account_id " +
                        " LEFT JOIN  `iklant_mifos_mapping`  imm ON  imm.`mifos_customer_id`= nl.`customer_id` LEFT JOIN iklant_prospect_group pg ON pg.`group_id`=imm.`group_id` " +
                        "WHERE nr.response_txt IN (" + capabilityPercentage + ") " +
                        "AND nld.status_id IS NOT NULL GROUP BY nld.`account_id` ORDER BY overdue_amount DESC";

            } else {
                npaSearchResultsQuery = "SELECT nld.account_id,nl.customer, " +
                        "(nl.actual_principal_overdue+nl.actual_interest_overdue) AS overdue_amount, " +
                        "nl.days_in_arrears,us.user_name,nur.response_txt,IFNULL (pg.group_name,'Created in mifos') as group_name " +
                        "FROM " + dbTableName.tenantPrefixForNPA + "npa_util_loan_detail nld " +
                        "INNER JOIN " + dbTableName.tenantPrefixForNPA + "npa_util_loan nl ON nl.account_id = nld.account_id " +
                        "INNER JOIN " + dbTableName.iklantUsers + " us ON us.user_id = nld.recovery_officer_id " +
                        "INNER JOIN " + dbTableName.tenantPrefixForNPA + "npa_util_response nr ON nr.account_id = nld.account_id " +
                        "INNER JOIN " + dbTableName.tenantPrefixForNPA + "npa_util_response nur ON nur.account_id = nld.account_id " +
                        " LEFT JOIN  `iklant_mifos_mapping`  imm ON  imm.`mifos_customer_id`= nl.`customer_id` LEFT JOIN iklant_prospect_group pg ON pg.`group_id`=imm.`group_id` " +
                        "WHERE nr.response_txt IN (" + capabilityPercentage + ") " +
                        "AND nl.npa_indicator <> 'N' " +
                        "AND nld.status_id IS NOT NULL GROUP BY nld.`account_id` ORDER BY overdue_amount DESC";
            }
        } else if (isLeaderTraceableID != 0) {
            if (isLeaderTraceableID == 2) {
                isLeaderTraceableID = 0;
            }
            npaSearchResultsQuery = "SELECT nld.account_id,nl.customer, " +
                    "(nl.actual_principal_overdue+nl.actual_interest_overdue) AS overdue_amount, " +
                    "nl.days_in_arrears,us.user_name,nur.response_txt,IFNULL (pg.group_name,'Created in mifos') as group_name " +
                    "FROM " + dbTableName.tenantPrefixForNPA + "npa_util_loan_detail nld " +
                    "INNER JOIN " + dbTableName.tenantPrefixForNPA + "npa_util_loan nl ON nl.account_id = nld.account_id " +
                    "INNER JOIN " + dbTableName.iklantUsers + " us ON us.user_id = nld.recovery_officer_id " +
                    "INNER JOIN " + dbTableName.tenantPrefixForNPA + "npa_util_response nr ON nr.account_id = nld.account_id " +
                    "INNER JOIN " + dbTableName.tenantPrefixForNPA + "npa_util_response nur ON nur.account_id = nld.account_id " +
                    " LEFT JOIN  `iklant_mifos_mapping`  imm ON  imm.`mifos_customer_id`= nl.`customer_id` LEFT JOIN iklant_prospect_group pg ON pg.`group_id`=imm.`group_id` " +
                    "WHERE nr.response_txt IN (" + isLeaderTraceableID + ") " +
                    "AND nl.npa_indicator <> 'N' AND " +
                    "nld.status_id IS NOT NULL GROUP BY nld.`account_id` ORDER BY overdue_amount DESC";
        } else if (reasonForNPA != 0) {
            npaSearchResultsQuery = "SELECT nld.account_id,nl.customer, " +
                    "(nl.actual_principal_overdue+nl.actual_interest_overdue) AS overdue_amount, " +
                    "nl.days_in_arrears,us.user_name,nur.response_txt,IFNULL (pg.group_name,'Created in mifos') as group_name " +
                    "FROM " + dbTableName.tenantPrefixForNPA + "npa_util_loan_detail nld " +
                    "INNER JOIN " + dbTableName.tenantPrefixForNPA + "npa_util_loan nl ON nl.account_id = nld.account_id " +
                    "INNER JOIN " + dbTableName.iklantUsers + " us ON us.user_id = nld.recovery_officer_id " +
                    "INNER JOIN " + dbTableName.tenantPrefixForNPA + "npa_util_recovery_reason nlrr ON nlrr.account_id = nld.account_id " +
                    "INNER JOIN " + dbTableName.tenantPrefixForNPA + "npa_util_response nur ON nur.account_id = nld.account_id " +
                    " LEFT JOIN  `iklant_mifos_mapping`  imm ON  imm.`mifos_customer_id`= nl.`customer_id` LEFT JOIN iklant_prospect_group pg ON pg.`group_id`=imm.`group_id` " +
                    "WHERE nlrr.recovery_reason_type_id IN (" + reasonForNPA + ") " +
                    "AND nl.npa_indicator <> 'N' AND " +
                    "nld.status_id IS NOT NULL GROUP BY account_id ORDER BY overdue_amount DESC";
        } else if (overdueDurationFrom != 0 && overdueDurationTo != 0) {
            npaSearchResultsQuery = "SELECT nld.account_id,nl.customer, " +
                    "(nl.actual_principal_overdue+nl.actual_interest_overdue) AS overdue_amount, " +
                    "nl.days_in_arrears,us.user_name,nur.response_txt,IFNULL (pg.group_name,'Created in mifos') as group_name " +
                    "FROM " + dbTableName.tenantPrefixForNPA + "npa_util_loan_detail nld " +
                    "LEFT JOIN " + dbTableName.tenantPrefixForNPA + "npa_util_loan nl ON nl.account_id = nld.account_id " +
                    "INNER JOIN " + dbTableName.iklantUsers + " us ON us.user_id = nld.recovery_officer_id " +
                    "INNER JOIN " + dbTableName.tenantPrefixForNPA + "npa_util_response nur ON nur.account_id = nld.account_id " +
                    " LEFT JOIN  `iklant_mifos_mapping`  imm ON  imm.`mifos_customer_id`= nl.`customer_id` LEFT JOIN iklant_prospect_group pg ON pg.`group_id`=imm.`group_id` " +
                    "WHERE nl.days_in_arrears >=" + overdueDurationFrom + " AND " +
                    "nl.days_in_arrears <=" + overdueDurationTo + " " +
                    "AND nl.npa_indicator <> 'N' AND " +
                    "nld.status_id IS NOT NULL GROUP BY nld.`account_id` ORDER BY overdue_amount DESC";
        } else if (amountFrom != 0 && amountTo != 0) {
            npaSearchResultsQuery = "SELECT nld.account_id,nl.customer, " +
                    "(nl.actual_principal_overdue+nl.actual_interest_overdue) AS overdue_amount, " +
                    "nl.days_in_arrears,us.user_name,nur.response_txt,IFNULL (pg.group_name,'Created in mifos') as group_name " +
                    "FROM " + dbTableName.tenantPrefixForNPA + "npa_util_loan_detail nld " +
                    "LEFT JOIN " + dbTableName.tenantPrefixForNPA + "npa_util_loan nl ON nl.account_id = nld.account_id " +
                    "INNER JOIN " + dbTableName.iklantUsers + " us ON us.user_id = nld.recovery_officer_id " +
                    "INNER JOIN " + dbTableName.tenantPrefixForNPA + "npa_util_response nur ON nur.account_id = nld.account_id " +
                    " LEFT JOIN  `iklant_mifos_mapping`  imm ON  imm.`mifos_customer_id`= nl.`customer_id` LEFT JOIN iklant_prospect_group pg ON pg.`group_id`=imm.`group_id` " +
                    "WHERE (nl.actual_principal_overdue+nl.actual_interest_overdue) >=" + amountFrom + " " +
                    "AND (nl.actual_principal_overdue+nl.actual_interest_overdue) <=" + amountTo + " " +
                    "AND nl.npa_indicator <> 'N' AND " +
                    "nld.status_id IS NOT NULL GROUP BY nld.`account_id` ORDER BY overdue_amount DESC";
        } else if (branch != 0) {
            npaSearchResultsQuery = "SELECT nld.account_id,nl.customer, " +
                    "(nl.actual_principal_overdue+nl.actual_interest_overdue) AS overdue_amount, " +
                    "nl.days_in_arrears,us.user_name,nur.response_txt,IFNULL (pg.group_name,'Created in mifos') as group_name " +
                    "FROM " + dbTableName.tenantPrefixForNPA + "npa_util_loan_detail nld " +
                    "INNER JOIN " + dbTableName.tenantPrefixForNPA + "npa_util_loan nl ON nl.account_id = nld.account_id " +
                    "INNER JOIN " + dbTableName.iklantUsers + " us ON us.user_id = nld.recovery_officer_id " +
                    "INNER JOIN " + dbTableName.tenantPrefixForNPA + "npa_util_response nur ON nur.account_id = nld.account_id " +
                    " LEFT JOIN  `iklant_mifos_mapping`  imm ON  imm.`mifos_customer_id`= nl.`customer_id` LEFT JOIN iklant_prospect_group pg ON pg.`group_id`=imm.`group_id` " +
                    "WHERE nl.office_id IN(" + branch + ") AND nl.npa_indicator <> 'N' " +
                    "GROUP BY nld.`account_id` ORDER BY overdue_amount DESC";
        }
        customlog.info("npaSearchResultsQuery : " + npaSearchResultsQuery);
        if (npaSearchResultsQuery != "") {
            connectionDataSource.getConnection(function (clientConnect) {
                clientConnect.query(npaSearchResultsQuery,
                        function (err, results, fields) {
                            connectionDataSource.releaseConnectionPool(clientConnect);
                            if (err) {
                                customlog.error(err);
                            } else {
                                for (var i in results) {
                                    accountIdArray[i] = results[i].account_id;
                                    customerNameArray[i] = results[i].customer;
                                    overDueAmountArray[i] = Math.round(results[i].overdue_amount);
                                    daysInArrearsArray[i] = results[i].days_in_arrears;
                                    recoveryOfficerNameArray[i] = results[i].user_name;
                                    if (results[i].response_txt.length > 3) {
                                        expectedCompletionDateArray[i] = formatDateForUI(results[i].response_txt);
                                    } else {
                                        expectedCompletionDateArray[i] = '';
                                    }
                                    groupName[i] = results[i].group_name;
                                }
                                callback(accountIdArray, customerNameArray, overDueAmountArray, daysInArrearsArray, recoveryOfficerNameArray, expectedCompletionDateArray, groupName);
                            }
                        });
            });
        } else {
            callback(accountIdArray, customerNameArray, overDueAmountArray, daysInArrearsArray, recoveryOfficerNameArray, expectedCompletionDateArray);
        }
    },

    getNPAReasonsDatamodel: function (tenantId, userId, callback) {
        var npaReasonIdArray = new Array();
        var npaReasonArray = new Array();
        var self = this;
        var npaReasonsQuery = "SELECT * FROM " + dbTableName.tenantPrefixForNPA + "npa_util_recovery_reason_type";
        customlog.info("npaReasonsQuery : " + npaReasonsQuery);
        connectionDataSource.getConnection(function (clientConnect) {
            clientConnect.query(npaReasonsQuery, function (err, results, fields) {
                connectionDataSource.releaseConnectionPool(clientConnect);
                if (err) {
                    customlog.error(err);
                } else {
                    for (var i in results) {
                        npaReasonIdArray[i] = results[i].recovery_reason_type_id;
                        npaReasonArray[i] = results[i].recovery_reason_type;
                    }
                    self.getRecoveryOfficersDatamodel(tenantId, userId, function (recoveryOfficerId, recoveryOfficerName) {
                        callback(npaReasonIdArray, npaReasonArray, recoveryOfficerId, recoveryOfficerName);
                    });
                }
            });
        });
    },

    getRecoveryOfficersDatamodel: function (tenantId, userId, callback) {
        var self = this;
        var constantsObj = this.constants;
        var recoveryOfficerId = new Array();
        var recoveryOfficerName = new Array();

        var recoveryOfficersQuery = " SELECT u_role.personnel_id AS user_id,u_role.role_id,p.display_name AS user_name " +
                " FROM " + dbTableName.mfiPersonnelRole + " u_role " +
                " JOIN " + dbTableName.iklantUsers + " u ON u.user_id = u_role.personnel_id " +
                " JOIN personnel p ON p.personnel_id = u.user_id " +
                " JOIN iklant_office io ON io.office_id = u.office_id" +
                " WHERE u_role.role_id =" + constantsObj.getFOroleId() + " " +
                " AND u.tenant_id = " + tenantId + " AND io.bc_id = 3 " +
                " AND u.active_indicator = 1 ORDER BY p.display_name";
        customlog.info("recoveryOfficersQuery : " + recoveryOfficersQuery);
        connectionDataSource.getConnection(function (clientConnect) {
            clientConnect.query(recoveryOfficersQuery, function (err, results, fields) {
                connectionDataSource.releaseConnectionPool(clientConnect);
                if (err) {
                    customlog.error(err);
                } else {
                    for (var i in results) {
                        recoveryOfficerId[i] = results[i].user_id;
                        recoveryOfficerName[i] = results[i].user_name;
                    }
                    callback(recoveryOfficerId, recoveryOfficerName);
                }
            });
        });
    },

    getNPADefaultSearchDatamodel: function (userId, callback) {
        var self = this;
        var constantsObj = this.constants;
        var recoveryOfficerNameArray = new Array();
        var accountIdArray = new Array();
        var customerNameArray = new Array();
        var overDueAmountArray = new Array();
        var daysInArrearsArray = new Array();
        var expectedCompletionDateArray = new Array();
        var groupName = new Array();

        var npaDefaultSearchQuery = "SELECT nld.account_id,nl.customer,IFNULL (pg.group_name,'Created in mifos') as group_name, " +
                "(nl.actual_principal_overdue+nl.actual_interest_overdue) AS overdue_amount, " +
                "nl.days_in_arrears,us.user_name,nur.response_txt,nld.`expected_payment_date` AS expected_date FROM " + dbTableName.tenantPrefixForNPA + "npa_util_loan_detail nld " +
                "LEFT JOIN " + dbTableName.tenantPrefixForNPA + "npa_util_loan nl ON nl.account_id = nld.account_id " +
                "INNER JOIN " + dbTableName.iklantUsers + " us ON us.user_id = nld.recovery_officer_id " +
                "INNER JOIN " + dbTableName.tenantPrefixForNPA + "npa_util_response nur ON nur.account_id = nld.account_id " +
                "LEFT JOIN rm_regional_office_list rmro ON rmro.office_id = nl.office_id " +
                " LEFT JOIN  `iklant_mifos_mapping`  imm ON  imm.`mifos_customer_id`= nl.`customer_id` LEFT JOIN iklant_prospect_group pg ON pg.`group_id`=imm.`group_id` " +
                "WHERE nl.npa_indicator <> 'N' " +
                "AND (rmro.user_id = " + userId + " OR " + userId + " = -1)" +
                "AND nld.status_id IS NOT null GROUP BY nld.`account_id` ORDER BY overdue_amount desc";
        customlog.info("npaDefaultSearchQuery : " + npaDefaultSearchQuery);
        connectionDataSource.getConnection(function (clientConnect) {
            clientConnect.query(npaDefaultSearchQuery, function (err, results, fields) {
                connectionDataSource.releaseConnectionPool(clientConnect);
                if (err) {
                    customlog.error(err);
                } else {
                    for (var i in results) {

                        accountIdArray[i] = results[i].account_id;
                        customerNameArray[i] = results[i].customer;
                        overDueAmountArray[i] = Math.round(results[i].overdue_amount);
                        recoveryOfficerNameArray[i] = results[i].user_name;
                        daysInArrearsArray[i] = results[i].days_in_arrears;
                        if (results[i].expected_date) {
                            expectedCompletionDateArray[i] = formatDateForUI(results[i].expected_date);
                        } else {
                            expectedCompletionDateArray[i] = '';
                        }
                        customlog.info("expectedCompletionDateArray: " + results[i].response_txt + ", Qid: " + results[i].account_id);
                        groupName[i] = results[i].group_name;
                    }
                    callback(accountIdArray, customerNameArray, overDueAmountArray, recoveryOfficerNameArray,
                            daysInArrearsArray, expectedCompletionDateArray, groupName);
                }
            });
        });
    },

    getNpaCaseStatusDatamodel: function (tenantId, callback) {
        var self = this;

        var npaCaseStatusIdArray = new Array();
        var npaCaseStatusNameArray = new Array();
        var npaCaseStatusQuery = "SELECT * FROM " + dbTableName.tenantPrefixForNPA + "npa_util_case_status";
        connectionDataSource.getConnection(function (clientConnect) {
            clientConnect.query(npaCaseStatusQuery, function (err, results, fields) {
                connectionDataSource.releaseConnectionPool(clientConnect);
                if (err) {
                    customlog.error(err);
                } else {
                    for (var i in results) {
                        npaCaseStatusIdArray[i] = results[i].status_id;
                        npaCaseStatusNameArray[i] = results[i].status_name;
                    }
                    callback(npaCaseStatusIdArray, npaCaseStatusNameArray);
                }
            });
        });
    },

    getNpaCaseDatamodel: function (userId, date, callback) {
        var self = this;
        var constantsObj = this.constants;
        var taskIdArray = new Array();
        var accountIdArray = new Array();
        var customerArray = new Array();
        var taskNameArray = new Array();
        var dueDateArray = new Array();
        var dueTimeArray = new Array();
        var statusIdArray = new Array();
        var statusNameArray = new Array();
        var npaCaseStatusQuery;
        if (date == "current") {
            npaCaseStatusQuery = "SELECT nut.task_id,nut.account_id,nut.task_name,nut.due_date,nut.due_time, " +
                    "nul.customer,nucs.status_id,nucs.status_name " +
                    "FROM " + dbTableName.tenantPrefixForNPA + "npa_util_task nut " +
                    "INNER JOIN " + dbTableName.tenantPrefixForNPA + "npa_util_task_alloc nuta ON nuta.task_id = nut.task_id " +
                    "INNER JOIN " + dbTableName.tenantPrefixForNPA + "npa_util_loan nul ON nul.account_id = nut.account_id " +
                    "INNER JOIN " + dbTableName.tenantPrefixForNPA + "npa_util_case_status nucs ON nucs.status_id = nuta.status_id " +
                    "WHERE nuta.allocated_to = " + userId + " AND nut.due_date = CURDATE() " +
                    "AND nuta.status_id = " + constantsObj.getNpaCaseOpenStatusId() + " ORDER BY nut.due_date";
        } else if (date == "overdue") {
            npaCaseStatusQuery = "SELECT nut.task_id,nut.account_id,nut.task_name,nut.due_date,nut.due_time, " +
                    "nul.customer,nucs.status_id,nucs.status_name " +
                    "FROM " + dbTableName.tenantPrefixForNPA + "npa_util_task nut " +
                    "INNER JOIN " + dbTableName.tenantPrefixForNPA + "npa_util_task_alloc nuta ON nuta.task_id = nut.task_id " +
                    "INNER JOIN " + dbTableName.tenantPrefixForNPA + "npa_util_loan nul ON nul.account_id = nut.account_id " +
                    "INNER JOIN " + dbTableName.tenantPrefixForNPA + "npa_util_case_status nucs ON nucs.status_id = nuta.status_id " +
                    "WHERE nuta.allocated_to = " + userId + " AND nut.due_date < CURDATE() " +
                    "AND nuta.status_id = " + constantsObj.getNpaCaseOpenStatusId() + " ORDER BY nut.due_date";
        } else if (date == "future") {
            npaCaseStatusQuery = "SELECT nut.task_id,nut.account_id,nut.task_name,nut.due_date,nut.due_time, " +
                    "nul.customer,nucs.status_id,nucs.status_name " +
                    "FROM " + dbTableName.tenantPrefixForNPA + "npa_util_task nut " +
                    "INNER JOIN " + dbTableName.tenantPrefixForNPA + "npa_util_task_alloc nuta ON nuta.task_id = nut.task_id " +
                    "INNER JOIN " + dbTableName.tenantPrefixForNPA + "npa_util_loan nul ON nul.account_id = nut.account_id " +
                    "INNER JOIN " + dbTableName.tenantPrefixForNPA + "npa_util_case_status nucs ON nucs.status_id = nuta.status_id " +
                    "WHERE nuta.allocated_to = " + userId + " AND nut.due_date > CURDATE() " +
                    "AND nuta.status_id = " + constantsObj.getNpaCaseOpenStatusId() + " ORDER BY nut.due_date";
        }
        customlog.info("npaCaseStatusQuery: " + npaCaseStatusQuery);
        connectionDataSource.getConnection(function (clientConnect) {
            clientConnect.query(npaCaseStatusQuery, function (err, results, fields) {
                connectionDataSource.releaseConnectionPool(clientConnect);
                if (err) {
                    customlog.error(err);
                } else {
                    for (var i in results) {
                        taskIdArray[i] = results[i].task_id;
                        accountIdArray[i] = results[i].account_id;
                        customerArray[i] = results[i].customer;
                        taskNameArray[i] = results[i].task_name;
                        dueDateArray[i] = formatDateForUI(results[i].due_date);
                        dueTimeArray[i] = results[i].due_time;
                        statusIdArray[i] = results[i].status_id;
                        statusNameArray[i] = results[i].status_name;
                    }
                    callback(taskIdArray, accountIdArray, customerArray, taskNameArray, dueDateArray, dueTimeArray, statusIdArray, statusNameArray);
                }
            });
        });
    },

    getNpaClosedCaseDatamodel: function (userId, date, callback) {
        var self = this;
        var constantsObj = this.constants;
        var taskIdArray = new Array();
        var accountIdArray = new Array();
        var customerArray = new Array();
        var taskNameArray = new Array();
        var dueDateArray = new Array();
        var dueTimeArray = new Array();
        var statusIdArray = new Array();
        var statusNameArray = new Array();
        var closedDateArray = new Array();
        var remarksArray = new Array();

        var npaClosedCaseStatusQuery = "SELECT nut.task_id,nut.account_id,nut.task_name,nut.due_date,nut.due_time, " +
                "nul.customer,nucs.status_id,nucs.status_name,nuta.remarks,nuta.closed_date " +
                "FROM " + dbTableName.tenantPrefixForNPA + "npa_util_task nut " +
                "INNER JOIN " + dbTableName.tenantPrefixForNPA + "npa_util_task_alloc nuta ON nuta.task_id = nut.task_id " +
                "INNER JOIN " + dbTableName.tenantPrefixForNPA + "npa_util_loan nul ON nul.account_id = nut.account_id " +
                "INNER JOIN " + dbTableName.tenantPrefixForNPA + "npa_util_case_status nucs ON nucs.status_id = nuta.status_id " +
                "WHERE nuta.allocated_to = " + userId + " " +
                "AND nuta.status_id = " + constantsObj.getNpaCaseClosedStatusId() + " ORDER BY nut.due_date";
        connectionDataSource.getConnection(function (clientConnect) {
            clientConnect.query(npaClosedCaseStatusQuery, function (err, results, fields) {
                connectionDataSource.releaseConnectionPool(clientConnect);
                if (err) {
                    customlog.error(err);
                } else {
                    for (var i in results) {
                        taskIdArray[i] = results[i].task_id;
                        accountIdArray[i] = results[i].account_id;
                        customerArray[i] = results[i].customer;
                        taskNameArray[i] = results[i].task_name;
                        dueDateArray[i] = formatDateForUI(results[i].due_date);
                        statusIdArray[i] = results[i].status_id;
                        statusNameArray[i] = results[i].status_name;
                        closedDateArray[i] = formatDateForUI(results[i].closed_date);
                        remarksArray[i] = results[i].remarks;
                    }
                    callback(taskIdArray, accountIdArray, customerArray, taskNameArray, dueDateArray, statusIdArray, statusNameArray, closedDateArray, remarksArray);
                }
            });
        });
    },

    submitNpaCaseDatamodel: function (taskId, taskRemarks, callback) {
        var self = this;
        var constantsObj = this.constants;
        var npaCaseSubmitQuery = "UPDATE " + dbTableName.tenantPrefixForNPA + "npa_util_task_alloc SET status_id = " + constantsObj.getNpaCaseClosedStatusId() + ", " +
                "last_updated_date = NOW() + INTERVAL 5 HOUR + INTERVAL 30 MINUTE,remarks='" + taskRemarks + "',closed_date = NOW() + INTERVAL 5 HOUR + INTERVAL 30 MINUTE " +
                "WHERE task_id = " + taskId + "";
        connectionDataSource.getConnection(function (clientConnect) {
            clientConnect.query(npaCaseSubmitQuery, function (err, results, fields) {
                connectionDataSource.releaseConnectionPool(clientConnect);
                if (err) {
                    customlog.error(err);
                } else {
                    callback('success');
                }
            });
        });
    },

    getGroupsForRecoveryDataModel: function (userId, callback) {
        customlog.info("Inside getGroupsForRecoveryDataModel");
        var self = this;
        var groupDetailsArray = new Array();
        var retrieveGroupListForLR = "SELECT  " +
                "npa_det.account_id, " +
                "npa_det.customer_id, " +
                "npa_det.global_account_num, " +
                "npa_det.customer, " +
                "group_lead_add.address " +
                "FROM " +
                "( " +
                "SELECT  " +
                "npa.account_id, " +
                "npa.customer_id, " +
                "npa.global_account_num, " +
                "npa.customer, " +
                "npa.npa_indicator  " +
                "FROM  " +
                "" + dbTableName.tenantPrefixForNPA + "npa_util_loan npa " +
                ")npa_det " +
                "LEFT JOIN " +
                "( " +
                "SELECT  " +
                "MIN(c.customer_id) AS customer_id, " +
                "c.parent_customer_id, " +
                "cad.line_1 AS address  " +
                "FROM customer c  " +
                "INNER JOIN customer_address_detail cad ON c.customer_id = cad.customer_id " +
                "WHERE c.customer_level_id =1  " +
                "GROUP BY c.parent_customer_id " +
                "ORDER BY c.parent_customer_id,c.customer_id " +
                ")group_lead_add " +
                "ON npa_det.customer_id = group_lead_add.parent_customer_id " +
                "INNER JOIN  " + dbTableName.tenantPrefixForNPA + "npa_util_loan_detail nla ON nla.account_id =  npa_det.account_id " +
                "WHERE nla.recovery_officer_id = " + userId + " AND npa_det.npa_indicator <> 'N' AND (nla.`status_id` = 1 OR nla.`status_id` IS NULL)";
        customlog.info("retrieveGroupListForLR : " + retrieveGroupListForLR);
        connectionDataSource.getConnection(function (clientConnect) {
            clientConnect.query(retrieveGroupListForLR,
                    function (err, results, fields) {
                        connectionDataSource.releaseConnectionPool(clientConnect);
                        if (err) {
                            customlog.error(err);
                        } else {
                            for (var i in results) {
                                var LoanRepaymentForFO1 = require(path.join(rootPath, "app_modules/dto/domain/recoveryHolder"));
                                var LoanRepaymentForFO = new LoanRepaymentForFO1();
                                LoanRepaymentForFO.setAccountId(results[i].account_id);
                                LoanRepaymentForFO.setGroupName(results[i].customer);
                                LoanRepaymentForFO.setAddress(results[i].address);
                                groupDetailsArray[i] = LoanRepaymentForFO;
                            }
                        }
                        callback(groupDetailsArray);
                    });
        });
    },

    saveActionPlansDataModel: function (todoActivity, todoDueDate, todoDueTime, RoId, userId, accountId, callback) {
        connectionDataSource.getConnection(function (clientConnect) {
            for (var j = 0; j < todoActivity.length; j++) {
                var insertTodoQuery = "INSERT INTO npa_util_task(activity_id,account_id,created_personnel_id,task_name,created_date, " +
                        "due_date,due_time)VALUES(1," + accountId + "," + RoId + ",'" + todoActivity[j] + "', " +
                        "NOW() + INTERVAL 5 HOUR + INTERVAL 30 MINUTE,'" + formatDate(todoDueDate[j]) + "','" + todoDueTime[j] + ":00:00')";
                customlog.info("insertTodoQuery from RM:" + insertTodoQuery);
                clientConnect.query(insertTodoQuery, function (err) {
                    if (err) {
                        customlog.error('insertTodoQuery:',err);
                        connectionDataSource.releaseConnectionPool(clientConnect);
                    }
                });

                var insertTodoAllocQuery = "INSERT INTO " + dbTableName.tenantPrefixForNPA + "npa_util_task_alloc(task_id,status_id,allocated_by,allocated_to, " +
                        "allocated_date, due_date, due_time)VALUES " +
                        "((SELECT IFNULL(MAX(task_id),1) FROM " + dbTableName.tenantPrefixForNPA + "npa_util_task WHERE created_personnel_id = " + RoId + "),1," +
                        "" + userId + "," + RoId + ",CURDATE(),'" + formatDate(todoDueDate[j]) + "','" + todoDueTime[j] + ":00:00')";
                customlog.info("insertTodoAllocQuery from RM:" + insertTodoAllocQuery);
                clientConnect.query(insertTodoAllocQuery, function (err) {
                    if (err) {
                        connectionDataSource.releaseConnectionPool(clientConnect);
                        customlog.error('insertTodoAllocQuery:',err);
                    }
                });
                
                if (j == todoActivity.length - 1) {
                    connectionDataSource.releaseConnectionPool(clientConnect);
                    customlog.info("Todo list added successfully");
                    callback(null);
                }
            }
        });
    },

    updateVerifiedInformationDataModel: function (params, callback) {
        var updateVerifiedInformationQuery;

        connectionDataSource.getConnection(function (clientConnect) {
            if (params.todoActivity != '' && params.todoActivity.length > 0 && params.todoDueDate.length > 0) {
                for (var j = 0; j < params.todoActivity.length; j++) {
                    var insertTodoQuery = "INSERT INTO " + dbTableName.tenantPrefixForNPA + "npa_util_task(activity_id,account_id,created_personnel_id,task_name,created_date, " +
                            "due_date,due_time)VALUES(1," + params.accountId + "," + params.userId + ",'" + params.todoActivity[j] + "', " +
                            "NOW() + INTERVAL 5 HOUR + INTERVAL 30 MINUTE,'" + formatDateForNpa(params.todoDueDate[j]) + "','" + params.todoDueTime[j] + ":00:00')";
                    clientConnect.query(insertTodoQuery, function (err, result) {
                        if (err) {
                            connectionDataSource.releaseConnectionPool(clientConnect);
                            customlog.error(err);
                        }
                    });
                    var insertTodoAllocQuery = "INSERT INTO " + dbTableName.tenantPrefixForNPA + "npa_util_task_alloc(task_id,status_id,allocated_by,allocated_to, " +
                            "allocated_date, due_date, due_time)VALUES " +
                            "((SELECT MAX(task_id) FROM " + dbTableName.tenantPrefixForNPA + "npa_util_task WHERE account_id = " + params.accountId + 
                            " AND created_personnel_id = " + params.userId + "),1," + params.userId + "," + params.userId + ",CURDATE(),'" + formatDateForNpa(params.todoDueDate[j]) + "','" + params.todoDueTime[j] + ":00:00')";
                    clientConnect.query(insertTodoAllocQuery, function (err) {
                        if (err) {
                            connectionDataSource.releaseConnectionPool(clientConnect);
                            customlog.error(err);
                        }
                    });
                }
            }
            
            var deleteReasonsQuery = "DELETE FROM " + dbTableName.tenantPrefixForNPA + "npa_util_recovery_reason WHERE account_id = " + params.accountId;
            clientConnect.query(deleteReasonsQuery,function (err) {
                if (err) {
                    connectionDataSource.releaseConnectionPool(clientConnect);
                    customlog.error(err);
                }
            });

            if (params.reason && params.otherReason) {
                for (var i = 0; i < params.reason.length; i++) {
                    if(params.reason[i] != ','){
                        var insertReasonsQuery = "INSERT INTO " + dbTableName.tenantPrefixForNPA + "npa_util_recovery_reason (account_id, recovery_reason_type_id) " +
                                "VALUES( " + params.accountId + ", " + params.reason[i] + "); ";
                        clientConnect.query(insertReasonsQuery,function (err) {
                            customlog.error('insertReasonsQuery:' + insertReasonsQuery);
                            if (err) {
                                connectionDataSource.releaseConnectionPool(clientConnect);
                                customlog.error('insertReasonsQuery error:' + err);
                            }
                        });
                    }
                }
            }

            /* util question response insertion for NPA info type */
            var currentDate = format(new Date(), "yyyy-mm-dd");
            async.eachOfSeries(params.npaQuestionsArray, function (list, index, npa_callback) {
                var questionId = params.npaQuestionsArray[index].questionId;
                var answer = params.npaQuestionsArray[index].checked;
                var dateCheckQueryForloanInfo = 'SELECT * FROM ' + dbTableName.tenantPrefixForNPA + 'npa_util_response WHERE enquiry_date=?  AND question_id=? AND account_id=? ';
                if (answer !== -1 && answer != undefined) {
                    clientConnect.query(dateCheckQueryForloanInfo, [currentDate, questionId, params.accountId], function (err, rows) {
                        if (err) {
                            connectionDataSource.releaseConnectionPool(clientConnect);
                            customlog.info('dateCheckQueryForloanInfo error:' + err);
                        } else if (rows.length === 0) {
                            var insertUtilQuestionQuery = "INSERT INTO " + dbTableName.tenantPrefixForNPA + "npa_util_response " +
                                    "(account_id, question_id, response_txt, created_date, enquiry_cycle, enquiry_date) " +
                                    "VALUES(?,?,?, NOW() + INTERVAL 5 HOUR + INTERVAL 30 MINUTE, ?, ?); ";
                            clientConnect.query(insertUtilQuestionQuery, [params.accountId, questionId, answer, 1, currentDate], function (err) {
                                if (!err) {
                                    if (params.npaQuestionsArray.length === index) {
                                        connectionDataSource.releaseConnectionPool(clientConnect);
                                    }
                                    npa_callback();
                                } else {
                                    customlog.error("insertNPAQuery error", err);
                                    if (params.npaQuestionsArray.length === index) {
                                        connectionDataSource.releaseConnectionPool(clientConnect);
                                    }
                                    npa_callback();
                                }
                            });
                        } else {
                            var enquiry_cycle = (rows[0].enquiry_cycle) + 1;
                            var insertUtilQuestionQuery = "UPDATE " + dbTableName.tenantPrefixForNPA + "npa_util_response  " +
                                    "SET response_txt = ?, updated_date = NOW() + INTERVAL 5 HOUR + INTERVAL 30 MINUTE, enquiry_cycle=? " +
                                    "WHERE account_id = ? AND question_id = ?;";
                            clientConnect.query(insertUtilQuestionQuery, [answer, enquiry_cycle, params.accountId, questionId], function (err) {
                                if (!err) {
                                    if (params.npaQuestionsArray.length === index) {
                                        connectionDataSource.releaseConnectionPool(clientConnect);
                                    }
                                    npa_callback();
                                } else {
                                    customlog.error("insertNPAQuery Error ", err);
                                    npa_callback();
                                }
                            });
                        }
                    });
                }
            });

            /*util question response insertion */

            /* geo details storing*/
            var dateCheckQueryForLattitude = 'SELECT * FROM ' + dbTableName.tenantPrefixForNPA + 'npa_util_response WHERE enquiry_date=?  AND question_id=? AND account_id=? ';
            connectionDataSource.getConnection(function (clientConnect) {
                clientConnect.query(dateCheckQueryForLattitude, [currentDate, params.geoQuestionIds[0], params.accountId], function (err, rows) {
                    if (err) {
                        connectionDataSource.releaseConnectionPool(clientConnect);
                        customlog.error(err);
                        customlog.info('dateCheckQuery:' + dateCheckQueryForLattitude);
                    } else if (rows.length === 0) {
                        insertLattitudeQuery = "INSERT INTO " + dbTableName.tenantPrefixForNPA + "npa_util_response " +
                            "(account_id, question_id, response_txt, created_date, enquiry_date) " +
                            "VALUES(" + params.accountId + ", " + params.geoQuestionIds[0] + ", " + params.lattitude + ", NOW() + INTERVAL 5 HOUR + INTERVAL 30 MINUTE, '" + currentDate + "'); ";
                        clientConnect.query(insertLattitudeQuery, function (err) {
                            if (!err) {
                                connectionDataSource.releaseConnectionPool(clientConnect);
                            } else {
                                connectionDataSource.releaseConnectionPool(clientConnect);
                                customlog.info('insertLattitudeQuery' + insertLattitudeQuery);
                                customlog.error("Geo tag insert error ",err);
                            }
                        });
                    } else {
                        updateLattitudeQuery = "UPDATE " + dbTableName.tenantPrefixForNPA + "npa_util_response  " +
                            "SET response_txt = " + params.lattitude + ", updated_date = NOW() + INTERVAL 5 HOUR + INTERVAL 30 MINUTE " +
                            "WHERE account_id = " + params.accountId + " AND question_id = " + params.geoQuestionIds[0] + ";";
            
                        clientConnect.query(updateLattitudeQuery, function (err) {
                            if (!err) {
                                connectionDataSource.releaseConnectionPool(clientConnect);
                            } else {
                                connectionDataSource.releaseConnectionPool(clientConnect);
                                customlog.error("geo tag update error ",err);
                            }
                        });
                    }
                });
            });
            
            //longitude insert,update
            var dateCheckQueryForLongitude = 'SELECT * FROM ' + dbTableName.tenantPrefixForNPA + 'npa_util_response WHERE enquiry_date=? AND question_id=? AND account_id=?';
            clientConnect.query(dateCheckQueryForLongitude, [currentDate, params.geoQuestionIds[1], params.accountId], function (err, rows) {
                if (err) {
                    connectionDataSource.releaseConnectionPool(clientConnect);
                    customlog.error(err);
                    customlog.info('dateCheckQuery:' + dateCheckQueryForLattitude);
                } else if (rows.length === 0) {
                    insertLongitudeQuery = "INSERT INTO " + dbTableName.tenantPrefixForNPA + "npa_util_response " +
                        "(account_id, question_id, response_txt, created_date, enquiry_date) " +
                        "VALUES(" + params.accountId + ", " + params.geoQuestionIds[1] + ", " + params.longitude + ", NOW() + INTERVAL 5 HOUR + INTERVAL 30 MINUTE, '" + currentDate + "'); ";
                    clientConnect.query(insertLongitudeQuery, function (err) {
                        if (err) {
                            connectionDataSource.releaseConnectionPool(clientConnect);
                            customlog.error(err);
                        }
                    });
                } else {
                    updateLongitudeQuery = "UPDATE " + dbTableName.tenantPrefixForNPA + "npa_util_response  " +
                    "SET response_txt = " + params.longitude + ", updated_date = NOW() + INTERVAL 5 HOUR + INTERVAL 30 MINUTE " +
                    "WHERE account_id = " + params.accountId + " AND question_id = " + params.geoQuestionIds[1] + ";";
                    clientConnect.query(updateLongitudeQuery, function (err) {
                        if (err) {
                            connectionDataSource.releaseConnectionPool(clientConnect);
                            customlog.error(err);
                        }
                    });
                }
            });
            
            if (params.statusId == 1) {
                updateVerifiedInformationQuery = "UPDATE " + dbTableName.tenantPrefixForNPA + "npa_util_loan_detail SET status_id = 1,last_updated_date = NOW() + INTERVAL 5 HOUR + INTERVAL 30 MINUTE WHERE account_id = " + params.accountId + "; ";
            } else if (params.statusId == 2) {
                updateVerifiedInformationQuery = "UPDATE " + dbTableName.tenantPrefixForNPA + "npa_util_loan_detail SET status_id = 2,last_updated_date = NOW() + INTERVAL 5 HOUR + INTERVAL 30 MINUTE WHERE account_id = " + params.accountId + "; ";
            } else if (params.statusId == 3) {
                updateVerifiedInformationQuery = "UPDATE " + dbTableName.tenantPrefixForNPA + "npa_util_loan_detail SET status_id = 3,reason_for_not_paid = '" + params.reason + "',other_reasons = '"+params.otherReason+"',remarks ='" + params.remarks + "',last_updated_date = NOW() + INTERVAL 5 HOUR + INTERVAL 30 MINUTE  WHERE account_id =" + params.accountId + "; ";
            } else if (params.statusId == 4) {
                if (params.expecteddate) {
                    updateVerifiedInformationQuery = "UPDATE " + dbTableName.tenantPrefixForNPA + "npa_util_loan_detail SET status_id = 4,reason_for_not_paid = '" + params.reason + "',other_reasons = '"+params.otherReason+"',capablility_percentage =" + params.capabilitypercentage + ",expected_payment_date='" + formatDateForNpa(params.expecteddate) + "',last_updated_date = NOW() + INTERVAL 5 HOUR + INTERVAL 30 MINUTE  WHERE account_id = " + params.accountId + "; ";
                } else {
                    updateVerifiedInformationQuery = "UPDATE " + dbTableName.tenantPrefixForNPA + "npa_util_loan_detail SET status_id = 4,reason_for_not_paid = '" + params.reason + "',other_reasons = '"+params.otherReason+"',capablility_percentage =" + params.capabilitypercentage + ",last_updated_date = NOW() + INTERVAL 5 HOUR + INTERVAL 30 MINUTE  WHERE account_id = " + params.accountId + "; ";
                }
            }
            clientConnect.query(updateVerifiedInformationQuery,function (err) {
                customlog.info('updateVerifiedInformationQuery'+updateVerifiedInformationQuery);
                connectionDataSource.releaseConnectionPool(clientConnect);
                if (!err) {
                    customlog.info("success");
                } else {
                    customlog.error(err);
                }
                callback();
            });
        });
    },
    //upload File
    updateFileLocationDataModel: function (accountId, fileName, selectedClientId, callback) {
        customlog.info("reached Datamodel " + accountId + " " + fileName);
        var self = this;
        if (fileName != "") {
            for (var i = 0; i < fileName.length; i++) {
                var fileLocation = "documents/group_documents/" + fileName[i];
                var query = "INSERT INTO account_doc(account_id,client_id,captured_image,account_doc_name)VALUES('" + accountId + "','" + selectedClientId + "','" + fileLocation + "','" + fileName[i] + "')";
                connectionDataSource.getConnection(function (clientConnect) {
                    clientConnect.query(query,
                            function (err, results, fields) {
                                connectionDataSource.releaseConnectionPool(clientConnect);
                                if (err) {
                                    customlog.error(err);
                                } else {
                                    customlog.info("Image Location Instered..!");
                                    if (fileName.length - 1 == i)
                                        callback();
                                }
                            });
                });
            }
        }
        callback();
    },
    retrieveClientDetailsDataModel: function (accountId, callback) {
        var customerIdArray = new Array();
        var customerNameArray = new Array();
        var customerFirstNameArray = new Array();
        var customerLastNameArray = new Array();
        var customerAddressArray = new Array();
        var customerCodeArray = new Array();
        var overdueArray = new Array();
        var arrearDaysArray = new Array();
        var retrieveClientDetailsQuery = "SELECT bcc.`BC_CUSTOMER_DETAILS_ID`, npa.account_id AS parent_account_id, bcc.`BC_CUSTOMER_DETAILS_ID` AS customer_id," +
                "bcc.`M_MEMBER_NAME_1` AS display_name, bcc.`M_MEMBER_NAME_1` AS first_name, bcc.`M_MEMBER_NAME_2` AS last_name," +
                "CONCAT(IF(bcc.`M_GROUP_CODE` <> '',CONCAT(bcc.`M_GROUP_CODE`,'-'),''),IF(bcc.`BC_CUSTOMER_DETAILS_ID` > 9,CONCAT('00',bcc.`BC_CUSTOMER_DETAILS_ID`),CONCAT('000',bcc.`BC_CUSTOMER_DETAILS_ID`))) AS client_code," +
                "npa.account_id AS parent_customer_id,IF(bcc.AC_OUTSTANDING_BALANCE > 0 && DATEDIFF(NOW(), bcc.AC_DATE_OF_LAST_PAYMENT) > 0,(DATEDIFF(NOW(), bcc.AC_DATE_OF_LAST_PAYMENT)),0)  AS arrear_days," +
                "bcc.`AC_AMOUNT_OVERDUE` AS overdue_amount,bcc.AD_MBR_PERM_ADDR AS address " +
                "FROM `iklant_bc_customer_details` bcc " +
                "JOIN `bc_npa_group_mapping` bcm ON bcm.`group_code` = bcc.`M_GROUP_CODE` AND bcm.`group_name` = bcc.`M_GROUP_NAME` " +
                "JOIN `" + dbTableName.tenantPrefixForNPA + "npa_util_loan` npa ON npa.`account_id` = bcm.`bc_npa_group_mapping_id` WHERE bcm.`bc_npa_group_mapping_id` = " + accountId;
        customlog.info("retrieveClientDetailsQuery: " + retrieveClientDetailsQuery);
        connectionDataSource.getConnection(function (clientConnect) {
            clientConnect.query(retrieveClientDetailsQuery,
                    function (err, results, fields) {
                        connectionDataSource.releaseConnectionPool(clientConnect);
                        if (err) {
                            customlog.error(err);
                        } else {
                            for (var i in results) {
                                customerIdArray[i] = results[i].customer_id;
                                customerNameArray[i] = results[i].display_name;
                                customerAddressArray[i] = results[i].address;
                                customerFirstNameArray[i] = results[i].first_name;
                                customerLastNameArray[i] = results[i].last_name;
                                ;
                                customerCodeArray[i] = results[i].client_code;
                                overdueArray[i] = results[i].overdue_amount;
                                arrearDaysArray[i] = results[i].arrear_days;
                            }
                        }
                        callback(customerIdArray, customerNameArray, customerAddressArray, customerFirstNameArray, customerLastNameArray, customerCodeArray, overdueArray, arrearDaysArray);
                    });
        });
    },

    retrieveUploadedDocsPageDataModel: function (accountId, clientId, callback) {
        var self = this;
        var docsListArray = new Array();
        var docsNameListArray = new Array();
        var retrieveUploadedDocsQuery = "SELECT * FROM account_doc WHERE account_id=" + accountId + " AND client_id=" + clientId + "";
        connectionDataSource.getConnection(function (clientConnect) {
            clientConnect.query(retrieveUploadedDocsQuery,
                    function (err, results, fields) {
                        connectionDataSource.releaseConnectionPool(clientConnect);
                        if (err) {
                            customlog.error(err);
                        } else {
                            for (var i in results) {
                                docsListArray[i] = results[i].captured_image;
                                docsNameListArray[i] = results[i].account_doc_name;
                            }
                        }
                        callback(docsListArray, docsNameListArray);
                    });
        });
    },
    getGroupDetailDataModel: function (RO, branchId, tenantId, userId1, offset, limit, callback) {
        var constantsObj = this.constants;

        customlog.info("Ro Filter roid: " + branchId + ", Ro:" + RO);
        var retrieveGroupsDetailQuery = "SELECT npa.office_id,(SELECT display_name FROM office WHERE office_id = npa.office_id) AS office,npa.account_id," +
                "npa.customer_id,npa.global_account_num,npa.customer,(SELECT `M_GROUP_CODE` FROM iklant_bc_customer_details bcc JOIN bc_npa_group_mapping bcm " +
                "ON bcc.`M_GROUP_CODE` = bcm.group_code AND bcc.`M_GROUP_NAME` = bcm.group_name WHERE bcm.`bc_npa_group_mapping_id` = npa.account_id LIMIT 1) AS group_name," +
                "npa.npa_indicator,(SELECT display_name FROM personnel WHERE personnel_id = nuld.recovery_officer_id) AS recovery_officer," +
                "npa.actual_principal_overdue + npa.actual_interest_overdue AS overdue_amount,npa.days_in_arrears," +
                "(SELECT `AD_MBR_PERM_ADDR` FROM iklant_bc_customer_details bcc JOIN bc_npa_group_mapping bcm ON bcc.`M_GROUP_CODE` = bcm.group_code AND bcc.`M_GROUP_NAME` = bcm.group_name " +
                "WHERE bcm.`bc_npa_group_mapping_id` = npa.account_id LIMIT 1) AS address " +
                "FROM " + dbTableName.tenantPrefixForNPA + "npa_util_loan npa LEFT JOIN " + dbTableName.tenantPrefixForNPA + "npa_util_loan_detail nuld ON nuld.account_id = npa.account_id " +
                "WHERE npa.npa_indicator <> 'N' AND npa.days_in_arrears > 0 ";
        if (branchId && branchId > 0) {
            retrieveGroupsDetailQuery += " AND npa.office_id=" + branchId;
        }
        if (RO) {
            if (RO > 1) {
                retrieveGroupsDetailQuery += " AND nuld.recovery_officer_id=" + RO;
            }
            if (RO == -1) {
                retrieveGroupsDetailQuery += " AND nuld.recovery_officer_id IS NULL";
            }
        }

        connectionDataSource.getConnection(function (clientConnect) {
            clientConnect.query(retrieveGroupsDetailQuery, function (err, totalResults, fields) {
                if (!err) {
                    var totalRecords = totalResults.length;
                    retrieveGroupsDetailQuery += " ORDER BY npa.days_in_arrears DESC LIMIT " + offset + ", " + limit;
                    customlog.info("Query=" + retrieveGroupsDetailQuery);
                    clientConnect.query(retrieveGroupsDetailQuery, function (err, results, fields) {
                        var NPALoanRecoveryGroupsObject = new Array();
                        var NPALoanRecoveryGroups = require(path.join(rootPath, "app_modules/dto/domain/NPALoanRecoveryGroups"));
                        for (var i in results) {
                            var NPALoanRecoveryGroupsDetail = new NPALoanRecoveryGroups();
                            NPALoanRecoveryGroupsDetail.setAccountId(results[i].account_id);
                            NPALoanRecoveryGroupsDetail.setCustomerId(results[i].customer_id);
                            NPALoanRecoveryGroupsDetail.setGlobalAccountNum(results[i].global_account_num);
                            NPALoanRecoveryGroupsDetail.setGroupName(results[i].customer);
                            NPALoanRecoveryGroupsDetail.setGlAddress(results[i].address);
                            NPALoanRecoveryGroupsDetail.setGroupCode(results[i].group_name);
                            NPALoanRecoveryGroupsDetail.setOffice(results[i].office);
                            NPALoanRecoveryGroupsDetail.setOfficeId(results[i].office_id);
                            NPALoanRecoveryGroupsDetail.setRecoveryOfficer(results[i].recovery_officer);
                            NPALoanRecoveryGroupsDetail.setOverdueAmount(Math.floor(results[i].overdue_amount));
                            NPALoanRecoveryGroupsDetail.setArrearDays(results[i].days_in_arrears);
                            NPALoanRecoveryGroupsObject[i] = NPALoanRecoveryGroupsDetail;
                        }
                        var retrieveRO = " SELECT io.office_id,u_role.personnel_id AS user_id,u_role.role_id,p.display_name AS user_name " +
                                " FROM " + dbTableName.mfiPersonnelRole + " u_role " +
                                " JOIN " + dbTableName.iklantUsers + " u ON u.user_id = u_role.personnel_id " +
                                " JOIN personnel p ON p.personnel_id = u.user_id " +
                                " JOIN iklant_office io ON io.office_id = u.office_id" +
                                " WHERE u_role.role_id =" + constantsObj.getFOroleId() + " " +
                                " AND u.tenant_id = " + tenantId + " AND io.bc_id = 3 " +
                                " AND u.active_indicator = 1";
                        if (branchId && branchId > 0) {
                            retrieveRO = retrieveRO + " AND io.office_id=" + branchId + " ORDER BY p.display_name ASC";
                        } else {
                            retrieveRO = retrieveRO + " ORDER BY p.display_name ASC";
                        }
                        customlog.info("retrieveRO " + retrieveRO);
                        clientConnect.query(retrieveRO, function (err, results, fields) {
                            if (!err) {
                                var userName = new Array();
                                var userId = new Array();
                                var userOfficeId = new Array();
                                for (var i in results) {
                                    userName[i] = results[i].user_name;
                                    userId[i] = results[i].user_id;
                                    userOfficeId[i] = results[i].office_id;
                                }

                                var retrieveClosedGroups = " SELECT  " +
                                        " nld.account_id, " +
                                        " nl.customer, " +
                                        " nld.status_id, " +
                                        " nld.reason_for_not_paid, " +
                                        " nld.capablility_percentage, " +
                                        " nld.expected_payment_date, " +
                                        " nld.remarks, " +
                                        " nls.status_name " +
                                        " FROM " +
                                        " 	" + dbTableName.tenantPrefixForNPA + "npa_util_loan_detail nld " +
                                        " 	INNER JOIN " + dbTableName.tenantPrefixForNPA + "npa_util_loan_status nls ON nls.status_id = nld.status_id " +
                                        " 	INNER JOIN " + dbTableName.tenantPrefixForNPA + "npa_util_loan nl ON nl.account_id = nld.account_id " +
                                        "   LEFT JOIN rm_regional_office_list rmro ON rmro.office_id = nl.office_id" +
                                        "   WHERE (rmro.user_id = " + userId1 + " OR " + userId1 + " = -1)";
                                clientConnect.query(retrieveClosedGroups, function (err, results, fields) {
                                    connectionDataSource.releaseConnectionPool(clientConnect);
                                    if (!err) {
                                        var NPALoanRecoveryGroupsStatusObject = new Array();
                                        var NPALoanRecoveryGroupsStatus = require(path.join(rootPath, "app_modules/dto/domain/NPALoanRecoveryGroupsStatus"));
                                        for (var i in results) {
                                            var NPALoanRecoveryGroupsStatusDetail = new NPALoanRecoveryGroupsStatus();
                                            NPALoanRecoveryGroupsStatusDetail.setAccountId(results[i].account_id);
                                            NPALoanRecoveryGroupsStatusDetail.setGroupName(results[i].customer);
                                            NPALoanRecoveryGroupsStatusDetail.setIsRecovered(results[i].status_id);
                                            NPALoanRecoveryGroupsStatusDetail.setReasonForNotPaid(results[i].reason_for_not_paid);
                                            NPALoanRecoveryGroupsStatusDetail.setCapablilityPercentage(results[i].capablility_percentage);
                                            NPALoanRecoveryGroupsStatusDetail.setExpectedPaymentDate(formatDateForUI(results[i].expected_payment_date));
                                            NPALoanRecoveryGroupsStatusDetail.setRemarks(results[i].remarks);
                                            NPALoanRecoveryGroupsStatusDetail.setLoansStatusDescription(results[i].status_name);
                                            NPALoanRecoveryGroupsStatusObject[i] = NPALoanRecoveryGroupsStatusDetail;
                                        }
                                        callback(NPALoanRecoveryGroupsObject, NPALoanRecoveryGroupsStatusObject, userName, userId, totalRecords, userOfficeId);
                                    } else {
                                        customlog.error(err);
                                        callback(NPALoanRecoveryGroupsObject, NPALoanRecoveryGroupsStatusObject, userName, userId, totalRecords, userOfficeId);
                                    }
                                });
                            } else {
                                connectionDataSource.releaseConnectionPool(clientConnect);
                                customlog.error(err);
                                callback();
                            }
                        });
                    });

                } else {
                    connectionDataSource.releaseConnectionPool(clientConnect);
                    customlog.error(err);
                    callback();
                }
            });
        });
    },

    getUtilQuestionsDataModel: function (callback) {
        customlog.info("Inside getUtilQuestionsDataModel");
        var getUtilQuestions = 'SELECT * FROM ' + dbTableName.tenantPrefixForNPA + 'npa_util_question';
        connectionDataSource.getConnection(function (clientConnect) {
            clientConnect.query(getUtilQuestions, function (err, rows) {
                connectionDataSource.releaseConnectionPool(clientConnect);
                if (err) {
                    customlog.error(err);
                } else {
                    callback(rows);
                }
            });
        });
    },

    getGroupDetailsForDataModel: function (userId, accountId, callback) {
        customlog.info("Inside getGroupsForRecoveryDataModel");
        var self = this;
        var recoveryReasonsArray = new Array();
        var LoanRepaymentForFO1 = require(path.join(rootPath, "app_modules/dto/domain/recoveryHolder"));
        var LoanRepaymentForFO = new LoanRepaymentForFO1();
        var isNpa = 0;
        self.checkForNpaAssignedorNot(accountId, function (resultLength) {
            customlog.info("Inside checkForNpaAssignedorNot result length: " + resultLength);
            isNpa = resultLength;
            if (resultLength == 0) {
                self.assignSingleGroupToFODataModel(accountId, userId, function () {
                });
            }
        });
        var getReasonsForNPAQuery = "SELECT recovery_reason_type_id FROM " + dbTableName.tenantPrefixForNPA + "npa_util_recovery_reason WHERE account_id =  " + accountId + ";  ";
        connectionDataSource.getConnection(function (clientConnect) {
            clientConnect.query(getReasonsForNPAQuery,
                    function (err, results, fields) {
                        if (err) {
                            connectionDataSource.releaseConnectionPool(clientConnect);
                            customlog.error(err);
                        } else {
                            for (var i in results) {
                                recoveryReasonsArray[i] = results[i].recovery_reason_type_id;
                            }
                            customlog.info("recoveryReasonsArray" + recoveryReasonsArray);
                            LoanRepaymentForFO.setReasonsIdArray(recoveryReasonsArray);
                        }
                    });


            var getAccountDetailsForGroup = "SELECT npa_det.account_id, CASE WHEN nld.status_id IS NULL THEN 0 ELSE 1 END AS loan_status," +
                    "nld.status_id,nld.recovery_officer_id,nld.remarks,nld.other_reasons,nr.question_id,nr.response_txt,npa_det.customer_id," +
                    "npa_det.global_account_num,npa_det.customer,npa_det.personnel,npa_det.office_id,npa_det.amount_demanded,npa_det.amount_paid," +
                    "npa_det.overdue,DATE_FORMAT(npa_det.disbursement_date,'%d-%m-%Y') AS disbursement_date, npa_det.loan_amount, npa_det.no_of_installments,npa_det.address " +
                    "FROM (SELECT npa.account_id,npa.customer_id,npa.global_account_num,npa.office_id,npa.customer,npa.personnel,npa.disbursement_date, npa.loan_amount, npa.no_of_installments," +
                    "ROUND(actual_principal_demd+actual_interest_demd) AS amount_demanded,ROUND(actual_principal_paid+actual_interest_paid) AS amount_paid," +
                    "ROUND(actual_principal_overdue+actual_interest_overdue) AS overdue, " +
                    "(SELECT `AD_MBR_PERM_ADDR` FROM iklant_bc_customer_details bcc JOIN bc_npa_group_mapping bcm ON bcc.`M_GROUP_CODE` = bcm.group_code " +
                    "AND bcc.`M_GROUP_NAME` = bcm.group_name WHERE bcm.`bc_npa_group_mapping_id` = npa.account_id LIMIT 1) AS address " +
                    "FROM " + dbTableName.tenantPrefixForNPA + "npa_util_loan npa)npa_det  " +
                    "LEFT JOIN " + dbTableName.tenantPrefixForNPA + "npa_util_loan_detail nld ON nld.account_id = npa_det.account_id " +
                    "LEFT JOIN " + dbTableName.tenantPrefixForNPA + "npa_util_response nr ON nr.account_id = npa_det.account_id " +
                    "WHERE npa_det.account_id = " + accountId;
            customlog.info("getAccountDetailsForGroup: " + getAccountDetailsForGroup);
            var answerIdArray = new Array();
            clientConnect.query(getAccountDetailsForGroup,
                    function (err, results, fields) {
                        connectionDataSource.releaseConnectionPool(clientConnect);
                        if (err) {
                            customlog.error(err);
                        } else {
                            for (var i in results) {
                                customlog.info("question_id==" + results[i].question_id);
                                customlog.info("response==" + results[i].response_txt);
                                LoanRepaymentForFO.setAccountId(results[i].account_id);
                                LoanRepaymentForFO.setGroupName(results[i].customer);
                                LoanRepaymentForFO.setAddress(results[i].address);
                                LoanRepaymentForFO.setAmountDemanded(results[i].amount_demanded);
                                LoanRepaymentForFO.setAmountPaid(results[i].amount_paid);
                                LoanRepaymentForFO.setAmountOverdue(results[i].overdue);
                                LoanRepaymentForFO.setLoanOfficer(results[i].personnel);
                                LoanRepaymentForFO.setCustomerId(results[i].customer_id);
                                LoanRepaymentForFO.setRemarks(results[i].remarks);
                                LoanRepaymentForFO.setOtherReasons(results[i].other_reasons);
                                LoanRepaymentForFO.setLoanAmount(results[i].loan_amount);
                                LoanRepaymentForFO.setDBDate(results[i].disbursement_date);
                                LoanRepaymentForFO.setTenure(results[i].no_of_installments);
                                LoanRepaymentForFO.setStatusFlag(results[i].loan_status);
                                LoanRepaymentForFO.setOfficeId(results[i].office_id);
                                LoanRepaymentForFO.setLoanOfficerId(results[i].recovery_officer_id);
                                if (results[i].response_txt != null) {
                                    if (i == 6) {
                                        answerIdArray[i] = dateUtils.formatDateForUI(results[i].response_txt);
                                    } else {
                                        answerIdArray[i] = results[i].response_txt;
                                    }
                                }
                            }
                            customlog.info("answerIdArray", answerIdArray);
                            LoanRepaymentForFO.setAnswerIdArray(answerIdArray, isNpa);
                        }
                        callback(LoanRepaymentForFO, isNpa);
                    });
        });
    },

    checkForNpaAssignedorNot: function (accountId, callback) {
        var self = this;
        var constantsObj = this.constants;
        var npaAssignedCheckQuery = "SELECT * FROM " + dbTableName.tenantPrefixForNPA + "npa_util_loan_detail WHERE account_id = " + accountId + "";
        connectionDataSource.getConnection(function (clientConnect) {
            clientConnect.query(npaAssignedCheckQuery, function (err, results, fields) {
                connectionDataSource.releaseConnectionPool(clientConnect);
                if (err) {
                    customlog.error(err);
                } else {
                    for (var i in results) {

                    }
                    callback(results.length);
                }
            });
        });
    },

    assignSingleGroupToFODataModel: function (accountId, roId, callback) {
        var self = this;
        var insertNPALoansDetail = "INSERT INTO " + dbTableName.tenantPrefixForNPA + "npa_util_loan_detail " +
                "(account_id, recovery_officer_id, allocated_date, last_updated_date) " +
                " VALUES(" + accountId + "," + roId + ",NOW() + INTERVAL 5 HOUR + INTERVAL 30 MINUTE,NOW() + INTERVAL 5 HOUR + INTERVAL 30 MINUTE)";
        connectionDataSource.getConnection(function (clientConnect) {
            clientConnect.query(insertNPALoansDetail, function (err) {
                connectionDataSource.releaseConnectionPool(clientConnect);
                if (!err) {
                    callback();
                } else {
                    customlog.error(err);
                }
            });
        });
    },
    
    //added on 25-7-17
    getRecoveryReasonsDataModel: function (callback) {
        var self=this;
        var reasonId = new Array();
        var reasonDescription = new Array();
        var getReasonsQuery = "SELECT * FROM npa_util_recovery_reason_type;";
        connectionDataSource.getConnection(function (clientConnect) {
            clientConnect.query(getReasonsQuery,function selectCb(err, results, fields) {
                connectionDataSource.releaseConnectionPool(clientConnect);
                if (err) {
                    customlog.error(err);
                }
                else {
                    for (var i in results) {
                        reasonId[i] = results[i].recovery_reason_type_id;
                        reasonDescription[i] = results[i].recovery_reason_type;
                    }
                }
                callback(reasonId, reasonDescription);
            });
        });
    },
    retrieveAllUploadedDocsDataModel: function (accountId, callback) {
        var retrieveUploadedDocsQuery = "SELECT ad.`account_doc_name`,ad.`captured_image`,IF(ad.client_id = 0, 'Group Doc','Client Doc') AS doc_for,(SELECT display_name FROM customer WHERE customer_id = ad.`client_id`) AS client_name FROM account_doc ad "+
            "INNER JOIN account a ON a.`account_id` = ad.`account_id`"+
            " WHERE ad.account_id = "+accountId+" GROUP BY ad.`account_doc_id`";
        connectionDataSource.getConnection(function (clientConnect) {
            clientConnect.query(retrieveUploadedDocsQuery,
                function(err, results, fields) {
                    connectionDataSource.releaseConnectionPool(clientConnect);
                    if (err) {
                        customlog.error(err);
                    }
                    callback(results);
                }
            );
        });
    },

    getGroupDetailsForWebDataModel: function (userId, accountId, callback) {
        customlog.info("Inside getGroupsForRecoveryDataModel");
        var self = this;
        var recoveryReasonsArray = new Array();
        var LoanRepaymentForFO1 = require(path.join(rootPath, "app_modules/dto/domain/recoveryHolder"));
        var LoanRepaymentForFO = new LoanRepaymentForFO1();
        var isNpa = 0;
        self.checkForNpaAssignedorNot(accountId, function (resultLength) {
            customlog.info("Inside checkForNpaAssignedorNot result length: " + resultLength);
            isNpa = resultLength;
            if (resultLength == 0) {
                self.assignSingleGroupToFODataModel(accountId, userId, function () {
                });
            }
        });
        var getReasonsForNPAQuery = "SELECT recovery_reason_type_id FROM " + dbTableName.tenantPrefixForNPA + "npa_util_recovery_reason WHERE account_id =  " + accountId + ";  ";
        connectionDataSource.getConnection(function (clientConnect) {
            clientConnect.query(getReasonsForNPAQuery,
                    function (err, results, fields) {
                        if (err) {
                            connectionDataSource.releaseConnectionPool(clientConnect);
                            customlog.error(err);
                        } else {
                            for (var i in results) {
                                recoveryReasonsArray[i] = results[i].recovery_reason_type_id;
                            }
                            customlog.info("recoveryReasonsArray" + recoveryReasonsArray);
                            LoanRepaymentForFO.setReasonsIdArray(recoveryReasonsArray);
                        }
                    });

            var getAccountDetailsForGroup = "SELECT   " +
                    "	npa_det.account_id,  " +
                    "	CASE " +
                    "		WHEN nld.status_id IS NULL THEN 0 " +
                    "		ELSE 1 " +
                    "	END AS loan_status, " +
                    "	nld.status_id, " +
                    "	IF((nld.remarks),nld.remarks,'') AS remarks, " +
                    "	nld.recovery_officer_id, " +
                    "	npa_det.office_id,  " +
                    "       GROUP_CONCAT(nq.question_text) AS questions, " +
                    "       GROUP_CONCAT(nr.response_txt) AS answers, " +
                    "	nld.other_reasons, " +
                    "       nurrt.recovery_reason_type, " +
                    "	npa_det.customer_id,  " +
                    "       DATE_FORMAT(nr.enquiry_date,'%d-%m-%Y') AS enquiry_date,      " +
                    "       nr.enquiry_cycle,     " +
                    "	npa_det.global_account_num,  " +
                    "	npa_det.customer,  " +
                    "	npa_det.personnel,  " +
                    "	npa_det.amount_demanded,  " +
                    "	npa_det.amount_paid,  " +
                    "	npa_det.overdue,  " +
                    "	group_lead_add.address  " +
                    "FROM  " +
                    "(  " +
                    "	SELECT   " +
                    "		npa.account_id,  " +
                    "		npa.customer_id,  " +
                    "		npa.office_id,  " +
                    "		npa.global_account_num,  " +
                    "		npa.customer,  " +
                    "		npa.personnel,npa.disbursement_date, npa.loan_amount, npa.no_of_installments, " +
                    "		ROUND(actual_principal_demd+actual_interest_demd) AS amount_demanded,  " +
                    "		ROUND(actual_principal_paid+actual_interest_paid) AS amount_paid,  " +
                    "		ROUND(actual_principal_overdue+actual_interest_overdue) AS overdue  " +
                    "	FROM   " +
                    "		" + dbTableName.tenantPrefixForNPA + "npa_util_loan npa  " +
                    ")npa_det  " +
                    "LEFT JOIN  " +
                    "(  " +
                    "	SELECT   " +
                    "		MIN(c.customer_id) AS customer_id,  " +
                    "		c.parent_customer_id,  " +
                    "		cad.line_1 AS address   " +
                    "	FROM customer c   " +
                    "		INNER JOIN customer_address_detail cad ON c.customer_id = cad.customer_id  " +
                    "	WHERE c.customer_level_id =1   " +
                    "		GROUP BY c.parent_customer_id  " +
                    "		ORDER BY c.parent_customer_id,c.customer_id  " +
                    ")group_lead_add  " +
                    "ON npa_det.customer_id = group_lead_add.parent_customer_id  " +
                    "LEFT JOIN " + dbTableName.tenantPrefixForNPA + "npa_util_loan_detail nld ON nld.account_id = npa_det.account_id " +
                    "LEFT JOIN " + dbTableName.tenantPrefixForNPA + "npa_util_response nr ON nr.account_id = npa_det.account_id " +
                    "LEFT JOIN " + dbTableName.tenantPrefixForNPA + "npa_util_question nq ON nq.question_id=nr.question_id " +
                    "LEFT JOIN " + dbTableName.tenantPrefixForNPA + "npa_util_recovery_reason_type nurrt ON nurrt.recovery_reason_type_id=nld.reason_for_not_paid " +
                    "WHERE npa_det.account_id = " + accountId + " GROUP BY nr.enquiry_cycle ASC ";
            customlog.info("getAccountDetailsWebForGroup: " + getAccountDetailsForGroup);
            var answerIdArray = new Array();
            clientConnect.query(getAccountDetailsForGroup,
                    function (err, results, fields) {
                        connectionDataSource.releaseConnectionPool(clientConnect);
                        if (err) {
                            customlog.error(err);
                        } else {
                            var QADetails = new Array();
                            var LoanRepaymentForFOBasic = new LoanRepaymentForFO1();
                            for (var i in results) {
                                var LoanRepaymentForFO = new LoanRepaymentForFO1();
                                QADetails[i] = LoanRepaymentForFO;
                                LoanRepaymentForFOBasic.setReasonsIdArray(recoveryReasonsArray);
                                LoanRepaymentForFOBasic.setAccountId(results[i].account_id);
                                LoanRepaymentForFOBasic.setGroupName(results[i].customer);
                                LoanRepaymentForFOBasic.setAddress(results[i].address);
                                LoanRepaymentForFOBasic.setAmountDemanded(results[i].amount_demanded);
                                LoanRepaymentForFOBasic.setAmountPaid(results[i].amount_paid);
                                LoanRepaymentForFOBasic.setAmountOverdue(results[i].overdue);
                                LoanRepaymentForFOBasic.setLoanOfficer(results[i].personnel);
                                LoanRepaymentForFOBasic.setCustomerId(results[i].customer_id);
                                LoanRepaymentForFOBasic.setRemarks(results[i].remarks);
                                LoanRepaymentForFOBasic.setStatusId(results[i].status_id);
                                LoanRepaymentForFOBasic.setOtherReasons(results[i].other_reasons);
                                LoanRepaymentForFOBasic.setStatusFlag(results[i].loan_status);
                                LoanRepaymentForFOBasic.setAnswerIdArray(results[i].answers);
                                LoanRepaymentForFOBasic.setOfficeId(results[i].office_id);
                                LoanRepaymentForFOBasic.setLoanOfficerId(results[i].recovery_officer_id);
                                var questions = results[i].questions.split(",");
                                questions.push("Reason for not paid");
                                QADetails[i].setQuestionText(questions);
                                var answers = results[i].answers.split(",");
                                answers.push(results[i].recovery_reason_type);
                                QADetails[i].setAnswerIdArray(answers);
                                QADetails[i].setEnquiryDate(results[i].enquiry_date);

                            }
                        }
                        customlog.info("LoanRepaymentForFO: ", QADetails);
                        callback(LoanRepaymentForFOBasic, QADetails, isNpa);
                    });
        });
    },

    migrateCustomerDataModel: function () {
        var insertNPAGroupMappingQuery = "INSERT INTO `bc_npa_group_mapping`(`group_code`,`group_name`) SELECT bcc.`M_GROUP_CODE`,bcc.`M_GROUP_NAME` FROM `iklant_bc_customer_details` bcc GROUP BY bcc.`M_GROUP_CODE`,bcc.`M_GROUP_NAME` ORDER BY bcc.`M_GROUP_CODE`,bcc.`M_GROUP_NAME`";
        customlog.info("insertNPAGroupMappingQuery: " + insertNPAGroupMappingQuery);
        connectionDataSource.getConnection(function (clientConnect) {
            clientConnect.query(insertNPAGroupMappingQuery, function (err) {
                if (err) {
                    customlog.error(err);
                    connectionDataSource.releaseConnectionPool(clientConnect);
                } else {
                    var insertNPAGroupsQuery = "DELETE FROM `" + dbTableName.tenantPrefixForNPA + "npa_util_loan`;" +
                            "INSERT INTO `" + dbTableName.tenantPrefixForNPA + "npa_util_loan`(`account_id`,`global_account_num`,`office_id`,`office`,`customer_id`,`customer`,`account_type_id`,`account_type`,`prd_category_id`,`prd_category`," +
                            "`prd_offering_id`,`prd_offering`,`personnel_id`,`personnel`,`account_state_id`,`account_state`,`interest_rate`,`no_of_installments`,`no_of_installments_paid`,`no_of_installments_not_paid`," +
                            "`no_of_npa_members`,`disbursement_date`,`loan_amount`,`orig_principal`,`orig_interest`,`orig_fees`,`orig_penalty`,`orig_principal_paid`,`orig_interest_paid`,`orig_fees_paid`," +
                            "`orig_penalty_paid`,`orig_income`,`orig_principal_outstanding`,`orig_interest_outstanding`,`orig_fees_outstanding`,`orig_penalty_outstanding`,`actual_principal_demd`,`actual_interest_demd`," +
                            "`actual_fees_demd`,`actual_penalty_demd`,`actual_extra_interest_demd`,`actual_misc_penalty_demd`,`actual_principal_not_demd`,`actual_interest_not_demd`,`actual_fees_not_demd`," +
                            "`actual_penalty_not_demd`,`actual_principal_paid`,`actual_interest_paid`,`actual_fees_paid`,`actual_penalty_paid`,`actual_principal_overdue`,`actual_interest_overdue`,`actual_fees_overdue`," +
                            "`actual_penalty_overdue`,`actual_principal_outstanding`,`actual_interest_outstanding`,`actual_fees_outstanding`,`actual_penalty_outstanding`,`exact_principal_paid`,`exact_interest_paid`," +
                            "`exact_fees_paid`,`exact_penalty_paid`,`days_in_arrears`,`loan_status`,`npa_indicator`,`npa_principal_outstanding`,`npa_interest_outstanding`,`no_of_members`,`last_updated_date`)" +
                            "SELECT bcm.`bc_npa_group_mapping_id` AS account_id,CONCAT(o.global_office_num,'00000',bcm.bc_npa_group_mapping_id) AS global_account_num," +
                            "o.`office_id` AS office_id,o.`display_name` AS office,bcm.`bc_npa_group_mapping_id` AS customer_id,bcc.`M_GROUP_NAME` AS customer,'1' AS account_type_id," +
                            "'Loan Account' AS account_type,'' AS prd_category_id,'' AS prd_category,'' AS prd_offering_id,'' AS prd_offering,'' AS personnel_id,bcc.`AC_LOAN_OFFICER_ORIG_LOAN` AS personnel," +
                            "'9' AS account_state_id,'Active In Bad Standing' AS account_state,'' AS interest_rate,MAX(bcc.`AC_NUMBER_OF_INSTALLMENTS`) AS no_of_installments,'' AS no_of_installments_paid," +
                            "'' AS no_of_installments_not_paid,'999' AS no_of_npa_members,bcc.`AC_SANCTIONED_DATE` AS disbursement_date,SUM(bcc.`AC_TOTAL_AMOUNT_DISBURSED`) AS loan_amount," +
                            "SUM( bcc.`AC_TOTAL_AMOUNT_DISBURSED`) AS orig_principal,'0' AS orig_interest,'0' AS orig_fees,'0' AS orig_penalty,SUM(bcc.`AC_TOTAL_AMOUNT_DISBURSED`)-SUM(bcc.`AC_OUTSTANDING_BALANCE`) AS orig_principal_paid," +
                            "'0' AS orig_interest_paid,'0' AS orig_fees_paid,'0' AS orig_penalty_paid,'0' AS orig_income,SUM(bcc.`AC_OUTSTANDING_BALANCE`) AS orig_principal_outstanding,'0' AS orig_interest_outstanding," +
                            "'0' AS orig_fees_outstanding,'0' AS orig_penalty_outstanding,SUM(bcc.`AC_OUTSTANDING_BALANCE`) AS actual_principal_demd,'0' AS actual_interest_demd,'0' AS actual_fees_demd,'0' AS actual_penalty_demd,'0' AS actual_extra_interest_demd," +
                            "'0' AS actual_misc_penalty_demd,'0' AS actual_principal_not_demd,'0' AS actual_interest_not_demd,'0' AS actual_fees_not_demd,'0' AS actual_penalty_not_demd,SUM(bcc.`AC_TOTAL_AMOUNT_DISBURSED`)-SUM(bcc.`AC_OUTSTANDING_BALANCE`) AS actual_principal_paid," +
                            "'0' AS actual_interest_paid,'0' AS actual_fees_paid,'0' AS actual_penalty_paid,SUM(bcc.`AC_AMOUNT_OVERDUE`) AS actual_principal_overdue,'0' AS actual_interest_overdue," +
                            "'0' AS actual_fees_overdue,'0' AS actual_penalty_overdue,SUM(bcc.`AC_OUTSTANDING_BALANCE`) AS actual_principal_outstanding,'0' AS actual_interest_outstanding,'0' AS actual_fees_outstanding," +
                            "'0' AS actual_penalty_outstanding,'0' AS exact_principal_paid,'0' AS exact_interest_paid,'0' AS exact_fees_paid,'0' AS exact_penalty_paid,IF(SUM(bcc.AC_OUTSTANDING_BALANCE) > 0 && DATEDIFF(NOW(), MIN(bcc.AC_DATE_OF_LAST_PAYMENT)) > 0,(DATEDIFF(NOW(), MIN(bcc.AC_DATE_OF_LAST_PAYMENT))),0)  AS days_in_arrears," +
                            "'Open' AS loan_status,'Y' AS npa_indicator,SUM(bcc.`AC_OUTSTANDING_BALANCE`) AS npa_principal_outstanding,'0' AS npa_interest_outstanding,COUNT(bcc.BC_CUSTOMER_DETAILS_ID) AS no_of_members," +
                            "NOW() AS last_updated_date FROM `iklant_bc_customer_details` bcc " +
                            "JOIN office o ON o.`office_id` = bcc.`office_id` LEFT JOIN `bc_npa_group_mapping` bcm ON bcm.`group_code` = bcc.`M_GROUP_CODE` AND bcm.`group_name` = bcc.`M_GROUP_NAME`" +
                            "GROUP BY bcc.`M_GROUP_CODE`,bcc.`M_GROUP_NAME` ORDER BY bcc.`M_GROUP_CODE`,bcc.`M_GROUP_NAME`;";
                    customlog.info("insertNPAGroupsQuery: " + insertNPAGroupsQuery);
                    clientConnect.query(insertNPAGroupsQuery, function (err) {
                        connectionDataSource.releaseConnectionPool(clientConnect);
                        if (err) {
                            customlog.error(err);
                        }
                    });
                }
            });
        });
    }
};

function formatDate(tempDate) {
    var ddd = tempDate.split("/");
    var now = new Date(ddd[2], ddd[1] - 1, ddd[0]);
    var curr_date = ("0" + now.getDate()).slice(-2);
    var curr_month = ("0" + (now.getMonth() + 1)).slice(-2);
    var curr_year = now.getFullYear();
    var tempDate = curr_year + "-" + curr_month + "-" + curr_date;
    if (isNaN(curr_date)) {
        tempDate = '0000-00-00';
    }
    return tempDate;
}

function formatDateForNpa(tempDate) {
    var ddd = tempDate.split("-");
    var now = new Date(ddd[0], ddd[1] - 1, ddd[2]);
    var curr_date = ("0" + now.getDate()).slice(-2);
    var curr_month = ("0" + (now.getMonth() + 1)).slice(-2);
    var curr_year = now.getFullYear();
    var tempDate = curr_year + "-" + curr_month + "-" + curr_date;
    if (isNaN(curr_date)) {
        tempDate = '0000-00-00';
    }
    return tempDate;
}

function formatDateForUI(tempDate) {
    var now = new Date(tempDate);
    var curr_date = ("0" + now.getDate()).slice(-2);
    var curr_month = ("0" + (now.getMonth() + 1)).slice(-2);
    var curr_year = now.getFullYear();
    var tempDate = curr_date + "/" + curr_month + "/" + curr_year;
    if (isNaN(curr_date)) {
        tempDate = "";
    }
    return tempDate;
}