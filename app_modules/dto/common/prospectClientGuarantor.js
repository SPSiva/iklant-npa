module.exports = prospectClientGuarantor;

var prospectClientGuarantorId;
var clientId;
var guarantorName;
var guarantorAge;
var guarantorAgeAsOnDate;
var guarantorDob;
var guarantorRelationship;
var guarantorAddress; //Removed

//Added for address new format
var guarantorLine1;
var guarantorLine2;
var guarantorLine3;
var guarantorCity;
var guarantorState;
var guarantorLandmark;
var guarantorPincode;

var guarantorId;
var otherGuarantorRelationshipName = "";
var aadhaarCardNumber;
var voterId;
var rationCardNumber;
var jobCardNumber;

var guarantorMobileNumber;
var guarantorLandlineNumber;
var guarantorMaritalStatus;
var guarantorReligion;
var guarantorCaste;
var guarantorEducationStatus;
var guarantorGender;
var guarantorNationality;


function prospectClientGuarantor() {
    this.clearAll();
}

prospectClientGuarantor.prototype = {

	getProspectClientGuarantorId: function(){
		return this.prospectClientGuarantorId;
	},
	setProspectClientGuarantorId: function (prospect_client_guarantor_id){
        this.prospectClientGuarantorId = prospect_client_guarantor_id;
	},

	getClientId: function(){
		return this.clientId;
	},
	setClientId: function (client_id){
        this.clientId = client_id;
	},
	
	getGuarantorName: function(){
		return this.guarantorName;
	},
	setGuarantorName: function (guarantor_name){
        this.guarantorName = guarantor_name;
	},
	getProspectClientGuarantorAge: function(){
		return this.guarantorAge;
	},
	setProspectClientGuarantorAge: function (guarantorAge){
		this.guarantorAge = guarantorAge;
	},

	getProspectClientGuarantorAgeAsOnDate: function(){
		return this.guarantorAgeAsOnDate;
	},
	setProspectClientGuarantorAgeAsOnDate: function (guarantorAgeAsOnDate){
		this.guarantorAgeAsOnDate = guarantorAgeAsOnDate;
	},
	getGuarantorDob: function(){
		return this.guarantorDob;
	},
	setGuarantorDob: function (guarantor_dob){
        this.guarantorDob = guarantor_dob;
	},
	
	getGuarantorRelationship: function(){
		return this.guarantorRelationship;
	},
	setGuarantorRelationship: function (guarantor_relationship){
        this.guarantorRelationship = guarantor_relationship;
	},
	
	getGuarantorAddress: function(){
		return this.guarantorAddress;
	},
	setGuarantorAddress: function (guarantor_address){
        this.guarantorAddress = guarantor_address;
	},
	getGuarantorId: function(){
		return this.guarantorId;
	},
	setGuarantorId: function (guarantor_Id){
        this. guarantorId = guarantor_Id;
	},
    getOtherGuarantorRelationshipName: function(){
        return this.otherGuarantorRelationshipName;
    },
    setOtherGuarantorRelationshipName: function (other_GuarantorRelationshipName){
        this.otherGuarantorRelationshipName = other_GuarantorRelationshipName;
    },
	getAadhaarCardNumber: function(){
		return this.aadhaarCardNumber;
	},
	setAadhaarCardNumber: function (AadhaarCardNumber){
		this.aadhaarCardNumber = AadhaarCardNumber;
	},
	getVoterIdNumber: function(){
		return this.voterId;
	},
	setVoterIdNumber: function (VoterId){
		this.voterId = VoterId;
	},
	getRationCardNumber: function(){
		return this.rationCardNumber;
	},
	setRationCardNumber: function (RationCardNumber){
		this.rationCardNumber = RationCardNumber;
	},
	getJobCardNumber: function(){
		return this.jobCardNumber;
	},
	setJobCardNumber: function (JobCardNumber){
		this.jobCardNumber = JobCardNumber;
	},

	getGuarantorMobileNumber: function(){
		return this.guarantorMobileNumber;
	},
	setGuarantorMobileNumber: function (guarantorMobileNumber){
		this.guarantorMobileNumber = guarantorMobileNumber;
	},

	getGuarantorLandlineNumber: function(){
		return this.guarantorLandlineNumber;
	},
	setGuarantorLandlineNumber: function (guarantorLandlineNumber){
		this.guarantorLandlineNumber = guarantorLandlineNumber;
	},
	getGuarantorMaritalStatus: function(){
		return this.guarantorMaritalStatus;
	},
	setGuarantorMaritalStatus: function (guarantorMaritalStatus){
		this. guarantorMaritalStatus = guarantorMaritalStatus;
	},
	getGuarantorReligion: function(){
		return this.guarantorReligion;
	},
	setGuarantorReligion: function (guarantorReligion){
		this.guarantorReligion = guarantorReligion;
	},
	getGuarantorEducationStatus: function(){
		return this.guarantorEducationStatus;
	},
	setGuarantorEducationStatus: function (guarantorEducationStatus){
		this.guarantorEducationStatus = guarantorEducationStatus;
	},
	getGuarantorGender: function(){
		return this.guarantorGender;
	},
	setGuarantorGender: function (guarantorGender){
		this.guarantorGender = guarantorGender;
	},
	getGuarantorCaste: function(){
		return this.guarantorCaste;
	},
	setGuarantorCaste: function (guarantorCaste){
		this.guarantorCaste = guarantorCaste;
	},
	getGuarantorNationality: function(){
		return this.guarantorNationality;
	},
	setGuarantorNationality: function (guarantorNationality){
		this.guarantorNationality = guarantorNationality;
	},
	setGuarantorLine1: function(t_guarantorLine1){
		if(t_guarantorLine1 == null){
			this.guarantorLine1 = "";
		} else {
			this.guarantorLine1 = t_guarantorLine1;
		}
	},
	getGuarantorLine1: function(){
		return this.guarantorLine1;
	},
	setGuarantorLine2: function(t_guarantorLine2){
		if(t_guarantorLine2 == null){
			this.guarantorLine2 = "";
		} else {
			this.guarantorLine2 = t_guarantorLine2;
		}
	},
	getGuarantorLine2: function(){
		return this.guarantorLine2;
	},
	setGuarantorLine3: function(t_guarantorLine3){
		if(t_guarantorLine3 == null){
			this.guarantorLine3 = "";
		} else {
			this.guarantorLine3 = t_guarantorLine3;
		}
	},
	getGuarantorLine3: function(){
		return this.guarantorLine3;
	},
	setGuarantorCity: function(t_guarantorCity){
		if(t_guarantorCity == null){
			this.guarantorCity = "";
		} else {
			this.guarantorCity = t_guarantorCity;
		}
	},
	getGuarantorCity: function(){
		return this.guarantorCity;
	},
	setGuarantorState: function(t_guarantorState){
		if(t_guarantorState == null){
			this.guarantorState = "";
		} else {
			this.guarantorState = t_guarantorState;
		}
	},
	getGuarantorState: function(){
		return this.guarantorState;
	},
	setGuarantorLandmark: function(t_guarantorLandmark){
		if(t_guarantorLandmark == null){
			this.guarantorLnadmark = "";
		} else {
			this.guarantorLandmark = t_guarantorLandmark;
		}
	},
	getGuarantorLandmark: function(){
		return this.guarantorLandmark;
	},
	setGuarantorPincode: function(t_guarantorPincode){
		if(t_guarantorPincode == null){
			this.guarantorPincode = "";
		} else {
			this.guarantorPincode = t_guarantorPincode;
		}
	},
	getGuarantorPincode: function(t_guarantorPincode){
		return this.guarantorPincode;
	},
	clearAll: function(){
        this.setProspectClientGuarantorId("");
        this.setClientId("");
        this.setGuarantorName("");
        this.setGuarantorDob("");
        this.setGuarantorAddress("");
        this.setGuarantorId("");
        this.setGuarantorRelationship("");
        this.setOtherGuarantorRelationshipName("");
		this.setAadhaarCardNumber("");
		this.setVoterIdNumber("");
		this.setRationCardNumber("");
		this.setJobCardNumber("");
		this.setProspectClientGuarantorAge("");
		this.setProspectClientGuarantorAgeAsOnDate("");
		this.setGuarantorMobileNumber("");
		this.setGuarantorLandlineNumber("");
		this.setGuarantorReligion("");
		this.setGuarantorMaritalStatus("");
		this.setGuarantorEducationStatus("");
		this.setGuarantorGender("");
		this.setGuarantorCaste("");
		this.setGuarantorNationality("");
		this.setGuarantorLine1("");
		this.setGuarantorLine2("");
		this.setGuarantorLine3("");
		this.setGuarantorCity("");
		this.setGuarantorState("");
		this.setGuarantorLandmark("");
		this.setGuarantorPincode("");
    }

};