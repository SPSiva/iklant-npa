module.exports = NPALoanRecoveryGroups;

function NPALoanRecoveryGroups() {	
   //this.clearAll();
}
var accountId;
var customerId;
var globalAccountNum;
var groupName;
var glAddress;
var groupCode;
var office;

NPALoanRecoveryGroups.prototype = {
	
	getAccountId: function(){
		return this.accountId;
	},
	
	setAccountId: function (t_accountId){
		this.accountId = t_accountId;
	},

	getCustomerId: function(){
		return this.customerId;
	},
	
	setCustomerId: function (t_customerId){
		this.customerId = t_customerId;
	},

	getGlobalAccountNum: function(){
		return this.globalAccountNum;
	},
	
	setGlobalAccountNum: function (t_globalAccountNum){
		this.globalAccountNum = t_globalAccountNum;
	},

	getGroupName: function(){
		return this.groupName;
	},
	
	setGroupName: function (t_groupName){
		this.groupName = t_groupName;
	},

	getGlAddress: function(){
		return this.glAddress;
	},
	
	setGlAddress: function (t_glAddress){
		this.glAddress = t_glAddress;
	},
        getGroupCode: function(){
		return this.groupCode;
	},
	setGroupCode: function (groupCode){
		this.groupCode = groupCode;
	},
        getOffice : function(){
            return this.office;
	},
	setOffice : function (t_office){
            this.office = t_office;
	},
        getOfficeId : function(){
            return this.office_id;
	},
	setOfficeId : function (t_office_id){
            this.office_id = t_office_id;
	},
        getRecoveryOfficer : function(){
            return this.recoveryOfficer;
        },
        setRecoveryOfficer : function(officer){
            this.recoveryOfficer = officer;
        },
        getOverdueAmount : function(){
            return this.overdueAmount;
        },
        setOverdueAmount : function(amount){
            this.overdueAmount = amount;
        },
        getArrearDays : function(){
            return this.arrearDays;
        },
        setArrearDays : function(days){
            this.arrearDays = days;
        },
    clearAll: function(){
       this.setAccountId("");
       this.setCustomerId("");
       this.setGlobalAccountNum("");
       this.setGroupName("");
       this.setGlAddress("");
       this.setGroupCodee("");
       this.setOffice("");
    }
};