module.exports = docTypeForAndroid;

var docId = new Array();
var docEntityId = new Array();
var docName = new Array();
var isMandatory = new Array();
function docTypeForAndroid() {
   //this.clearAll();
}
docTypeForAndroid.prototype = {
	//docId
	getDocId: function(){
		return this.docId;
	},
	
	setDocId: function (t_docId){
        this.docId = t_docId;
	},
	
	//docEntityId
	getDocEntityId: function(){
		return this.docEntityId;
	},
	
	setDocEntityId: function (t_docEntityId){
        this.docEntityId = t_docEntityId;
	},
	
	//docName	
	getDocName: function(){
		return this.docName;
	},
	
	setDocName: function (t_docName){
        this.docName = t_docName;
	},

	getMandatory : function(){
		return this.isMandatory;
	},
	setMandatory :function(t_is_mandatory){
		this.isMandatory = t_is_mandatory;
	},
	
	clearAll: function() {
		this.setDocId("");
		this.setDocEntityId("");
		this.setDocName("");
		this.setMandatory("");
	}

};