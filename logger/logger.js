var log4js = require('log4js');
var path = require('path');
var mifosConfiguration = require(path.dirname(process.mainModule.filename)+"/"+"properties.json");
var log =  mifosConfiguration.logPath+"/Iklant/iKlant_"+ mifosConfiguration.iklantPort + ".log";
log4js.configure({

    appenders: [

        {
            type: 'dateFile',
            filename: log ,
            "maxLogSize": 10485760,
            "numBackups": 10 }
    ]
});



