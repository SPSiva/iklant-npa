var express = require('express');
var path = require('path');
var favicon = require('serve-favicon');
var logger = require('morgan');
var bodyParser = require('body-parser');
var cookieParser = require('cookie-parser');

var multer = require('multer');
var session = require('express-session');
var connect = require('connect');
var MemoryStore = session.MemoryStore;
var sessionStore = new MemoryStore();
var applicationHome = path.dirname(process.mainModule.filename);
var props = require(path.join(applicationHome,"properties.json"));
var AWS = require('aws-sdk');

AWS.config.update({
    accessKeyId: "AKIAJCY3G5GS2UT5QVBA",
    secretAccessKey: "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",
});

var app = express();

// view engine setup
app.set('views', path.join(applicationHome, 'views'));
app.set('view engine', 'jade');
app.set('sessionStore', sessionStore);
app.use(favicon(applicationHome + '/public/images/favicon.ico'));
app.use(logger('dev'));
app.use(bodyParser.json({limit:'100mb', extended: false}));
app.use(bodyParser.urlencoded({limit:'100mb', extended: false}));
app.use(multer({limit:'100mb'}));
app.use(cookieParser());
app.use(connect.session({secret:'$dfs453&*$@', store: sessionStore}));

app.use(express.static(path.join(applicationHome, 'public')));

var Handler = require(path.join(applicationHome,"/app_modules/mapping/Handler"));
app = new Handler(app);

// catch 404 and forward to error handler
app.use(function(req, res, next) {
    res.redirect(props.contextPath+'/client/ci/login');
});

// development error handler
// will print stacktrace
if (app.get('env') === 'development') {
    app.use(function(err, req, res, next) {
        res.status(err.status || 500);
        res.render('error', {
            message: err.message,
            error: err,
            contextPath:props.contextPath
        });
    });
}

// production error handler
// no stacktraces leaked to user
app.use(function(err, req, res, next) {
    res.status(err.status || 500);
    res.render('error', {
        message: err.message,
        error: {},
        contextPath:props.contextPath
    });
});


module.exports = app;
